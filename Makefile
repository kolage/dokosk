#
# Kalisto
#
# Copyright (c) 2001-2010
#   Department of Distributed and Dependable Systems
#   Faculty of Mathematics and Physics
#   Charles University, Czech Republic
#
# Kalisto primary makefile. The defalt target ("all")
# build the kernel and user space.
# The target "doc" can be used to create
# Doxygen documentation. Targets "clean" and "distclean"
# can be used to cleanup the source tree.
#

### Phony targets

.PHONY: all clean distclean kernel user doc

### Default target

all: kernel user

kernel:
	$(MAKE) -C kernel KERNEL_TEST="$(KERNEL_TEST)" KERNEL_DEBUG="$(KERNEL_DEBUG)"

user:
	$(MAKE) -C user USER_TEST="$(USER_TEST)" USER_DEBUG="$(USER_DEBUG)"

doc:
	$(MAKE) -C doc

clean:
	$(MAKE) -C kernel clean
	$(MAKE) -C user clean
	$(MAKE) -C doc clean

distclean:
	$(MAKE) -C kernel distclean
	$(MAKE) -C user distclean
	$(MAKE) -C doc distclean

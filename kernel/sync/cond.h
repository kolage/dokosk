/**
 * @file kernel/sync/cond.h
 *
 * Declarations of condition variable synchronization primitive.
 *
 * This file is related to the @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef COND_H_
#define COND_H_

#include <adt/list.h>
#include <sync/mutex.h>


/** Magic value for condition variable protection. */
#define COND_MAGIC 0xCEBF0703


/* Process forward declaration. */
struct process_impl;

/** The condition variable structure.
 *
 */
typedef struct cond {
	/** Magic value */
	uint32_t magic;

	/** List of threads waiting for the condition variable. */
	list_t waiting_threads;

	/** Condition variable unique id at the moment. */
	unative_t cvar_id;

	/** Condition variable can be registered as resource of the process. */
	tree_node_t resource_item;

	/** How many times is the condition variable used in some syscall. It can not be destroyed
	    until this number is 0. */
	atomic_t syscall_use_count;

	/** Condition variable can be an item in the zombie list. */
	item_t zombie_item;

	/** Process which created the condition variable. */
	struct process_impl *process;
} cond_t;


/* Externals are commented with implementation. */
extern void cond_init(struct cond *cvar);
extern void cond_destroy(struct cond *cvar);
extern void cond_signal(struct cond *cvar);
extern void cond_broadcast(struct cond *cvar);
extern void cond_wait(struct cond *cvar, struct mutex *mtx);
extern int cond_wait_timeout(struct cond *cvar, struct mutex *mtx, const unsigned int usec);

#endif /* COND_H_ */

/**
 * @file kernel/sync/cond.c
 *
 * Implementation of condition variable synchronization primitive.
 *
 * This file is related to the @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <adt/list.h>
#include <drivers/printer.h>
#include <sched/sched.h>
#include <sched/timer.h>
#include <sync/cond.h>
#include <sync/mutex.h>

#if !defined(DEBUG_CVAR) || defined(__DOXYGEN__)
/** Debug the condition variables?
 *
 * By default debugging prints for condition variables are disabled.
 * You can define ::DEBUG_CVAR to true to enable condition variables debugging prints.
 * To do this, add CVAR to KERNEL_DEBUG when building the OS:
 * <pre>make KERNEL_DEBUG="CVAR"</pre>
 */
#define DEBUG_CVAR false
#endif

#define DEBUG_CVAR_ID 0

#if DEBUG_CVAR
#define cvar_debug_print_init()	ipl_t status_state;
#define cvar_debug_print(cvar, ARGS...) \
	status_state = query_and_disable_interrupts(); \
	if (DEBUG_CVAR_ID == 0 || cvar->cvar_id == DEBUG_CVAR_ID) \
		printk_nb(ARGS); \
	conditionally_enable_interrupts(status_state);
#else
#define cvar_debug_print_init()
#define cvar_debug_print(cvar, ARGS...)
#endif


/** Magic value for the condition variable to recognize whether it is not corrupted. */
#define CHECK_COND_INTEGRITY(cvar) \
	assert(cvar != NULL); 	\
	assert(cvar->magic == COND_MAGIC);


/** The condition variable and thread structure. We need that structure when
 *  we interrupt waiting for some condition variable in the thread.
 *
 */
typedef struct thread_cond {
	thread_impl_t *thread;
	cond_t *cvar;
} thread_cond_t;


/** The id for the cvar that will be created next. */
ATOMIC_DECLARE(newly_created_cvar_id, 0);


/** Initialize the condition variable.
 *
 * @param cvar Condition variable.
 *
 */
void cond_init(struct cond *cvar)
{
	assert(cvar != NULL);

	/* Initialize the cvar structure. */
	cvar->magic = COND_MAGIC;
	cvar->resource_item.key = (unative_t)cvar;
	cvar->process = process_self_impl();

	atomic_set(&cvar->syscall_use_count, 0);
	list_init(&cvar->waiting_threads);
	item_init(&cvar->zombie_item, cvar);

	/* Set the cvar id and atomically increase it. */
	cvar->cvar_id = atomic_increment(&newly_created_cvar_id);
}

/** Destroy the passed condition variable and checks its integrity.
 *
 * When there are still some threads waiting for on the condition
 * variable, the ::panic is caused.
 *
 * @param cvar Condition variable.
 *
 */
void cond_destroy(struct cond *cvar)
{
	CHECK_COND_INTEGRITY(cvar);

	if (!list_empty(&cvar->waiting_threads)) {
		panic("Destroying condition variable with waiting threads!");
	}

	/* Unset the condition variable magic, so we can easily recognize some design errors. */
	cvar->cvar_id = 0;
	cvar->magic = 0;
}

/** Wake up the passed thread and unsets the thread from the queue of
 *  waiting threads for the passed @p  cvar.
 *
 * We have to check inside whether the thread is waiting for the @p cvar.
 * It could stop waiting from various places.
 *
 * @param thread      Probably blocked thread.
 * @param cvar        Cvar on which the thread should be blocked.
 * @param priority    Should we plan the waking thread with high priority?
 *                    This is when signaling.
 * @param sync_status A new synchronization status of passed @p thread.
 *
 */
static void cond_wakeup(thread_impl_t *thread, cond_t *cvar, int priority, thread_sync_status_t sync_status)
{
	CHECK_COND_INTEGRITY(cvar);
	thread->cvar_sync_status = sync_status;

	if (thread_impl_is_valid(thread)) {
		thread_wakeup_priority(thread, priority);
	}

	/* We do want to leave the thread in the waiting thread list, when the timer
	   has elapsed, because some signal might arrive in the meantime. */
	if (thread->cvar_sync_item.list == &cvar->waiting_threads && sync_status != THREAD_SYNC_STATUS_TIMED_OUT) {
		list_remove(&cvar->waiting_threads, &thread->cvar_sync_item);
	}
}

/** Wake up one thread waiting for the condition variable.
 *
 * @param cvar Condition variable.
 *
 */
void cond_signal(struct cond *cvar)
{
	CHECK_COND_INTEGRITY(cvar);
	ipl_t state = query_and_disable_interrupts();
	thread_impl_t *wake_up_thread = scheduler_get_from_queue(&cvar->waiting_threads);

	cvar_debug_print_init();
	cvar_debug_print(cvar, "COND SIGNAL: Thread %u signaling on cvar %u\n", thread_get_current_id(), cvar->cvar_id);

	if (wake_up_thread != NULL) {
		cond_wakeup(wake_up_thread, cvar, 1, THREAD_SYNC_STATUS_SIGNALED);
		cvar_debug_print(
			cvar, "COND SIGNAL: Thread %u on cvar %u waking up thread %u\n",
			thread_get_current_id(), cvar->cvar_id, wake_up_thread->thread_id
		);
	}

	conditionally_enable_interrupts(state);
}

/** Wake up all the threads waiting for the condition variable.
 *
 * @param cvar Condition variable.
 *
 */
void cond_broadcast(struct cond *cvar)
{
	CHECK_COND_INTEGRITY(cvar);
	list_it_t it;
	ipl_t state = query_and_disable_interrupts();

	cvar_debug_print_init();
	cvar_debug_print(
		cvar, "COND BROADCAST: Thread %u broadcasting on cvar %u; waiting threads: %u\n",
		thread_get_current_id(), cvar->cvar_id, list_count(&cvar->waiting_threads)
	);

	for (list_it_init(&it, &cvar->waiting_threads); list_it_has_current(&it); list_it_next(&it)) {
		cond_wakeup((thread_impl_t *)list_it_data(&it), cvar, 0, THREAD_SYNC_STATUS_SIGNALED);
	}
	
	conditionally_enable_interrupts(state);
}

/** Handle the timeout when waiting for the condition variable.
 *
 * This function sets the cvar_sync_status property of the blocked thread
 * which indicated, that the waiting has timed out.
 *
 * @param self Timed out timer.
 * @param data Structure with the blocked thread and the condition variable
 *             it is waiting for.
 *
 */
static void cond_handle_timeout(struct timer *self, void *data)
{
	thread_cond_t *passed_data = (thread_cond_t*)data;
	ipl_t state = query_and_disable_interrupts();

	if (thread_impl_is_valid(passed_data->thread)) {
		if (passed_data->thread->cvar_sync_status == THREAD_SYNC_STATUS_KEEP_WAITING) {
			cond_wakeup(passed_data->thread, passed_data->cvar, 0, THREAD_SYNC_STATUS_TIMED_OUT);
		}

		cvar_debug_print_init();
		cvar_debug_print(
			passed_data->cvar, "COND TIMEOUT: Thread %u waiting for cvar %u expired\n",
			thread_get_current_id(), passed_data->cvar->cvar_id
		);
	}

	conditionally_enable_interrupts(state);
}

/** Internal help function for condition variable waiting which
 *  returns the waiting status of current thread before it locked
 *  the passed mutex.
 *
 * @param  cvar Condition variable.
 * @param  mtx  Mutex to get unlocked and locked again.
 *
 * @return Synchronization status of current thread before it locked the
 *         passed mutex.
 *
 */
static uint8_t cond_wait_simple(struct cond *cvar, struct mutex *mtx)
{
	CHECK_COND_INTEGRITY(cvar);

	thread_impl_t *current_thread = thread_get_current_impl();
	ipl_t state = query_and_disable_interrupts();

	assert(current_thread != NULL);
	cvar_debug_print_init();
	cvar_debug_print(cvar, "COND WAIT: Thread %u waiting on cvar %u\n", current_thread->thread_id, cvar->cvar_id);

	unative_t mutex_id = mtx->mutex_id;
	unative_t cvar_id = cvar->cvar_id;

	/* We assume that the mutex is locked in the moment. */
	mutex_unlock(mtx);

	/* Sleep the thread and wait until someone else wakes it up. */
	if (current_thread->cvar_sync_status != THREAD_SYNC_STATUS_TIMED_OUT) {
		disabled(current_thread);
		assert(current_thread->cvar_sync_item.list == NULL);
		list_append(&cvar->waiting_threads, &current_thread->cvar_sync_item);
		cvar_debug_print(cvar, "COND WAIT: Thread %u sleeping on cvar %u\n", current_thread->thread_id, cvar->cvar_id);
	}

	/* Yield (wait for waking up). */
	thread_yield();

	/* Control whether the mutex was not uninitialized in the meantime. */
	if (mtx != NULL && mtx->magic == MUTEX_MAGIC && mtx->mutex_id == mutex_id) {
		/* Lock the mutex again - we know that now no signal should be missed. If so, it is
		   bad program design and we can do nothing about that we could miss it. */
		mutex_lock(mtx);
	}

	/* Control whether the condition variable was not uninitialized in the meantime. */
	if (cvar != NULL && cvar->magic == COND_MAGIC && cvar->cvar_id == cvar_id) {
		/* Thread might still be in the waiting threads list, because when it is waken
		   up by the timer interrupt, it is not removed from this list. */
		if (current_thread->cvar_sync_item.list == &cvar->waiting_threads) {
			list_remove(&cvar->waiting_threads, &current_thread->cvar_sync_item);
		}

		/* Debug - thread has been waken up. */
		cvar_debug_print(
			cvar, "COND WAIT: Thread %u waken up on cvar %u; sync status: %s\n",
			current_thread->thread_id, cvar->cvar_id, thread_tr_sync_status(current_thread->cvar_sync_status)
		);
	}

	conditionally_enable_interrupts(state);

	return current_thread->cvar_sync_status;
}

/** Atomically unlock the mutex and blocks the calling thread.
 *
 * After unblocking the mutex is locked again.
 *
 * @param cvar Condition variable.
 * @param mtx  Mutex to get unlocked.
 *
 */
void cond_wait(struct cond *cvar, struct mutex *mtx)
{
	thread_impl_t *current_thread = thread_get_current_impl();
	current_thread->cvar_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
	cond_wait_simple(cvar, mtx);
	current_thread->cvar_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
}

/** Atomically unlock the mutex and blocks the calling thread.
 *
 * After unblocking the mutex is locked again. Thread is blocked at most for
 * the microseconds. When the thread is unblocked during the waiting process
 * it returns ::EOK otherwise ::ETIMEDOUT. When the @p usec = 0 the thread is
 * not blocked at all, but before locking again it yield once.
 *
 * @param  cvar Condition variable.
 * @param  mtx  Mutex to get unlocked.
 * @param  usec Timeout for the locking.
 *
 * @return ::EOK when the thread is waken up before the time limit,
 *         ::ETIMEDOUT when it timed out.
 *
 */
int cond_wait_timeout(struct cond *cvar, struct mutex *mtx, const unsigned int usec)
{
	thread_impl_t *current_thread = thread_get_current_impl();
	int return_val;

	struct timer tmr;
	struct thread_cond tc;
	tc.thread = current_thread;
	tc.cvar = cvar;

	/* Reset the thread synchronization status. */
	current_thread->cvar_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;

	/* Set the timer to wait for the mutex for the limited time. */
	timer_init(&tmr, usec, cond_handle_timeout, &tc);
	timer_start(&tmr);

	/* Try to lock the mutex and check whether we have timed out or not. */
	return_val = cond_wait_simple(cvar, mtx) == THREAD_SYNC_STATUS_SIGNALED ? EOK : ETIMEDOUT;

	/* Reset the thread synchronization status. */
	current_thread->cvar_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;

	/* When the thread was waken up before the timer interrupt, we destroy
	   the timer, therefore it will not be handled at all. But when the
	   timer routine was called, the thread's cvar_sync_status property is set
	   to THREAD_SYNC_STATUS_TIMED_OUT. */
	timer_destroy(&tmr);

	return return_val;
}

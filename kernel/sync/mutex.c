/**
 * @file kernel/sync/mutex.c
 *
 * Implementation of mutex synchronization primitive.
 *
 * This file is related to the @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page synchronization_feature Synchronization primitives
 *
 * This page describes how synchronization primitives are implemented.
 *
 * We have implemented two basic synchronization primitives in out
 * operating system: mutexes (implemented in ::mutex structure)
 * and condition variables (implemented in ::cond structure).
 * Condition variables are used for all the synchronization that
 * include waiting for some event.
 *
 * When a thread demands to lock some mutex, it falls into the loop
 * with disabled interrupts. Every thread has thread::mutex_sync_status
 * attribute which indicates, whether the mutex it is waiting for
 * has been locked, thread is still waiting or the timeout has elapsed.
 * The waiting loop in ::mutex_lock function can be leaved only
 * when the mutex was successfully locked (so no other thread has it locked)
 * or when the timeout has elapsed in the ::mutex_lock_timeout function.
 *
 * Mutex can be initialized as a simple one by ::mutex_init function
 * or as a recursive one by ::mutex_init_recursive function.
 * Recursive mutexes are realized by atomic counter of locks in
 * mutex::lock_count attribute of ::mutex_t structure.
 *
 * Both mutexes and condition variables are exported to the user space
 * with relevant syscalls, but only handles to the kernel structures
 * are exported. Handle is an address where the synchronization
 * primitive is stored at kernel heap. Those handles are always validated
 * when passed to the kernel through syscall interface.
 *
 * In syscalls we have to ensure that no memory leap should occur
 * and when a thread is killed while it is in syscall, that kernel data
 * structures do not get to the inconsistent state. Those two objectives
 * are ensured by ::THREAD_ENTER_CRITICAL_SYSCALL_SECTION and
 * ::THREAD_LEAVE_CRITICAL_SYSCALL_SECTION pair of macros. Thread that
 * is running code between those two macros can not get killed, it is
 * just marked with ::THREAD_TO_BE_KILLED_MASK and when it leaves the
 * critical section it commits suicide. Those macros must be used
 * carefully - every critical section should be left in finite time,
 * so the caller of ::kill function does not wait forever.
 *
 * The second problem with mutexes and condition variables in syscalls
 * is that when ::mutex_destroy is called, synchronization primitive
 * can not be immediately deallocated, because some other thread might
 * still use it. Therefore in ::syscall_mutex_destroy and
 * ::syscall_cond_destroy syscall implementations the relevant
 * synchronization primitive is just added to the zombie list where
 * it waits for deallocation. Deallocation is possible only when
 * no thread is using the synchronization primitive any more. This
 * state is recognized by atomic counter mutex::syscall_use_count - when
 * it reaches 0, the deallocation can be performed.
 *
 * There is a little problem with that: when the thread is killed
 * while it is waiting for some mutex or condition variable, it will
 * never decrement the mutex::syscall_use_count and therefore the
 * synchronization primitive can not be deallocated. But those
 * are deallocated when the process finishes (there is a special condition
 * for that in system work thread), so at the end it is not so big problem.
 *
 * This strange situation can happen only when thread is killed
 * just after it has incremented the mutex::syscall_use_count attribute,
 * but before it has entered ::mutex_lock function or some other relevant
 * kernel function which works with synchronization primitives. When the
 * thread starts to wait for a mutex or condition variable and some
 * other thread is trying to destroy it, the thread that calls the _destroy
 * function gets killed, because there are still some waiting threads on
 * the synchronization primitive.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <adt/list.h>
#include <adt/tree.h>
#include <drivers/printer.h>
#include <include/codes.h>
#include <proc/kernel.h>
#include <sched/sched.h>
#include <sched/timer.h>
#include <sync/mutex.h>

#if !defined(DEBUG_MUTEX) || defined(__DOXYGEN__)
/** Debug the mutexes?
 *
 * By default debugging prints for mutexes are disabled.
 * You can define ::DEBUG_MUTEX to true to enable mutexes debugging prints.
 * To do this, add MUTEX to KERNEL_DEBUG when building the OS:
 * <pre>make KERNEL_DEBUG="MUTEX"</pre>
 */
#define DEBUG_MUTEX false
#endif

#define DEBUG_MUTEX_ID 0

#if DEBUG_MUTEX
#define mutex_debug_print_init()	ipl_t status_state;
#define mutex_debug_print(mtx, ARGS...) \
	status_state = query_and_disable_interrupts(); \
	if (DEBUG_MUTEX_ID == 0 || mtx->mutex_id == DEBUG_MUTEX_ID) \
		printk_nb(ARGS); \
	conditionally_enable_interrupts(status_state);
#else
#define mutex_debug_print_init()
#define mutex_debug_print(mtx, ARGS...)
#endif


/** Magic value for the mutex to recognize whether it is not corrupted. */
#define CHECK_MUTEX_INTEGRITY(mtx) \
	assert(mtx != NULL); 	\
	assert(mtx->magic == MUTEX_MAGIC);


/** The mutex and thread structure. We need that structure when
 *  we interrupt waiting for some mutex in the thread.
 *
 */
typedef struct thread_mutex {
	thread_impl_t *thread;
	mutex_t *mutex;
} thread_mutex_t;


/** The id for the mutex that will be created next. */
ATOMIC_DECLARE(newly_created_mutex_id, 0);


/** Initialize the mutex to the unlocked state and set its flags.
 *
 * @param mtx   Mutex to be initialized.
 * @param flags Mutex flags.
 *
 */
static void mutex_init_with_flags(struct mutex *mtx, mutex_flags_t flags)
{
	assert(mtx != NULL);

	/* Initialize the mutex structure. */
	mtx->magic = MUTEX_MAGIC;
	mtx->flags = flags;
	mtx->locked_thread = NULL;
	mtx->resource_item.key = (unative_t)mtx;
	mtx->process = process_self_impl();

	atomic_set(&mtx->lock_count, 0);
	atomic_set(&mtx->syscall_use_count, 0);
	list_init(&mtx->waiting_threads);
	item_init(&mtx->zombie_item, mtx);

	/* Set the mutex id and atomically increase it. */
	mtx->mutex_id = atomic_increment(&newly_created_mutex_id);
}

/** Initialize the mutex to the state unlocked.
 *
 * @param mtx Mutex to be initialized.
 *
 */
void mutex_init(struct mutex *mtx)
{
	mutex_init_with_flags(mtx, 0);
}

/** Initialize the recursive mutex to the state unlocked.
 *
 * @param mtx Mutex to be initialized.
 *
 */
void mutex_init_recursive(struct mutex *mtx)
{
	mutex_init_with_flags(mtx, MUTEX_RECURSIVE_MASK);
}

/** Destroy the passed mutex and check its integrity.
 *
 * When there are still some threads waiting for the mutex, the ::panic
 * is caused.
 *
 * @param mtx Mutex to be destroyed.
 *
 */
void mutex_destroy(struct mutex *mtx)
{
	CHECK_MUTEX_INTEGRITY(mtx);

	if (!list_empty(&mtx->waiting_threads)) {
		panic("Destroying mutex with blocked threads!");
	}

	/* Unset the mutex magic, so we can easily recognize some design errors. */
	mtx->mutex_id = 0;
	mtx->magic = 0;
}

/** Internal help function for mutex locking which does not care about
 *  the calling thread waiting status.
 *
 * @param mtx Mutex to be locked.
 *
 */
static uint8_t mutex_lock_simple(struct mutex *mtx)
{
	CHECK_MUTEX_INTEGRITY(mtx);

	/* We can modify the value of current_thread->mutex_sync_status by the timer. So we are
	   able to interrupt the waiting loop. */
	thread_impl_t *current_thread = thread_get_current_impl();
	assert(current_thread != NULL);
    mutex_debug_print_init();
	mutex_debug_print(mtx, "-- MUTEX LOCK: Thread %u acquires the mutex %u\n", current_thread->thread_id, mtx->mutex_id);

	/* In debug mode we control that we do not lock twice. */
#if DEBUG_MUTEX
	if (!MUTEX_IS_RECURSIVE(mtx)) {
		assert(mtx->locked_thread != current_thread);
	}
#endif

	/* Disable interrupts while trying to lock the mutex. */
	ipl_t state = query_and_disable_interrupts();

	/* Wait for the lock. */
	do {
		bool canLock = atomic_get(&mtx->lock_count) == 0
				|| (MUTEX_IS_RECURSIVE(mtx) && mtx->locked_thread == current_thread);

		if (canLock) {
			/* Lock the mutex. */
			atomic_increment(&mtx->lock_count);
			mtx->locked_thread = current_thread;
			current_thread->mutex_sync_status = THREAD_SYNC_STATUS_LOCKED;

			/* Debug. */
			mutex_debug_print(
				mtx, "-- MUTEX LOCK: Thread %u locked mutex %u; lock count: %u\n",
				current_thread->thread_id, mtx->mutex_id, atomic_get(&mtx->lock_count)
			);
		} else if (current_thread->mutex_sync_status == THREAD_SYNC_STATUS_KEEP_WAITING) {
			if (!list_contains(&mtx->waiting_threads, &current_thread->mutex_sync_item)) {
				assert(current_thread->mutex_sync_item.list == NULL);
				list_append(&mtx->waiting_threads, &current_thread->mutex_sync_item);
			}

			/* Debug. */
			assert(mtx->locked_thread != NULL);
			mutex_debug_print(
				mtx, "-- MUTEX LOCK: Thread %u not received mutex %u; locked by: %u, lock count: %u\n",
				current_thread->thread_id, mtx->mutex_id, mtx->locked_thread->thread_id, atomic_get(&mtx->lock_count)
			);

			/* Detach the thread from the scheduler and wait for the lock to be released. */
			thread_suspend();
		}
	} while (current_thread->mutex_sync_status == THREAD_SYNC_STATUS_KEEP_WAITING);

	conditionally_enable_interrupts(state);

	mutex_debug_print(
		mtx, "-- MUTEX LOCK: Thread %u leaving mutex %u; sync status: %s\n",
		current_thread->thread_id, mtx->mutex_id, thread_tr_sync_status(current_thread->mutex_sync_status)
	);
	return current_thread->mutex_sync_status;
}

/** Try to lock the passed mutex.
 *
 * When it does not succeed, it blocks actually running thread and puts it
 * into the queue of threads waiting for the mutex. mutex_unlock then wakes
 * up all those threads and the first to get planned then captures the mutex,
 * rest is waiting again etc. Waiting loop can be also interrupted by
 * setting mutex_sync_status property of the thread to false. This is used for
 * the _timeout variant of this function.
 *
 * @param mtx Mutex to be locked.
 *
 */
void mutex_lock(struct mutex *mtx)
{
	/* When the kernel is not initialized, there are no threads
	   and locking does not make sense. */
	if (!kernel_is_initialized()) {
		return;
	}

	thread_impl_t *current_thread = thread_get_current_impl();
	assert(current_thread != NULL);
	current_thread->mutex_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
	mutex_lock_simple(mtx);
	current_thread->mutex_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
}

/** Wake up the passed thread and unset the thread from the queue of
 *  waiting threads of the passed @p mtx.
 *
 * We have to check inside whether the thread is waiting for the lock or
 * not. It could stop waiting from various places.
 *
 * @param thread   Probably blocked thread.
 * @param mtx      Mutex on which the thread should be blocked.
 * @param priority Should we plan the waking thread with high priority?
 *
 */
static void mutex_wakeup(thread_impl_t *thread, mutex_t *mtx, int priority)
{
	CHECK_MUTEX_INTEGRITY(mtx);

	if (thread_impl_is_valid(thread) && thread->mutex_sync_item.list == &mtx->waiting_threads) {
		list_remove(&mtx->waiting_threads, &thread->mutex_sync_item);
		thread_wakeup_priority(thread, priority);
	}
}

/** Handle the timeout when waiting for the lock.
 *
 * This function sets the mutex_sync_status property of the blocked thread
 * which releases it from the waiting loop for the mutex.
 *
 * @param self Timed out timer.
 * @param data Structure with the blocked thread and the mutex it
 *             is waiting for.
 *
 */
static void mutex_handle_timeout(struct timer *self, void *data)
{
	thread_mutex_t *passed_data = (thread_mutex_t*)data;
	ipl_t state = query_and_disable_interrupts();

	if (thread_impl_is_valid(passed_data->thread)) {
		mutex_wakeup(passed_data->thread, passed_data->mutex, 0);
		passed_data->thread->mutex_sync_status = THREAD_SYNC_STATUS_TIMED_OUT;

		mutex_debug_print_init();
		mutex_debug_print(
			passed_data->mutex, "-- MUTEX TIMEOUT: Thread %u waiting for mutex %u expired\n",
			passed_data->thread->thread_id, passed_data->mutex->mutex_id
		);
	}

	conditionally_enable_interrupts(state);
}

/** Try to lock the mutex with timeout.
 *
 * If it is not possible it blocks the calling thread. This function waits
 * at most @p usec microseconds to the lock. When the timeout is exceeded,
 * the waiting process is interrupted and the mutex is not locked. When
 * @p usec = 0 this method does not block the calling thread at all.
 *
 * @param  mtx  Mutex to lock.
 * @param  usec Timeout for the locking.
 *
 * @return ::EOK when the mutex was locked by the running thread,
 *         ::ETIMEDOUT when it timed out.
 *
 */
int mutex_lock_timeout(struct mutex *mtx, const unsigned int usec)
{
	/* When the kernel is not initialized, there are no threads
	   and locking does not make sense. */
	if (!kernel_is_initialized()) {
		return EOK;
	}

	thread_impl_t *current_thread = thread_get_current_impl();
	assert(current_thread != NULL);

	int return_val;
	struct timer tmr;
	struct thread_mutex tm;
	tm.thread = current_thread;
	tm.mutex = mtx;

	/* Reset the thread synchronization status. */
	current_thread->mutex_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;

	/* Set the timer to wait for the mutex for the limited time. */
	timer_init(&tmr, usec, mutex_handle_timeout, &tm);
	timer_start(&tmr);

	/* Try to lock the mutex and check whether we have timed out or not. */
	return_val = mutex_lock_simple(mtx) == THREAD_SYNC_STATUS_LOCKED ? EOK : ETIMEDOUT;
	
	/* Reset the thread synchronization status again. */
	current_thread->mutex_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;

	/* When the lock was successful, the timer handler is not called. When it
	   is not successful, the mutex_lock routine was interrupted by the timer
	   and it finished without locking. */
	timer_destroy(&tmr);
	
	return return_val;
}

/** Unlock the mutex and wake up one of the threads that are actually blocked
 *  on the mutex.
 *
 * This thread is planned to run and can lock the mutex. The rest of the
 * threads keep waiting in the queue.
 *
 * This method also checks whether the mutex is unlocked by the thread that
 * has locked it.
 *
 * @param mtx Mutex to be unlocked.
 *
 */
void mutex_unlock_checked(struct mutex *mtx)
{
	CHECK_MUTEX_INTEGRITY(mtx);

	/* When the kernel is not initialized, there are no threads
	   and locking does not make sense. */
	if (!kernel_is_initialized()) {
		return;
	}

	thread_impl_t *current_thread = thread_get_current_impl();
	assert(atomic_get(&mtx->lock_count) > 0);
	assert(current_thread != NULL);

	if (mtx->locked_thread != current_thread) {
		if (mtx->locked_thread == NULL) {
			panic("No thread locked the mutex %u!\n", mtx->mutex_id);
		} else {
			panic(
				"Mutex %u unlocked from the thread that did not lock it (id: %u)! Locked by thread with id %u.\n",
				mtx->mutex_id, current_thread->thread_id, mtx->locked_thread->thread_id
			);
		}
	}
	
	mutex_unlock_unchecked(mtx);
}

/** Unlock the mutex and wake up one of the threads that are actually blocked
 *  on the mutex.
 *
 * This thread is planned to run and can lock the mutex. The rest of the
 * threads keep waiting in the queue.
 *
 * @param mtx Mutex to be unlocked.
 *
 */
void mutex_unlock_unchecked(struct mutex *mtx)
{
	CHECK_MUTEX_INTEGRITY(mtx);

	/* When the kernel is not initialized, there are no threads
	   and locking does not make sense. */
	if (!kernel_is_initialized()) {
		return;
	}

	/* Disable interrupts while unlocking. We have to do that, because we are manipulating
	   with the list of waiting threads and we do not to get that list modified in the meantime. */
	ipl_t state = query_and_disable_interrupts();

	assert(thread_get_current_id() > 0);
	assert(mtx->locked_thread != NULL);
	assert(atomic_get(&mtx->lock_count) > 0);

	/* Unlock the mutex. */
	atomic_decrement(&mtx->lock_count);

	mutex_debug_print_init();
	mutex_debug_print(
		mtx, "-- MUTEX UNLOCK: Thread %u unlocking mutex %u; lock count: %u\n",
		thread_get_current_id(), mtx->mutex_id, atomic_get(&mtx->lock_count)
	);

	if (atomic_get(&mtx->lock_count) == 0) {
		mtx->locked_thread = NULL;

		/* Let the scheduler decide which thread should be waken up -
		   maybe some competition could be implemented in the scheduler. */
		thread_impl_t *wake_up_thread = scheduler_get_from_queue(&mtx->waiting_threads);

		if (wake_up_thread != NULL) {
			mutex_wakeup(wake_up_thread, mtx, 1);
			mutex_debug_print(
				mtx, "-- MUTEX UNLOCK: Thread %u waking up the thread %u on mutex %u\n",
				thread_get_current_id(), wake_up_thread->thread_id, mtx->mutex_id
			);
		}
	}
	
	conditionally_enable_interrupts(state);
}

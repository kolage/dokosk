/**
 * @file kernel/sync/mutex.h
 *
 * Declarations of mutex synchronization primitive.
 *
 * This file is related to the @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <adt/atomic.h>
#include <adt/list.h>
#include <adt/tree.h>


/** Magic value for mutex protection. */
#define MUTEX_MAGIC				 	0xBEAF8001

/** Mask for recursive mutexes. */
#define MUTEX_RECURSIVE_MASK		0x00000001

/** Checks whether the mutex is recursive. */
#define MUTEX_IS_RECURSIVE(mtx) 	(mtx->flags & MUTEX_RECURSIVE_MASK)


/** Mutex flags.
 *
 */
typedef unative_t mutex_flags_t;

/* Process forward declaration. */
struct process_impl;

/** The mutex structure.
 *
 */
typedef struct mutex {
	/** Magic value */
	uint32_t magic;

	/** Mutex flags. */
	mutex_flags_t flags;

	/** How many times the mutex has been locked. */
	atomic_t lock_count;

	/** Thread which has locked the mutex. */
	struct thread_impl *locked_thread;

	/** Mutex unique id at the moment. */
	unative_t mutex_id;

	/** List of threads waiting for the lock. */
	list_t waiting_threads;

	/** Mutex can be registered as resource of the process. */
	tree_node_t resource_item;

	/** How many times is the mutex used in some syscall. It can not be destroyed
	    until this number is 0. */
	atomic_t syscall_use_count;

	/** Mutex can be an item in the zombie list. */
	item_t zombie_item;

	/** Process which created the mutex. */
	struct process_impl *process;
} mutex_t;


/* Externals are commented with implementation. */
extern void mutex_init(struct mutex *mtx);
extern void mutex_init_recursive(struct mutex *mtx);
extern void mutex_destroy(struct mutex *mtx);
extern void mutex_lock(struct mutex *mtx);
extern int mutex_lock_timeout(struct mutex *mtx, const unsigned int usec);
extern void mutex_unlock_checked(struct mutex *mtx);
extern void mutex_unlock_unchecked(struct mutex *mtx);


/* Trick with the DEBUG_MUTEX macro - checked/unchecked version
   of mutex_unlock function. */
#if DEBUG_MUTEX
#define mutex_unlock(mtx)	mutex_unlock_checked(mtx)
#else
#define mutex_unlock(mtx)	mutex_unlock_unchecked(mtx)
#endif

#endif /* MUTEX_H_ */

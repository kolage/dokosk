/**
 * @file kernel/main.c
 *
 * Kernel main file.
 *
 * This file contains ::bsp_start function which is called just
 * after the kernel has booted up.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */
/** @mainpage OS Kernel Documentation
 *  @anchor kernel_main
 *
 * This documentation is a part of @maindoc.
 *
 * When the microprocessor is started, it boots into the kernel.
 * The kernel sets up the processor and initializes kernel features
 * (in ::bsp_start function). Then it launches the user process
 * (@ref kernel_feature) and allows it to use the features via syscalls
 * (@ref syscall_feature).
 *
 * Debugging of some kernel features can be turned by specifying
 * the KERNEL_DEBUG="<feature1> <feature2> ..." variable when building.
 * For example if we want to debug kernel heap, we build the kernel with
 * <pre>make KERNEL_DEBUG="HEAP"</pre>
 * which overrides the definition of the ::DEBUG_HEAP macro in the
 * kernel/mm/malloc.c file. This turns on debug prints which might help
 * with understanding of heap memory allocator or when searching for some
 * error.
 *
 * @section kernel_features Implemented features
 *
 * The OS implements the following features:
 * - @ref memory_feature
 * - @ref falloc_feature
 * - @ref malloc_feature
 * - @ref vma_feature
 * - @ref tlb_feature
 * - @ref thrcopy_feature
 * - @ref timer_feature
 * - @ref synchronization_feature
 * - @ref thread_feature
 * - @ref process_management_feature
 * - @ref io_feature
 * - @ref disk_feature
 * - @ref syscall_feature
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <adt/list.h>
#include <mm/falloc.h>
#include <mm/malloc.h>
#include <proc/thread.h>
#include <proc/kernel.h>
#include <sched/sched.h>
#include <sched/timer.h>
#include <mm/tlb.h>
#include <mm/asid.h>
#include <drivers/printer.h>
#include <drivers/keyboard.h>
#include <drivers/disk.h>
#include <main.h>

#ifdef KERNEL_TEST
	#include <api.h>
#endif

/** Main thread procedure.
 *
 * Runs initial user process or kernel tests.
 *
 * @param data Data passed to the thread.
 *
 */
static void *main_thread(void *data)
{
#ifdef KERNEL_TEST

	/* If kernel test is compiled in, run it. */
	run_test();

#else /* KERNEL_TEST */

	kernel_run_user_processes();

#endif /* KERNEL_TEST */

	/* The machine is halted as soon as any thread finishes. */
	msim_halt();

	return NULL;
}

/** The kernel entry point in C code for bootstrap processor (BSP).
 *
 * This function is called by the assembler code shortly after bootstrap,
 * with disabled interrupts and temporary stack. The function initializes
 * the kernel data structures and only then switches to a standard thread.
 *
 * After the initialization is complete, application processors entry
 * points are allowed to proceed.
 *
 */
void bsp_start(void)
{
	/* Initialize the printer driver. */
	init_printer();

	/* Say hello :-) We write a small message after each
	   initialization stage to make it easier to see
	   where things go wrong. Should that happen,
	   of course. */
	puts_nb("Initializing ...\n");

	/* Initialize ASIDs. */
	puts_nb("cpu0: ASID ...");
	init_asid();
	puts_nb(" OK\n");

	/* Initialize TLB. */
	puts_nb("cpu0: Address translation ...");
	init_tlb();
	puts_nb(" OK\n");

	/* Initialize frames. */
	puts_nb("cpu0: Physical memory init ...\n");
	init_frames();

	/* Heap initialization */
	puts_nb("cpu0: Heap init ...");
	init_heap();
	puts_nb(" OK\n");

	/* Timers. */
	puts_nb("cpu0: Timers ...");
	init_timers();
	puts_nb(" OK\n");

	/* Scheduler. */
	puts_nb("cpu0: Scheduler ...");
	init_scheduler();
	puts_nb(" OK\n");

	/* Keyboard. */
	puts_nb("cpu0: Keyboard ...");
	init_keyboard();
	puts_nb(" OK\n");

	/* Disk. */
	puts_nb("cpu0: Disk ...");
	init_disk();
	puts_nb(" OK\n");

	/* Threading. */
	puts_nb("cpu0: Threading and processes ...");
	init_threads();
	init_kernel();
	init_processes();
	puts_nb(" OK\n\n");

	/* Everything is ready for moving
	   to a standard thread. */
	thread_t thread;

	if (thread_create(&thread, main_thread, THREAD_NO_DATA, THREAD_STD_FLAGS) != EOK) {
		panic("Cound not create the main kernel thread.");
	}

	/* Schedule the first thread to run. */
	schedule();

	/* The execution should never
	   reach this point. */
	panic("Execution returned to bsp_start().");
}

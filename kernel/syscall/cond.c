/**
 * @file kernel/syscall/cond.c
 *
 * Implementation of conditional variables syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/sync/cond.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <syscall/cond.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <proc/process.h>
#include <proc/kernel.h>
#include <mm/malloc.h>


/** This is a syscall wrapper around ::cond_init function.
 *
 * Initializes the condition variable.
 *
 * @param cond_handle_ptr Pointer to the condition variable handle that will
 *                        be initialized.
 *
 * @return ::EOK when everything is all right, ::ENOMEM when there is no
 *         memory for creating the condition variable.
 *
 */
int syscall_cond_init(unative_t *cond_handle_ptr)
{
	/* Control the passed pointer. */
	syscall_validate_useg_ptr(cond_handle_ptr, "Invalid pointer passed to the cvar_init syscall.");

	if (!syscall_is_ptr_mapped(cond_handle_ptr)) {
		syscall_thread_suicide("Unmapped pointer passed to the cond_init syscall.");
	}

	/* Allocation of condition variable is a critical section (thread can not be killed while it is
	   in some syscall critical section), because it might lead to memory leap or the
	   mutex of kernel heap might remain locked forever. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();

	/* Create new condition variable. */
	cond_t *cond = malloc(sizeof(cond_t));

	if (cond == NULL) {
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return ENOMEM;
	}

	cond_init(cond);

	/* Register condition variable as the process resource - this is an atomic operation. */
	process_register_resource(cond, &cond->resource_item, RESOURCE_TYPE_CVAR);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Identifier of the condition variable in user space is its address on the kernel heap. */
	*cond_handle_ptr = (unative_t)cond;

	return EOK;
}

/** This is a syscall wrapper around ::cond_destroy function.
 *
 * Destroys the passed condition variable and checks its integrity.
 *
 * @param cond_handle Handle of the condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_cond_destroy(unative_t cond_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. We can not unregister the condition
	   variable immediately, because we need to check if there are
	   no waiting threads. We also lock the resource tree with condition
	   variables of currently running process, so no other thread can start
	   waiting for the condition variable in the meantime. */
	process_lock_resources(RESOURCE_TYPE_CVAR);
	cond_t *cond = process_get_cvar(cond_handle);

	if (cond == NULL) {
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* Kill the thread when there are waiting threads on the condition variable. */
	if (!list_empty(&cond->waiting_threads)) {
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		syscall_thread_suicide("There are waiting threads on the condition variable, killing the thread!");
	}

	/* Unregister the condition variable. */
	process_unregister_cvar(cond_handle);
	process_unlock_resources(RESOURCE_TYPE_CVAR);

	/* Put the condition variable to the zombie list where it will get deallocated. */
	kernel_put_cond_to_zombies(cond);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return EOK;
}

/** This is a syscall wrapper around ::cond_signal function.
 *
 * Wakes up one thread waiting for the condition variable.
 *
 * @param  cond_handle Handle of the condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_cond_signal(unative_t cond_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_lock_resources(RESOURCE_TYPE_CVAR);
	cond_t *cond = process_get_cvar(cond_handle);

	/* Control the passed handle. */
	if (cond == NULL) {
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* We mark down that we are using the condition variable so it can not be deallocated while we are using it. */
	atomic_increment(&cond->syscall_use_count);
	process_unlock_resources(RESOURCE_TYPE_CVAR);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Signal on the condition variable. */
	cond_signal(cond);
	atomic_decrement(&cond->syscall_use_count);

	return EOK;
}

/** This is a syscall wrapper around ::cond_broadcast function.
 *
 * Wakes up all the threads waiting for the condition variable.
 *
 * @param  cond_handle Handle of the condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_cond_broadcast(unative_t cond_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_lock_resources(RESOURCE_TYPE_CVAR);
	cond_t *cond = process_get_cvar(cond_handle);

	/* Control the passed handle. */
	if (cond == NULL) {
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* We mark down that we are using the condition variable so it can not be deallocated while we are using it. */
	atomic_increment(&cond->syscall_use_count);
	process_unlock_resources(RESOURCE_TYPE_CVAR);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Broadcast on the condition variable. */
	cond_broadcast(cond);
	atomic_decrement(&cond->syscall_use_count);

	return EOK;
}

/** This is a syscall wrapper around ::cond_wait and
 *  ::cond_wait_timeout functions.
 *
 * @param  cond_handle Handle of the condition variable.
 * @param  mtx_handle  Handle of the mutex to get unlocked.
 * @param  usec        Timeout for the waiting.
 * @param  timeout     Use the _timeout variant of the cond_wait function?
 *
 * @return ::EOK, ::EINVAL or ::ETIMEDOUT.
 *
 */
static int syscall_cond_wait_proc(unative_t cond_handle, unative_t mtx_handle, const unsigned int usec, bool timeout)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_lock_resources(RESOURCE_TYPE_CVAR);
	cond_t *cond = process_get_cvar(cond_handle);

	/* Control the passed condition variable handle. */
	if (cond == NULL) {
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	process_lock_resources(RESOURCE_TYPE_MUTEX);
	mutex_t *mtx = process_get_mutex(mtx_handle);

	/* Control the passed mutex handle. */
	if (mtx == NULL) {
		process_unlock_resources(RESOURCE_TYPE_MUTEX);
		process_unlock_resources(RESOURCE_TYPE_CVAR);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* We mark down that we are using the condition variable and mutex so
	   they can not be deallocated while we are using them. */
	atomic_increment(&cond->syscall_use_count);
	atomic_increment(&mtx->syscall_use_count);
	process_unlock_resources(RESOURCE_TYPE_MUTEX);
	process_unlock_resources(RESOURCE_TYPE_CVAR);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Wait on the condition variable. */
	int result = EOK;

	if (timeout) {
		result = cond_wait_timeout(cond, mtx, usec);
	} else {
		cond_wait(cond, mtx);
	}

	atomic_decrement(&cond->syscall_use_count);
	atomic_decrement(&mtx->syscall_use_count);

	return result;
}

/** This is a syscall wrapper around ::cond_wait function.
 *
 * Atomically unlocks the mutex and blocks the calling thread.
 * The mutex is locked again after unblocking.
 *
 * @param  cond_handle Handle of the condition variable.
 * @param  mtx_handle  Handle of the mutex to get unlocked.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_cond_wait(unative_t cond_handle, unative_t mtx_handle)
{
	return syscall_cond_wait_proc(cond_handle, mtx_handle, 0, false);
}

/** This is a syscall wrapper around ::cond_wait_timeout function.
 *
 * Atomically unlocks the mutex and blocks the calling thread. After unblocking
 * the mutex is locked again. Thread is blocked at most for the @p usec micro
 * seconds. When the thread is unblocked during the waiting process it returns
 * ::EOK otherwise ::ETIMEDOUT. When the @p usec = 0 the thread is not blocked
 * at all, but before locking again it yields once.
 *
 * @param  cond_handle Handle of the condition variable.
 * @param  mtx_handle  Handle of the mutex to get unlocked.
 * @param  usec        Timeout for the waiting.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK when the thread
 *         is waken up before the time limit, ::ETIMEDOUT when it timed out.
 *
 */
int syscall_cond_wait_timeout(unative_t cond_handle, unative_t mtx_handle, const unsigned int usec)
{
	return syscall_cond_wait_proc(cond_handle, mtx_handle, usec, true);
}

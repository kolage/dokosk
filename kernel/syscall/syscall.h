/**
 * @file kernel/syscall/syscall.h
 *
 * Declarations for @ref syscall_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_H_
#define SYSCALL_H_

#include <include/c.h>


/** Definition of syscall handler. */
typedef unative_t (*syscall_t)(unative_t arg0, unative_t arg1, unative_t arg2, unative_t arg3);


/* Externals are commented with implementation. */
extern void syscall(context_t *registers);
extern void syscall_validate_useg_ptr(void* addr, char *msg);
extern void syscall_validate_range(unative_t value, unative_t begin, unative_t end, char *msg);
extern bool syscall_is_ptr_mapped(void* addr);
extern void syscall_thread_suicide(const char *msg);

#endif /* SYSCALL_H_ */

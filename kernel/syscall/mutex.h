/**
 * @file kernel/syscall/mutex.h
 *
 * Declarations of mutex syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/sync/mutex.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef SYSCALL_MUTEX_H_
#define SYSCALL_MUTEX_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern int syscall_mutex_init(unative_t *mutex_handle_ptr, bool recursive);
extern int syscall_mutex_destroy(unative_t mtx_handle);
extern int syscall_mutex_lock(unative_t mtx_handle);
extern int syscall_mutex_lock_timeout(unative_t mtx_handle, unsigned int usec);
extern int syscall_mutex_unlock(unative_t mtx_handle, bool checked);

#endif /* SYSCALL_MUTEX_H_ */

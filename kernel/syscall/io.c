/**
 * @file kernel/syscall/io.c
 *
 * Implementation of standard input and output syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/drivers/printer.h and kernel/drivers/keyboard.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#include <syscall/io.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <drivers/printer.h>
#include <drivers/keyboard.h>


/** This is a syscall wrapper around ::getc function.
 *
 * Reads the character from the standard input device.
 *
 * @return Character read from the standard input device.
 *
 */
char syscall_getc(void)
{
	/* There is not a critical section around getc function (thread can not
	   be killed while it is in some syscall critical section), because when
	   the thread is killed with locked std input mutex, then the mutex is
	   automatically unlocked. */
	return getc();
}

/** This is a syscall wrapper around ::checkc function.
 *
 * Reads the character from the standard input device and leaves it in
 * the std input buffer.
 *
 * @return Character read from the standard input device.
 *
 */
char syscall_checkc(void)
{
	/* There is not a critical section around checkc function (thread can not
	   be killed while it is in some syscall critical section), because when
	   the thread is killed with locked std input mutex, then the mutex is
	   automatically unlocked. */
	return checkc();
}

/** This is a syscall wrapper around ::puts function.
 *
 * Print a zero terminated string.
 *
 * @param  str The string to be printed.
 *
 * @return Number of printed characters.
 *
 */
size_t syscall_puts(char *str)
{
	/* It is enough to validate just the pointer. We do not validate
	   whether the whole printed string is in the mapped area. If so,
	   then the thread is killed when the TLB refill exception is thrown. */
	syscall_validate_useg_ptr(str, "Invalid pointer passed to the output syscall.");

	if (!syscall_is_ptr_mapped(str)) {
		syscall_thread_suicide("Unmapped pointer passed to the output syscall.");
	}

	/* This is a critical section (thread can not be killed while it is
	   in some syscall critical section), because the output mutex might
	   remain locked if the thread would get killed in the puts function. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	size_t size = puts(str);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return size;
}

/**
 * @file kernel/syscall/memory.c
 *
 * Implementation of user space memory management syscalls.
 *
 * Those syscalls manage the size of user space heap of currently
 * running process.
 *
 * The syscalls are specified in the @ref syscall_table.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#include <syscall/memory.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <mm/vma.h>
#include <proc/process.h>


/** Syscall implementation for user space heap initialization.
 *
 * @param  heap_size Initial size of the heap.
 *
 * @return Start address of the heap.
 *
 */
void *syscall_heap_init(size_t heap_size)
{
	process_impl_t *current_process = process_self_impl();

	/* We initialize the heap only if it is not already initialized. */
	if (current_process->heap_start == NULL) {
		THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
		heap_size = ALIGN_UP(heap_size, PAGE_SIZE);
		int result = vma_map(&current_process->heap_start, heap_size, VF_VA_AUTO | VF_AT_KUSEG);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

		if (result != EOK) {
			syscall_thread_suicide("Could not initialize the process heap!");
		}
	}

	return current_process->heap_start;
}

/** Syscall implementation for user space heap resize.
 *
 * @param  heap_size New the heap.
 *
 * @return ::EOK when the heap was successfully resized, ::ENOMEM when there
 *         is not enough memory for resizing the heap.
 *
 */
int syscall_heap_resize(size_t heap_size)
{
	process_impl_t *current_process = process_self_impl();

	if (current_process->heap_start != NULL && heap_size > 0) {
		THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
		heap_size = ALIGN_UP(heap_size, PAGE_SIZE);
		int result = vma_resize(current_process->heap_start, heap_size);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

		return result;
	} else {
		return EINVAL;
	}
}

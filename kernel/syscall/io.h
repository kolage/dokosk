/**
 * @file kernel/syscall/io.h
 *
 * Declarations for input and output syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/drivers/printer.h and kernel/drivers/keyboard.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_IO_H_
#define SYSCALL_IO_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern char syscall_getc(void);
extern char syscall_checkc(void);
extern size_t syscall_puts(char *str);

#endif /* SYSCALL_IO_H_ */

/**
 * @file kernel/syscall/disk.c
 * @ingroup kernel_group
 *
 * Implementation of disk syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/drivers/disk.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#include <syscall/disk.h>
#include <drivers/disk.h>
#include <proc/thread.h>
#include <drivers/printer.h>


/** This is a syscall wrapper around ::disk_read_sector function.
 *
 * Read specified data sector from disk.
 *
 * @param  sector Number of sector which should be read.
 * @param  buffer Buffer for reading the data from the sector.
 *
 * @return Status of disk read operation.
 *
 */
int syscall_disk_read_sector(int sector, char buffer[])
{
	/* Control the passed pointer. */
	syscall_validate_useg_ptr(buffer, "Invalid pointer passed to the disk_read_sector syscall.");

	if (!syscall_is_ptr_mapped(buffer)) {
		syscall_thread_suicide("Unmapped pointer passed to the disk_read_sector syscall.");
	}

	/* This is a critical section (thread can not be killed while it is
	   in some syscall critical section), because the disk mutex might
	   remain locked if the thread would get killed in the disk_read_sector
	   function. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	int status = disk_read_sector(sector,buffer);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return status;
}

/** This is a syscall wrapper around ::disk_size function.
 *
 * Gets disk size in bytes.
 *
 * @return Capacity of disk device in bytes.
 *
 */
int syscall_disk_size(void)
{
	return disk_size();
}

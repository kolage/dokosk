/**
 * @file kernel/syscall/syscall.c
 *
 * Implementation of @ref syscall_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page syscall_feature Syscalls
 *
 * Syscalls provide an interface to the user space.
 *
 * Syscall is identified by an ID, which is specified in @ref syscall_table.
 * The semantics of the individual syscalls are documented with the functions.
 *
 * Syscall can be called from the user space by the syscall instruction.
 * The syscall ID should be in the v0 register which is also used as the return value.
 * Arguments should be in the a0, a1, a2 and a3 registers.
 * Therefore the syscall is compatible with normal C ABI call, but can take at
 * most 4 arguments.
 *
 * The function that executes the syscall (syscall handler) is selected by the
 * ::syscall_find_handler function.
 * The syscall handler is responsible for validating the arguments which
 * are provided by the user space and therefore cannot be trusted.
 *
 * Pointers that are used to store values to user memory (such as in
 * ::syscall_thread_create) are validated that they do not point to
 * the directly mapped segments. The check that they are really
 * mapped in the address space of the calling user thread can be used
 * too, but is not reliable because the mapping can be changed
 * concurrently. For that reason, all stores to the pointers are
 * postponed to the end of the syscall and if they fail,
 * the thread is killed on invalid memory access the same way
 * it would happen in the user space.
 *
 * Handles of kernel primitives (such as ::thread_t) are validated
 * by looking up the resource in the resource trees of the current process
 * (@ref process_management_feature).
 *
 * There is also a possibility that a user thread is cancelled (by calling ::syscall_thread_cancel on it)
 * while it is executing a syscall.
 * To ensure consistency in this case, sections where the user thread cannot be
 * safely cancelled are surrounded with
 * ::THREAD_ENTER_CRITICAL_SYSCALL_SECTION and ::THREAD_LEAVE_CRITICAL_SYSCALL_SECTION
 * and the user thread is cancelled after leaving the critical section.
 *
 * @section syscall_table Table of syscalls
 * <table>
 * <tr><th>Syscall ID</th><th>Definition</th></tr>
 * <tr><td> 1</td><td>::syscall_getc</td></tr>
 * <tr><td> 2</td><td>::syscall_checkc</td></tr>
 * <tr><td> 3</td><td>::syscall_puts</td></tr>
 * <tr><td> 4</td><td>::syscall_disk_read_sector</td></tr>
 * <tr><td> 5</td><td>::syscall_disk_size</td></tr>
 * <tr><td>10</td><td>::syscall_heap_init</td></tr>
 * <tr><td>11</td><td>::syscall_heap_resize</td></tr>
 * <tr><td>20</td><td>::syscall_mutex_init</td></tr>
 * <tr><td>21</td><td>::syscall_mutex_destroy</td></tr>
 * <tr><td>22</td><td>::syscall_mutex_lock</td></tr>
 * <tr><td>23</td><td>::syscall_mutex_lock_timeout</td></tr>
 * <tr><td>24</td><td>::syscall_mutex_unlock</td></tr>
 * <tr><td>30</td><td>::syscall_thread_create</td></tr>
 * <tr><td>31</td><td>::syscall_thread_self</td></tr>
 * <tr><td>32</td><td>::syscall_thread_join</td></tr>
 * <tr><td>33</td><td>::syscall_thread_join_timeout</td></tr>
 * <tr><td>34</td><td>::syscall_thread_detach</td></tr>
 * <tr><td>35</td><td>::syscall_thread_cancel</td></tr>
 * <tr><td>36</td><td>::syscall_thread_sleep</td></tr>
 * <tr><td>37</td><td>::syscall_thread_usleep</td></tr>
 * <tr><td>38</td><td>::syscall_thread_yield</td></tr>
 * <tr><td>39</td><td>::syscall_thread_suspend</td></tr>
 * <tr><td>40</td><td>::syscall_thread_wakeup</td></tr>
 * <tr><td>41</td><td>::syscall_thread_exit</td></tr>
 * <tr><td>42</td><td>::syscall_exit</td></tr>
 * <tr><td>50</td><td>::syscall_cond_init</td></tr>
 * <tr><td>51</td><td>::syscall_cond_destroy</td></tr>
 * <tr><td>52</td><td>::syscall_cond_signal</td></tr>
 * <tr><td>53</td><td>::syscall_cond_broadcast</td></tr>
 * <tr><td>54</td><td>::syscall_cond_wait</td></tr>
 * <tr><td>55</td><td>::syscall_cond_wait_timeout</td></tr>
 * <tr><td>60</td><td>::syscall_process_create</td></tr>
 * <tr><td>61</td><td>::syscall_process_self</td></tr>
 * <tr><td>62</td><td>::syscall_process_join</td></tr>
 * <tr><td>63</td><td>::syscall_process_join_timeout</td></tr>
 * <tr><td>64</td><td>::syscall_kill</td></tr>
 * <tr><td>65</td><td>::syscall_get_pid</td></tr>
 * </table>
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <drivers/printer.h>
#include <drivers/keyboard.h>
#include <mm/malloc.h>
#include <sched/sched.h>
#include <syscall/syscall.h>
#include <syscall/process.h>
#include <syscall/thread.h>
#include <syscall/mutex.h>
#include <syscall/io.h>
#include <syscall/disk.h>
#include <syscall/cond.h>
#include <syscall/memory.h>


/** Kill the current thread from a syscall.
 *
 * @param msg Message to be written when the thread is killed.
 *
 */
void syscall_thread_suicide(const char *msg)
{
	/* This is a critical section because of the kernel I/O mutexes
	  (thread can not be killed while it is in the critical section in some syscall). */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	printk("%s\nKilling the thread %i\n", msg, thread_get_current_id());
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Committing suicide is not a critical section - it can not corrupt kernel. */
	thread_commit_suicide();
}

/** Find a syscall handler.
 *
 * This is implemented as a function and not as an array to allow
 * sparse id assignment.
 *
 * @param  syscall_id Id of the syscall that we want to handle.
 *
 * @return The syscall handler or ::NULL if the identifier is not assigned.
 *
 */
static syscall_t syscall_find_handler(unative_t syscall_id)
{
	switch (syscall_id) {
		case 1:
			return (syscall_t)&syscall_getc;

		case 2:
			return (syscall_t)&syscall_checkc;

		case 3:
			return (syscall_t)&syscall_puts;

		case 4:
			return (syscall_t)&syscall_disk_read_sector;

		case 5:
			return (syscall_t)&syscall_disk_size;

		case 10:
			return (syscall_t)&syscall_heap_init;

		case 11:
			return (syscall_t)&syscall_heap_resize;

		case 20:
			return (syscall_t)&syscall_mutex_init;

		case 21:
			return (syscall_t)&syscall_mutex_destroy;

		case 22:
			return (syscall_t)&syscall_mutex_lock;

		case 23:
			return (syscall_t)&syscall_mutex_lock_timeout;

		case 24:
			return (syscall_t)&syscall_mutex_unlock;

		case 30:
			return (syscall_t)&syscall_thread_create;

		case 31:
			return (syscall_t)&syscall_thread_self;

		case 32:
			return (syscall_t)&syscall_thread_join;

		case 33:
			return (syscall_t)&syscall_thread_join_timeout;

		case 34:
			return (syscall_t)&syscall_thread_detach;

		case 35:
			return (syscall_t)&syscall_thread_cancel;

		case 36:
			return (syscall_t)&syscall_thread_sleep;

		case 37:
			return (syscall_t)&syscall_thread_usleep;

		case 38:
			return (syscall_t)&syscall_thread_yield;

		case 39:
			return (syscall_t)&syscall_thread_suspend;

		case 40:
			return (syscall_t)&syscall_thread_wakeup;

		case 41:
			return (syscall_t)&syscall_thread_exit;

		case 42:
			return (syscall_t)&syscall_exit;

		case 50:
			return (syscall_t)&syscall_cond_init;

		case 51:
			return (syscall_t)&syscall_cond_destroy;

		case 52:
			return (syscall_t)&syscall_cond_signal;

		case 53:
			return (syscall_t)&syscall_cond_broadcast;

		case 54:
			return (syscall_t)&syscall_cond_wait;

		case 55:
			return (syscall_t)&syscall_cond_wait_timeout;

		case 60:
			return (syscall_t)&syscall_process_create;

		case 61:
			return (syscall_t)&syscall_process_self;

		case 62:
			return (syscall_t)&syscall_process_join;

		case 63:
			return (syscall_t)&syscall_process_join_timeout;

		case 64:
			return (syscall_t)&syscall_kill;

		case 65:
			return (syscall_t)&syscall_get_pid;

		default:
			return NULL;
	}
}

/** Handle a system call.
 *
 * The function is called from the exception handler when the exception
 * is identified as being caused by a system call. Interrupts are
 * disabled and the various registers identify what system
 * call is requested.
 *
 * Before returning, the routine adjusts the program counter to
 * point to the instruction following the system call.
 *
 * @param registers The registers at the time of the interrupt.
 *
 */
void syscall(context_t *registers)
{
	/* We don't need disabled interrupts. */
	enable_interrupts();

	/* Fail here if the syscall was in branch delay slot. */
	if (CP0_CAUSE_BD(registers->cause)) {
		syscall_thread_suicide("Syscall from branch delay slot.");
	}

	/* Find the handler according to v0 register. */
	syscall_t handler = syscall_find_handler(registers->v0);

	if (handler == NULL) {
		syscall_thread_suicide("Invalid syscall id.");
	}

	/* Execute the handler. */
	registers->v0 = handler(registers->a0, registers->a1, registers->a2, registers->a3);

	/* The sycall was not in a branch delay slot, so we can just go to the next instruction. */
	registers->epc = registers->epc + 4;

	disable_interrupts();
}

/** Validates pointer passed by user code. If the pointer is not valid,
 *  kills the thread.
 *
 * The pointer is valid if it is in user segment. Then, when accessing it,
 * virtual address mapping applies just like in user mode.
 * Note that you must still expect accesses to the pointer to fail.
 *
 * @param addr The pointer passed by user code.
 * @param msg  Message to be written out when the validation is unsuccessful.
 *
 */
void syscall_validate_useg_ptr(void *addr, char *msg)
{
	syscall_validate_range((unative_t)addr, ADDR_PREFIX_KUSEG, ADDR_PREFIX_KSEG0, msg);
}

/** Validate the integer passed by user code.
 *
 * If the integer is not valid, this function kills the currently
 * running thread.
 *
 * @param value The value passed by user code.
 * @param begin The lower bound. Inclusive.
 * @param end   The upper bound. Exclusive and 0 means 1<<32.
 * @param msg   Message to be written out when the validation is unsuccessful.
 *
 */
void syscall_validate_range(unative_t value, unative_t begin, unative_t end, char *msg)
{
	if (value < begin || (end != 0 && value >= end)) {
		syscall_thread_suicide(msg);
	}
}

/** Check whether the passed pointer is mapped to some physical address.
 *
 * @param  addr Tested pointer.
 *
 * @return Is the passed pointer mapped to some physical address?
 *
 */
bool syscall_is_ptr_mapped(void *addr)
{
	/* This is a critical section because of the mutex of process address
	   space. If thread is killed while holding this mutex, it would corrupt
	   currently running process. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	bool is_mapped = addr_space_get_area(addr) != NULL;
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return is_mapped;
}

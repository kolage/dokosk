/**
 * @file kernel/syscall/process.c
 *
 * Implementation of process syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/proc/process.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <syscall/process.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <proc/process.h>


/** This is a syscall wrapper around ::process_create function.
 *
 * Creates new user process with a new address space. The binary image
 * of the process is loaded from the address space of currently running
 * process from address @p img.
 *
 * @param  proc_handle_ptr Pointer to the handle of process which should
 *                         get filled.
 * @param  img             Address of process binary image in the current
 *                         address space.
 * @param  size            Size of the process binary image.
 *
 * @return Returns ::EOK when the process is successfully created,
 *         ::ENOMEM when there is not enough memory for creating the process,
 *         ::EINVAL when something else goes wrong while creating new process.
 *
 */
int syscall_process_create(unative_t *proc_handle_ptr, const void *img, const size_t size)
{
	/* Control the passed pointer. */
	syscall_validate_useg_ptr(proc_handle_ptr, "Invalid pointer passed to the process_create syscall.");
	syscall_validate_useg_ptr((void *)img, "Invalid pointer to process binary image passed to the process_create syscall.");

	if (!syscall_is_ptr_mapped(proc_handle_ptr) || !syscall_is_ptr_mapped((void *)img)) {
		syscall_thread_suicide("Unmapped pointer passed to the process_create syscall.");
	}

	/* Allocation of process is a critical section (thread can not be killed while it is
	   in some syscall critical section), because it might lead to memory leap or the
	   mutex of kernel heap might remain locked forever. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_t process;
	int result = process_create(&process, img, size);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	if (result == EOK) {
		/* Identifier of the process in user space is its address on the kernel heap. */
		*proc_handle_ptr = (unative_t)process.impl;
	}

	return result;
}

/** This is a syscall wrapper around ::process_self function.
 *
 * Returns a handle to the currently running process.
 *
 * @return Handle to the currently running process.
 *
 */
unative_t syscall_process_self(void)
{
	return (unative_t)process_self_impl();
}

/** This is a syscall wrapper around ::process_join function.
 *
 * Blocks the calling thread until the passed process finishes. Returns
 * ::EINVAL when someone else is waiting for the process, or when thread is
 * calling the function for its own process.
 *
 * @param  proc_handle Handle of process which we want to wait for.
 *
 * @return ::EINVAL when the process can not be joined or when the passed
 *         @p proc_handle is invalid, otherwise ::EOK.
 *
 */
int syscall_process_join(unative_t proc_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_impl_t *proc_impl = process_get(proc_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (proc_impl == NULL) {
		return EINVAL;
	}

	/* Wait for the process. */
	process_t proc;
	proc.impl = proc_impl;

	return process_join(proc);
}

/** This is a syscall wrapper around ::process_join_timeout function.
 *
 * Blocks the calling thread until the passed process finishes. Returns
 * ::EINVAL when someone else is waiting for the process, or when thread is
 * calling the function for its own process. This function waits maximally
 * @p usec microseconds.
 *
 * When the waiting expired this function returns ::ETIMEDOUT.
 *
 * @param  proc_handle Handle of process which we want to wait for.
 * @param  usec        Number of microseconds to wait maximally.
 *
 * @return ::EINVAL when the process can not be joined or when the passed
 *         @p proc_handle is invalid, ::ETIMEDOUT when the timeout has elapsed
 *         and ::EOK otherwise.
 *
 */
int syscall_process_join_timeout(unative_t proc_handle, const unsigned int usec)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_impl_t *proc_impl = process_get(proc_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (proc_impl == NULL) {
		return EINVAL;
	}

	/* Wait for the process. */
	process_t proc;
	proc.impl = proc_impl;

	return process_join_timeout(proc, usec);
}

/** This is a syscall wrapper around ::kill function.
 *
 * Kills the passed running process. Thread which is waiting for the process
 * to finish is waken up.
 *
 * @param  proc_handle Handle of process which should be killed.
 *
 * @return ::EINVAL when the process identifier is invalid or when the
 *         kill was not possible, ::EOK otherwise.
 *
 */
int syscall_kill(unative_t proc_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_impl_t *proc_impl = process_unregister(proc_handle);

	/* Control the passed handle. */
	if (proc_impl == NULL) {
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* Wait for the process. */
	process_t proc;
	proc.impl = proc_impl;

	/* Kill is in critical section, because if it would not and we would get killed, then
	   the killed process might remain in the weird semi-killed state. */
	int result = kill(proc);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return result;
}

/** Exit from the currently running process.
 *
 * Kills the currently running process - it commits suicide.
 *
 */
void syscall_exit(void)
{
	kill(process_self());
}

/** Syscall for getting the id of currently running process.
 *
 * @return Id of currently running process.
 *
 */
unative_t syscall_get_pid(void)
{
	return process_self_impl()->pid;
}

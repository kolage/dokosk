/**
 * @file kernel/syscall/thread.c
 *
 * Implementation of thread syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/proc/thread.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#include <syscall/thread.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <drivers/printer.h>
#include <sched/sched.h>


/** This is a syscall wrapper around ::thread_create function.
 *
 * The function creates new attached thread.
 *
 * When the ::TF_NEW_VMM flag is set, then a new virtual memory space
 * is created for the thread. Otherwise the created thread will share
 * the address space with the currently running thread.
 *
 * @param thread_handle_ptr Handle to newly created thread structure.
 * @param start_wrapper     Pointer to @p thread_start wrapper function.
 * @param thread_start      Function to be started in newly created thread.
 * @param data              Data related to @p thread_start function.
 *
 * @return ::ENOMEM when there was not enough memory to allocate thread
 *         structure, otherwise ::EOK.
 *
 */
int syscall_thread_create(unative_t *thread_handle_ptr, void *start_wrapper, void *thread_start, void *data)
{
	/* Control the passed pointers. */
	syscall_validate_useg_ptr(thread_handle_ptr, "Invalid pointer (thread_ptr) passed to the thread_create syscall.");
	syscall_validate_useg_ptr(start_wrapper, "Invalid pointer (start_wrapper) passed to the thread_create syscall.");
	syscall_validate_useg_ptr(thread_start, "Invalid pointer (thread_start) passed to the thread_create syscall.");

	if (!syscall_is_ptr_mapped(thread_handle_ptr)
			|| !syscall_is_ptr_mapped(start_wrapper)
			|| !syscall_is_ptr_mapped(thread_start)) {
		syscall_thread_suicide("Unmapped pointer passed to the thread_create syscall.");
	}

	/* Allocation of thread is a critical section (thread can not be killed while it is
	   in some syscall critical section), because it might lead to memory leak or the
	   mutex of kernel heap might remain locked forever. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_t thread;
	int ret_value = thread_create_user(&thread, start_wrapper, thread_start, data, process_self_impl(), THREAD_STD_FLAGS);

	if (ret_value == EOK) {
		/* Register thread as the process resource - this is an atomic operation. */
		process_register_resource(thread.impl, &thread.impl->resource_item, RESOURCE_TYPE_THREAD);

		/* Run the thread. */
		runnable(thread.impl);
	}

	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	if (ret_value == EOK) {
		/* Identifier of the thread in user space is its address on the kernel heap. */
		*thread_handle_ptr = (unative_t)thread.impl;
	}

	return ret_value;
}

/** This is a syscall wrapper around ::thread_get_current_impl function.
 *
 * Returns an identifier of currently running thread.
 *
 * @return Currently running thread.
 *
 */
unative_t syscall_thread_self(void)
{
	return (unative_t)thread_get_current_impl();
}

/** This is a syscall wrapper around ::thread_join function.
 *
 * Wait until thread run ends.
 *
 * The function waits till the given thread ends  and then initiates memory
 * release containing kernel handling structure and stack.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread
 * was detached using thread_detach function, if someone else is
 * already waiting for this thread or if thread is calling function to
 * itself. If no error is present, it returns ::EOK.
 *
 * @param  thread_handle Handle to thread we are waiting for.
 * @param  thread_retval Place to writeback return value.
 *
 * @return Error code.
 *
 */
int syscall_thread_join(unative_t thread_handle, void **thread_retval)
{
	syscall_validate_useg_ptr(thread_retval, "Invalid pointer (thread_retval) passed to the thread_join syscall.");

	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_impl_t *thread_impl = process_get_thread(thread_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (thread_impl == NULL) {
		return EINVAL;
	}

	thread_t thread;
	thread.impl = thread_impl;

	/* Temporarily store the return value to a local variable
	   to avoid invalid pointer access in the thread_join_timeout_retval function. */
	void *thread_retval_temp;

	/* Join the thread. We do not care if the passed thread is deallocated in
	   the meantime, because this situation is handled in the thread_join_retval function. */
	int return_code = thread_join_retval(thread, &thread_retval_temp);

	/* Store the return value to the location provided by user. */
	if (thread_retval != NULL) {
		*thread_retval = thread_retval_temp;
	}

	return return_code;
}

/** This is a syscall wrapper around ::thread_join_timeout function.
 *
 * Wait until thread run ends with maximum timeout.
 *
 * The function waits till the given thread ends, but no more than time
 * given by @p usec. Then initiates memory release containing kernel
 * handling structure and stack.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread
 * was detached using ::thread_detach function, if someone else is
 * already waiting for this thread or if thread is calling function to
 * itself. It returns ::ETIMEDOUT if thread is not terminated before
 * @p usec time. If no error is present, it returns ::EOK.
 *
 * @param  thread_handle Handle to thread we are waiting for.
 * @param  thread_retval Place to writeback return value.
 * @param  usec          Time in microseconds representing
 *                       the maximum time to be waited for the thread.
 *
 * @return Error code.
 *
 */
int syscall_thread_join_timeout(unative_t thread_handle, void **thread_retval, const unsigned int usec)
{
	syscall_validate_useg_ptr(thread_retval, "Invalid pointer (thread_retval) passed to the thread_join_timeout syscall.");

	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_impl_t *thread_impl = process_get_thread(thread_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (thread_impl == NULL) {
		return EINVAL;
	}

	thread_t thread;
	thread.impl = thread_impl;

	/* Temporarily store the return value to a local variable
	   to avoid invalid pointer access in the thread_join_timeout_retval function. */
	void *thread_retval_temp;

	/* Join the thread. We do not care if the passed thread is deallocated in
	   the meantime, because this situation is handled in the thread_join_timeout_retval function. */
	int return_code = thread_join_timeout_retval(thread, &thread_retval_temp, usec);

	/* Store the return value to the location provided by user. */
	if(thread_retval != NULL) {
		*thread_retval = thread_retval_temp;
	}

	return return_code;
}

/** This is a syscall wrapper around ::thread_detach function.
 *
 * Detach given thread.
 *
 * The function detach given thread.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread
 * is already detached, if thread is already not running and waiting for
 * join, if someone is already waiting for thread in thread_join or
 * if the thread is killed, otherwise ::EOK.
 *
 * @param  thread_handle Handle to thread to be detached.
 *
 * @return Error code.
 *
 */
int syscall_thread_detach(unative_t thread_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_impl_t *thread_impl = process_get_thread(thread_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (thread_impl == NULL) {
		return EINVAL;
	}

	/* Detach the thread. We do not care if the passed thread is deallocated in
	   the meantime, because this situation is handled in the thread_detach function. */
	thread_t thread;
	thread.impl = thread_impl;

	return thread_detach(thread);
}

/** This is a syscall wrapper around ::thread_kill function.
 *
 * Cancel specified (running) thread.
 *
 * If thread is in a critical section, it is only marked to be killed
 * and on leaving CS it commits suicide. Otherwise function kills given
 * thread, thread that is waiting for termination of thread being killed
 * is unblocked.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread has
 * been already killed, if thread has been already marked to be killed or
 * if thread has already finished, otherwise ::EOK.
 *
 * @param  thread_handle Handle to thread to be killed.
 *
 * @return Error code.
 *
 */
int syscall_thread_cancel(unative_t thread_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_impl_t *thread_impl = process_get_thread(thread_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (thread_impl == NULL) {
		return EINVAL;
	}

	/* When the thread is in some critical section, we just mark it as it should
	   be killed. Thread then commits suicide when it leaves the critical section. */
	ipl_t state = query_and_disable_interrupts();
	int result;

	if (!thread_impl_is_valid(thread_impl)
			|| THREAD_IS_KILLED(thread_impl)
			|| THREAD_HAS_FINISHED(thread_impl)
			|| THREAD_TO_BE_KILLED(thread_impl)) {
		result = EINVAL;
	} else if (THREAD_IS_IN_CRITICAL_SYSCALL_SECTION(thread_impl)) {
		thread_impl->attrs |= THREAD_TO_BE_KILLED_MASK;
		result = EOK;
	} else {
		/* Kill the thread. We do not care if the passed thread is deallocated in
		   the meantime, because this situation is handled in the thread_kill function.
		   Killed threads can not enter a critical section in some syscall. */
		thread_t thread;
		thread.impl = thread_impl;
		result = thread_kill(thread);
	}

	conditionally_enable_interrupts(state);
	return result;
}

/** This is a syscall wrapper around ::thread_sleep function.
 *
 * Block currently running thread for given @p sec.
 *
 * @param sec Number of seconds to block thread.
 *
 */
void syscall_thread_sleep(const unsigned int sec)
{
	thread_sleep(sec);
}

/** This is a syscall wrapper around ::thread_usleep function.
 *
 * Blocks currently running thread for given @p usec.
 *
 * @param usec Number of microseconds to block thread.
 *
 */
void syscall_thread_usleep(const unsigned int usec)
{
	thread_usleep(usec);
}

/** This is a syscall wrapper around ::thread_yield function.
 *
 * Pause currently running thread.
 *
 * The function pauses currently running thread and allow other threads
 * to execute. It is paused until it is scheduled again.
 *
 */
void syscall_thread_yield(void)
{
	thread_yield();
}

/** This is a syscall wrapper around ::thread_suspend function.
 *
 * Suspend currently running thread.
 *
 * The function suspends currently running thread until some other thread
 * unblocks it using thread_wakeup function.
 *
 */
void syscall_thread_suspend(void)
{
	thread_suspend();
}

/** This is a syscall wrapper around ::thread_wakeup function.
 *
 * Wake up given thread.
 *
 * The function wakes up (unblocks) given thread. The calling thread
 * continues execution and it isn't rescheduled during this call.
 *
 * It returns ::EINVAL if thread identification is invalid, otherwise ::EOK.
 *
 * @param  thread_handle Handle to thread to be woken up.
 *
 * @return Error code.
 *
 */
int syscall_thread_wakeup(unative_t thread_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	thread_impl_t *thread_impl = process_get_thread(thread_handle);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. */
	if (thread_impl == NULL) {
		return EINVAL;
	}

	/* Wake up the thread. We do not care if the passed thread is deallocated in
	   the meantime, because this situation is handled in the thread_wakeup function. */
	thread_t thread;
	thread.impl = thread_impl;

	return thread_wakeup(thread);
}

/** This is a syscall wrapper around ::thread_exit function.
 *
 * Terminate thread execution.
 *
 * @param  thread_retval Place to writeback return value.
 *
 */
void syscall_thread_exit(void *thread_retval)
{
	thread_exit(thread_retval);
}

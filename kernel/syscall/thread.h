/**
 * @file kernel/syscall/thread.h
 *
 * Declarations for thread syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/proc/thread.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_THREAD_H_
#define SYSCALL_THREAD_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern int syscall_thread_create(unative_t *thread_handle_ptr, void *start_wrapper, void *thread_start, void *data);
extern unative_t syscall_thread_self(void);
extern int syscall_thread_join(unative_t thread_handle, void **thread_retval);
extern int syscall_thread_join_timeout(unative_t thread_handle, void **thread_retval, unsigned int usec);
extern int syscall_thread_detach(unative_t thread_handle);
extern int syscall_thread_cancel(unative_t thread_handle);
extern void syscall_thread_sleep(unsigned int sec);
extern void syscall_thread_usleep(unsigned int usec);
extern void syscall_thread_yield(void);
extern void syscall_thread_suspend(void);
extern int syscall_thread_wakeup(unative_t thread_handle);
extern void syscall_thread_exit(void *thread_retval);

#endif /* SYSCALL_THREAD_H_ */

/**
 * @file kernel/syscall/process.h
 *
 * Declarations for process syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/proc/process.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_PROCESS_H_
#define SYSCALL_PROCESS_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern void syscall_exit(void);
extern int syscall_process_create(unative_t *proc_handle_ptr, const void *img, const size_t size);
extern unative_t syscall_process_self(void);
extern int syscall_process_join(unative_t proc_handle);
extern int syscall_process_join_timeout(unative_t proc_handle, unsigned int usec);
extern int syscall_kill(unative_t proc_handle);
extern unative_t syscall_get_pid(void);

#endif /* SYSCALL_PROCESS_H_ */

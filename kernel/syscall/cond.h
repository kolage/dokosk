/**
 * @file kernel/syscall/cond.h
 *
 * Declarations for condition variables syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/sync/cond.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_COND_H_
#define SYSCALL_COND_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern int syscall_cond_init(unative_t *cond_handle_ptr);
extern int syscall_cond_destroy(unative_t cond_handle);
extern int syscall_cond_signal(unative_t cond_handle);
extern int syscall_cond_broadcast(unative_t cond_handle);
extern int syscall_cond_wait(unative_t cond_handle, unative_t mtx_handle);
extern int syscall_cond_wait_timeout(unative_t cond_handle, unative_t mtx_handle, unsigned int usec);

#endif /* SYSCALL_COND_H_ */

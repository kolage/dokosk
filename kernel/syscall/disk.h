/**
 * @file kernel/syscall/disk.h
 * @ingroup kernel_group
 *
 * Declaration of disk syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/drivers/disk.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_DISK_H_
#define SYSCALL_DISK_H_

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <syscall/syscall.h>


/* Externals are documented with implementation. */
extern int syscall_disk_read_sector(int sector, char buffer[]);
extern int syscall_disk_size(void);

#endif /* SYSCALL_DISK_H_ */

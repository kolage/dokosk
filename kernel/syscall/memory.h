/**
 * @file kernel/syscall/memory.h
 *
 * Declarations for memory management syscalls.
 *
 * The syscalls are specified in the @ref syscall_table.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef SYSCALL_MEMORY_H_
#define SYSCALL_MEMORY_H_

#include <include/c.h>


/* Externals are documented with implementation. */
extern int syscall_heap_resize(size_t heap_size);
extern void *syscall_heap_init(size_t heap_size);

#endif /* SYSCALL_MEMORY_H_ */

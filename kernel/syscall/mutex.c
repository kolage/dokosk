/**
 * @file kernel/syscall/mutex.c
 *
 * Implementation of mutex syscalls.
 *
 * The syscalls are specified in the @ref syscall_table. This is a layer
 * above kernel/sync/mutex.h.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <syscall/mutex.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <adt/atomic.h>
#include <sync/mutex.h>
#include <proc/process.h>
#include <proc/kernel.h>
#include <mm/malloc.h>
#include <drivers/printer.h>


/** This is a syscall wrapper around ::mutex_init function.
 *
 * Initializes the mutex to the unlocked state. This function returns
 * ::EOK when there was no problem with initialization, ::ENOMEM when
 * there is no memory for creating the mutex.
 *
 * @param  mutex_handle_ptr Pointer to the mutex handle that will
 *                          be initialized.
 * @param  recursive        Initialize mutex as recursive?
 *
 * @return ::EOK when there was no problem with initialization,
 *         ::ENOMEM when there is no memory for creating the mutex.
 *
 */
int syscall_mutex_init(unative_t *mutex_handle_ptr, bool recursive)
{
	/* Control the passed pointer. */
	syscall_validate_useg_ptr(mutex_handle_ptr, "Invalid pointer passed to the mutex_init syscall.");

	if (!syscall_is_ptr_mapped(mutex_handle_ptr)) {
		syscall_thread_suicide("Unmapped pointer passed to the mutex_init syscall.");
	}

	/* Allocation of mutex is a critical section (thread can not be killed while it is
	   in some syscall critical section), because it might lead to memory leap or the
	   mutex of kernel heap might remain locked forever. */
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();

	/* Create new mutex. */
	mutex_t *mtx = malloc(sizeof(mutex_t));

	if (mtx == NULL) {
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return ENOMEM;
	}

	/* Do we want a recursive mutex? */
	if (recursive) {
		mutex_init_recursive(mtx);
	} else {
		mutex_init(mtx);
	}

	/* Register mutex as the process resource - this is an atomic operation. */
	process_register_resource(mtx, &mtx->resource_item, RESOURCE_TYPE_MUTEX);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Identifier of the mutex in user space is its address on the kernel heap. */
	*mutex_handle_ptr = (unative_t)mtx;

	return EOK;
}

/** This is a syscall wrapper around ::mutex_destroy function.
 *
 * Destroys the passed mutex and clears its memory. If there are any blocked
 * threads on the mutex, then the current thread is killed. This function
 * returns ::EOK when there was no problem with mutex destruction and
 * ::EINVAL when the mutex handle address is invalid.
 *
 * @param  mtx_handle Handle of the mutex that should be destroyed.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_mutex_destroy(unative_t mtx_handle)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();

	/* Control the passed handle. We can not unregister the mutex
	   immediately, because we need to check if there are
	   no waiting threads. We also lock the resource tree with mutexes
	   of currently running process, so no other thread can start
	   waiting for the mutex in the meantime. */
	process_lock_resources(RESOURCE_TYPE_MUTEX);
	mutex_t *mtx = process_get_mutex(mtx_handle);

	if (mtx == NULL) {
		process_unlock_resources(RESOURCE_TYPE_MUTEX);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* Kill the thread when there are blocked threads on the mutex. */
	if (!list_empty(&mtx->waiting_threads)) {
		process_unlock_resources(RESOURCE_TYPE_MUTEX);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		syscall_thread_suicide("There are blocked threads on the mutex, killing the thread!");
	}

	/* Unregister the mutex. */
	process_unregister_mutex(mtx_handle);
	process_unlock_resources(RESOURCE_TYPE_MUTEX);

	/* Put the mutex to the zombie list where it will get deallocated. */
	kernel_put_mutex_to_zombies(mtx);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	return EOK;
}

/** This is a syscall wrapper around ::mutex_lock and
 *  ::mutex_lock_timeout functions.
 *
 * @param  mtx_handle Handle of the mutex that should be locked.
 * @param  usec       Timeout for the locking.
 * @param  timeout    Use the _timeout variant of the mutex_lock function?
 *
 * @return ::EOK, ::EINVAL or ::ETIMEDOUT.
 *
 */
static int syscall_mutex_lock_proc(unative_t mtx_handle, unsigned int usec, bool timeout)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_lock_resources(RESOURCE_TYPE_MUTEX);
	mutex_t *mtx = process_get_mutex(mtx_handle);

	/* Control the passed handle. */
	if (mtx == NULL) {
		process_unlock_resources(RESOURCE_TYPE_MUTEX);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* We mark down that we are using the mutex so it can not be deallocated while we are using it. */
	atomic_increment(&mtx->syscall_use_count);
	process_unlock_resources(RESOURCE_TYPE_MUTEX);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Try to lock the mutex. */
	int result = EOK;

	if (timeout) {
		result = mutex_lock_timeout(mtx, usec);
	} else {
		mutex_lock(mtx);
	}

	atomic_decrement(&mtx->syscall_use_count);
	return result;
}

/** This is a syscall wrapper around ::mutex_lock function.
 *
 * Tries to lock the passed mutex. When it does not succeed, it blocks actually
 * running thread and puts it into the queue of threads waiting for the mutex.
 * ::mutex_unlock then wakes up all those threads and the first to get
 * planned then captures the mutex, rest is waiting again etc.
 *
 * This function returns ::EOK when there was no problem with mutex locking
 * and ::EINVAL when the mutex handle address is invalid.
 *
 * @param  mtx_handle Handle of the mutex that should to be locked.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_mutex_lock(unative_t mtx_handle)
{
	return syscall_mutex_lock_proc(mtx_handle, 0, false);
}

/** This is a syscall wrapper around ::mutex_lock_timeout function.
 *
 * Tries to lock the passed mutex. When it does not succeed, it blocks actually
 * running thread and puts it into the queue of threads waiting for the mutex.
 * ::mutex_unlock then wakes up all those threads and the first to get
 * planned then captures the mutex, rest is waiting again etc.
 *
 * This function waits at most @p usec microseconds to the lock. When the
 * timeout is exceeded, the waiting process  is interrupted and the mutex
 * is not locked. When @p usec = 0 this method does not block the calling
 * thread at all. This function returns ::EOK when there was no problem with
 * mutex locking, ::EINVAL when the mutex handle address is invalid and
 * ::ETIMEDOUT the timeout has exceeded.
 *
 * @param  mtx_handle Handle of the mutex that should be locked.
 * @param  usec       Timeout for the locking.
 *
 * @return ::EOK when the mutex was locked by the running thread, ::ETIMEDOUT
 *         when it timed out and ::EINVAL when passed handle is invalid.
 *
 */
int syscall_mutex_lock_timeout(unative_t mtx_handle, unsigned int usec)
{
	return syscall_mutex_lock_proc(mtx_handle, usec, true);
}

/** This is a syscall wrapper around ::mutex_unlock function.
 *
 * Unlocks the mutex and wakes up one of the threads that are actually blocked
 * on the mutex. The first thread planned to run than locks the mutex and the
 * rest keeps waiting in the ::mutex_lock waiting loop. This function returns
 * ::EOK when there was no problem with mutex unlocking and ::EINVAL when
 * the mutex handle address is invalid.
 *
 * If @p checked is ::true, this function checks whether the mutex is unlocked
 * from the same thread as it was locked by.
 *
 * @param  mtx_handle Handle of the mutex that should be unlocked.
 * @param  checked    Check whether the mutex is unlocked from the same
 *                    thread that has locked it?
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int syscall_mutex_unlock(unative_t mtx_handle, bool checked)
{
	THREAD_ENTER_CRITICAL_SYSCALL_SECTION();
	process_lock_resources(RESOURCE_TYPE_MUTEX);
	mutex_t *mtx = process_get_mutex(mtx_handle);

	/* Control the passed handle. */
	if (mtx == NULL) {
		process_unlock_resources(RESOURCE_TYPE_MUTEX);
		THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();
		return EINVAL;
	}

	/* We mark down that we are using the mutex so it can not be deallocated while we are using it. */
	atomic_increment(&mtx->syscall_use_count);
	process_unlock_resources(RESOURCE_TYPE_MUTEX);
	THREAD_LEAVE_CRITICAL_SYSCALL_SECTION();

	/* Check whether the mutex is unlocked from the same thread that has locked it. */
	if (checked) {
		if (mtx->locked_thread != thread_get_current_impl()) {
			if (mtx->locked_thread == NULL) {
				syscall_thread_suicide("No thread locked the mutex!");
			} else {
				syscall_thread_suicide("Mutex unlocked from the thread that did not lock it!");
			}
		}
	}

	/* Unlock the mutex. */
	mutex_unlock_unchecked(mtx);
	atomic_decrement(&mtx->syscall_use_count);

	return EOK;
}

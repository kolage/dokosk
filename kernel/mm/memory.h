/**
 * @file kernel/mm/memory.h
 *
 * Declarations of @ref memory_feature.
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include <include/c.h>


/** Address of the end of the kernel image.
 *
 * The symbol containing the last address of the kernel image is provided by
 * the linker, see the linker script for details of how this is done.
 *
 */
extern uint8_t _kernel_end;

/** End of the kernel physical memory. */
#define PHYS_KERNEL_END_ADDR	((void *)0x20000000)

/** End of the physical memory -it must be 4 bytes aligned. */
#define PHYS_MEM_END_ADDR	    ((void *)0xfffffffc)

/** Hardwired address of the start of the block of devices in the memory (from msim.conf). */
#define DEVICES_ADDR			((void *)(0x10000000))

/** Hardwired address of the end of the block of devices and user start process binary image in the memory (from msim.conf). */
#define DEVICES_END_ADDR		((void *)(0x10011000))

/** Processor hardwired address of kernel loader binary. */
#define LOADER_BINARY_ADDR		((void *)(0x1FC00000))

/** Generic memory alignment. */
#define MEMORY_ALIGNMENT		4


/** Types of the iterators for the physical memory blocks. */
typedef enum {
	PHYS_MEM_IT_KERNEL,
	PHYS_MEM_IT_USER,
	PHYS_MEM_IT_ALL
} memory_it_type_t;

/** Structure representing one physical memory block. */
typedef struct memory_block {
	/** Start physical address of the block. */
	void *start;

	/** Block end physical address. */
	void *end;
} memory_block_t;

/** Structure with info about physical memory map. */
typedef struct memory {
	/** Total number of physical memory blocks of the kernel. */
	size_t kernel_block_count;

	/** Total number of physical memory blocks of the user space. */
	size_t user_block_count;
} memory_t;

/** An iterator for looping through all discovered physical memory blocks. */
typedef struct memory_it {
	native_t current;
	native_t last;
} memory_it_t;


/** Check whether the iterator has reached the end of iteration.
 *
 * @param  it Memory blocks iterator.
 *
 * @return Returns whether the iterator has reached the end of iteration.
 *
 */
static inline bool memory_it_has_current(memory_it_t *it)
{
	return it->current < it->last;
}

/** Advance the iterator to the next memory block.
 *
 * @param it Memory blocks iterator.
 *
 */
static inline void memory_it_next(memory_it_t *it)
{
	it->current++;
}

/** Get the total size of passed memory block.
 *
 * @param  block_info Memory block of which we want its size.
 *
 * @return Total size of passed memory block.
 *
 */
static inline size_t memory_get_block_size(memory_block_t *block_info)
{
	return ((uintptr_t)block_info->end) - ((uintptr_t)block_info->start);
}

/* Externals are commented with implementation. */
extern void memory_fill_with_null(void *start, size_t size);
extern memory_block_t *memory_find_block(native_t index);
extern void memory_it_init(memory_it_t *it, memory_it_type_t type);
extern memory_block_t *memory_it_block(memory_it_t *it);
extern char *memory_size_units(size_t *memory_size);
extern void memory_discover(void);
extern void *memory_find_start(void *start, const void *end_address, const unsigned int step);
extern void *memory_find_end(void *start, const void *end_address, const unsigned int step);

#endif /* MEMORY_H_ */

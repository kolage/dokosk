/**
 * @file kernel/mm/falloc.h
 *
 * Declarations of @ref falloc_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef FALLOC_H_
#define FALLOC_H_

#include <include/shared.h>
#include <include/c.h>
#include <mm/vma.h>


/* Externals are commented with implementation. */
extern void init_frames(void);
extern int frame_alloc(void **paddr, const size_t cnt, const vm_flags_t flags);
extern int frame_free(const void *paddr, const size_t cnt);

#endif /* FALLOC_H_ */

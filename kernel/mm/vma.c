/**
 * @file kernel/mm/vma.c
 *
 * Implementation of @ref vma_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page vma_feature Virtual memory management
 *
 * This page describes the implementation of virtual memory management.
 *
 * The goal of virtual memory management is to keep track of allocated virtual
 * memory areas and to provide interface for translation from virtual addresses
 * to physical addresses. This interface is then used by @ref tlb_feature.
 *
 * Each process has one instance of ::vm_address_space structure which represents
 * the whole virtual address space. Sharing of address space between threads means
 * just sharing of the pointer.
 *
 * Total number of allocated areas is limited by ::MAX_AREAS constant. Because
 * every user space thread allocates one area for its stack, there is a limit
 * for the total number of allocated threads per process (this limit is
 * ::MAX_AREAS - 2, because each process has one area for its binary image
 * and one area for its heap). All the allocated areas are managed in the array
 * vm_address_space::areas.
 *
 * The interface for address translation consists of ::addr_space_page_address
 * and ::addr_space_translate functions. We use two-level paging tables
 * for mapping from virtual addresses to physical addresses. A frame representing
 * the first level of this table is allocated when new ::vm_address_space
 * instance is created. Pointer to this first level table is stored in
 * vm_address_space::addr_translations attribute of ::vm_address_space structure.
 *
 * A second level tables area allocated on demand, i. e. when ::vma_map or ::vma_resize
 * function is called a new frame for second level table is allocated if it has
 * not been allocated yet. When an area is deallocated by ::vma_unmap function
 * or shrinked by ::vma_resize function, the relevant frames with mapping are
 * checked whether they are empty (do not contain any translations) and if so, they
 * are released. The implementation of an area is very simple - the ::vm_area_t structure
 * is used for that purpose.
 *
 * When the thread is deallocated, the ::addr_space_release function is called.
 * This function is responsible for address space deallocation. We use reference
 * counting for the address space - the vm_address_space::reference_count attribute of
 * an address space is incremented when the address space is shared and it decremented when
 * ::addr_space_release function is called. When this counter reaches 0, the whole
 * ::vm_address_space structure is released.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <include/math.h>
#include <drivers/printer.h>
#include <mm/asid.h>
#include <mm/falloc.h>
#include <mm/malloc.h>
#include <mm/memory.h>
#include <mm/tlb.h>
#include <mm/vma.h>
#include <proc/thread.h>

#if !defined(DEBUG_VMA) || defined(__DOXYGEN__)
/** Debug the VMA?
 *
 * By default debugging prints for VMA are disabled.
 * You can define ::DEBUG_VMA to true to enable VMA debugging prints.
 * To do this, add VMA to KERNEL_DEBUG when building the OS:
 * <pre>make KERNEL_DEBUG="VMA"</pre>
 */
#define DEBUG_VMA false
#endif

#if DEBUG_VMA
#define vma_debug_print(ARGS...) printk(ARGS);
#else
#define vma_debug_print(ARGS...)
#endif

/** Number of bits in virtual address used as index to the first-level directory. */
#define ADDR_DIRECTORY_BITS_COUNT	10

/** Number of bits in virtual address used as index to the second-level directory. */
#define ADDR_PAGE_BITS_COUNT		10

/** Number of bits in virtual address used as offset. */
#define ADDR_INDEX_BITS_COUNT		12

/** Mask for the last 10 bits of address. */
#define ADDR_ENTRY_MASK				0x000003FF

/** Get the index to the first-level directory from passed virtual address. */
#define ADDR_SPACE_PAGE_DIRECTORY_INDEX(virt_addr) \
	(((uintptr_t)virt_addr >> (ADDR_INDEX_BITS_COUNT + ADDR_PAGE_BITS_COUNT)) & ADDR_ENTRY_MASK)

/** Get the index to the second-level directory from passed virtual address. */
#define ADDR_SPACE_PAGE_TABLE_INDEX(virt_addr) \
	(((uintptr_t)virt_addr >> (ADDR_INDEX_BITS_COUNT)) & ADDR_ENTRY_MASK)

/** Get pointer to the PDE associated with passed virtual address. */
#define ADDR_SPACE_PAGE_DIRECTORY_ENTRY(directory_addr, virt_addr) \
	(uintptr_t *)(directory_addr + MEMORY_ALIGNMENT * ADDR_SPACE_PAGE_DIRECTORY_INDEX(virt_addr))

/** Get pointer to the PTE associated with passed virtual address. */
#define ADDR_SPACE_PAGE_TABLE_ENTRY(table_addr, virt_addr) \
	(uintptr_t *)(table_addr + MEMORY_ALIGNMENT * ADDR_SPACE_PAGE_TABLE_INDEX(virt_addr))


/** Initialize the virtual memory area.
 *
 * @param area Area to be initialized.
 *
 */
static void vma_init(vm_area_t *area)
{
	area->vma_start = NULL;
	area->size = 0;
}

/** Allocate one frame for the first or second level directory in
 *  two-level paging.
 *
 * Allocated frame is filled with ::NULL value.
 *
 * @param  virt_addr Virtual address to which the pointer to the allocated
 *                   frame will be written.
 *
 * @return ::EOK when the frame allocation is successful, ::EINVAL
 *         or ::ENOMEM otherwise.
 *
 */
static int addr_space_alloc_paging_frame(void **virt_addr)
{
	void *paddr = NULL;
	int result = frame_alloc(&paddr, 1, VF_AT_KSEG0 | VF_VA_AUTO);

	if (result == EOK) {
		/* Fill the allocated frame with NULL value, because there are
		   no translation in the directory/table. */
		*virt_addr = PTR_ADDR_IN_KSEG0(paddr);
		unsigned char *pos = *virt_addr;

		for (int i = 0; i < FRAME_SIZE; i++) {
			*pos = NULL;
			pos++;
		}
	}

	return result;
}

/** Get the pointer to the page directory entry associated with given
 *  virtual address.
 *
 * When the PDE does not exist, this function creates one. If this creation is not
 * successful, then dereferencing of returned pointer is ::NULL.
 *
 * @param  addr_space Address space in which we want to get PDE for the
 *                    passed virtual address.
 * @param  virt_addr  Virtual address to which we want to get PDE.
 *
 * @return Pointer to the page directory entry associated with given
 *         virtual address.
 *
 */
static uintptr_t *addr_space_get_page_directory_entry(vm_address_space_t *addr_space, void *virt_addr)
{
	/* Get the directory entry in two-level paging. */
	uintptr_t *page_directory_entry = ADDR_SPACE_PAGE_DIRECTORY_ENTRY(addr_space->addr_translations, virt_addr);

	if (*page_directory_entry == NULL) {
		void *addr = NULL;

		/* Allocate the second-level directory in the two-level paging
		   and store the pointer in the first level directory. */
		if (addr_space_alloc_paging_frame(&addr) == EOK) {
			*page_directory_entry = (uintptr_t)addr;
		}
	}

	return page_directory_entry;
}

/** Set the translation from virtual address to physical address.
 *
 * The mapping from virtual address to physical address is stored in the
 * two-level paging structure. The caller of this method must ensure,
 * that there is a valid page directory entry for given virtual address.
 *
 * @param addr_space Address space in which we want to set new translation.
 * @param virt_addr  Translated virtual address.
 * @param phys_addr  Translation to physical address.
 *
 */
static void addr_space_set_translation(vm_address_space_t *addr_space, void *virt_addr, void *phys_addr)
{
	uintptr_t *page_directory_entry = ADDR_SPACE_PAGE_DIRECTORY_ENTRY(addr_space->addr_translations, virt_addr);

	/* Save the va -> pa mapping. */
	uintptr_t *page_table_entry = ADDR_SPACE_PAGE_TABLE_ENTRY(*page_directory_entry, virt_addr);
	*page_table_entry = (uintptr_t)phys_addr;
}

/** Initialize the virtual memory.
 *
 * @param  addr_space Virtual address space to be initialized.
 *
 * @return Returns ::EOK when the address space was successfully initialized,
 *         ::ENOMEM or ::EINVAL otherwise.
 *
 */
int addr_space_init(vm_address_space_t *addr_space)
{
	assert(addr_space != NULL);
	addr_space->area_count = 0;
	addr_space->asid = ASID_INVALID;

	atomic_set(&addr_space->reference_count, 1);
	mutex_init(&addr_space->mutex);

	for (int i = 0; i < MAX_AREAS; i++) {
		vma_init(&addr_space->areas[i]);
	}

	/* Allocate one frame for the first level directory of two-level paging. */
	return addr_space_alloc_paging_frame(&addr_space->addr_translations);
}


/** Change the used address space of the current thread.
 *
 * @param addr_space The new address space.
 *
 */
void addr_space_change_used(vm_address_space_t *addr_space)
{
	assert(addr_space != NULL);

	ipl_t state = query_and_disable_interrupts();

	thread_impl_t *thrcur = thread_get_current_impl();
	thrcur->used_addr_space = addr_space;
	asid_current_map_changed();

	conditionally_enable_interrupts(state);
}

/** Add reference to the passed address space.
 *
 * This prevents the address space from being deallocated.
 *
 * @param addr_space Referenced address space.
 *
 */
void addr_space_add_ref(vm_address_space_t *addr_space)
{
	assert(addr_space != NULL);
	atomic_increment(&addr_space->reference_count);
}

/** Release the passed address space.
 *
 * When there are no references to the address space, then it is deallocated,
 * otherwise the reference count is decremented.
 *
 * @param addr_space Address space to be deallocated.
 *
 */
void addr_space_release(vm_address_space_t *addr_space)
{
	assert(addr_space != NULL);
	assert(atomic_get(&addr_space->reference_count) > 0);

	native_t new_reference_count = atomic_decrement(&addr_space->reference_count);

	if (new_reference_count == 0) {
		/* Ensure there are no entries in the TLB and ASID is not assigned to
		   this address space. */
		asid_invalidate(addr_space);
		vma_debug_print("-- VMA DEALLOCATE: Deallocating address space %p.\n", addr_space);

		/* Unmap all the mapped memory. */
		for (uint32_t i = 0; i < addr_space->area_count; i++) {
			vma_unmap_in_addr_space(addr_space, addr_space->areas[i].vma_start);
		}

		/* Release the frame for page directory. */
		if (frame_free(VA_DIRECT_TO_PHYSICAL(addr_space->addr_translations), 1) != EOK) {
			panic("Could not free the frame for page directory!");
		}

		/* Deallocate the address space. */
		free(addr_space);
	} else {
		vma_debug_print(
			"-- VMA DEALLOCATE: The reference count to address space %p was decremented to %u.\n",
			addr_space, new_reference_count
		);
	}
}

/** Get the memory map owned by the current thread.
 *
 * @return Memory map owned by the current thread.
 *
 */
vm_address_space_t *addr_space_get_current_owned(void)
{
	assert(thread_get_current_id() > 0);
	return thread_get_current_impl()->owned_addr_space;
}

/** Get the memory map currently used by the current thread.
 *
 * @return Memory map currently used by the current thread.
 *
 */
vm_address_space_t *addr_space_get_current_used(void)
{
	assert(thread_get_current_id() > 0);
	return thread_get_current_impl()->used_addr_space;
}

/** Get the area which maps the passed @p virtual_address in passed
 *  @p addr_space.
 *
 * @param  addr_space      Address space we want to work with.
 * @param  virtual_address Virtual address to which we need an area.
 *
 * @return Area which maps the passed @p virtual_address.
 *
 */
vm_area_t *addr_space_get_area_from_space(vm_address_space_t *addr_space, const void *virtual_address)
{
	for (uint32_t i = 0; i < addr_space->area_count; i++) {
		vm_area_t *area = &addr_space->areas[i];

		/* Is the virtual address in the area? */
		if (area->vma_start <= virtual_address && virtual_address < area->vma_start + area->size) {
			return area;
		}
	}

	return NULL;
}

/** Get the area which maps the passed @p virtual_address in current
 *  address space.
 *
 * Considers the owned address space of the current thread.
 *
 * @param  virtual_address Virtual address to which we need an area.
 *
 * @return Area which maps the passed @p virtual_address.
 *
 */
vm_area_t *addr_space_get_area(const void *virtual_address)
{
	return addr_space_get_area_from_space(addr_space_get_current_owned(), virtual_address);
}

/** Get the address of physical frame which the passed @p virtual_address
 *  is mapped to in passed @p addr_space.
 *
 * If there is no mapping, the NULL pointer is returned.
 *
 * This could be confused with the mapping of the page at virtual
 * address 0x80000000 or 0xA0000000 from KSEG0 or KSEG1, therefore
 * this function should not be used for the mapping of the directly
 * mapped segments.
 *
 * @param  addr_space      Used address space.
 * @param  virtual_address Virtual address to which we need a physical page.
 *
 * @return Physical page to which the passed virtual address is mapped to
 *         or ::NULL when no mapping exists.
 *
 */
void *addr_space_page_address(vm_address_space_t *addr_space, void *virtual_address)
{
	/* Do the direct virtual address mapping. */
	if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(virtual_address)) {
		return VA_DIRECT_TO_PHYSICAL(virtual_address);
	/* Find the physical page for the virtual address. */
	} else {
		uintptr_t *page_directory_entry = ADDR_SPACE_PAGE_DIRECTORY_ENTRY(addr_space->addr_translations, virtual_address);

		if (*page_directory_entry != NULL) {
			uintptr_t *page_table_entry = ADDR_SPACE_PAGE_TABLE_ENTRY(*page_directory_entry, virtual_address);
			return (void *)(*page_table_entry);
		}
	}

	return NULL;
}

/** Get the physical address which the passed @p virtual_address is
 *  mapped to in passed @p addr_space.
 *
 * If there is no mapping, the NULL pointer is returned.
 *
 * This could be confused with the mapping of the 0x80000000 or 0xA0000000
 * virtual address from KSEG0 or KSEG1, therefore this function should
 * not be used for the mapping of the directly mapped segments.
 *
 * @param  addr_space      Used address space.
 * @param  virtual_address Virtual address to which we need a physical address.
 *
 * @return Physical address to which the passed virtual address is mapped to
 *         or ::NULL when no mapping exists.
 *
 */
void *addr_space_translate(vm_address_space_t *addr_space, void *virtual_address)
{
	void *physical_page = addr_space_page_address(addr_space, virtual_address);

	if (physical_page != NULL) {
		return physical_page + ADDRESS_PAGE_OFFSET(virtual_address);
	} else {
		return NULL;
	}
}

/** Find the start virtual address for the new area.
 *
 * Tries to find the free space between some areas in the given segment
 * where the new area with specified @p size would fit in. If there are
 * no areas in the specified segment, then the @p seg_start address is returned.
 *
 * Returned address + @p size may overlap the segment bound, therefore the
 * is_area_in_segment function has to be called afterwards to control
 * whether the area fits in the segment.
 *
 * The areas in the segment are ordered by the start address and we get
 * an advantage of that when we test whether the new area would fit in.
 *
 * @param  addr_space Address space to work with.
 * @param  seg_start  Start address of the segment, where we want to
 *                    allocate the area.
 * @param  seg_end    End address of the segment, where we want to allocate
 *                    the area.
 * @param  size       Size of the area we want to place in the
 *                    specified segment.
 * @param  from_end   Should we search for the empty space from the end
 *                    of the segment?
 *
 * @return Address of the area start within the passed segment.
 *
 */
static void *addr_space_find_start(vm_address_space_t *addr_space, void *seg_start,
		void *seg_end, size_t size, bool from_end)
{
	uint32_t area_count = addr_space->area_count;
	void *area_start = seg_start;

	if (from_end) {
		/* We have to align up the address, because segment end might be also
		   0xffffffff). Also we do not want to be just at the edge of the
		   segment - we want to have one reserved page to protect kernel. */
		area_start = (void *)ALIGN_UP((uintptr_t)(seg_end - size - PAGE_SIZE), PAGE_SIZE);

		for (int32_t i = area_count - 1; i >= 0; i--) {
			vm_area_t area = addr_space->areas[i];

			/* Is the tested area from appropriate segment? */
			if (!(area.vma_start >= seg_start && area.vma_start < seg_end)) {
				continue;
			}

			/* Does the new area fit in (is its start after the tested area end)? */
			if (area_start >= area.vma_start + area.size) {
				break;
			}

			/* Move the possible start just before the start of the tested area. */
			area_start = area.vma_start - size;
		}
	} else {
		for (uint32_t i = 0; i < area_count; i++) {
			vm_area_t area = addr_space->areas[i];

			/* Is the tested area from appropriate segment? */
			if (!(area.vma_start >= seg_start && area.vma_start < seg_end)) {
				continue;
			}

			/* Does the new area fit in? */
			if (area_start + size <= area.vma_start) {
				break;
			}

			/* Move the possible start to the actual area end. */
			area_start = area.vma_start + area.size;
		}
	}

	/* If we overlapped to the next segment, we return an invalid area start address. */
	if (!(area_start >= seg_start && area_start < seg_end)) {
		return seg_end - 1;
	} else {
		return area_start;
	}
}

/** Test whether the passed area is whole in some segment (it does
 *  not overlap across the segment border).
 *
 * @param  area_start Start address of the area.
 * @param  area_end   End address of the area.
 *
 * @return Is the whole passed area in some segment?
 *
 */
static bool vma_is_area_in_segment(void *area_start, void *area_end)
{
	return (area_start < area_end)
			&& (
				   (area_end <= ADDR_KSEG0)
				|| (area_start >= ADDR_KSEG0 && area_end <= ADDR_KSEG1)
				|| (area_start >= ADDR_KSEG1 && area_end <= ADDR_KSSEG)
				|| (area_start >= ADDR_KSSEG && area_end <= ADDR_KSEG3)
				|| (area_start >= ADDR_KSEG3 && area_end <= ADDR_VM_END)
			);
}

/** Checks whether the passed area overlaps with some other already
 *  allocated virtual area of the current thread.
 *
 * We can also pass the @p ignore_area parameter to exclude one area
 * from testing. This is useful when we expand or collapse the area
 * size and we do not want to test whether the area would overlap
 * with itself.
 *
 * @param  addr_space  Address space to work with.
 * @param  area_start  Start address of the area.
 * @param  area_end    End address of the area.
 * @param  ignore_area Area to be ignored during the testing.
 *
 * @return Is the passed area overlapped with some other area?
 *
 */
static bool addr_space_area_overlaps_with_ignore(vm_address_space_t *addr_space, void *area_start,
		void *area_end, vm_area_t *ignore_area)
{
	uint32_t area_count = addr_space->area_count;
	bool is_area_in_directly_mapped_segment = VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area_start);

	if (is_area_in_directly_mapped_segment) {
		area_start = VA_DIRECT_TO_PHYSICAL(area_start);
		area_end = VA_DIRECT_TO_PHYSICAL(area_end);
	}

	for (uint32_t i = 0; i < area_count; i++) {
		vm_area_t *area = &addr_space->areas[i];

		/* We can ignore one area in the test. */
		if (area == ignore_area) {
			continue;
		}

		/* In the KSEG0 and KSEG1 we check overlap of physical addresses. */
		if (is_area_in_directly_mapped_segment && VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area->vma_start)) {
			void *area_pa_start = VA_DIRECT_TO_PHYSICAL(area->vma_start);

			/* Does area overlap with actually checked area? */
			if (!(area_end <= area_pa_start || area_pa_start + area->size <= area_start)) {
				return true;
			}
		} else {
			/* Does area overlap with actually checked area? */
			if (!(area_end <= area->vma_start || area->vma_start + area->size <= area_start)) {
				return true;
			}
		}
	}

	return false;
}

/** Check whether the passed area overlaps with some other already allocated
 *  virtual area of the current thread.
 *
 * @param  addr_space Address space to work with.
 * @param  area_start Start address of the area.
 * @param  area_end   End address of the area.
 *
 * @return Is the passed area overlapped with some other area?
 *
 */
static bool addr_space_area_overlaps(vm_address_space_t *addr_space, void *area_start, void *area_end)
{
	return addr_space_area_overlaps_with_ignore(addr_space, area_start, area_end, NULL);
}

/** Find the virtual area which starts at address @p from.
 *
 * @param  addr_space Address space to work with.
 * @param  from       Start virtual address of the searched area.
 * @param  index      Output parameter for the index of the searched area
 *                    in the virtual memory address space of the current thread.
 *
 * @return Area which starts at address from or ::NULL when no such
 *         an area exists.
 *
 */
static vm_area_t *addr_space_find_area(vm_address_space_t *addr_space, const void *from, uint32_t *index)
{
	uint32_t area_count = addr_space->area_count;

	for (uint32_t i = 0; i < area_count; i++) {
		vm_area_t *area = &addr_space->areas[i];

		/* Have we found the searched area? */
		if (area->vma_start == from) {
			*index = i;
			return area;
		}
	}

	return NULL;
}

/** Place the passed area to the structure of allocated areas of the
 *  passed @p addr_space.
 *
 * The array of allocated areas of each address space is maintained sorted
 * according to the start address of those areas.
 *
 * @param addr_space  Address space to work with.
 * @param placed_area Area to be places to the right spot.
 *
 */
static void addr_space_ensort_area(vm_address_space_t *addr_space, vm_area_t placed_area)
{
	uint32_t area_count = addr_space->area_count;
	int area_index = 0;

	/* This cycle is not infinite, because the @p ensorted_area is already in the @p areas array. */
	while (addr_space->areas[area_index].vma_start < placed_area.vma_start) {
		area_index++;
	}

	for (int i = area_count - 1; i >= area_index; i--) {
		addr_space->areas[i + 1] = addr_space->areas[i];
	}

	addr_space->areas[area_index] = placed_area;
}

/** Checks whether the passed area @p size is ok (not zero and aligned to 4kB).
 *
 * @param  size Area size to get checked.
 *
 * @return Is the passed area size ok?
 *
 */
static bool vma_is_area_size_ok(const size_t size)
{
	return IS_ALLIGNED_TO_PAGE_SIZE(size) && size > 0;
}

/** Free all the allocated frames relevant to the passed @p vma_start address.
 *
 * Frames to be freed in area with passed @p vma_start address are specified
 * by the @p start_index and @p end_index arguments.
 *
 * All the relevant frames with second level paging tables are freed
 * if possible.
 *
 * @param addr_space  Affected address space.
 * @param vma_start   Start address of affected area.
 * @param start_index Start index of frames to get free.
 * @param end_index   End index of frames to get free.
 *
 */
static void vma_free_at_address(vm_address_space_t *addr_space, void *vma_start,
		uint32_t start_index, uint32_t end_index)
{
	/* Release all the frames from given area between specified indexes. */
	for (uint32_t i = start_index; i < end_index; i++) {
		void *virt_addr = vma_start + (i * PAGE_SIZE);
		void *paddr = addr_space_page_address(addr_space, virt_addr);

		if (frame_free(paddr, 1) != EOK) {
			panic("Could not free the frame previously allocated by VMA engine: %p.", paddr);
		}

		addr_space_set_translation(addr_space, virt_addr, NULL);
	}

	/* Check whether some of the page directory entries of affected area are completely empty. */
	uint32_t increment = 1 << ADDR_PAGE_BITS_COUNT;

	for (uint32_t i = start_index; i < end_index; i += increment) {
		void *virt_addr = vma_start + (i * PAGE_SIZE);
		uintptr_t *page_directory_entry = ADDR_SPACE_PAGE_DIRECTORY_ENTRY(addr_space->addr_translations, virt_addr);
		assert(*page_directory_entry != NULL);

		/* Loop through whole directory entry frame and check its contents. */
		unsigned char *pos = (unsigned char *)(*page_directory_entry);
		bool is_pde_frame_empty = true;

		for (int j = 0; j < FRAME_SIZE; j++) {
			if (*pos != NULL) {
				is_pde_frame_empty = false;
				break;
			}

			pos++;
		}

		/* Is the PDE frame empty? */
		if (is_pde_frame_empty) {
			if (frame_free(VA_DIRECT_TO_PHYSICAL(*page_directory_entry), 1) != EOK) {
				panic("Could not free the frame for page directory entry: %p.", *page_directory_entry);
			}

			/* Unset the free frame from the page directory. */
			*page_directory_entry = NULL;
		}
	}
}

/** Allocate the area in the virtual address space of the specified @p size
 *  in passed @p addr_space.
 *
 * It allocates the physical memory for this area (not necessarily coherent)
 * and creates the mapping of the created virtual memory to the physical memory.
 *
 * When ::VF_VA_AUTO is specified at @p flags parameter then the @p from address
 * is computed automatically and is returned in that pointer. When ::VF_VA_USER
 * is specified then the passed @p from address is used as the area start and
 * the vma_map function tries to create an area with this start and @p size.
 * When none of those is specified then ::EINVAL is returned and no area
 * is created.
 *
 * When ::VF_VA_AUTO is specified we can set at which segment we want to
 * allocate the virtual memory area. The possible (disjunctive) values are
 * ::VF_AT_KUSEG, ::VF_AT_KSEG0, ::VF_AT_KSEG1, ::VF_AT_KSSEG and ::VF_AT_KSEG3.
 *
 * Because the segments KSEG0 and KSEG1 are mapped to the same physical memory,
 * the allocation of the virtual memory which would overlap with some already
 * allocated segment in the physical memory is not allowed. For example when we
 * allocate an area at virtual address 0x80000000 (at KSEG0) of size 8kB
 * (which is mapped to the physical address 0x00000000) and then we try to
 * allocate area which starts at virtual address 0xA0001000 (at KSEG1, which
 * maps to the physical address 0x00001000) it is forbidden, because those
 * two areas would be overlapped in the physical memory.
 *
 * When ::VF_VA_FROM_END is specified then the search algorithm for empty space
 * for the new area loops from the end of the required segment (works only
 * with KUSER, KSSEG and KSEG3 and ::VF_VA_AUTO).
 *
 * The function returns ::EINVAL when the segment is not specified when
 * ::VF_VA_AUTO is specified. It also returns ::EINVAL when the @p size
 * parameter is not aligned to the 4kB (size of the physical page), when
 * ::VF_VA_USER is specified and the @p from parameter is not aligned to
 * the page size or when the area with the start at @p from can not be
 * allocated (it would overlap with some already allocated area or with
 * some segment border). It returns ::ENOMEM when there is not enough physical
 * memory for the operation or when there are already ::MAX_AREAS allocated
 * in the passed @p addr_space.
 *
 * @param  addr_space Address space in which we want to map the memory.
 * @param  from       An address where the area should start.
 * @param  size  	  The size of the newly created area (in bytes).
 * @param  flags 	  Flags for the created area.
 *
 * @return Result of the area creating: ::EOK when the new area is successfully
 *         created, ::EINVAL or ::ENOMEM otherwise.
 *
 */
int vma_map_in_addr_space(vm_address_space_t *addr_space, void **from, const size_t size, const vm_flags_t flags)
{
	mutex_t *mutex = &addr_space->mutex;

	mutex_lock(mutex);
	vma_debug_print(
		"-- VMA MAP: Thread %u mapping in address space %p; from %p, size: %u.\n",
		thread_get_current_id(), addr_space, *from, size
	);

	/* Are all areas already allocated? */
	if (addr_space->area_count == MAX_AREAS) {
		vma_debug_print("-- VMA MAP: MAX_AREAS reached.\n");
		mutex_unlock(mutex);
		return ENOMEM;
	}

	/* Area size must be aligned to 4kB. */
	if (!vma_is_area_size_ok(size)) {
		vma_debug_print("-- VMA MAP: Invalid area size.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	vm_flags_t frame_flags = VF_AT_KUSEG | VF_VA_AUTO;

	/* Get the start address automatically. */
	if (flags & VF_VA_AUTO) {
		bool from_end = (flags & VF_VA_FROM_END) ? true : false;

		if (flags & VF_AT_KUSEG) {
			*from = addr_space_find_start(addr_space, ADDR_KUSEG, ADDR_KSEG0, size, from_end);
		} else if (flags & VF_AT_KSEG0) {
			frame_flags = VF_AT_KSEG0 | VF_VA_AUTO;
		} else if (flags & VF_AT_KSEG1) {
			frame_flags = VF_AT_KSEG1 | VF_VA_AUTO;
		} else if (flags & VF_AT_KSSEG) {
			*from = addr_space_find_start(addr_space, ADDR_KSSEG, ADDR_KSEG3, size, from_end);
		} else if (flags & VF_AT_KSEG3) {
			*from = addr_space_find_start(addr_space, ADDR_KSEG3, ADDR_VM_END, size, from_end);
		/* Invalid segment. */
		} else {
			vma_debug_print("-- VMA MAP: Invalid segment.\n");
			mutex_unlock(mutex);
			return EINVAL;
		}

		/* Control the validity of the *from pointer - if it
		   does not overlap with the segment edge. */
		if (!((frame_flags & VF_AT_KSEG0) || (frame_flags & VF_AT_KSEG1))) {
			if (!vma_is_area_in_segment(*from, *from + size - 1)) {
				vma_debug_print("-- VMA MAP: Not enough memory for area allocation - overlaps with segment edge.\n");
				mutex_unlock(mutex);
				return ENOMEM;
			}
		}
	/* User has set the area start address manually. */
	} else if (flags & VF_VA_USER) {
		/* Start address must be aligned to 4kB. */
		if (!IS_ADDR_ALLIGNED_TO_PAGE_SIZE(*from)) {
			vma_debug_print("-- VMA MAP: From pointer is not aligned to page size.\n");
			mutex_unlock(mutex);
			return EINVAL;
		}

		/* Is start address in KSEG0 or KSEG1? */
		if (VA_ADDR_IS_IN_KSEG0(*from)) {
			frame_flags = VF_AT_KSEG0 | VF_VA_USER;
		} else if (VA_ADDR_IS_IN_KSEG1(*from)) {
			frame_flags = VF_AT_KSEG1 | VF_VA_USER;
		}

		/* Control the validity of *from pointer - if it
		   does not overlap with the segment edge or with
		   some already allocated area. We have to subtract
		   1 because of the possibility of arithmetic overflow
		   at the end virtual memory. */
		if (!vma_is_area_in_segment(*from, *from + size - 1) || addr_space_area_overlaps(addr_space, *from, *from + size)) {
			vma_debug_print("-- VMA MAP: From pointer is invalid.\n");
			mutex_unlock(mutex);
			return EINVAL;
		}
	/* Invalid flags. */
	} else {
		vma_debug_print("-- VMA MAP: Invalid flags.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	uint32_t page_count = size / PAGE_SIZE;
	uint32_t area_count = addr_space->area_count;
	vm_area_t *actual_area = &addr_space->areas[area_count];

	/* Initialize the area. */
	actual_area->size = size;

	/* Allocate the physical memory in KSEG0 or KSEG1 - we have to allocate
	   exactly page_count coherent blocks, because of the direct mapping. */
	if ((frame_flags & VF_AT_KSEG0) || (frame_flags & VF_AT_KSEG1)) {
		if ((flags & VF_VA_NOFRAME) && (flags & VF_VA_USER)) {
			/* We just create the mapping, we do not allocate any frames (because
			   there are no frames in the physical memory - memory has not been
			   found by memory discovery, because it is protected, for ex. because
			   there is user space binary image). */
			vma_debug_print("-- VMA MAP: No frames allocation in kernel.\n");
		} else {
			/* We could leave the flags as they are. When VF_VA_USER is set, than
			   we pass the virtual address recounted to the physical address in
			   paddr argument. When VF_VA_AUTO is set, then the paddr argument
			   is filled by frame_alloc function. */
			vma_debug_print("-- VMA MAP: Allocating frames in kernel.\n");
			void *paddr = VA_DIRECT_TO_PHYSICAL(*from);		/* Recount the virtual address to physical address. */
			int result = frame_alloc(&paddr, page_count, frame_flags);

			/* Control whether the memory allocation was successful. */
			if (result != EOK) {
				vma_debug_print("-- VMA MAP: Not enough memory for frames allocation.\n");
				mutex_unlock(mutex);
				return ENOMEM;
			}

			vma_debug_print("-- VMA MAP: Frames for %u pages allocated at physical address %p.\n", page_count, paddr);

			/* Count the area start. */
			if (frame_flags & VF_AT_KSEG0) {
				*from = PTR_ADDR_IN_KSEG0(paddr);
			} else {
				*from = PTR_ADDR_IN_KSEG1(paddr);
			}
		}
	/* Allocate the physical memory in some other segment than KSEG0 or KSEG1. */
	} else {
		vma_debug_print("-- VMA MAP: Allocating frames at user space.\n");

		for (uint32_t i = 0; i < page_count; i++) {
			void *paddr = NULL;
			int result = frame_alloc(&paddr, 1, frame_flags);

			/* Control whether the memory allocation was successful. */
			if (result != EOK) {
				vma_free_at_address(addr_space, *from, 0, i);
				mutex_unlock(mutex);
				return ENOMEM;
			}

			/* Get the directory entry in two-level paging. */
			void *actual_addr = (*from) + (i * PAGE_SIZE);
			uintptr_t *page_directory_entry = addr_space_get_page_directory_entry(addr_space, actual_addr);

			/* We did not manage to allocate a frame for directory entry. */
			if (*page_directory_entry == NULL) {
				/* We have to free the last allocated frame manually, vma_free_at_address will not do that. */
				if (frame_free(paddr, 1) != EOK) {
					panic("VMA MAP: Could not free the frame previously allocated by VMA engine: %p.", paddr);
				}

				vma_free_at_address(addr_space, *from, 0, i);
				mutex_unlock(mutex);
				return ENOMEM;
			}

			/* Save the va -> pa mapping. */
			addr_space_set_translation(addr_space, actual_addr, paddr);
		}

		vma_debug_print("-- VMA MAP: Frames for %u pages allocated.\n", page_count);
	}

	/* Save the area start. */
	actual_area->vma_start = *from;

	/* Mark the area as initialized. */
	addr_space_ensort_area(addr_space, *actual_area);
	addr_space->area_count++;

	/* Delete TLB entries associated with this mapping. This is to ensure that there
	   are no invalid entries in the TLB, where this function could make the previously
	   invalid pages valid. */
	asid_invalidate(addr_space);

	vma_debug_print("-- VMA MAP: Success.\n\n");
	mutex_unlock(mutex);

	return EOK;
}

/** Allocate the area in the virtual address space of the specified @p size
 *  in current address space.
 *
 * It allocates the physical memory for this area (not necessarily coherent)
 * and creates the mapping of the created virtual memory to the physical memory.
 *
 * When ::VF_VA_AUTO is specified at @p flags parameter then the @p from address
 * is computed automatically and is returned in that pointer. When ::VF_VA_USER
 * is specified then the passed @p from address is used as the area start and
 * the vma_map function tries to create an area with this start and @p size.
 * When none of those is specified then ::EINVAL is returned and no area
 * is created.
 *
 * When ::VF_VA_AUTO is specified we can set at which segment we want to
 * allocate the virtual memory area. The possible (disjunctive) values are
 * ::VF_AT_KUSEG, ::VF_AT_KSEG0, ::VF_AT_KSEG1, ::VF_AT_KSSEG and ::VF_AT_KSEG3.
 *
 * Because the segments KSEG0 and KSEG1 are mapped to the same physical memory,
 * the allocation of the virtual memory which would overlap with some already
 * allocated segment in the physical memory is not allowed. For example when we
 * allocate an area at virtual address 0x80000000 (at KSEG0) of size 8kB
 * (which is mapped to the physical address 0x00000000) and then we try to
 * allocate area which starts at virtual address 0xA0001000 (at KSEG1, which
 * maps to the physical address 0x00001000) it is forbidden, because those two
 * areas would be overlapped in the physical memory.
 *
 * When ::VF_VA_FROM_END is specified then the search algorithm for empty space
 * for the new area loops from the end of the required segment (works only
 * with KUSER, KSSEG and KSEG3 and ::VF_VA_AUTO).
 *
 * The function returns ::EINVAL when the segment is not specified when
 * ::VF_VA_AUTO is specified. It also returns ::EINVAL when the @p size
 * parameter is not aligned to the 4kB (size of the physical page), when
 * ::VF_VA_USER is specified and the @p from parameter is not aligned to the
 * page size or when the area with the start at @p from can not be allocated
 * (it would overlap with some already allocated area or with some segment
 * border). It returns ::ENOMEM when there is not enough physical memory
 * for the operation or when there are already ::MAX_AREAS allocated for
 * the current thread.
 *
 * @param  from  An address where the area should start.
 * @param  size  The size of the newly created area (in bytes).
 * @param  flags Flags for the created area.
 *
 * @return Result of the area creating: ::EOK when the new area is
 *         successfully created, ::EINVAL or ::ENOMEM otherwise.
 *
 */
int vma_map(void **from, const size_t size, const vm_flags_t flags)
{
	return vma_map_in_addr_space(addr_space_get_current_owned(), from, size, flags);
}

/** Expand or collapse the virtual memory area which starts at address
 *  @p from to the specified @p size.
 *
 * It returns ::EOK when the size of the area was successfully changed. It
 * returns ::EINVAL when no area with specified @p from as the start address
 * exists. It also returns ::EINVAL when the expansion would result in the
 * overlap with some other already existing area or with the segment border.
 * ::EINVAL is also returned when the @p size parameter is not aligned to the
 * physical page size (4kB). It returns ::ENOMEM when the area expansion was
 * not possible because there is not enough physical memory.
 *
 * @param  from Start address of the expanded/collapsed area.
 * @param  size New size of the area.
 *
 * @return ::EOK, ::EINVAL or ::ENOMEM.
 *
 */
int vma_resize(const void *from, const size_t size)
{
	vm_address_space_t *current_addr_space = addr_space_get_current_owned();
	mutex_t *mutex = &current_addr_space->mutex;

	mutex_lock(mutex);
	vma_debug_print(
		"-- VMA RESIZE: Thread %u resizing in address space %p; from %p, size: %u.\n",
		thread_get_current_id(), current_addr_space, from, size
	);

	uint32_t area_index;
	vm_area_t *area = addr_space_find_area(current_addr_space, from, &area_index);

	/* Does area exist? */
	if (area == NULL) {
		vma_debug_print("-- VMA RESIZE: Area not found.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	/* Area size must be aligned to 4kB. */
	if (!vma_is_area_size_ok(size)) {
		vma_debug_print("-- VMA RESIZE: Area size is invalid.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	/* Control the validity of *from pointer - if it
	   does not overlap with the segment edge or with
	   some already allocated area. */
	if (!vma_is_area_in_segment(area->vma_start, area->vma_start + size - 1)
			|| addr_space_area_overlaps_with_ignore(current_addr_space, area->vma_start, area->vma_start + size, area)
	) {
		vma_debug_print("-- VMA RESIZE: Area would overlap with another one.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	/* Resize the area. */
	if (area->size != size) {
		uint32_t pages_count = area->size / PAGE_SIZE;
		uint32_t new_pages_count = size / PAGE_SIZE;

		/* The new size is smaller => we have to deallocate the pages
		   that are beneath the new size. */
		if (size < area->size) {
			/* Delete the contents of unmapped block for better error recognition. */
#if DEBUG_VMA >= 1
			if (thread_get_current_impl()->owned_addr_space == current_addr_space) {
				memory_fill_with_null(area->vma_start + size, area->size - size);
			}
#endif
			/* Is the area in KSEG0 or KSEG1? */
			if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area->vma_start)) {
				/* Free the relevant frames. */
				void *paddr = VA_DIRECT_TO_PHYSICAL(area->vma_start) + size;

				if (frame_free(paddr, pages_count - new_pages_count) != EOK) {
					panic("Could not free the frame previously allocated by VMA engine when resizing: %p.", paddr);
				}

				vma_debug_print("-- VMA RESIZE: Shrinked at kernel.\n");
			} else {
				/* Deallocate the pages in user segment. */
				vma_free_at_address(current_addr_space, area->vma_start, new_pages_count, pages_count);
				vma_debug_print("-- VMA RESIZE: Shrinked at user space.\n");
			}
		/* The new size is greater than the old size => we have to allocate new pages. */
		} else {
			size_t alloc_frames = new_pages_count - pages_count;

			/* Is the area in KSEG0 or KSEG1? */
			if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area->vma_start)) {
				void *paddr = VA_DIRECT_TO_PHYSICAL(area->vma_start) + area->size;
				int result = frame_alloc(&paddr, alloc_frames, VF_VA_USER);

				if (result == ENOMEM) {
					vma_debug_print("-- VMA RESIZE: Could not allocate new frames at kernel.\n");
					mutex_unlock(mutex);
					return ENOMEM;
				}

				assert(result == EOK);
				vma_debug_print("-- VMA RESIZE: Stretched at kernel, allocated %u new frames.\n", alloc_frames);
			} else {
				for (uint32_t i = pages_count; i < new_pages_count; i++) {
					void *paddr = NULL;
					int result = frame_alloc(&paddr, 1, VF_AT_KUSEG | VF_VA_AUTO);

					/* Control whether the memory allocation was successful. */
					if (result != EOK) {
						vma_free_at_address(current_addr_space, area->vma_start, pages_count, i);
						mutex_unlock(mutex);
						return ENOMEM;
					}

					/* Get the directory entry in two-level paging. */
					void *actual_addr = area->vma_start + (i * PAGE_SIZE);
					uintptr_t *page_directory_entry = addr_space_get_page_directory_entry(current_addr_space, actual_addr);

					/* We did not manage to allocate a frame for directory entry. */
					if (*page_directory_entry == NULL) {
						/* We have to free the last allocated frame manually, vma_free_at_address will not do that. */
						if (frame_free(paddr, 1) != EOK) {
							panic("VMA RESIZE: Could not free the frame previously allocated by VMA engine: %p.", paddr);
						}

						vma_free_at_address(current_addr_space, area->vma_start, pages_count, i);
						mutex_unlock(mutex);
						return ENOMEM;
					}

					/* Save the va -> pa mapping. */
					addr_space_set_translation(current_addr_space, actual_addr, paddr);
				}

				vma_debug_print("-- VMA RESIZE: Stretched at user space, allocated %u new frames.\n", alloc_frames);
			}
		}

		area->size = size;
	} else {
		vma_debug_print("-- VMA RESIZE: No resize necessary.\n");
	}

	/* Delete the relevant entries from TLB. */
	asid_invalidate(current_addr_space);

	vma_debug_print("-- VMA RESIZE: Success.\n\n");
	mutex_unlock(mutex);

	return EOK;
}

/** Void the mapping of the virtual memory to the physical memory
 *  in the area specified by its start address (parameter @p from).
 *
 * This area had to be previously created by the ::vma_map call.
 *
 * It also deallocates the physical memory where the area is mapped to
 * and deallocates the area itself.
 *
 * It returns ::EOK when the virtual memory area was successfully
 * deallocated. It returns ::EINVAL when no area with start address @p from
 * exists in the passed @p addr_space.
 *
 * This method also deletes the entries attached to the area from the TLB.
 *
 * @param  addr_space Address space in which we want to unmap the memory.
 * @param  from       Start address of the deallocated area.
 *
 * @return ::EOK or ::EINVAL.
 *
 */
int vma_unmap_in_addr_space(vm_address_space_t *addr_space, const void *from)
{
	mutex_t *mutex = &addr_space->mutex;
	mutex_lock(mutex);
	vma_debug_print(
		"-- VMA UNMAP: Thread %u unmapping in address space %p; from %p.\n",
		thread_get_current_id(), addr_space, from
	);

	uint32_t area_index;
	vm_area_t *area = addr_space_find_area(addr_space, from, &area_index);

	/* Does area exist? */
	if (area == NULL) {
		vma_debug_print("-- VMA UNMAP: Area not found.\n");
		mutex_unlock(mutex);
		return EINVAL;
	}

	uint32_t pages_count = area->size / PAGE_SIZE;

	/* Delete the contents of unmapped block for better error recognition. */
#if DEBUG_VMA >= 1
	if (thread_get_current_impl()->owned_addr_space == addr_space) {
		memory_fill_with_null(area->vma_start, area->size);
	}
#endif

	/* Is the area in KSEG0 or KSEG1? */
	if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area->vma_start)) {
		void *paddr = VA_DIRECT_TO_PHYSICAL(area->vma_start);

		if (frame_free(paddr, pages_count) != EOK) {
			panic("Could not free the frame previously allocated by VMA engine when unmapping: %p.", paddr);
		}

		vma_debug_print("-- VMA UNMAP: Unmapped in kernel.\n");
	} else {
		/* Deallocate the pages in user segment. */
		vma_free_at_address(addr_space, area->vma_start, 0, pages_count);
		vma_debug_print("-- VMA UNMAP: Unmapped in user space.\n");
	}

	uint32_t area_count = addr_space->area_count;

	/* Make sure there are some areas. */
	assert(area_count > 0);

	/* Move all the areas after the deallocated area. */
	for (uint32_t i = area_index; i < area_count - 1; i++) {
		if (i < MAX_AREAS - 1) {
			addr_space->areas[i] = addr_space->areas[i + 1];
		}
	}

	/* Re-initialize the last area in the list. */
	vma_init(&addr_space->areas[area_count - 1]);

	/* Decrease the total number of thread virtual memory areas. */
	addr_space->area_count--;

	/* Delete the relevant entries from TLB. */
	asid_invalidate(addr_space);

	vma_debug_print("-- VMA UNMAP: Success.\n\n");
	mutex_unlock(mutex);

	return EOK;
}

/** Voids the mapping of the virtual memory to the physical memory
 *  in the area specified by its start address (parameter @p from).
 *
 * This area had to be previously created by the vma_map call.
 *
 * It also deallocates the physical memory where the area is mapped to
 * and deallocates the area itself.
 *
 * It returns ::EOK when the virtual memory area was successfully
 * deallocated. It returns ::EINVAL when no area with start address @p from
 * exists in the current thread virtual memory map.
 *
 * This method also deletes the entries attached to the area from the TLB.
 *
 * @param  from               Start address of the deallocated area.
 *
 * @return ::EOK or ::EINVAL.
 *
 */
int vma_unmap(const void *from)
{
	return vma_unmap_in_addr_space(addr_space_get_current_owned(), from);
}

/** Print out the passed address space.
 *
 * @param addr_space Address space to be printed out.
 *
 */
void addr_space_print(vm_address_space_t *addr_space)
{
	/* We do not want any interrupts when printing out the address space. */
	size_t total_size = 0;
	mutex_t *mutex = &addr_space->mutex;

	mutex_lock(mutex);

	printk("\n-------------------- PRINTING THE ADDRESS SPACE OF THREAD %u -----------------\n", thread_get_current_id());

	/* Lets loop through all the allocated areas. */
	for (uint32_t i = 0; i < addr_space->area_count; i++) {
		vm_area_t *area = &addr_space->areas[i];
		total_size += area->size;

		printk("---- ENTERING AREA: at %p, size: %u bytes ----\n", area->vma_start, area->size);

		if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(area->vma_start)) {
			printk("No translations - directly mapped area...\n");
		} else {
			uint32_t page_count = area->size / PAGE_SIZE;

			for (uint32_t j = 0; j < page_count; j++) {
				void *virt_addr = area->vma_start + j * PAGE_SIZE;
				printk("Translation %u: %p --> %p\n", j, virt_addr, addr_space_translate(addr_space, virt_addr));
			}
		}

		printk("---- LEAVING AREA %p ----\n\n", area->vma_start);
	}

	printk("Total areas count: %u\n", addr_space->area_count);
	printk("Total size: %u bytes\n", total_size);
	printk("Addr. space reference count: %u\n", atomic_get(&addr_space->reference_count));
	printk("Addr. space pointer: %p\n", addr_space);
	printk("-------------------- ADDRESS SPACE PRINTED ----------------------------------\n\n");

	mutex_unlock(mutex);
}

/** Print out the owned address space of the currently running thread.
 *
 */
void addr_space_print_current(void)
{
	addr_space_print(addr_space_get_current_owned());
}

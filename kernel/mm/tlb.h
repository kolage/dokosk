/**
 * @file tlb.h
 *
 * Declarations of @ref tlb_feature.
 *
 * OS
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 * Modifications 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef TLB_H_
#define TLB_H_

#include <include/c.h>


/* Externals are commented with implementation. */
extern void init_tlb(void);
extern void wrapped_tlbrefill(context_t *registers);
extern void tlb_invalid_access(context_t *registers);
extern void tlb_invalidate_asid(uint8_t asid);
extern void tlb_empty(void);
extern void tlb_nonthread_insert_temp_entry(uintptr_t page_addr, uintptr_t frame_addr);

#endif /* TLB_H_ */

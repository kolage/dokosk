/**
 * @file kernel/mm/memory.c
 *
 * Automatic incoherent memory discovery.
 *
 * Memory discovery is started by calling the function ::memory_discover
 * function. It finds all the physical memory blocks that are writable.
 * Memory blocks can be easily looped through the provided interface of
 * iterators (without the knowledge of internal data structures).
 *
 * This file implements the @ref memory_feature.
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */
/**
 * @page memory_feature Memory discovery
 *
 * This page describes how physical memory is discovered.
 *
 * One of the first things that kernel has to do is to determine how
 * much memory it can use. This task is handled by ::memory_discover
 * function which loops through the whole physical address space and
 * repeatedly writes a prespecified constant to the memory and tries
 * to read it back. If this couple of operations successes, we know
 * that there is probably a memory at tested address.
 *
 * We loop through the address space with jump of size
 * ::DISCOVER_MEMORY_FAST_JUMP. When the memory starts or stops to
 * respond, jump is decreased to ::DISCOVER_MEMORY_SLOWER_JUMP and
 * finally to ::MEMORY_ALIGNMENT, so we can discover memory blocks
 * very precisely in a relatively little time.
 *
 * All the found memory blocks are then stored in the main memory just
 * after the kernel binary image in the ::memory structure. Those
 * memory blocks can be looped with an iterator ::memory_it which
 * is used by frame allocator. An iterator can be initialized in three
 * different modes defined in ::memory_it_type_t enumeration.
 *
 * It is very important to jump over some predefined blocks of physical
 * memory, because devices, kernel binary image or user space binary image
 * are place there. We discover memory between the ::_kernel_end address
 * and ::DEVICES_ADDR. Then the block between ::DEVICES_ADDR and
 * ::DEVICES_END_ADDR is jumped over (it contains mapped devices and user
 * space binary image) and memory is then discovered between ::DEVICES_END_ADDR
 * and ::LOADER_BINARY_ADDR addresses. The last block of memory between physical
 * addresses ::PHYS_KERNEL_END_ADDR and ::PHYS_MEM_END_ADDR is discovered
 * with TLB. We naturally support incoherent memory.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <drivers/printer.h>
#include <mm/memory.h>
#include <mm/falloc.h>
#include <mm/tlb.h>


/** The jump with lower resolution, when we discover memory blocks. */
#define DISCOVER_MEMORY_FAST_JUMP		32 * FRAME_SIZE		/* 128 kB */

/** The jump with higher resolution, when we discover memory blocks. */
#define DISCOVER_MEMORY_SLOWER_JUMP		1024				/* 1 kB */

/** We support at max 128 memory blocks. It is more than enough, because
 *  every memory block corresponds to one physical memory module.
 *  The discovery algorithms could work without this limitation, but
 *  it is also good for error discovery and debugging.
 */
#define MAX_BLOCKS_COUNT				128

/** We use this static virtual address for accessing the physical
 *  addresses beyond the kernel address space. Ksseg is mapped through TLB.
 */
#define DISCOVERY_VIRT_ADDR				ADDR_KSSEG


/** Map of the physical memory. */
static memory_t *memory_map;


/** Zero the memory area when it is marked as unused.
 *
 * @param start Address where to start formating.
 * @param size  Size of the formated area.
 *
 */
void memory_fill_with_null(void *start, size_t size)
{
	unsigned char *pos = (unsigned char *)start;

	while (size-- > 0) {
		*pos = 0;
		pos++;
	}
}

/** Find the address where the info about memory block with passed
 *  index should be.
 *
 * @param  index Index of the memory block.
 *
 * @return Address where the info about memory block should be
 *         stored in the memory.
 *
 */
memory_block_t *memory_find_block(native_t index)
{
	memory_block_t *first_block = (memory_block_t *)(memory_map + sizeof(memory_t));
	memory_block_t *block_addr = first_block + index * sizeof(memory_block_t);

	return block_addr;
}

/** Save the info about passed memory block definition.
 *
 * The memory block definition is saved to the collection of discovered
 * memory blocks. It finds the address where the info should be stored
 * and saves it there.
 *
 * @param start Start address of discovered physical memory block.
 * @param end   End address of discovered physical memory block.
 *
 */
static void memory_save_block_info(void *start, void *end)
{
	native_t block_index = memory_map->kernel_block_count + memory_map->user_block_count;

	if (block_index < MAX_BLOCKS_COUNT) {
		memory_block_t *block_info = memory_find_block(block_index);

		block_info->start = start;
		block_info->end = end;

		/* Check that the block is not corrupter. */
		assert(start < end);

		/* Check that the block does not overlap the bound between
		   kernel and user space. */
		assert(end < PHYS_KERNEL_END_ADDR || start >= PHYS_KERNEL_END_ADDR);

		if (end < PHYS_KERNEL_END_ADDR) {
			memory_map->kernel_block_count++;
		} else {
			memory_map->user_block_count++;
		}
	} else {
		printk_nb("    -- Warning: Max number of physical memory blocks reached! Block %p-%p ignored...\n", start, end);
	}
}

/** Translate the physical address to the virtual.
 *
 * Addresses in the kernel are translated directly and addresses in the
 * user space are translated to one predefined virtual address which is mapped
 * to the passed physical address through TLB.
 *
 * @param  paddr Physical address to be translated.
 *
 * @return Virtual address which is surely translated to the passed
 *         physical address.
 *
 */
static void *memory_translate_phys_addr(const void *paddr)
{
	if (paddr < PHYS_KERNEL_END_ADDR) {
		return PTR_ADDR_IN_KSEG0(paddr);
	} else {
		/* Map the tested frame in TLB to the predefined page. */
		uintptr_t page_addr = (uintptr_t)DISCOVERY_VIRT_ADDR;
		uintptr_t frame_addr = ALIGN_DOWN((uintptr_t)paddr, FRAME_SIZE);
		tlb_nonthread_insert_temp_entry(page_addr, frame_addr);

		return DISCOVERY_VIRT_ADDR + ((uintptr_t)paddr - frame_addr);
	}
}

/** Discover all the physical memory blocks within the passed bounds.
 *
 * @param start_mantinel Start bound for memory discovery.
 * @param end_mantinel   End bound for memory discovery.
 *
 */
static void memory_discover_within_bounds(const void *start_mantinel, const void *end_mantinel)
{
	void *start = (void *)ALIGN_UP((uintptr_t)start_mantinel, MEMORY_ALIGNMENT);

	while (true) {
		/* We try to find the memory really fast - 128 kB jumps. */
		void *end = memory_find_end(start, end_mantinel, DISCOVER_MEMORY_FAST_JUMP);

		/* Have we discovered some block? */
		if (end > start) {
			/* Now try to find the real end with higher resolution searching. */
			end = memory_find_end(end, end_mantinel, DISCOVER_MEMORY_SLOWER_JUMP);
			end = memory_find_end(end, end_mantinel, MEMORY_ALIGNMENT);

			/* Memory did not respond on the end address + MEMORY_ALIGNMENT, therefore end
			   address did respond, so we can increase the size of the block by 4 bytes.
			   We should increase the total size by one frame when the memory block is
			   already frame aligned. */
			end += MEMORY_ALIGNMENT;

			/* Save the info about the found memory block. */
			memory_save_block_info(start, end);
		}

		/* We have not discovered the block at address start -> we try to
		   find some other block start starting at the end of the previous block. */
		start = memory_find_start(end, end_mantinel, DISCOVER_MEMORY_FAST_JUMP);

		/* Have we found some new memory block? */
		if (start >= end) {
			/* Now try to find the real start with higher resolution searching. */
			start = memory_find_start(start, end_mantinel, DISCOVER_MEMORY_SLOWER_JUMP);
			start = memory_find_start(start, end_mantinel, MEMORY_ALIGNMENT);

			/* Memory firstly responded on the start address + MEMORY_ALIGNMENT (find_memory_start
			   returns the address decreased by the search step), therefore we have to increase
			   the start address by the MEMORY_ALIGNMENT. */
			start += MEMORY_ALIGNMENT;
		} else {
			/* There is no other memory block -> finish the searching. */
			break;
		}
	}
}

/** Initialize the iterator for the memory blocks iteration.
 *
 * There are three possible types of iterators: ::PHYS_MEM_IT_KERNEL
 * which iterates only through memory blocks in the kernel,
 * ::PHYS_MEM_IT_USER which which iterates only through memory blocks
 * in the user space and ::PHYS_MEM_IT_ALL which iterates only through
 * both of those types of memory blocks.
 *
 * @param it   Memory blocks iterator.
 * @param type Type of the iterator.
 *
 */
void memory_it_init(memory_it_t *it, memory_it_type_t type)
{
	switch (type) {
		case PHYS_MEM_IT_KERNEL:
			it->current = 0;
			it->last = memory_map->kernel_block_count;
			break;

		case PHYS_MEM_IT_USER:
			it->current = memory_map->kernel_block_count;
			it->last = memory_map->kernel_block_count + memory_map->user_block_count;
			break;

		case PHYS_MEM_IT_ALL:
			it->current = 0;
			it->last = memory_map->kernel_block_count + memory_map->user_block_count;
			break;

		default:
			panic("Invalid type for physical memory iterator! Passed type: %u", type);
	}
}

/** Get the current memory block in the iteration.
 *
 * @param  it Memory blocks iterator.
 *
 * @return Current memory block in the iteration.
 *
 */
memory_block_t *memory_it_block(memory_it_t *it)
{
	return memory_find_block(it->current);
}

/** Get the string representing memory units of passed memory size.
 *
 * @param  memory_size Size of the memory block.
 *
 * @return String representing memory units of passed memory size.
 *
 */
char *memory_size_units(size_t *memory_size)
{
	char *units = "B";

	if (*memory_size >= 1024) {
		*memory_size /= 1024;
		units = "kB";
	}

	if (*memory_size >= 1024) {
		*memory_size /= 1024;
		units = "MB";
	}

	return units;
}

/** Discover all the available physical memory.
 *
 * Information about discovered memory is stored in internal data structures.
 *
 * All the discovered memory blocks can be looped through the
 * ::memory_it interface.
 *
 */
void memory_discover(void)
{
	/* The memory map structure is right after the kernel code and static data. */
	memory_map = (memory_t *)ALIGN_UP((uintptr_t)&_kernel_end, MEMORY_ALIGNMENT);
	memory_map->kernel_block_count = 0;
	memory_map->user_block_count = 0;

	/* Count the start address where we start the memory discovery - we reserve
	   the space for the memory blocks data structures - it is ~1kB of memory.
	   We assume that there is some more memory right after the kernel end
	   and we have to control that by assert. */
	void *start = (void *)memory_map + sizeof(memory_t);

	/* Lets find the memory in the KSEG0 (resp. also KSEG1,
	   because they are mapped to the same physical memory.
	   We start at the end address of the kernel static data
	   and source code. We do not need TLB here. */
	memory_discover_within_bounds(VA_DIRECT_TO_PHYSICAL(start), DEVICES_ADDR);
	memory_discover_within_bounds(DEVICES_END_ADDR, LOADER_BINARY_ADDR);

	/* Discover user space memory - with TLB. */
	memory_discover_within_bounds(PHYS_KERNEL_END_ADDR, PHYS_MEM_END_ADDR);

	/* Make sure we have found some memory blocks for kernel. */
	assert(memory_map->kernel_block_count > 0);

	/* Make sure there is enough memory right after the kernel end. */
	size_t total_blocks_count = memory_map->kernel_block_count + memory_map->user_block_count;
	memory_block_t *first_block = memory_find_block(0);
	void *memory_map_structures_end_address = memory_find_block(total_blocks_count);

	assert(first_block->start == VA_DIRECT_TO_PHYSICAL(start));
	assert(first_block->end > VA_DIRECT_TO_PHYSICAL(memory_map_structures_end_address));

	/* Set the start address of the first block to the end address of the memory map structures. */
	first_block->start = VA_DIRECT_TO_PHYSICAL(memory_map_structures_end_address);

	/* Write out info about discovered memory blocks and align blocks to the frame size. */
	memory_it_t it;

	for (memory_it_init(&it, PHYS_MEM_IT_ALL); memory_it_has_current(&it); memory_it_next(&it)) {
		memory_block_t *block_info = memory_it_block(&it);

		/* Align the block to the frame size. */
		block_info->start = (void *)ALIGN_UP((uintptr_t)block_info->start, FRAME_SIZE);
		block_info->end = (void *)ALIGN_DOWN((uintptr_t)block_info->end, FRAME_SIZE);

		/* Make sure that the block is not empty. */
		size_t size = memory_get_block_size(block_info);
		assert(block_info->start < block_info->end);
		assert(size >= FRAME_SIZE);

		/* Write out info about block. */
		size_t size_orig = size;
		char *units = memory_size_units(&size);
		printk_nb("-- Discovered physical memory at %p-%p; total: ~%u%s (%u bytes).\n", block_info->start, block_info->end, size, units, size_orig);
	}
}

/** Find a start of the next physical memory block.
 *
 * To find the start of a physical memory block, we simply try
 * modifying memory at addresses beyond the passed start address.
 * When our modifications start being accepted, we
 * consider that as indication of reaching the start of
 * the next physical memory block. When the end_address is reached,
 * the ::NULL is returned.
 *
 * @param  start       The address where we start searching.
 * @param  end_address Maximal address where we stop searching.
 * @param  step        Step for looping through the memory.
 *
 * @return Start address of the next memory block.
 *
 */
void *memory_find_start(void *start, const void *end_address, const unsigned int step)
{
	void *pos = start;

	while (true) {
		/* We have gone almost behind the upper bound for the discovered memory block. */
		if ((uintptr_t)end_address - (uintptr_t)pos <= step) {
			return NULL;
		}

		/* Try write two values, if both are
		   read back, we have found the start of the new block. */
		volatile uint8_t *pos_wr = (uint8_t *)memory_translate_phys_addr(pos);
		(*pos_wr) = 0x55;

		if ((*pos_wr) == 0x55) {
			(*pos_wr) = 0xAA;

			if ((*pos_wr) == 0xAA) {
				break;
			}
		}

		/* The increment is chosen for
		   this to be reasonably fast. */
		pos += step;
	}

	void *result = pos - step;

	if (result < start) {
		result = start;
	}

	return result;
}

/** Find an end of the physical memory block.
 *
 * To find the end of a physical memory block, we simply try
 * modifying memory at addresses beyond the passed start address.
 * When our modifications stop being accepted, we
 * consider that as indication of reaching the end of
 * the physical memory block.
 *
 * @param  start       The starting address of the memory block.
 * @param  end_address Maximal end address of the memory block.
 * @param  step        Step for looping through the memory.
 *
 * @return End address of the memory block.
 *
 */
void *memory_find_end(void *start, const void *end_address, const unsigned int step)
{
	void *pos = start;

	while (true) {
		/* We have gone almost behind the upper bound for the discovered memory block. */
		if ((uintptr_t)end_address - (uintptr_t)pos <= step) {
			pos = (void *)end_address;
			break;
		}

		/* Try write two values, if either is
		   not read back, we have reached the end. */
		volatile uint8_t *pos_wr = (uint8_t *)memory_translate_phys_addr(pos);

		(*pos_wr) = 0x55;

		if ((*pos_wr) != 0x55) {
			break;
		}

		(*pos_wr) = 0xAA;

		if ((*pos_wr) != 0xAA) {
			break;
		}

		/* The increment is chosen for
		   this to be reasonably fast. */
		pos += step;
	}

	void *result = pos - step;

	if (result < start) {
		result = start;
	}

	return result;
}

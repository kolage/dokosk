/**
 * @file kernel/mm/tlb.c
 *
 * Implementation of @ref tlb_feature.
 *
 * OS
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 * Modifications 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page tlb_feature TLB address translation
 *
 * This page describes how TLB is used to acces virtual memory.
 *
 * The MIPS R4000 microprocessor provides three exceptions related to TLB.
 * Only one of them - the TLB Refill exception - is used, the other indicate
 * error and result in killing the thread that caused them.
 *
 * The TLB Refill exception is handled by looking up the address (@ref vma_feature).
 * If the mapping exists, it is inserted into the TLB, otherwise it is an error
 * and the thread is killed.
 *
 * Each entry in the TLB has an associated ASID, which is temporarily assigned
 * to ::vm_address_space_t::asid by LRU algorithm (::asid_lru_used, ::asid_lru_unused
 * and ::asid_assign) when switching to the thread that uses the address space.
 *
 * TLB function support removing all TLB entries or entries with specified ASID,
 * which is useful when the ASID is assigned to a different ::vm_address_space_t or
 * when the address space is changed and the entries might become outdated.
 *
 * There is also special function to allow mapping one page before threading is started.
 * This is used by memory discovery. Because ::ASID_INVALID is never used after threading is started,
 * it can be used at this stage.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <drivers/printer.h>
#include <exc/exc.h>
#include <mm/tlb.h>
#include <mm/asid.h>


/** Convert page pointer to page number. */
#define PAGE_PTR_TO_NUM(ptr) 	((uintptr_t)(ptr)>>PAGE_SHIFT)

/** Convert frame pointer to frame number. */
#define FRAME_PTR_TO_NUM(ptr) 	((uintptr_t)(ptr)>>PAGE_SHIFT)

/** Convert page number to page pointer. */
#define PAGE_NUM_TO_PTR(num) 	((void*)((num)<<PAGE_SHIFT))

/** Create lower part of an invalid TLB entry. */
#define TLB_ENTRYLO_INVALID 	0

/** Create higher part of an invalid TLB entry. */
#define TLB_ENTRYHI_INVALID 	ASID_INVALID

/** Create lower part of a valid and dirty TLB entry for a frame. */
#define TLB_ENTRYLO_VALID_DIRTY(frame) \
	((frame) << CP0_ENTRYLO_PFN_SHIFT) \
	| CP0_ENTRYLO_D_MASK | CP0_ENTRYLO_V_MASK


/** Write invalid entry to the entry registers (not to the TLB).
 *
 */
static void tlb_write_invalid_entry(void)
{
	/* Write invalid values to the entry registers. */
	write_cp0_entryhi(TLB_ENTRYHI_INVALID);
	write_cp0_entrylo0(TLB_ENTRYLO_INVALID);
	write_cp0_entrylo1(TLB_ENTRYLO_INVALID);
}

/** Initialize address translation.
 *
 * All TLB entries are cleared and invalidated, new entries will be
 * created on demand by the TLB Refill Exception handler.
 *
 * This function can be called only in kernel initialization.
 *
 */
void init_tlb(void)
{
	/* The Wired register contains the number of entries that
	   are never selected by the random TLB replacement
	   algorithm. We need none of those now. */
	write_cp0_wired(0);

	/* Each TLB entry is set by storing its fields
	   in special registers and then copying the
	   registers to the entry by the TLBWI
	   instruction. */

	/* The size of the page to map. */
	write_cp0_pagemask(CP0_PAGEMASK_4K);

	tlb_write_invalid_entry();

	/* Copy this to all TLB entries. */
	for (unsigned int i = 0; i < CP0_INDEX_INDEX_COUNT; i++) {
		write_cp0_index(i);
		TLBWI();
	}

	/* Clear the ERL bit so that the TLB is used */
	write_cp0_status(0);
}

/** Remove all entries with the specified ASID from the TLB.
 *
 * This can be used to reuse the ASID for a different memory table.
 *
 * Interrupts must be disabled when calling this function.
 *
 * @param asid The ASID that should be removed from the TLB.
 *
 */
void tlb_invalidate_asid(uint8_t asid)
{
	/* Store the current asid because TLBR destroys it. */
	uint8_t current_asid = read_cp0_entryhi();

	write_cp0_pagemask(CP0_PAGEMASK_4K);

	for (unsigned i=0; i < CP0_INDEX_INDEX_COUNT; ++i){
		/* Load the content of the entry. */
		write_cp0_index(i);
		TLBR();
		uint32_t entry = read_cp0_entryhi();

		/* Look for entries with the ASID. */
		if (CP0_ENTRYHI_ASID(entry) == asid) {
			/* Invalidate the entry. */
			tlb_write_invalid_entry();
			TLBWI();
		}
	}

	/* Restore the ASID. */
	write_cp0_entryhi(current_asid);
}

/** Remove all the entries from the TLB.
 *
 */
void tlb_empty(void)
{
	/* Store the current asid because TLBR destroys it */
	uint8_t current_asid = read_cp0_entryhi();

	write_cp0_pagemask(CP0_PAGEMASK_4K);

	for (unsigned i=0; i < CP0_INDEX_INDEX_COUNT; ++i) {
		write_cp0_index(i);
		/* Invalidate the entry */
		tlb_write_invalid_entry();
		TLBWI();
	}

	/* Restore the asid*/
	write_cp0_entryhi(current_asid);
}

/** Translate page pointer to a TLB entry part.
 *
 * If the page is not mapped, it results in an invalid entry part.
 *
 * @param  page_num Page number of the page that should be mapped.
 *
 * @return Lower part of the TLB entry that should be used in the TLB entry
 *         for the page.
 *
 */
static uintptr_t tlb_page_num_to_entry(uintptr_t page_num)
{
	void* frame_addr = addr_space_page_address(addr_space_get_current_used(), PAGE_NUM_TO_PTR(page_num));

	if (frame_addr == NULL) {
		return TLB_ENTRYLO_INVALID;
	} else {
		return TLB_ENTRYLO_VALID_DIRTY(FRAME_PTR_TO_NUM(frame_addr));
	}
}


/** Insert an entry to the TLB for use in kernel initializations.
 *
 * This function must not be called after threading is started.
 * The virtual address is valid until this function is called again or
 * threading is started.
 *
 * @param page_addr  Address (pointer) of the page that should be
 *                   temporarily mapped.
 * @param frame_addr Address (pointer) of the frame that should be
 *                   temporarily mapped.
 *
 */
void tlb_nonthread_insert_temp_entry(uintptr_t page_addr, uintptr_t frame_addr)
{
	uintptr_t page_num = PAGE_PTR_TO_NUM(page_addr);
	uintptr_t entry_even = TLB_ENTRYLO_INVALID;
	uintptr_t entry_odd = TLB_ENTRYLO_INVALID;

	if (page_num & 1) {
		entry_odd = TLB_ENTRYLO_VALID_DIRTY(FRAME_PTR_TO_NUM(frame_addr));
	} else {
		entry_even = TLB_ENTRYLO_VALID_DIRTY(FRAME_PTR_TO_NUM(frame_addr));
	}

	unative_t entryhi=0;
	entryhi = ASID_INVALID | (page_addr & CP0_ENTRYHI_VPN2_MASK);

	ipl_t state = query_and_disable_interrupts();

	/* Always use the first entry of the TLB. */
	write_cp0_index(0);
	write_cp0_entryhi(entryhi);
	write_cp0_entrylo0(entry_even);
	write_cp0_entrylo1(entry_odd);
	TLBWI();

	conditionally_enable_interrupts(state);
}

/** Handle TLB Invalid Exception.
 *
 * If an invalid entry in the TLB is accessed, it means that the page
 * is not mapped (when mapping new pages, old entries are removed).
 * That means we need to kill the thread.
 *
 * @param registers Context of the interrupted thread.
 *
 */
void tlb_invalid_access(context_t *registers)
{
	panic_or_kill(registers, "Unmapped page access - TLB invalid exception: %p", registers->badva);
}

/** Handle TLB Refill Exception.
 *
 * Executed where no corresponding entry in TLB is found.
 * This function assumes that CP0 EntryHi is correctly filled.
 * The function looks up the addresses of two pages in the current address space
 * and if the one that is accessed is valid, adds an entry into the TLB.
 * Otherwise, the access is invalid and the current thread is killed.
 *
 * This function must be called with disabled interrupts and stack pointer
 * in kernel static area.
 *
 * @param registers Context of the interrupted thread.
 *
 */
void wrapped_tlbrefill(context_t *registers)
{
	/* Get the needed page number. */
	uintptr_t page_num = PAGE_PTR_TO_NUM(registers->badva);

	/* Translate the page numbers to TLB entry parts. */
	uintptr_t entry_even = tlb_page_num_to_entry(page_num & ~(uintptr_t)1);
	uintptr_t entry_odd = tlb_page_num_to_entry(page_num | 1);

	/* Test the entry part that is actually needed. */
	uintptr_t needed_entry = (page_num&1) ? entry_odd : entry_even;

	if (needed_entry == TLB_ENTRYLO_INVALID) {
		panic_or_kill(registers, "Unmapped page access - TLB refill: %p", registers->badva);
	}

	/* The size of the page to map. */
	write_cp0_pagemask(CP0_PAGEMASK_4K);

	/* Write the low entry parts. The high entry part is set automatically. */
	write_cp0_entrylo0(entry_even);
	write_cp0_entrylo1(entry_odd);

	/* Fill a random TLB entry. */
	TLBWR();
}

/**
 * @file kernel/mm/thrcopy.h
 *
 * Declarations of @ref thrcopy_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef THRCOPY_H_
#define THRCOPY_H_

#include <include/c.h>
#include <proc/thread.h>


/* Externals are commented with implementation. */
extern int copy_to_thread(const thread_t thr, void *dest, const void *src, const size_t len);
extern int copy_from_thread(const thread_t thr, void *dest, const void *src, const size_t len);

#endif /* THRCOPY_H_ */

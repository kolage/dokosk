/**
 * @file thrcopy.c
 *
 * Implementaion of @ref thrcopy_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page thrcopy_feature Copying data across memory spaces
 *
 * This page describes how the kernel copies data between memory spaces.
 *
 * Both required functions (::copy_to_thread and ::copy_from_thread)
 * are solved by one procedure of copying data
 * between address spaces, which do not have to have anything in common
 * with the thread that does the copy (current thread).
 *
 * The thread that does the copy, changes its current address space
 * (thread_impl_t::used_addr_space) inside ::copy_across_spaces.
 * For this purpose, each thread has two fields for handling the address spaces.
 * thread_impl_t::owned_addr_space is the address space determined when creating
 * the thread and this field remains unchanged. Every function which accesses the
 * address space of a thread, should use this field. As the field never changes,
 * accesses to this field do not have to be synchronized and can happen from
 * any thread (this is used in ::copy_to_thread and ::copy_from_thread).
 *
 * On the other hand, thread_impl_t::used_addr_space can be changed,
 * but can be accessed only from the same thread. Changing the field changes
 * the address space used by virtual memory management (TLB refill handler)
 * to resolve virtual addresses. After changing the field, you must
 * ensure that the address space has correctly assigned ASID by calling
 * ::asid_current_map_changed.
 *
 * Modifying thread_impl_t::used_addr_space is used in ::copy_across_spaces to
 * switch between the source and destination address space. That way, the
 * thread can read and write data from and to the specified address spaces.
 *
 * In order not to switch between the address spaces too often,
 * the copying thread reads the data into a buffer in KSEG0 and writes them after switching
 * the address space. The buffer in KSEG0 is usually allocated on the heap,
 * but if the allocation on the heap fails (out of memory), or the copied
 * data is short, a smaller buffer allocated on the stack is used.
 *
 * If the arguments for the functions are correct, the function always succeed.
 *
 * Killing the thread during execution of the functions may leave shared structures
 * in an inconsistent state.
 *
 */

#include <mm/thrcopy.h>
#include <mm/asid.h>
#include <mm/malloc.h>
#include <include/codes.h>
#include <proc/thread.h>
#include <drivers/printer.h>


#define THRCOPY_STACK_BUFFER_SIZE 	16
#define THRCOPY_HEAP_BUFFER_SIZE 	1024


/** Copy data across address spaces.
 *
 * The spaces can be both different from the address space of the current
 * thread. The spaces can be identical.
 *
 * @param space_dest Address space of the destination memory. Must be valid.
 * @param dest       Pointer to the destination memory. Must be valid.
 * @param space_src  Address space of the source memory. Must be valid.
 * @param src        Pointer to the source memory. Must be valid.
 * @param length     Length of the copied memory in bytes.
 *
 */
static void copy_across_spaces(vm_address_space_t* space_dest, void *dest, vm_address_space_t* space_src, const void *src, size_t length)
{
	/* Avoid modifying the address spaces.
	   Therefore copy the data to KSEG, switch the address space
	   and copy the data to the destination. */

	/* The KSEG buffer can be on the stack or on the heap.
	   if the data are very short or the buffer cannot be
	   allocated on the heap, use the stack buffer.
	   This means this function can not fail because of
	   low free space on the heap. */
	uint8_t stack_buffer[THRCOPY_STACK_BUFFER_SIZE];
	uint8_t *buffer = stack_buffer;
	uint32_t buffer_size = THRCOPY_STACK_BUFFER_SIZE;

	if (length >= THRCOPY_HEAP_BUFFER_SIZE) {
		/* Try allocating the buffer on the heap. */
		uint8_t *heap_buffer = malloc(THRCOPY_HEAP_BUFFER_SIZE);

		if (heap_buffer != NULL) {
			/* Use the heap buffer only if successful. */
			buffer = heap_buffer;
			buffer_size = THRCOPY_HEAP_BUFFER_SIZE;
		}
	}

	volatile const uint8_t *src8 = src;
	volatile uint8_t *dest8 = dest;

	/* Switch address spaces back and forth and copy bytes through the buffer. */
	for (size_t i = 0; i < length; i += buffer_size) {

		/* Change space to source. */
		addr_space_change_used(space_src);

		/* Copy bytes from source to buffer. */
		for (size_t j = 0 ; j < buffer_size && j + i < length; ++j) {
			buffer[j] = src8[j + i];
		}

		/* Change space to destination. */
		addr_space_change_used(space_dest);

		/* Copy bytes from buffer to destination. */
		for (size_t j = 0; j < buffer_size && j + i < length; ++j) {
			dest8[j + i] = buffer[j];
		}
	}

	/* Restore the address space of this thread. */
	addr_space_change_used(thread_get_current_impl()->owned_addr_space);

	/* Free the buffer if it was allocated. */
	if (buffer != stack_buffer) {
		free(buffer);
	}
}

/** Get the owned address space of the thread or ::NULL if the
 *  thread is invalid.
 *
 * The reference count of the address space is incremented.
 *
 * @param  thr A thread which may be invalid.
 *
 * @return Owned address space of the thread or ::NULL.
 *
 */
static vm_address_space_t *copy_get_safe_addr_space_ref(const thread_t thr)
{
	ipl_t state = query_and_disable_interrupts();

	/* Check validity of the thread. */
	if (!thread_is_valid(thr)) {
		conditionally_enable_interrupts(state);
		return NULL;
	}

	/* Get the address space and add a reference. */
	vm_address_space_t *addr_space = thr.impl->owned_addr_space;
	addr_space_add_ref(addr_space);

	conditionally_enable_interrupts(state);

	return addr_space;
}

/** Copy data from the address space of the current thread to the address
 *  space of the specified thread.
 *
 * The specified pointers must be valid pointers in the respective address
 * spaces. The mapped memory must not overlap.
 *
 * @param  thr  Thread owning the destination address space.
 * @param  dest Pointer to destination memory in the destination address space.
 * @param  src  Pointer to source memory in the address space of the
 *              current thread.
 * @param  len  Number of bytes to copy.
 *
 * @return ::EINVAL if @p thr is not a thread, ::EOK if the data has been
 *         copied successfully.
 *
 */
int copy_to_thread(const thread_t thr, void *dest, const void *src, const size_t len)
{
	vm_address_space_t *addr_space = copy_get_safe_addr_space_ref(thr);

	if (addr_space == NULL) {
		return EINVAL;
	}

	copy_across_spaces(addr_space, dest, thread_get_current_impl()->owned_addr_space, src, len);
	addr_space_release(addr_space);

	return EOK;
}

/** Copy data from the address space of the specified thread to the address
 *  space of the current thread.
 *
 * The specified pointers must be valid pointers in the respective
 * address spaces. The mapped memory must not overlap.
 *
 * @param  thr  Thread owning the source address space.
 * @param  dest Pointer to destination memory in the address space of the
 *              current thread.
 * @param  src  Pointer to source memory in the source address space.
 * @param  len  Number of bytes to copy.
 *
 * @return ::EINVAL if @p thr is not a thread, ::EOK if the data has been
 *         copied successfully.
 *
 */
int copy_from_thread(const thread_t thr, void *dest, const void *src, const size_t len)
{
	vm_address_space_t *addr_space = copy_get_safe_addr_space_ref(thr);

	if (addr_space == NULL) {
		return EINVAL;
	}

	copy_across_spaces(thread_get_current_impl()->owned_addr_space, dest, addr_space, src, len);
	addr_space_release(addr_space);

	return EOK;
}

/**
 * @file kernel/mm/malloc.h
 *
 * Declarations of @ref malloc_feature.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef MALLOC_H_
#define MALLOC_H_

#include <include/c.h>


/* Externals are commented with implementation. */
extern void init_heap(void);
extern void *malloc(const size_t size);
extern void *safe_malloc(const size_t size);
extern void free(const void *ptr);
extern void heap_check(void);
extern void heap_print(void);

#endif /* MALLOC_H_ */

/**
 * @file kernel/mm/vma.h
 *
 * Declarations of @ref vma_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef VMA_H_
#define VMA_H_

#include <include/shared.h>
#include <include/c.h>
#include <sync/mutex.h>


/** Maximum number of areas in one address space. */
#define MAX_AREAS  1024


/** Virtual memory flags. */
typedef unative_t vm_flags_t;

/** A structure for the virtual memory area. */
typedef struct vm_area {
	/** Every area starts at some virtual address. */
	void *vma_start;

	/** Total size of the area. */
	size_t size;
} vm_area_t;

/** A virtual address space structure with memory translation
    support. Every thread has one of those. */
typedef struct vm_address_space {
	/** Array of all possible areas - sorted by the area virtual address. */
	vm_area_t areas[MAX_AREAS];

	/** Pointer to the virtual address of directory of first level of two-level paging. */
	void *addr_translations;

	/** Total number of occupied areas. */
	uint32_t area_count;

	/** Every address space has its own ASID. */
	uint8_t asid;

	/** Number of references to the address space. */
	atomic_t reference_count;

	/** We need to lock the address spaces. */
	mutex_t mutex;
} vm_address_space_t;


/* Externals are commented with implementation. */
extern int addr_space_init(vm_address_space_t *addr_space);
extern void addr_space_release(vm_address_space_t *addr_space);
extern void addr_space_add_ref(vm_address_space_t *addr_space);
extern void addr_space_change_used(vm_address_space_t* space);
extern vm_address_space_t *addr_space_get_current_owned(void);
extern vm_address_space_t *addr_space_get_current_used(void);
extern vm_area_t *addr_space_get_area_from_space(vm_address_space_t *addr_space, const void *virtual_address);
extern vm_area_t *addr_space_get_area(const void *virtual_address);
extern void *addr_space_page_address(vm_address_space_t *addr_space, void *virtual_address);
extern void *addr_space_translate(vm_address_space_t *addr_space, void *virtual_address);

extern int vma_map_in_addr_space(vm_address_space_t *current_addr_space, void **from, const size_t size, const vm_flags_t flags);
extern int vma_map(void **from, const size_t size, const vm_flags_t flags);
extern int vma_resize(const void *from, const size_t size);
extern int vma_unmap_in_addr_space(vm_address_space_t *current_addr_space, const void *from);
extern int vma_unmap(const void *from);

extern void addr_space_print(vm_address_space_t *current_addr_space);
extern void addr_space_print_current(void);

#endif /* VMA_H_ */

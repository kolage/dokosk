/**
 * @file kernel/mm/asid.h
 *
 * Declarations of ASID handling.
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef ASID_H_
#define ASID_H_

#include <mm/vma.h>


/** Number of possible ASID values. */
#define ASID_COUNT 			256

/** Number of ASID values for normal use. */
#define ASID_VALID_COUNT 	255

/** ASID value for invalid entires. */
#define ASID_INVALID 		255


/* Externals are commented with implementation. */
extern void init_asid(void);
extern void asid_ensure_valid(vm_address_space_t *addr_space);
extern void asid_invalidate(vm_address_space_t *addr_space);
extern void asid_current_map_changed(void);

#endif /* ASID_H_ */

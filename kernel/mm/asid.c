/**
 * @file kernel/mm/asid.c
 *
 * Implementation of ASID handling.
 *
 * Handles ASID assignment and setting the EntryHi register.
 * Uses the LRU algorithm. The ASIDs are represented by items in a list.
 * The list is maintained so that the head is either an unused ASID or
 * the least recently used one.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <drivers/printer.h>
#include <adt/list.h>
#include <mm/asid.h>
#include <mm/tlb.h>


/** Items for ::asid_lru_list. */
static item_t asid_items[ASID_VALID_COUNT];

/** List of all available ASIDs.
 *
 * The list contains:
 * - first the unused ASIDs (unordered)
 * - then the used ASIDs from least to most recently used.
 * When the ASID is used, append it to the end.
 * When the ASID is invalidated, prepend it to the beginning.
 */
static list_t asid_lru_list;


/** Initialize the ASID static structures, namely ::asid_lru_list.
 *
 */
void init_asid(void)
{
	list_init(&asid_lru_list);

	/* Insert all ASID items into the list. */
	for (uint32_t i = 0; i < ASID_VALID_COUNT; ++i) {
		item_init(&asid_items[i], NULL);
		list_append(&asid_lru_list, &asid_items[i]);
	}
}

/** Convert an item from ::asid_items array to an ASID.
 *
 */
static uint8_t asid_item_to_asid(item_t *asid_item)
{
	return asid_item-asid_items;
}

/** Adjust ::asid_lru_list after an ASID has been used.
 *
 * @param asid_item Item corresponding to ASID that is now valid.
 *
 */
static void asid_lru_used(item_t *asid_item)
{
	list_remove(&asid_lru_list, asid_item);
	list_append(&asid_lru_list, asid_item);
}

/** Adjust ::asid_lru_list after an ASID has been invalidated.
 *
 * @param asid_item Item corresponding to ASID that is no longer valid.
 *
 */
static void asid_lru_unused(item_t *asid_item)
{
	list_remove(&asid_lru_list, asid_item);

	/* We can assume the head is not null as the list always contains at least 254 items. */
	list_insert_before(&asid_lru_list, asid_lru_list.head, asid_item);
}

/** Assign a new ASID for a memory map.
 *
 * This can reuse ASID already used by another memory map.
 * This function should be called with interrupts disabled.
 *
 */
static item_t *asid_assign(void)
{
	/* Return the head of the list - that is either an unused ASID or the least recently used one. */
	return asid_lru_list.head;
}

/** Adjust the ASIDs after memory map change (context switch).
 *
 * Check the asid for the current thread. If it has none,
 * assign it. Then update the asid in the EntryHi register.
 *
 * This function should be called with interrupts disabled.
 *
 */
void asid_current_map_changed(void)
{
	vm_address_space_t *addr_space = addr_space_get_current_used();
	asid_ensure_valid(addr_space);

	/* Write the current asid to the EntryHi register, from where it is used by the processor. */
	write_cp0_entryhi(addr_space->asid);
}

/** Ensures that the specified memory map has valid assigned ASID.
 *
 * Check the ASID for the specified memory map. If it has none, assign it.
 *
 */
void asid_ensure_valid(vm_address_space_t *addr_space)
{
	ipl_t state = query_and_disable_interrupts();
	item_t *asid_item;

	if (addr_space->asid == ASID_INVALID) {
		/* Assign a new asid. */
		asid_item = asid_assign();
		uint8_t asid = asid_item_to_asid(asid_item);

		/* Invalidate the previous assignment. */
		vm_address_space_t* old_table = asid_item->data;

		if (old_table != NULL) {
			old_table->asid = ASID_INVALID;
		}

		tlb_invalidate_asid(asid);

		/* Assign it to the current table. */
		addr_space->asid = asid;
		asid_item->data=addr_space;
	} else {
		asid_item =& asid_items[addr_space->asid];
	}

	/* Tell the LRU algorithm that the ASID has been used. */
	asid_lru_used(asid_item);

	conditionally_enable_interrupts(state);
}

/** Unassign the ASID of the specified memory map.
 *
 * If the memory map does not have a valid ASID, does nothing.
 * Otherwise, it is set to invalid and entries with that ASID are
 * removed from the TLB.
 *
 */
void asid_invalidate(vm_address_space_t *addr_space)
{
	ipl_t state = query_and_disable_interrupts();

	if (addr_space->asid!=ASID_INVALID) {
		asid_items[addr_space->asid].data = NULL;
		asid_lru_unused(&asid_items[addr_space->asid]);
		tlb_invalidate_asid(addr_space->asid);
		addr_space->asid = ASID_INVALID;
	}

	conditionally_enable_interrupts(state);
}

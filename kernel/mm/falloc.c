/**
 * @file kernel/mm/falloc.c
 *
 * Implementation of @ref falloc_feature.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** 
 * @page falloc_feature Frames allocator
 * 
 * Frame allocator uses memory discovery module to determine usable blocks
 * of physical memory. At the beginning of first usable memory block it
 * allocates space for frame allocation structures and the rest of memory
 * is then reserved only for frames data.
 *
 * Memory map structures are divided into kernel and user memory space for
 * separation and simplification of memory operations within these aresa.
 *
 * Allocator is inspired by buddy allocation system. It uses several
 * different sizes (by default 8) that can user allocate called orders.
 * Smallest order corresponds to size of one frame and every following order
 * is twice as large. For each order there is array of indexes corresponding
 * to the relevant part of the physical memory over which they lies. This is
 * good for quickly getting information about block at specified physical
 * address. There are also lists with available free memory blocks for each
 * order, so it can be easily checked there is free block of given order.
 * The negative aspect of this approach is relatively large internal and
 * external fragmentation.
 *
 * To reduce the amount of external fragmentation and to allow user allocating
 * larger memory blocks than the highest available order, user can use the
 * alternate allocation method by setting up the special flag ::VF_VA_ALT. 
 * It searches for coherent memory part of user specified size by iterating
 * over memory bitmap instead of using free lists.
 * 
 * For comparsion of these two available methods, brief results of inbuilt 
 * tests for allocating various random sizes of frame blocks (1 .. 100 frames) 
 * are folllowing:
 * 
 * 16MB memory ~ about 4000 frames available
 * - standard method has allocated 2749 frames (69 % use ratio)
 * - alternate method has allocated 3879 frames (97 % use ratio)
 * 
 * 32MB memory ~ about 8000 frames available
 * - standard method has allocated 5416 frames (68 % use ratio)
 * - alternate method has allocated 7917 frames (99 % use ratio)
 * 
 */

#include <mm/falloc.h>
#include <include/math.h>
#include <include/codes.h>
#include <drivers/printer.h>
#include <mm/memory.h>


/** Number of orders used in memory allocation system.
 *
 * Each memory map order is indicating frame blocks
 * of different sizes, precisely in map with order i
 * each block is of size (FRAME_SIZE << i).
 *
 */
#define FRAME_BLOCK_ORDER_COUNT		8

/** Not found index used when searching memory maps. */
#define FREE_INDEX_NOT_FOUND		-1

/** Number of used memory maps (kernel and user map). */
#define MEMORY_MAP_COUNT 		2

/** Maximum number of sections in memory map. */
#define MAP_SECTIONS_COUNT_MAX		64


/** Memory map types. */
typedef enum {
	KERNEL_MEMORY_MAP,
	USER_MEMORY_MAP
} memory_map_type_t;

/** Memory map section structure. */
typedef struct {
	/** Start address of section in memory. */
	void *start_address;

	/** End address of section in memory. */
	void *end_address;

	/** Array of lists with free blocks of different orders. */
	list_t *free_lists;

	/** 2dim array of blocks in each order. */
	item_t **free_lists_items;

	/** Array of block count in each order. */
	size_t *free_lists_items_count;
} memory_map_section_t;

/** Memory map structure. */
typedef struct {
	/** Type of memory map - either KERNEL_MEMORY_MAP or USER_MEMORY_MAP. */
	memory_map_type_t type;

	/** Number of sections in memory map. */
	size_t sections_count;

	/** Array of sections ~ coherent parts of memory map. */
	memory_map_section_t *sections;
} memory_map_t;

/** Structure for identifying memory map block. */
typedef struct {
	/** Index of first frame inside section. */
	int start_index;

	/** Section index inside related memory map. */
	int section_index;

	/** Related memory map structure. */
	memory_map_t *memory_map;

	/** Order of block. */
	size_t order;

	/** Number of frames related to this block. */
	size_t frames_count;
} memory_map_block_t;


/* Mutex for locking physical address space. */
static mutex_t falloc_mtx;

/* Memory map structures. */
static memory_map_t kernel_memory_map, user_memory_map;


/** Check if user wants to try allocate block by
 * coalescing smaller ones.
 *
 * @param  flags Flags to be analyzed.
 *
 * @return True if user wants to attempt alternate
 * allocation, otherwise false.
 *
 */
static inline bool falloc_alternate_allocation(vm_flags_t flags)
{
	return (flags & VF_VA_ALT);
}

/** Check if user wants to try allocating in user space.
 *
 * @param  flags Flags to be analyzed.
 *
 * @return True if user wants to attempt allocate in
 * user space, otherwise false.
 *
 */
static inline bool falloc_allocate_in_user_space(vm_flags_t flags)
{
	return (flags & (VF_AT_KUSEG | VF_AT_KSSEG | VF_AT_KSEG3));
}

/** Check if user wants to try allocate at some specified address.
 *
 * @param  flags Flags to be analyzed.
 *
 * @return True if user wants to attempt allocate at
 * specified address, otherwise false.
 *
 */
static inline bool falloc_allocate_at_user_def_address(vm_flags_t flags)
{
	return (flags & VF_VA_USER);
}

/** Get physical memory iterator type
 *
 * @param  memory_map_type Type of memory map
 *
 * @return Corresponding physical memory iterator type
 *
 */
static memory_it_type_t falloc_get_memory_it_type(memory_map_type_t memory_map_type)
{
	switch(memory_map_type) {
		case KERNEL_MEMORY_MAP:
			return PHYS_MEM_IT_KERNEL;

		case USER_MEMORY_MAP:
			return PHYS_MEM_IT_USER;

		default:
			return PHYS_MEM_IT_ALL;
	}
}

/** Get memory map related to input address.
 *
 * @param  paddr Address in physical memory.
 *
 * @return Memory map to which this address belongs.
 *
 */
static memory_map_t *falloc_get_map_relevant_to_address(const void *paddr)
{
	if (paddr < (void *)PHYS_KERNEL_END_ADDR) {
		return &kernel_memory_map;
	} else {
		return &user_memory_map;
	}
}

/** Extract information about memory map block from physical address.
 *
 *  @param paddr Physical memory address.
 *  @param block Structure with information about memory block.
 *
 */
static void falloc_get_block_relevant_to_address(const void *paddr, memory_map_block_t *block)
{
	size_t sections_count = block->memory_map->sections_count;

	/* Loop through all sections in memory map and find relevant one. */
	for (size_t i = 0; i < sections_count; i++) {
		memory_map_section_t *section = &block->memory_map->sections[i];

		if (paddr >= section->start_address && paddr < section->end_address) {
			block->section_index = i;
			block->start_index = (paddr - (void *)section->start_address) / FRAME_SIZE;
			return;
		}
	}
}

/** Memory map section block count of given order.
 *
 *  @param  start_address Memory map section start address.
 *  @param  end_address   Memory map section end address.
 *  @param  order         Order of blocks.
 *
 *  @return Block count.
 */
static size_t falloc_total_map_section_order_blocks_count(void *start_address, void *end_address, size_t order)
{
	size_t map_size = (uintptr_t)end_address - (uintptr_t)start_address;

	return (map_size / (FRAME_SIZE << order));
}

/** Memory map section block count.
 *
 *  @param  start_address Memory map section start address.
 *  @param  end_address   Memory map section end address.
 *
 *  @return Block count.
 */
static size_t falloc_total_map_section_blocks_count(void *start_address, void *end_address)
{
	size_t blocks_count = 0;
	size_t map_size = (uintptr_t)end_address - (uintptr_t)start_address;

	for (int it_order = 0; it_order < FRAME_BLOCK_ORDER_COUNT; it_order++) {
		blocks_count += (map_size / (FRAME_SIZE << it_order));
	}

	return blocks_count;
}

/** Check if block is free.
 *
 * @param  block Memory map block structure with block information.
 *
 * @return True if block is free, otherwise false.
 *
 */
static bool falloc_is_block_free(memory_map_block_t *block)
{
	list_t *free_list =
			&block->memory_map->sections[block->section_index].free_lists[block->order];

	/* Compute index inside relevant order map from index of first frame corresponding to this block. */
	size_t order_index = block->start_index / (1 << block->order);

	item_t *item =
			&block->memory_map->sections[block->section_index].free_lists_items[block->order][order_index];

	return list_contains(free_list, item);
}

/** Reserve physical memory for handling structures.
 *
 * @return Start address to be used for allocating memory structures.
 *
 */
static void *falloc_init_memory_for_structures()
{
	size_t total_blocks_count = 0;
	size_t total_sections_count = 0;

	/* Loop through available memory sections. */
	memory_it_t it;

	for (memory_it_init(&it, PHYS_MEM_IT_ALL); memory_it_has_current(&it); memory_it_next(&it)) {
		memory_block_t *block_info = memory_it_block(&it);
		total_blocks_count += falloc_total_map_section_blocks_count(block_info->start, block_info->end);
		total_sections_count++;
	}

	/* Count how much memory is needed for all structures it is little bit more than needed. */
	size_t total_memory_needed =
			/* Kernel and user memory map array blocks count for each order. */
			MEMORY_MAP_COUNT*FRAME_BLOCK_ORDER_COUNT * sizeof(size_t) +
			/* Total number of section structures. */
			total_sections_count * sizeof(memory_map_section_t) +
			/* Total number of blocks. */
			MEMORY_MAP_COUNT*FRAME_BLOCK_ORDER_COUNT * sizeof(item_t*) + total_blocks_count * sizeof(item_t) +
			/* Kernel and user free block lists for each order. */
			MEMORY_MAP_COUNT*FRAME_BLOCK_ORDER_COUNT * sizeof(list_t);

	/* We are gonna reserve memory from first physical memory block. */
	memory_block_t *first_block = memory_find_block(0);

	/* Check there is enough memory. */
	assert(memory_get_block_size(first_block) > total_memory_needed);

	void *structures_start = first_block->start;

	/* Shift pointer to first physical address. */
	first_block->start = (void *)ALIGN_UP(((uintptr_t)(first_block->start) + total_memory_needed), FRAME_SIZE);

	return structures_start;
}

/** Initialize memory map.
 *
 * @param  memory_map    Memory map to be initialized.
 * @param  type          Type of memory map to be initialized.
 * @param  start_address Starting address of this memory map in physical memory.
 *
 * @return End address of this memory map in physical memory.
 *
 */
static void *falloc_init_memory_map(memory_map_t *memory_map, memory_map_type_t type, void *start_address)
{
	memory_block_t *sections[MAP_SECTIONS_COUNT_MAX];
	size_t sections_count = 0;

	/* Get iterator type. */
	memory_it_type_t phys_mem_it_type = falloc_get_memory_it_type(type);

	/* Loop through available memory sections. */
	memory_it_t it;

	for (memory_it_init(&it, phys_mem_it_type); memory_it_has_current(&it); memory_it_next(&it)) {
		memory_block_t *block_info = memory_it_block(&it);
		sections[sections_count] = block_info;
		sections_count++;
	}

	/* Initialize memory map structure. */
	memory_map->type = type;
	memory_map->sections_count = sections_count;

	/* We are moving pointer in memory where we are actually allocating. */
	void *actual_address = PHYS_ADDR_TO_KSEG0_ADDR(start_address);

	/* Allocate memory map sections structures. */
	memory_map->sections = (memory_map_section_t *)actual_address;
	actual_address = (void *)memory_map->sections + sections_count * sizeof(memory_map_section_t);

	/* Initialize memory map sections. */
	for (size_t i = 0; i < sections_count; i++) {
		memory_map_section_t *memory_map_section = &memory_map->sections[i];
		memory_map_section->start_address = sections[i]->start;
		memory_map_section->end_address = sections[i]->end;

		/* Allocate section free lists. */
		memory_map_section->free_lists = (list_t *)actual_address;
		actual_address = (void *)memory_map_section->free_lists + FRAME_BLOCK_ORDER_COUNT * sizeof(list_t);

		/* Allocate section free lists items count array. */
		memory_map_section->free_lists_items_count = (size_t *)actual_address;
		actual_address = (void *)memory_map_section->free_lists_items_count + FRAME_BLOCK_ORDER_COUNT * sizeof(size_t);

		/* Allocate section free lists items - first step. */
		memory_map_section->free_lists_items = (item_t **)actual_address;
		actual_address = (void *)memory_map_section->free_lists_items + FRAME_BLOCK_ORDER_COUNT * sizeof(item_t*);

		/* Initialize free lists and free lists items. */
		for (int it_order = 0; it_order < FRAME_BLOCK_ORDER_COUNT; it_order++) {
			list_t *list = &memory_map_section->free_lists[it_order];
			list_init(list);

			size_t blocks_count = falloc_total_map_section_order_blocks_count(
					memory_map_section->start_address,
					memory_map_section->end_address,
					it_order);

			memory_map_section->free_lists_items_count[it_order] = blocks_count;

			/* Allocate section free lists items - second step (we must allocate array for each order). */
			memory_map_section->free_lists_items[it_order] = (item_t *)actual_address;
			actual_address = (void *)memory_map_section->free_lists_items[it_order] + blocks_count * sizeof(item_t);

			for (size_t it_block = 0; it_block < blocks_count; it_block++) {
				item_t *item = &memory_map_section->free_lists_items[it_order][it_block];
				item_init(item, (void *)it_block);
				list_append(list, item);
			}
		}
	}

	/* Return address where this memory map ends. */
	return VA_DIRECT_TO_PHYSICAL(actual_address);
}

/** Initialize physical memory.
 *
 */
void init_frames(void)
{
	/* Automatic discovery of physical memory blocks. */
	memory_discover();

	/* Init the mutex for physical memory frames. */
	mutex_init(&falloc_mtx);

	/* Reserve memory for handling structures. */
	void *structures_start = falloc_init_memory_for_structures();

	/* Init memory maps. */
	void *kernel_memory_map_end = falloc_init_memory_map(&kernel_memory_map, KERNEL_MEMORY_MAP, structures_start);
	void *user_memory_map_end = falloc_init_memory_map(&user_memory_map, USER_MEMORY_MAP, kernel_memory_map_end);

	printk_nb("-- Addresses %p - %p reserved for frame allocation structures.\n", structures_start, user_memory_map_end);
}

/** Initialize a frame block.
 *
 * Fills in the structures related to a frame block.
 *
 * @param block Information about block of memory to be initialized.
 * @param used  Specifies whether we are allocating or freeing.
 *
 */
static void falloc_frame_block_init(memory_map_block_t *block, bool used)
{
	bool nothing_marked_used;

	size_t section_index = block->section_index;
	size_t blocks_to_mark = block->frames_count;
	size_t start_block = block->start_index;

	/* Mark relevant blocks as free or used. */
	for (size_t it_order = 0; it_order < FRAME_BLOCK_ORDER_COUNT; it_order++) {
		nothing_marked_used = true;

		/* Fetch relevant blocks from memory map. */
		memory_map_section_t *section = &block->memory_map->sections[section_index];

		size_t blocks_count = section->free_lists_items_count[it_order];

		list_t *free_list = &section->free_lists[it_order];
		item_t *free_list_items = section->free_lists_items[it_order];

		/* Mark relevant blocks as used or free. */
		size_t i = 0;
		while (i < blocks_to_mark) {
			item_t *item = &free_list_items[start_block + i];

			if (used) {
				if (list_contains(free_list, item)) {
					list_remove(free_list, item);
					nothing_marked_used = false;
				}
			} else {
				if (!list_contains(free_list, item)) {
					list_append(free_list, item);
				}
			}

			i++;
		}
		
		/* No block was marked as used in this iteration, so no block 
		 * should be marked in any higher order - we can quit.
		 */
		if(used && nothing_marked_used) {
			return;
		}

		/* Compute how many blocks to mark in next iteration. */
		
		size_t temp_mark = blocks_to_mark / 2;

		/* Check whether the first block in current iteration is on
		 * aligned position due to its parent block with higher
		 * order.
		 */
		bool first_block_aligned = (start_block % 2) == 0;
		
		/* Check whether the last block in current iteration is on
		 * aligned position due to its parent block with higher
		 * order.
		 */
		bool last_block_aligned = ((start_block + (i-1)) % 2) == 1;
		
		/* Check whether the blocks before first block in current
		 * iteration and after the last block in current iteration
		 * are used. 
		 * 
		 * This is used when we are freeing blocks and the first
		 * or last block is not aligned to its parent block with
		 * higher order. Then we must check whether their buddy 
		 * blocks are free and so we can mark their parent blocks 
		 * in higher order free as well.
		 */
		bool first_block_prev_used = true;
		bool last_block_next_used = true;

		/* Compute how many blocks to mark in next iteration. It depends
		 * on whether the sequence of blocks in current iteration is aligned
		 * due to its parent blocks with higher order. Also when we are freeing,
		 * we must make sure that we dont mark anything in higher orders as
		 * free if some block in lower order is still used.
		 */
		if(used) {
			if(first_block_aligned && last_block_aligned) {
				blocks_to_mark = temp_mark;
			} else {
				blocks_to_mark = temp_mark + 1;
			}
		} else {
			if(first_block_aligned && last_block_aligned) {
				blocks_to_mark = temp_mark;
			} else {
				if(!first_block_aligned) {
					item_t *first_block_prev = &free_list_items[start_block - 1];
					first_block_prev_used = !list_contains(free_list,first_block_prev);
				}
				if(!last_block_aligned) {
					if(start_block + i < blocks_count) {
						item_t *last_block_next = &free_list_items[start_block + i];
						last_block_next_used = !list_contains(free_list,last_block_next);
					}
				}
				if(!first_block_aligned && !last_block_aligned) {
					if(first_block_prev_used && last_block_next_used) {
						blocks_to_mark = temp_mark - 1;
					} else if(first_block_prev_used || last_block_next_used) {
						blocks_to_mark = temp_mark;
					} else {
						blocks_to_mark = temp_mark + 1;
					}
				} else {
					if((!first_block_aligned && !first_block_prev_used) ||
						(!last_block_aligned && !last_block_next_used)) {
						blocks_to_mark = temp_mark + 1;
					} else {
						blocks_to_mark = temp_mark;
					}
				}
			}
		}
		
		/* If there are no blocks to be marked, we can terminate marking. */
		if(blocks_to_mark == 0) {
			return;
		}

		/* Compute starting index for marking in next iteration. */
		start_block = start_block / 2;
		
		/* If we determined that there is some used block before the unaligned first block
		 * in current iteration, we must start marking from the following position in
		 * next iteration.
		 */
		if(!used && temp_mark != 0 && !first_block_aligned && first_block_prev_used) {
			start_block++;
		}
	}
}

/** Extract physical address from memory block information structure.
 *
 * @param  block Block to be converted.
 *
 * @return Physical address.
 *
 */
static void *falloc_compute_phys_address(memory_map_block_t *block)
{
	memory_map_section_t *section = &block->memory_map->sections[block->section_index];

	return (void *)section->start_address + (block->start_index * FRAME_SIZE);
}

/** Worker function for free memory block finder.
 *
 * @param block Structure with information about memory block.
 *
 */
static void falloc_find_free_block_proc(memory_map_block_t *block)
{
	size_t sections_count = block->memory_map->sections_count;

	for (size_t i = 0; i < sections_count; i++) {
		list_t *free_list = &block->memory_map->sections[i].free_lists[block->order];

		if (!list_empty(free_list)) {
			size_t order_index = (int)list_rotate (free_list)->data;
			block->section_index = i;
			/* Compute index of the first frame from order map index. */
			block->start_index = order_index * (1 << block->order);
			return;
		}
	}

	block->start_index = FREE_INDEX_NOT_FOUND;
}

/** Worker function for alternate free memory block finder.
 *
 * We are trying to find a coherent block of frames (blocks with
 * lowest order) of total length specified by count.
 *
 * @param block Structure with information about memory block.
 *
 */
static void falloc_find_free_block_alternate_proc(memory_map_block_t *block)
{
	size_t sections_count = block->memory_map->sections_count;
	size_t it_section;

	for (it_section = 0; it_section < sections_count; it_section++) {
		/* Index 0 corresponds to block with FRAME_SIZE. */
		size_t total_frames_count = block->memory_map->sections[it_section].free_lists_items_count[0];
		list_t *free_list = &block->memory_map->sections[it_section].free_lists[0];

		size_t it_frame = 0;
		size_t temp_count = 0;
		size_t block_start_index = -1;

		/* Remaining number of free frames in free list. */
		size_t remain_free_list_count = list_count(free_list);

		/* If free list is empty or there are not enough free frames to allocate block or finally if there
		 * are not enough frames to allocate block at all, it makes no sense to continue ...
		 */
		if (block->frames_count < total_frames_count) {
			while (it_frame < total_frames_count && temp_count < block->frames_count && remain_free_list_count >= block->frames_count-temp_count) {
				item_t *frame_item = &block->memory_map->sections[it_section].free_lists_items[0][it_frame];

				if (list_contains(free_list, frame_item)) {
					if (temp_count == 0) {
						block_start_index = (int)frame_item->data;
					}

					remain_free_list_count--;
					temp_count++;
				} else {
					temp_count = 0;
				}

				it_frame++;
			}

			if (temp_count == block->frames_count) {
				block->section_index = it_section;
				block->start_index = block_start_index;
				return;
			}
		}
	}

	block->start_index = FREE_INDEX_NOT_FOUND;
}

/** Tries to find free memory block.
 *
 * @param block Structure with information about memory block.
 * @param try_user_space If true, we try to find free block in user memory segment,
 * otherwise kernel memory is searched.
 * @param alternate Flag used to determine if user want to use alternate allocation.
 *
 */
static void falloc_find_free_block(memory_map_block_t *block, bool try_user_space, bool alternate)
{
	if (try_user_space) {
		block->memory_map = &user_memory_map;
		alternate ? falloc_find_free_block_alternate_proc(block) : falloc_find_free_block_proc(block);

		if (block->start_index != FREE_INDEX_NOT_FOUND) {
			return;
		}
	}

	block->memory_map = &kernel_memory_map;
	alternate ? falloc_find_free_block_alternate_proc(block) : falloc_find_free_block_proc(block);;
}

/** Worker function for frame allocation function.
 *
 * @param  paddr If VF_VA_AUTO flag is set, it returns the
 * beginning address with allocated memory. If VF_VA_USER
 * flag is set, paddr address is used to determine where
 * user want to allocate the memory.
 * @param  block Structure with information about memory block.
 * @param  flags Flag mask indicates several attributes
 * that could be set by user. Besides paddr related flags,
 * it could be used to determine target memory segment
 * by masks VF_AT_KSEG0, VF_AT_KSEG1 or to determine
 * preffered target segment by VF_AT_KUSEG, VF_AT_KSSEG
 * and VF_AT_KSEG3.
 * @param  alternate Flag used to determine if user want to use alternate allocation.
 *
 * @return It returns ENOMEM when not enough memory
 * available, otherwise EOK.
 *
 */
static int frame_alloc_proc(void **paddr, memory_map_block_t *block, const vm_flags_t flags, bool alternate)
{
	block->start_index = FREE_INDEX_NOT_FOUND;

	/* Try to allocate memory at user defined place. */
	if (falloc_allocate_at_user_def_address(flags)) {
		block->memory_map = falloc_get_map_relevant_to_address(*paddr);

		/* Compute index in memory map corresponding to paddr. */
		falloc_get_block_relevant_to_address(*paddr, block);
	}

	/* If index on paddr defined by user was not found or VF_VA_AUTO is defined,
	 * we try to find free block somewhere else. */
	if (block->start_index == FREE_INDEX_NOT_FOUND || !falloc_is_block_free(block)) {
		/* Check if we should try user space first. */
		bool try_user_space = falloc_allocate_in_user_space(flags);

		falloc_find_free_block(block, try_user_space, alternate);

		/* No index found, therefore not enough memory. */
		if (block->start_index == FREE_INDEX_NOT_FOUND) {
			return ENOMEM;
		}
	}

	/* Block initialization. */
	falloc_frame_block_init(block, true);

	/* Compute real address. */
	void *addr = falloc_compute_phys_address(block);

	/* Return address with allocated memory. */
	*paddr = addr;

	return EOK;
}

/** Allocate physical memory block.
 *
 * The function finds and allocates continuous block of
 * free memory pages of size given by cnt.
 *
 * @param  paddr If VF_VA_AUTO flag is set, it returns the
 * beginning address with allocated memory. If VF_VA_USER
 * flag is set, paddr address is used to determine where
 * user want to allocate the memory.
 * @param  cnt Number of frames to be allocated.
 * @param  flags Flag mask indicates several attributes
 * that could be set by user. Besides paddr related flags,
 * it could be used to determine target memory segment
 * by masks VF_AT_KSEG0, VF_AT_KSEG1 or to determine
 * preffered target segment by VF_AT_KUSEG, VF_AT_KSSEG
 * and VF_AT_KSEG3. It can be also used to attempt
 * alternate frame allocation procedure by using flag
 * VF_VA_ALT.
 *
 * @return It returns ENOMEM when not enough memory
 * available or cnt is zero, otherwise EOK.
 *
 */
int frame_alloc(void **paddr, const size_t cnt, const vm_flags_t flags)
{
	if (cnt == 0) {
		return ENOMEM;
	}

	/* Prepare memory map block structure with all necessary
	 * information about block to be allocated. */
	memory_map_block_t block;
	block.frames_count = cnt;
	block.order = log2_of_pow2(pow2_roundup(cnt));

	int result;

	/* LOCK */
	mutex_lock(&falloc_mtx);

	/* Check if alternate allocation method should be used. */
	if (!falloc_alternate_allocation(flags) && block.order < FRAME_BLOCK_ORDER_COUNT) {
		result = frame_alloc_proc(paddr, &block, flags, false);
	} else {
		result = frame_alloc_proc(paddr, &block, flags, true);
	}

	/* UNLOCK */
	mutex_unlock(&falloc_mtx);

	return result;
}

/** Free physical memory block.
 *
 * The function frees continuous memory block starting
 * on address paddr.
 *
 * @param  paddr Address of block in physical memory to
 * be freed.
 * @param  cnt   Number of frames to be freed.
 *
 * @return It returns EINVAL when paddr is not
 * valid aligned physical address or cnt is zero,
 * otherwise EOK.
 *
 */
int frame_free(const void *paddr, const size_t cnt)
{
	if (cnt == 0 || !IS_ADDR_ALLIGNED_TO_PAGE_SIZE(paddr)) {
		return EINVAL;
	}

	/* Prepare memory map block structure with all necessary
	 * information about block to be freed. */
	memory_map_block_t block;
	block.frames_count = cnt;
	block.order = log2_of_pow2(pow2_roundup(cnt));
	block.memory_map = falloc_get_map_relevant_to_address(paddr);

	/* Compute index of block in memory map. */
	falloc_get_block_relevant_to_address(paddr, &block);

	/* LOCK */
	mutex_lock(&falloc_mtx);

	/* Check if block was previously allocated by frame_alloc. */
	if (!falloc_is_block_free(&block)) {
		falloc_frame_block_init(&block, false);
	} else {
		mutex_unlock(&falloc_mtx);
		return EINVAL;
	}

	/* UNLOCK */
	mutex_unlock(&falloc_mtx);

	return EOK;
}

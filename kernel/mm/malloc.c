/**
 * @file kernel/mm/malloc.c
 *
 * Implementation of @ref malloc_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page malloc_feature Kernel memory allocator
 *
 * This page describes how the memory is managed on the kernel heap.
 *
 * The allocator has its backend at the @ref falloc_feature. It allocates the specified
 * number of frames at start - this block of frames is called a superblock.
 * Superblocks have their header and footer with metadata about themselves, those
 * metadata are stored as data in the special heap blocks. Superblocks are stored
 * in the classical list structure, so traversing for debugging is easy.
 *
 * Every superblock is divided into several heap blocks. When we allocate the
 * memory, we find the first free block that fits the required size and split it
 * on the allocated and free block if possible. When there is no suitable free
 * block in the list of free blocks, the heap can stretch itself by allocating
 * some more frames - new superblock.
 *
 * Empty superblock consits of three blocks: block representing superblock header,
 * free block and block representing superblock footer.
 *
 * When we free the heap block, we look for its neighbors whether they are
 * free and if so, we do the merge of those free blocks. When the merged block
 * have as its predecessor a block of type ::HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD
 * and as its successor block of type ::HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT, we
 * know that the block is the only one in the superblock and therefore we can
 * release the whole superblock (all the frames it consists of).
 *
 * The allocation policy is first fit in the list of free blocks. We use several
 * one free lists for a little speed up. There is a list with blocks smaller than
 * 64 b, list with blocks of size between 64 and 128 b etc. When we search for
 * a free block of specific size we search the list that contains blocks of
 * searched size and all the lists that contain blocks greater than searched size.
 *
 * Heap can be also printed out for debugging with ::heap_print function.
 *
 * If you want to see how the memory is managed on the user space heap, see
 * @userpage{malloc_feature, user space heap allocator} description.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <include/math.h>
#include <drivers/printer.h>
#include <mm/malloc.h>
#include <mm/memory.h>
#include <mm/falloc.h>
#include <sync/mutex.h>


/** Magic used in heap headers. */
#define HEAP_BLOCK_HEAD_MAGIC  		0xBEEF0001

/** Magic used in heap footers. */
#define HEAP_BLOCK_FOOT_MAGIC  		0xBEEF0002

/** Default number of allocated frames - 512 kB of physical memory. */
#define HEAP_INIT_FRAMES_COUNT		128

/** The number of allocated frames when we increase the physical space for the heap. */
#define HEAP_ALLOC_FRAMES_COUNT		16

/** Total size of the heap block metadata structures. */
#define HEAP_BLOCK_METADATA_SIZE			(sizeof(heap_block_head_t) + sizeof(heap_block_foot_t))

/** Size of the data of the heap free block. */
#define HEAP_FREE_BLOCK_DATA_SIZE			(sizeof(heap_free_block_info_t))

/** Total minimal size of the heap free block. */
#define HEAP_FREE_BLOCK_SIZE				(HEAP_BLOCK_METADATA_SIZE + HEAP_FREE_BLOCK_DATA_SIZE)

/** Size of the data of the heap block with metadata about superblock. */
#define HEAP_SUPERBLOCK_HEAD_BLOCK_SIZE		(HEAP_BLOCK_METADATA_SIZE + sizeof(heap_superblock_info_t))

/** Size of the data of the heap block which indicates the end of superblock. */
#define HEAP_SUPERBLOCK_FOOT_BLOCK_SIZE		(HEAP_BLOCK_METADATA_SIZE)

#if !defined(DEBUG_HEAP) || defined(__DOXYGEN__)
/** Debug the heap?
 *
 * By default debugging prints for heap are disabled.
 * You can define ::DEBUG_HEAP to true to enable heap debugging prints.
 * To do this, add HEAP to KERNEL_DEBUG when building the OS:
 * <pre>make KERNEL_DEBUG="HEAP"</pre>
 */
#define DEBUG_HEAP false
#endif

#if DEBUG_HEAP
#define heap_debug_print(ARGS...) printk(ARGS);
#else
#define heap_debug_print(ARGS...)
#endif


/** Enumeration of all types of heap blocks.
 *
 */
typedef enum {
	HEAP_BLOCK_TYPE_FREE,
	HEAP_BLOCK_TYPE_ALLOCATED,
	HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD,
	HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT
} heap_block_type_t;

/** Header of a heap block.
 *
 */
typedef struct {
	/** Size of the block including header and footer. */
	size_t size;

	/** The type of the block. */
	heap_block_type_t type;

	/**
	 * A magic value to detect overwrite of heap header.
	 * The value is at the end of the header because
	 * that is where writing past block start will
	 * do damage.
	 */
	uint32_t magic;
} heap_block_head_t;

/** Footer of a heap block.
 *
 */
typedef struct {
	/**
	 * A magic value to detect overwrite of heap footer.
	 * The value is at the beginning of the footer
	 * because that is where writing past block
	 * end will do damage.
	 */
	uint32_t magic;

	/** Size of the block. */
	size_t size;
} heap_block_foot_t;

/** Simple structure for the metadata about superblock. Every superblock
 *  can be an item in the list of superblocks and it also evidences its
 *  size (for consistency checking).
 *
 */
typedef struct {
	size_t frames_cnt;
	item_t list_item;
} heap_superblock_info_t;

/** Simple structure for the free block data. Every free block can be an item
 *  in the list of free blocks.
 *
 */
typedef struct {
	item_t list_item;
} heap_free_block_info_t;


/** List with all the heap superblocks. */
static list_t superblocks_list;

/** Lists with all the free blocks. In those lists we try to find the first
    free block when the malloc is running. There are separate lists for
    different block sizes for the speed up. */
static list_t free_blocks_list_less_than_64b;
static list_t free_blocks_list_64b_128b;
static list_t free_blocks_list_128b_512b;
static list_t free_blocks_list_512b_2k;
static list_t free_blocks_list_2k_8k;
static list_t free_blocks_list_8k_32k;
static list_t free_blocks_list_32k_128k;
static list_t free_blocks_list_more_than_128k;

/** Lock for the heap - when we allocate and deallocate memory. */
static mutex_t heap_mutex;


/** Translate the block type from its constant representation to the human
 *  readable form.
 *
 * @param  block_type Type of the heap block.
 *
 * @return Human readable translation of the passed heap block type.
 *
 */
static char *heap_tr_block_type(heap_block_type_t block_type)
{
	switch (block_type) {
		case HEAP_BLOCK_TYPE_FREE:
			return "FREE";

		case HEAP_BLOCK_TYPE_ALLOCATED:
			return "ALLOCATED";

		case HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD:
			return "SUPERBLOCK_HEAD";

		case HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT:
			return "SUPERBLOCK_FOOT";
	}

	return "UNKNOWN";
}

#ifdef NDEBUG

/** Dummy heap block consistency check.
 *
 * Do not actually do any checking if we are building
 * a non-debug kernel.
 *
 */
#define heap_block_check(addr)

#else /* NDEBUG */

/** Check a heap block consistency.
 *
 * Verifies that the structures related to a heap block still contain
 * the magic constants. This helps detect heap corruption early on.
 *
 * @param addr Address of the block.
 *
 */
static void heap_block_check(void *addr)
{
	/* Calculate the position of the header. */
	heap_block_head_t *head = (heap_block_head_t *)addr;

	/* Make sure the header is still intact. */
	assert(head->magic == HEAP_BLOCK_HEAD_MAGIC);

	/* Calculate the position of the footer. */
	heap_block_foot_t *foot = (heap_block_foot_t *)(addr + head->size - sizeof(heap_block_foot_t));

	/* Make sure the footer is still intact. */
	assert(foot->magic == HEAP_BLOCK_FOOT_MAGIC);

	/* Check the size of the heap blocks. */
	switch (head->type) {
		case HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD:
			assert(head->size == HEAP_SUPERBLOCK_HEAD_BLOCK_SIZE);
			break;

		case HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT:
			assert(head->size == HEAP_SUPERBLOCK_FOOT_BLOCK_SIZE);
			break;

		default:
			/* Check the block size - every block must be at least as big as the free block
			   (we need to store some metadata about free blocks in the data part of those blocks,
			   especially the pointer to the list of free blocks). */
			assert(head->size >= HEAP_FREE_BLOCK_SIZE);
			break;
	}

	/* And one extra check for the fun of it. */
	assert(head->size == foot->size);
}

#endif /* NDEBUG */

/** Count the real size of the block to be allocated.
 *
 * When we allocate the block, we have to count in the size of the
 * metadata about that block. Also we have to align the block size
 * to the 4 bytes, because we want to avoid the unaligned memory
 * access exception when accessing the block footer.
 *
 * @param  size Required block size.
 *
 * @return Real block size to be allocated.
 *
 */
static size_t heap_count_block_real_size(size_t size)
{
	return ALIGN_UP(size, MEMORY_ALIGNMENT) + HEAP_BLOCK_METADATA_SIZE;
}

/** Count the address of the data part of the heap block.
 *
 * Data part of the block is right after the block head.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the data part of the block.
 *
 */
static void *heap_get_block_data_addr(void *block_start_addr)
{
	return block_start_addr + sizeof(heap_block_head_t);
}

/** Get the address of a block which is after the block with passed
 *  @p block_start_addr.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the next block.
 *
 */
static heap_block_head_t *heap_get_next_block_addr(void *block_start_addr)
{
	heap_block_head_t *block_head = (heap_block_head_t *)block_start_addr;

	return block_start_addr + block_head->size;
}

/** Get the address of a block which is before the block with passed
 *  @p block_start_addr.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the previous block.
 *
 */
static heap_block_head_t *heap_get_prev_block_addr(void *block_start_addr)
{
	heap_block_foot_t *prev_foot = (heap_block_foot_t *)(block_start_addr - sizeof(heap_block_foot_t));
	heap_block_head_t *prev_head = (heap_block_head_t *)(block_start_addr - prev_foot->size);

	return prev_head;
}

/** Get the structure with metadata about superblock based on the
 *  superblock address.
 *
 * @param  superblock_start_addr Address of the superblock start.
 *
 * @return Structure with superblock metadata.
 *
 */
static heap_superblock_info_t *heap_get_superblock_info(void *superblock_start_addr)
{
	return (heap_superblock_info_t*)heap_get_block_data_addr(superblock_start_addr);
}

/** Get the free blocks list where the block of specified @p size should be.
 *
 * @param  size Size of the block.
 *
 * @return Free blocks list where the block of specified @p size should be.
 *
 */
static list_t *heap_get_free_blocks_list(size_t size)
{
	if (size < 64) {
		return &free_blocks_list_less_than_64b;
	} else if (size < 128) {
		return &free_blocks_list_64b_128b;
	} else if (size < 512) {
		return &free_blocks_list_128b_512b;
	} else if (size < FRAME_SIZE / 2) {
		return &free_blocks_list_512b_2k;
#ifdef KERNEL_TEST
	/* Trick for the little speed up of malloc test. */
	} else if (size < heap_count_block_real_size(2500)) {
#else
	} else if (size < 2 * FRAME_SIZE) {
#endif
		return &free_blocks_list_2k_8k;
	} else if (size < 8 * FRAME_SIZE) {
		return &free_blocks_list_8k_32k;
	} else if (size < 32 * FRAME_SIZE) {
		return &free_blocks_list_32k_128k;
	} else {
		return &free_blocks_list_more_than_128k;
	}
}

/** Get the free blocks list which contains bigger blocks
 *  than passed @p blocks_list.
 *
 * @param  blocks_list Free blocks list.
 *
 * @return Free blocks list which contains bigger blocks than passed @p blocks_list.
 *
 */
static list_t *heap_get_next_free_blocks_list(list_t *blocks_list)
{
	if (blocks_list == &free_blocks_list_less_than_64b) {
		return &free_blocks_list_64b_128b;
	} else if (blocks_list == &free_blocks_list_64b_128b) {
		return &free_blocks_list_128b_512b;
	} else if (blocks_list == &free_blocks_list_128b_512b) {
		return &free_blocks_list_512b_2k;
	} else if (blocks_list == &free_blocks_list_512b_2k) {
		return &free_blocks_list_2k_8k;
	} else if (blocks_list == &free_blocks_list_2k_8k) {
		return &free_blocks_list_8k_32k;
	} else if (blocks_list == &free_blocks_list_8k_32k) {
		return &free_blocks_list_32k_128k;
	} else if (blocks_list == &free_blocks_list_32k_128k) {
		return &free_blocks_list_more_than_128k;
	} else {
		return NULL;
	}
}

/** Count all the free blocks in free blocks lists.
 *
 * @return Count of free blocks on heap.
 *
 */
static size_t heap_count_free_blocks_in_lists(void)
{
	return list_count(&free_blocks_list_less_than_64b)
			+ list_count(&free_blocks_list_64b_128b)
			+ list_count(&free_blocks_list_128b_512b)
			+ list_count(&free_blocks_list_512b_2k)
			+ list_count(&free_blocks_list_2k_8k)
			+ list_count(&free_blocks_list_8k_32k)
			+ list_count(&free_blocks_list_32k_128k)
			+ list_count(&free_blocks_list_more_than_128k);
}

/** Append the block to the list of free blocks.
 *
 * @param block_head Address of the heap block.
 *
 */
static void heap_append_block_to_free_list(heap_block_head_t *block_head)
{
	assert(block_head->type == HEAP_BLOCK_TYPE_FREE);
	heap_free_block_info_t *block_info = (heap_free_block_info_t *)heap_get_block_data_addr(block_head);
	item_init(&block_info->list_item, block_head);
	list_append(heap_get_free_blocks_list(block_head->size), &block_info->list_item);
}

/** Remove the block from the list of free blocks.
 *
 * @param block_head Address of the heap block.
 *
 */
static void heap_remove_block_from_free_list(heap_block_head_t *block_head)
{
	assert(block_head->type == HEAP_BLOCK_TYPE_FREE);
	heap_free_block_info_t *block_info = (heap_free_block_info_t *)heap_get_block_data_addr(block_head);
	list_remove(heap_get_free_blocks_list(block_head->size), &block_info->list_item);
}

/** Initialize a heap block.
 *
 * Fills in the structures related to a heap block. When new free block
 * is created it is added to the list of free blocks. When new allocated
 * block is created it is removed from the list of free blocks (we only
 * create allocated block from the free block).
 *
 * @param addr Address of the block.
 * @param size Size of the block including the header and the footer.
 * @param type Type of the block.
 *
 */
static void heap_block_init(void *addr, size_t size, heap_block_type_t type)
{
	/* Calculate the position of the header and the footer. */
	heap_block_head_t *head = (heap_block_head_t *)(addr);
	heap_block_foot_t *foot = (heap_block_foot_t *)(addr + size - sizeof(heap_block_foot_t));

	/* Remove the block from the list of free blocks when it is an allocated block. */
	if (type == HEAP_BLOCK_TYPE_ALLOCATED) {
		heap_remove_block_from_free_list(head);
	}

	/* Fill the free memory with zeroes when debugging is on. */
#if DEBUG_HEAP >= 1
	if (type == HEAP_BLOCK_TYPE_FREE) {
		memory_fill_with_null(head, head->size);
	}
#endif

	/* Fill the header. */
	head->size = size;
	head->type = type;
	head->magic = HEAP_BLOCK_HEAD_MAGIC;

	/* Fill the footer. */
	foot->magic = HEAP_BLOCK_FOOT_MAGIC;
	foot->size = size;

	/* Append the block to the list of free blocks when it is a free block. */
	if (type == HEAP_BLOCK_TYPE_FREE) {
		heap_append_block_to_free_list(head);
	}
}

/** Allocate new superblock.
 *
 * When no superblock is allocated, the ::NULL is returned. The superblock
 * resides in the frames allocated from the frame allocator.
 *
 * After allocation the superblock is divided into three heap blocks.
 * First of them is the block with superblock header which contains
 * superblock metadata in its data section. On the superblock end is the
 * block which is the indication of the superblock end. And between them
 * is one free block which covers the rest of the superblock space.
 *
 * @param  frames_cnt Total number of frames to be allocated.
 *
 * @return The address of the superblock or ::NULL when the frame
 *         allocation was not successful.
 *
 */
static void *heap_alloc_superblock(const size_t frames_cnt)
{
	void *superblock_phys_addr;
	int ret = frame_alloc(&superblock_phys_addr, frames_cnt, VF_AT_KSEG0 | VF_VA_AUTO);

	if (ret != EOK)  {
		heap_debug_print(
			"\nMalloc warning: We are going to be out of the memory - frame allocation failed!\n"
			"Returned value from frame_alloc: %s.\n", code_name(ret)
		);
		return NULL;
	}

	size_t superblock_size = frames_cnt * FRAME_SIZE;
	size_t start_block_size = heap_count_block_real_size(sizeof(heap_superblock_info_t));
	size_t end_block_size = heap_count_block_real_size(0);
	size_t main_free_block_size = superblock_size - start_block_size - end_block_size;

	void *start_block_addr = PHYS_ADDR_TO_KSEG0_ADDR(superblock_phys_addr);
	void *free_block_addr = start_block_addr + start_block_size;
	void *end_block_addr = free_block_addr + main_free_block_size;

	/* Init the first block with superblock meta data. */
	heap_block_init(start_block_addr, start_block_size, HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD);
	heap_block_init(free_block_addr, main_free_block_size, HEAP_BLOCK_TYPE_FREE);
	heap_block_init(end_block_addr, end_block_size, HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT);

	heap_superblock_info_t *superblock_info = heap_get_superblock_info(start_block_addr);
	superblock_info->frames_cnt = frames_cnt;
	item_init(&superblock_info->list_item, start_block_addr);
	list_append(&superblocks_list, &superblock_info->list_item);

	return start_block_addr;
}

/** Initialize the heap allocator.
 *
 * Creates the heap management structures and initializes the first
 * heap superblock - in this superblock are three blocks on the start.
 * First of them is the block with superblock header which contains
 * superblock metadata in its data section. On the superblock end is the
 * block which is the indication of the superblock end. And between them
 * is one free block which covers the rest of the superblock space.
 *
 */
void init_heap(void)
{
	list_init(&superblocks_list);
	list_init(&free_blocks_list_less_than_64b);
	list_init(&free_blocks_list_64b_128b);
	list_init(&free_blocks_list_128b_512b);
	list_init(&free_blocks_list_512b_2k);
	list_init(&free_blocks_list_2k_8k);
	list_init(&free_blocks_list_8k_32k);
	list_init(&free_blocks_list_32k_128k);
	list_init(&free_blocks_list_more_than_128k);

	/** We need a recursive mutex when we print the heap inside of malloc/free. */
	mutex_init_recursive(&heap_mutex);

	/** We allocate some predefined number of frames for start. */
	void *heap_start = heap_alloc_superblock(HEAP_INIT_FRAMES_COUNT);

	/** The allocation of default number of frames failed -> we can not start the kernel. */
	if (heap_start == NULL) {
		panic("Could not initialize heap - frame allocation failed!");
	}
}

/** Try to find the free block of suitable size for the block with passed size.
 *
 * When no block is found in the list with free blocks, then ::NULL is returned.
 *
 * @param  size Required size of the free block.
 *
 * @return Address of the suitable free block or ::NULL when no suitable
 *         free block exists.
 *
 */
static heap_block_head_t *heap_find_free_block(const size_t size)
{
	list_t *free_blocks_list = heap_get_free_blocks_list(size);
	list_it_t it;

	do {
		/* Loop through the list of all free blocks. */
		for (list_it_init(&it, free_blocks_list); list_it_has_current(&it); list_it_next(&it)) {
			heap_block_head_t *block_head = (heap_block_head_t *)list_it_data(&it);
			heap_block_check(block_head);

			/* Just some basic control of the list. */
			if (block_head->type != HEAP_BLOCK_TYPE_FREE) {
				panic("Corrupted kernel heap: there is not free block in the free blocks list!");
			}

			/* Have we found the suitable block? */
			if (block_head->size >= size) {
				return block_head;
			}
		}

		/* We have to loop through all the free blocks lists that contain bigger blocks. */
		free_blocks_list = heap_get_next_free_blocks_list(free_blocks_list);
	} while (free_blocks_list != NULL);

	return NULL;
}

/** Try to stretch the heap by allocating new superblock.
 *
 * @param  size The size of the block to allocate.
 *
 * @return The address of the superblock or ::NULL when there is
 *         not enough memory.
 *
 */
static heap_block_head_t *heap_try_stretch(const size_t size)
{
	/* Find out how many frames we need for the block of required real size.
	   We have to add little more, because we need to place somewhere the
	   superblock metadata. */
	size_t start_block_size = heap_count_block_real_size(sizeof(heap_superblock_info_t));
	size_t end_block_size = heap_count_block_real_size(0);
	size_t total_size = start_block_size + size + end_block_size;
	size_t frames_cnt = ALIGN_UP(total_size, FRAME_SIZE) / FRAME_SIZE;
	frames_cnt = max(frames_cnt, HEAP_ALLOC_FRAMES_COUNT);

	/* Optimize the frame utilization of frame allocator. */
	frames_cnt = pow2_roundup(frames_cnt);

	/* Debug. */
	heap_debug_print("-- MALLOC: Trying to allocate new %u frames.\n", frames_cnt);

	/* Try to allocate new superblock. */
	heap_block_head_t *block_info = heap_alloc_superblock(frames_cnt);

	if (block_info != NULL) {
		/* We were successful at allocating new frames for the heap. Now we have
		   to move to the next block, because the first block is just block for
		   superblock metadata. Next block is free and it is sure that it is
		   big enough. */
		block_info = heap_get_next_block_addr(block_info);
	}

	return block_info;
}

/** Allocate a memory block.
 *
 * When there is no suitable free block for the new block with passed
 * size, the new superblock is allocated.
 *
 * @param  size The size of the block to allocate.
 *
 * @return The address of the block or ::NULL when there is not enough memory.
 *
 */
void *malloc(const size_t size)
{
	/* Lock while accessing shared structures. */
	mutex_lock(&heap_mutex);
	heap_debug_print("-- MALLOC: Trying to allocate %u bytes.\n", size);

	/* We have to allocate a bit more to have room for
	   header and footer. The size of the memory block
	   also has to be 4 bytes aligned to avoid unaligned
	   memory access exception while accessing the
	   footer structure. */
	size_t real_size = heap_count_block_real_size(max(size, HEAP_FREE_BLOCK_DATA_SIZE));
	heap_block_head_t *block_info = heap_find_free_block(real_size);
	void *result = NULL;

	if (block_info == NULL) {
		/* Try to stretch the heap by allocating new superblock. */
		block_info = heap_try_stretch(real_size);
	}

	if (block_info != NULL) {
		/* We can only split the block when both of the smaller blocks
		   would fit into that block. */
		size_t split_limit = real_size + HEAP_FREE_BLOCK_SIZE;

		if (block_info->size > split_limit) {
			/* Block big enough -> split. */
			void *next = ((void *)block_info) + real_size;
			size_t free_block_size = block_info->size - real_size;

			/* Initialize the two smaller blocks. */
			heap_block_init(block_info, real_size, HEAP_BLOCK_TYPE_ALLOCATED);
			heap_block_init(next, free_block_size, HEAP_BLOCK_TYPE_FREE);

			/* Debug. */
			heap_debug_print("-- MALLOC: Splitting the block at %p into two blocks.\n", block_info);
		} else {
			/* Block too small -> use as is. */
			heap_block_init(block_info, block_info->size, HEAP_BLOCK_TYPE_ALLOCATED);

			/* Debug. */
			heap_debug_print("-- MALLOC: Leaving the block at %p as is.\n", block_info);
		}

		/* Either way we have our result. */
		result = ((void *)block_info) + sizeof(heap_block_head_t);
		heap_debug_print("-- MALLOC: Allocated new block at %p.\n", result);
	} else {
		heap_debug_print("-- MALLOC: Unsuccessful - no memory.\n");
	}

	mutex_unlock(&heap_mutex);
	return result;
}

/** Allocate a memory block.
 *
 * Unlike standard ::malloc, this routine never returns ::NULL to
 * indicate an out of memory condition. Recovering from an out
 * of memory condition in the kernel is difficult and we never
 * really need it in the example kernel.
 *
 * @param  size The size of the block to allocate.
 *
 * @return The address of the block.
 *
 */
void *safe_malloc(const size_t size)
{
	void *result = malloc(size);
	assert(result != NULL);
	return result;
}

/** Free a memory block.
 *
 * The address of the block must be a block allocated in the past by
 * ::malloc function.
 *
 * @param addr The address of the block.
 *
 */
void free(const void *addr)
{
	/* Calculate the position of the header. */
	heap_block_head_t *block_head = (heap_block_head_t *)(addr - sizeof(heap_block_head_t));

	/* Make sure the block is not corrupted and has been allocated in the past. */
	heap_block_check(block_head);
	assert(block_head->type == HEAP_BLOCK_TYPE_ALLOCATED);

	/* Lock while accessing shared structures. */
	mutex_lock(&heap_mutex);
	heap_debug_print("-- FREE: Releasing the block at %p.\n", block_head);

	/* Mark the block itself as free. */
	heap_block_head_t *new_block_addr = block_head;
	size_t new_block_size = block_head->size;

	/* Look at the next and previous blocks. If they are free, merge them together. */
	heap_block_head_t *prev_head = heap_get_prev_block_addr(block_head);
	heap_block_head_t *next_head = heap_get_next_block_addr(block_head);

	/* Check the previous neighbour of the deallocated block. */
	heap_block_check(prev_head);

	if (prev_head->type == HEAP_BLOCK_TYPE_FREE) {
		heap_remove_block_from_free_list(prev_head);
		new_block_size += prev_head->size;
		new_block_addr = prev_head;
		heap_debug_print("-- FREE: Joining the block with previous block at %p.\n", prev_head);
	}

	/* Check the next neighbour of the deallocated block. */
	heap_block_check(next_head);

	if (next_head->type == HEAP_BLOCK_TYPE_FREE) {
		heap_remove_block_from_free_list(next_head);
		new_block_size += next_head->size;
		heap_debug_print("-- FREE: Joining the block with next block at %p.\n", next_head);
	}

	/* Init the newly counted free block. */
	heap_block_init(new_block_addr, new_block_size, HEAP_BLOCK_TYPE_FREE);
	heap_debug_print("-- FREE: Initializing new block at %p of size %u.\n", new_block_addr, new_block_size);

	/* We can free the superblock when it contains only one block and it is
	   this newly created free block. When there are no superblocks, then
	   new superblock would be allocated by the malloc function. But this
	   situation never happens, because the system thread is allocated in
	   the first superblock and it resides there all the time the kernel runs. */
	prev_head = heap_get_prev_block_addr(new_block_addr);
	next_head = heap_get_next_block_addr(new_block_addr);

	if (prev_head->type == HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD && next_head->type == HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT) {
		/* Get info about the superblock (superblock starts at the same address
		   as its head - its first block). */
		heap_superblock_info_t *superblock_info = heap_get_superblock_info(prev_head);

		/* Some simple assertion for the superblock consictency. */
		assert(prev_head->size + new_block_addr->size + next_head->size == superblock_info->frames_cnt * FRAME_SIZE);

		/* Debug. */
		heap_debug_print("-- FREE: Releaseing superblock at %p, %u frames.\n", prev_head, superblock_info->frames_cnt);

		/* First of all remove the free block from the free blocks list. */
		heap_remove_block_from_free_list(new_block_addr);

		/* Then remove the superblock from the list of superblocks. */
		list_remove(&superblocks_list, &superblock_info->list_item);

		/* At finally we can deallocate all the frames that the superblock consists of. */
		if (frame_free(VA_DIRECT_TO_PHYSICAL(prev_head), superblock_info->frames_cnt) != EOK) {
			panic("Could not free the frame previously allocated by malloc.");
		}
	}

	heap_debug_print("-- FREE: Block %p released.\n", addr);
	mutex_unlock(&heap_mutex);
}

/** Check the consistency of the heap.
 *
 */
void heap_check(void)
{
	mutex_lock(&heap_mutex);
	printk("\nStarting to check the heap...\n");

	size_t free_block_count = 0;
	list_it_t it;

	/** Lets go through the list of all superblocks and control
	    the consistency of each of them. */
	for (list_it_init(&it, &superblocks_list); list_it_has_current(&it); list_it_next(&it)) {
		/* Superblock start address is the data of its list item. */
		void *superblock_start = list_it_data(&it);

		/* Get the metadata about superblock. */
		heap_superblock_info_t *superblock_info = heap_get_superblock_info(superblock_start);

		printk("-- Entering superblock: at %p, %u frames\n", superblock_start, superblock_info->frames_cnt);

		/* Get the first block in the superblock. */
		heap_block_head_t *block_info = (heap_block_head_t *)superblock_start;
		assert(block_info->type == HEAP_BLOCK_TYPE_SUPERBLOCK_HEAD);

		/* We will count the total size of the superblock.  */
		size_t total_size = 0;
#ifndef NDEBUG
		size_t superblock_size = superblock_info->frames_cnt * FRAME_SIZE;
#endif

		/* Loop through all the blocks in the superblock. */
		while (true) {
			printk(
				"Checking block %s: %p, %u bytes\n", heap_tr_block_type(block_info->type), block_info, block_info->size
			);

			/* Check the block itself. */
			heap_block_check(block_info);

			/* Increase the total size of the superblock. */
			total_size += block_info->size;

			if (block_info->type == HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT) {
				/* We have found the superblock end. */
				break;
			} else if (block_info->type == HEAP_BLOCK_TYPE_FREE) {
				/* We check whether the free block is in the free blocks list. */
#ifndef NDEBUG
				heap_free_block_info_t *free_block_info = (heap_free_block_info_t *)heap_get_block_data_addr(block_info);
				assert(list_contains(heap_get_free_blocks_list(block_info->size), &free_block_info->list_item) == true);
#endif
				free_block_count++;
			}

			/* Check whether the superblock footer is not corrupted. */
			assert(((void *)block_info) < superblock_start + superblock_size);

			/* Get the next heap block. */
			block_info = heap_get_next_block_addr(block_info);
		}

		printk("-- Leaving superblock %p\n", superblock_start);

		/* Control the consistency of superblock total size. */
		assert(total_size == superblock_size);
	}

	/* Control whether we have the right amount of free blocks in the
	   free blocks list. */
	assert(free_block_count == heap_count_free_blocks_in_lists());
	printk("Heap is all right!\n\n");
	mutex_unlock(&heap_mutex);
}

/** Print out the whole heap structure.
 *
 * Printing out of the heap structure means printing out its superblocks
 * and the blocks in each of those superblocks.
 *
 */
void heap_print(void)
{
	mutex_lock(&heap_mutex);
	printk("\n-------------------------- PRINTING THE HEAP --------------------------\n");

	uint32_t superblock_count = 0;
	uint32_t free_block_count = 0;
	uint32_t allocated_block_count = 0;
	list_it_t it;

	/* Lets go through the list of all superblocks. */
	for (list_it_init(&it, &superblocks_list); list_it_has_current(&it); list_it_next(&it)) {
		/* Superblock start address is the data of its list item. */
		void *superblock_start = list_it_data(&it);

		/* Get the metadata about superblock. */
		heap_superblock_info_t *superblock_info = heap_get_superblock_info(superblock_start);

		/* Get the first block in the superblock. */
		heap_block_head_t *block_info = (heap_block_head_t *)superblock_start;
		uint32_t block_count = 0;

		printk("---- ENTERING SUPERBLOCK: at %p, %u frames ----\n", superblock_start, superblock_info->frames_cnt);

		/* Loop through all the blocks in the superblock. */
		while (true) {
			heap_block_head_t *next_block_info = heap_get_next_block_addr(block_info);
			printk("Block %s: %p-%p, %u bytes\n", heap_tr_block_type(block_info->type), block_info, next_block_info, block_info->size);
			block_count++;

			if (block_info->type == HEAP_BLOCK_TYPE_SUPERBLOCK_FOOT) {
				/* We have found the superblock end. */
				break;
			} else if (block_info->type == HEAP_BLOCK_TYPE_FREE) {
				free_block_count++;
			} else if (block_info->type == HEAP_BLOCK_TYPE_ALLOCATED) {
				allocated_block_count++;
			}

			block_info = next_block_info;
		}

		printk("---- LEAVING SUPERBLOCK %p, total %u blocks ----\n\n", superblock_start, block_count);
		superblock_count++;
	}

	printk("Heap superblocks count: %u\n", superblock_count);
	printk("Heap allocated blocks count: %u\n", allocated_block_count);
	printk("Heap free blocks count: %u\n", free_block_count);
	printk("Heap free list items count: %u\n", heap_count_free_blocks_in_lists());
	printk("-------------------------- HEAP PRINTED -------------------------------\n\n");
	mutex_unlock(&heap_mutex);
}

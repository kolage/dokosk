/**
 * @file kernel/drivers/keyboard.c
 *
 * Implementation of simple standard input support.
 *
 * Access to the keyboard is protected by recursive mutex. Read characters
 * are added to the buffer which is then consumed by ::getc, ::getc_try or
 * ::gets functions. When the buffer is full, newly produced characters
 * are just forgotten.
 *
 * This file implements @ref io_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page io_feature Input and output
 *
 * This page describes how the drivers for input and output are implemented.
 *
 * Both input and output functions are protected with recursive mutexes, so only
 * one thread at the time has access to the input or output device (dkeyboard
 * or dprinter).
 *
 * In kernel all of the printer functions have their _nb (non-blocking) variant
 * which does not use the mutex. Those are useful for debugging for example when
 * checking some behavior in mutexes or printer itself.
 *
 * Characters read from the keyboard are added to the buffer of constant size.
 * This buffer is then consumed by ::getc, ::getc_try or ::gets functions.
 * When the buffer is empty ::getc and ::gets function passively wait on
 * condition variable. Handle of keyboard interrupt then signals on this condition
 * variable, so the waiting thread knows it can consume the buffer. When the buffer
 * is full, newly produced characters are just forgotten.
 *
 * The pointer to the string passed to the ::syscall_puts function is
 * partially validated. We do not check whether the whole string is in the mapped virtual memory,
 * but when it is not, then the calling thread is killed because of the TLB refill
 * exception.
 *
 * It is very important to ensure that some of the I/O mutexes does not remain
 * locked when the user space thread is cancelled while it is holding it or when the whole
 * process is killed. This is ensured by couple of ::THREAD_ENTER_CRITICAL_SYSCALL_SECTION
 * and ::THREAD_LEAVE_CRITICAL_SYSCALL_SECTION macros in ::syscall_puts
 * function. But we can not use this mechanism also in ::syscall_getc function,
 * because it has to be possible to cancel the thread which is waiting for the input
 * from keyboard. So there is no special protection, but when a thread is cancelled
 * or a process is killed, ::keyboard_release_mutex function is called which
 * unlocks the ::keyboard_mtx when it was currently locked by cancelled thread.
 *
 * See also @userpage{io_feature, User space Input and Output}.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <drivers/keyboard.h>
#include <drivers/printer.h>
#include <sync/cond.h>
#include <sync/mutex.h>


/** Size of the keyboard buffer. */
#define KEYBOARD_BUFFER_SIZE 32


/** Keyboard buffer structure.
 *
 */
typedef struct {
	/** Buffer for the characters from keyboard. */
	char buffer[KEYBOARD_BUFFER_SIZE];

	/** Actual number of characters in the buffer. */
	uint8_t count;

	/** Mutex for the buffer. */
	mutex_t mtx;

	/** Condition variable for the buffer. */
	cond_t cvar;
} keyboard_buffer_t;


/* Keyboard buffer. */
static keyboard_buffer_t keyboard_buffer;

/* Mutex for locking the keyboard, because it is accessed concurrently.
   Second thread has to wait until the first thread finishes reading of
   the whole string. */
static mutex_t keyboard_mtx;


/** Initialize the keyboard buffer.
 *
 * @param kb Keyboard buffer to get initialized.
 *
 */
static void keyboard_buffer_init(keyboard_buffer_t *kb)
{
	kb->count = 0;
	mutex_init(&kb->mtx);
	cond_init(&kb->cvar);
}

/** Add the passed character to the keyboard buffer.
 *
 * When the buffer is full, the character is ignored.
 *
 * @param kb   Keyboard buffer.
 * @param c Character to be added to the buffer.
 *
 */
static void keyboard_buffer_put(keyboard_buffer_t *kb, char c)
{
	mutex_lock(&kb->mtx);
	int count = kb->count;

	if (count < KEYBOARD_BUFFER_SIZE) {
		kb->buffer[count] = c;
		kb->count++;
	}

	/* Signal to the waiting thread that there is something in the
	   keyboard buffer. */
	cond_signal(&kb->cvar);
	mutex_unlock(&kb->mtx);
}

/** Get the first character from the keyboard buffer.
 *
 * This might get to unexpected results when the buffer is empty.
 * But this function is called with the mutex locked, so this
 * should never happen.
 *
 * @param  kb     Keyboard buffer.
 * @param  wait   Wait for some character when the buffer is empty?
 * @param  remove Remove the read character from the buffer?
 *
 * @return First character from the keyboard buffer.
 *
 */
static char keyboard_buffer_get(keyboard_buffer_t *kb, bool wait, bool remove)
{
	mutex_lock(&kb->mtx);

	if (wait) {
		/* Passively wait for until some character appears in the buffer. */
		while (kb->count == 0) {
			cond_wait(&kb->cvar, &kb->mtx);
		}
	}

	char buf = kb->buffer[0];

	if (kb->count > 0 && remove) {
		/* Shift the buffer. */
		for (int i = 0; i < kb->count - 1; i++) {
			kb->buffer[i] = kb->buffer[i + 1];
		}

		kb->count--;
	}

	mutex_unlock(&kb->mtx);
	return buf;
}

/** Initialize the keyboard buffer.
 *
 */
void init_keyboard(void)
{
	keyboard_buffer_init(&keyboard_buffer);
	mutex_init_recursive(&keyboard_mtx);
}

/** Handle the keyboard interrupt.
 *
 * Stores the character in the buffer.
 *
 */
void keyboard_handle(void)
{
	char c = *((volatile char *)KEYBOARD_ADDRESS);
	keyboard_buffer_put(&keyboard_buffer, c);
}

/** Release the mutex to the keyboard if the passed thread is holding it.
 *
 * @param thread Thread that might hold the keyboard mutex.
 *
 */
void keyboard_release_mutex(thread_impl_t *thread)
{
	ipl_t state = query_and_disable_interrupts();

	while (keyboard_mtx.locked_thread == thread) {
		mutex_unlock_unchecked(&keyboard_mtx);
	}

	while (keyboard_buffer.mtx.locked_thread == thread) {
		mutex_unlock_unchecked(&keyboard_buffer.mtx);
	}

	conditionally_enable_interrupts(state);
}

/** Read the character from the standard input device.
 *
 * This is a blocking function which waits passively until some
 * character is typed.
 *
 * @return Character read from the standard input device.
 *
 */
char getc(void)
{
	mutex_lock(&keyboard_mtx);
	int key = keyboard_buffer_get(&keyboard_buffer, true, true);
	mutex_unlock(&keyboard_mtx);

	return (char)key;
}

/** Try to read the character from the buffer of standard input device.
 *
 * @return Character from the standard input device buffer as integer or
 *         ::EWOULDBLOCK when the buffer is empty.
 *
 */
int getc_try(void)
{
	int key = EWOULDBLOCK;
	mutex_lock(&keyboard_mtx);

	/* We do not lock the keyboard buffer here, because no one else can remove
	   the character from the buffer in the meantime, because we have locked
	   the keyboard_mtx.  */
	if (keyboard_buffer.count > 0) {
		key = keyboard_buffer_get(&keyboard_buffer, false, true);
	}

	mutex_unlock(&keyboard_mtx);

	return key;
}

/** Get the character from the standard input device.
 *
 * This is a blocking function which waits passively until some character
 * is typed. This function just reads the character, it does not remove
 * it from the std input buffer.
 *
 * @return Character read from the standard input device.
 *
 */
char checkc(void)
{
	mutex_lock(&keyboard_mtx);
	int key = keyboard_buffer_get(&keyboard_buffer, true, false);
	mutex_unlock(&keyboard_mtx);

	return (char)key;
}

/** Get the string from the standard input device and save it to the
 *  parameter @p str.
 *
 * Strings are ended by the "\n" character which is also included in
 * the read string.
 *
 * @param  str Pointer to the memory where the read string should be saved.
 * @param  len Maximal length of the read string.
 *
 * @return Number of read characters without the ending '\0' character.
 *
 */
ssize_t gets(char *str, const size_t len)
{
	/* Length must be greater than 0. */
	if (len <= 0) {
		return EINVAL;
	}
	
	/* Read from the standard input. */
	int total = 0;
	int max_loops = len - 1;

	mutex_lock(&keyboard_mtx);

	for (int i = 0; i < max_loops; i++) {
		int key = getc();
		*str = key;
		total++;
		str++;
		
		if (key == '\n') {
			break;
		}
	}
	
	mutex_unlock(&keyboard_mtx);

	/* Add the ending zero character. */
	*str = '\0';
	
	return total;
}

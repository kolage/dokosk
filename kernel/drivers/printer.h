/**
 * @file kernel/drivers/printer.h
 *
 * Declarations of simple standard output support.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef PRINTER_H_
#define PRINTER_H_

#include <include/shared.h>
#include <include/c.h>
#include <proc/thread.h>


/** Hardwired address of the dprinter device. */
#define PRINTER_ADDRESS  (ADDR_IN_KSEG0(0x10000000))


/* Externals are commented with implementation. */
extern void init_printer (void);

extern size_t putc_nb(const char c);
extern size_t putc(const char c);
extern size_t puts_nb(const char *str);
extern size_t puts(const char *str);
extern size_t putx(const int32_t num, const bool align);
extern size_t putu(const uint32_t num, uint8_t base);
extern size_t puti(const int32_t num, uint8_t base);
extern size_t printk_list(const char *format, va_list argptr);
extern size_t printk_nb(const char *format, ...);
extern size_t printk(const char *format, ...);

extern __attribute__((noreturn)) void panic_list(const char *format, va_list argptr);
extern __attribute__((noreturn)) void panic(const char *format, ...);

#endif /* DORDER_H_ */

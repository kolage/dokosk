/**
 * @file kernel/drivers/keyboard.h
 *
 * Declarations of simple standard input support.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <include/shared.h>
#include <include/c.h>
#include <proc/thread.h>


/** Hardwired address of the dkeyboard device. */
#define KEYBOARD_ADDRESS	(ADDR_IN_KSEG0(0x1000000C))


/* Externals are commented with implementation. */
extern void init_keyboard(void);
extern void keyboard_handle(void);
extern void keyboard_release_mutex(thread_impl_t *thread);

extern char getc(void);
extern int getc_try(void);
extern char checkc(void);
extern ssize_t gets(char *str, const size_t len);

#endif /* KEYBOARD_H_ */

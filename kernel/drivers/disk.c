/**
 * @file kernel/drivers/disk.c
 *
 * Implementation of disk device management.
 *
 * Support for ddisk device in MSIM.
 *
 * Reading a sector from the disk is implemented in ::disk_read_sector function.
 *
 * This file implements @ref disk_feature.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** @page disk_feature Disk device
 *
 * This page describes how we manage communication with ddisk device.
 *
 * There is a basic support for reading data from MSIM ddisk device in kernel.
 * Data from specific disk sector could be read into preallocated buffer.
 *
 * When read function is called, disk is protected with mutex to prevent
 * concurrent disk accessing. At first the passed sector number is checked if
 * it is in valid range. Then ddisk registers are set up, namely the buffer
 * address where data should be stored, sector number and the bit indicating
 * read operation initiation. At this point, the initiating thread is suspended
 * until data are prepared. When disk writes data into buffer, the DMA interrupt
 * is caused which is handled by ::disk_handle procedure. Here the disk
 * operation status is checked and initiating thread is awakened. Also DMA
 * interrupt must be c to confirm data transfer.
 *
 * During development we had a bit of trouble with MSIM ddisk documentation,
 * because when read operation is initiated, the ddisk status register is not
 * reflecting this change although interrupt request is handled. Also in first
 * version of our ddisk implementation the static buffer for reading data from
 * disk was used, so its address was set up only once during initialization.
 * But this was leading to improper behavior, because ddisk is internally
 * incrementing its buffer address during data read which is again inconsistent
 * with the documentation.
 *
 */

#include <drivers/disk.h>
#include <drivers/printer.h>
#include <mm/malloc.h>
#include <include/codes.h>
#include <proc/process.h>


/** Status of last disk operation. */
static int last_status;

/** Mutex for locking the disk, because it is accessed concurrently. */
static mutex_t disk_mtx;

/** Condition variable for signaling prepared data. */
static cond_t disk_cvar;


/** Check if user is accessing valid sector.
 *
 * @param  sector Sector number.
 *
 * @return ::true if sector is valid, otherwise ::false.
 *
 */
static bool disk_check_valid_sector(int sector)
{
	return sector < disk_sectors();
}

/** Worker procedure for reading data sector from disk.
 *
 * Sends request for data read at specified sector number and
 * waits until data are loaded.
 *
 * @param  sector Sector number.
 * @param  buffer Allocated buffer where should data be stored.
 *
 * @return ::EOK if data were successfully read, otherwise ::EINVAL.
 *
 */
static int disk_read_sector_proc(int sector, char *buffer)
{
	if (!disk_check_valid_sector(sector)) {
		return EINVAL;
	}

	set_disk_buffer_address(&buffer[0]);
	set_disk_sector_number(sector);
	initiate_disk_read();
	cond_wait(&disk_cvar, &disk_mtx);

	return last_status;
}

/** Initialize the disk device.
 *
 */
void init_disk(void)
{
	cond_init(&disk_cvar);
	mutex_init(&disk_mtx);
}

/** Handle the disk device interrupt.
 *
 * After the data from disk are loaded into user buffer, the DMA
 * interrupt is initiated, which is handled by this function.
 *
 * Besides disk operation status check, DMA interrupt is deasserted
 * as a confirmation of data receipt.
 *
 * In the end the user process (waiting inside disk_read_sector_proc)
 * is informed about completed disk operation.
 *
 */
void disk_handle(void)
{
	ipl_t state = query_and_disable_interrupts();

	last_status = disk_operation_error() ? EINVAL : EOK;
	deassert_dma();

	conditionally_enable_interrupts(state);

	mutex_lock(&disk_mtx);
	cond_signal(&disk_cvar);
	mutex_unlock(&disk_mtx);
}

/** Read specified data sector from disk.
 *
 * Reads all bytes from specified data sector. Number of loaded bytes
 * are specified by ::DISK_SECTOR_SIZE constant.
 *
 * Buffer must be allocated with size corresponding to disk sector size.
 *
 * @param  sector Number of sector to be read.
 * @param  buffer Allocated buffer where should data be stored.
 *
 * @return ::EINVAL when disk operation failed, otherwise ::EOK.
 *
 */
int disk_read_sector(int sector, char *buffer)
{
	if (buffer == NULL) {
		return EINVAL;
	}

	mutex_lock(&disk_mtx);

	int result = disk_read_sector_proc(sector, buffer);

	mutex_unlock(&disk_mtx);

	return result;
}

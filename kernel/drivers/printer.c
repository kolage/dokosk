/**
 * @file kernel/drivers/printer.c
 *
 * Implementation of standard output support.
 *
 * Std output is protected by the recursive mutex, so there is always an
 * exclusive access to the output for one thread at the time.
 *
 * We provide standard functions for working with std output like ::putc,
 * ::puts, ::printk and their non-blocking variants (with _nb suffix) which
 * do not use mutex. Those should be used carefully, because they bypass
 * the mechanism of exclusive access to the output. But those are useful
 * while debugging.
 *
 * This file implements @ref io_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/math.h>
#include <drivers/printer.h>
#include <sync/mutex.h>


/** Printer. */
static mutex_t printer_mutex;


/** Initialize the printer.
 *
 */
void init_printer(void)
{
	mutex_init_recursive(&printer_mutex);
}

/** Display a character.
 *
 * This method is from Kalisto and it was slightly modified. This
 * is a non-blocking variant of the method.
 *
 * @param  c The character to display.
 *
 * @return Number of printed characters.
 *
 */
size_t putc_nb(const char c)
{
	(*(volatile char *)PRINTER_ADDRESS) = c;
	return 1;
}

/** Display a character with locking.
 *
 * @param  c The character to display.
 *
 * @return Number of printed characters.
 *
 */
size_t putc(const char c)
{
	mutex_lock(&printer_mutex);
	putc_nb(c);
	mutex_unlock(&printer_mutex);

	return 1;
}

/** Print a zero terminated string - non-blocking variant.
 *
 * @param  text The string to print.
 *
 * @return Number of printed characters.
 *
 */
size_t puts_nb(const char *text)
{
	size_t total = 0;

	while (*text) {
		total += putc_nb(*text);
		text++;
	}

	return total;
}

/** Print a zero terminated string.
 *
 * @param  text The string to print.
 *
 * @return Number of printed characters.
 *
 */
size_t puts(const char *text)
{
	mutex_lock(&printer_mutex);
	size_t total = puts_nb(text);
	mutex_unlock(&printer_mutex);

	return total;
}

/** Send an integer in the hexadecimal system to the output.
 *
 * When the align parameter is set, then all the bits of the number
 * are written (including starting zeros). We can not use the ::puti function,
 * because we want to treat differently with negatives - we do not want
 * to write -num, but we want to write the real bits of the given number
 * (starting with 1, because the number is represented in the two's complement).
 *
 * This is a non-blocking variant of the ::putx function.
 *
 * @param  num   The number to send to the output.
 * @param  align Send the starting zeros to the output device?
 *
 * @return Number of printed characters.
 *
 */
static size_t putx_nb(const int32_t num, const bool align)
{
	const uint8_t bits_count = 32;
	const uint8_t bits_per_digit = 4;
	const uint32_t mask = 0xf0000000;
	size_t total = 0;
	uint32_t num_bits = num;
	bool send_output = align;

	/* Treat specially with zero. */
	if (!align && num == 0) {
		total += putc_nb('0');
	} else {
		for (int i = 0; i < bits_count / bits_per_digit; i++) {
			/* Get the top 4 bits of the number and shift them to the lowest
			   4 bits of the number -> we get one digit. */
			uint32_t digit = (num_bits & mask) >> (bits_count - bits_per_digit);
			num_bits = num_bits << bits_per_digit;
		
			if (send_output == false && digit > 0) {
				send_output = true;
			}
		
			if (send_output) {
				/* Digits 0-9. */
				if (digit < 10) {
					total += putc_nb(digit + '0');
				/* Digits a-f. */
				} else {
					total += putc_nb((digit - 10) + 'a');
				}
			}
		}
	}
	
	return total;
}

/** Send an integer in the hexadecimal system to the output.
 *
 * When the align parameter is set, then all the bits of the number
 * are written (including starting zeros). We can not use the ::puti function,
 * because we want to treat differently with negatives - we do not want to
 * write -num, but we want to write the real bits of the given number
 * (starting with 1, because the number is represented in the two's complement).
 *
 * @param  num   The number to send to the output.
 * @param  align Send the starting zeros to the output device?
 *
 * @return Number of printed characters.
 *
 */
size_t putx(const int32_t num, const bool align)
{
	mutex_lock(&printer_mutex);
	size_t total = putx_nb(num, align);
	mutex_unlock(&printer_mutex);

	return total;
}

/** Send an unsigned integer to the output device.
 *
 * This is a non-blocking variant of the ::putu function.
 *
 * @param  num  The unsigned number to send to the output.
 * @param  base Base of the system in which the number should be
 *              written: 2-16, 10 for the standard decimal system.
 *
 * @return Number of printed characters.
 *
 */
static size_t putu_nb(const uint32_t num, uint8_t base)
{
	base = max(base, 2);
	base = min(base, 16);
	size_t total = 0;

	/* Treat specially with zero. */
	if (num == 0) {
		total += putc_nb('0');
	/* Non-zero value. */
	} else {
		/* Read the number into the buffer. The size of 11 should be
		   ok for the 32-bit numbers. */
		uint32_t num_dyn = num;
		int8_t buf_size = 0;
		char buf[11];

		while (num_dyn != 0) {
			uint32_t digit = num_dyn % base;

			/* Digits 0-9. */
			if (digit < 10) {
				buf[buf_size] = digit + '0';
			/* Digits a-f. */
			} else {
				buf[buf_size] = (digit - 10) + 'a';
			}

			num_dyn /= base;
			buf_size++;
		}

		/* Send the number to the output. */
		for (int8_t i = buf_size - 1; i >= 0; i--) {
			total += putc_nb(buf[i]);
		}
	}

	return total;
}

/** Sends an unsigned integer to the output device.
 *
 * @param  num  The unsigned number to send to the output.
 * @param  base Base of the system in which the number should be
 *              written: 2-16, 10 for the standard decimal system.
 *
 * @return Number of printed characters.
 *
 */
size_t putu(const uint32_t num, uint8_t base)
{
	mutex_lock(&printer_mutex);
	size_t total = putu_nb(num, base);
	mutex_unlock(&printer_mutex);

	return total;
}

/** Send a signed integer to the output device.
 *
 * This is a non-blocking variant of the ::puti function.
 *
 * @param  num  The number to send to the output.
 * @param  base Base of the system in which the number should be
 *              written: 2-16, 10 for the standard decimal system.
 *
 * @return Number of printed characters.
 *
 */
static size_t puti_nb(const int32_t num, uint8_t base)
{
	size_t total = 0;

	/* Handle negatives. */
	if (num < 0) {
		total += putc_nb('-');
	}

	total += putu_nb(abs(num), base);

	return total;
}

/** Send a signed integer to the output device.
 *
 * @param  num  The number to send to the output.
 * @param  base Base of the system in which the number should be
 *              written: 2-16, 10 for the standard decimal system.
 *
 * @return Number of printed characters.
 *
 */
size_t puti(const int32_t num, uint8_t base)
{
	mutex_lock(&printer_mutex);
	size_t total = puti_nb(num, base);
	mutex_unlock(&printer_mutex);

	return total;
}

/** Print a formated string based on the list of arguments.
 *
 * This is function is non-blocking - locking must be done around the
 * call of this function.
 *
 * @param  format The formating string.
 * @param  argptr List with arguments to the formating string specified in @p
 *                format parameter.
 *
 * @return Number of printed characters.
 *
 */
size_t printk_list(const char *format, va_list argptr)
{
	size_t total = 0;
	bool formating = false;

	/* Go through the formating string. */
	while (*format) {
		char actual = *format;

		/* No special formating is needed. */
		if (formating == false) {
			if (actual == '%') {
				formating = true;
			} else {
				total += putc_nb(actual);
			}
		/* There has been a formating character. */
		} else {
			switch (actual) {
				case 'c':
					total += putc_nb(va_arg(argptr, int));
					break;
					
				case 's':
					total += puts_nb(va_arg(argptr, char*));
					break;
					
				case 'd':
				case 'i':
					total += puti_nb(va_arg(argptr, int32_t), 10);
					break;
					
				case 'u':
					total += putu_nb(va_arg(argptr, uint32_t), 10);
					break;

				case 'x':
					total += putx_nb(va_arg(argptr, int32_t), false);
					break;
					
				case 'p':
					total += puts_nb("0x");
					total += putx_nb((int32_t)va_arg(argptr, void*), true);
					break;
					
				case '%':
					total += putc_nb('%');
					break;
					
				default:
					/* We ignore an invalid formating sequence. */
					break;
			}
			
			formating = false;
		}

		format++;
	}

	return total;
}

/** Print a formated string.
 *
 * This is a non-blocking variant of the ::printk function.
 *
 * @param  format The formating string.
 *
 * @return Number of printed characters.
 *
 */
size_t printk_nb(const char *format, ...)
{
	va_list argptr;
	va_start(argptr, format);

	size_t total = printk_list(format, argptr);

	va_end(argptr);

	return total;
}

/** Print a formated string.
 *
 * When the argument types do not correspond to the formating string,
 * then the panic is cased! Double %% means a % character. Invalid formating
 * characters are ignored.
 *
 * @param  format The formating string.
 *
 * @return Number of printed characters.
 *
 */
size_t printk(const char *format, ...)
{
	va_list argptr;
	va_start(argptr, format);
	
	mutex_lock(&printer_mutex);
	size_t total = printk_list(format, argptr);
	mutex_unlock(&printer_mutex);
	
	va_end(argptr);
	
	return total;
}

/** Print panic message and halt the CPU.
 *
 * @param format Panic message.
 * @param argptr List of variable arguments for the panic.
 *
 */
__attribute__((noreturn)) void panic_list(const char *format, va_list argptr)
{
	/* Disable interrupts for the rest of the function run
	   (because of the simplification of printing out the message). */
	disable_interrupts();

	/* Print the panic message. */
	puts_nb("PANIC: ");
	printk_list(format, argptr);
	va_end(argptr);

	/* Write the thread id. */
	printk_nb("\nThread id: %i\n", thread_get_current_id());

	/* Write the contents of CPU registers. */
	msim_reg_dump();

	/* Halt the CPU. */
	msim_halt();
}

/** Print panic message and halt the CPU.
 *
 * @param format Panic message formating string.
 *
 */
__attribute__((noreturn)) void panic(const char *format, ...)
{
	disable_interrupts();
	va_list argptr;
	va_start(argptr, format);
	panic_list(format, argptr);
}

/**
 * @file kernel/drivers/disk.h
 *
 * Declarations of @ref disk_feature.
 *
 * Support for ddisk device in MSIM.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef DISK_H_
#define DISK_H_

#include <include/shared.h>
#include <include/c.h>
#include <mm/vma.h>


/** Disk device physical address. */
#define DISK_DEVICE_ADDRESS			(ADDR_IN_KSEG0(0x10000010))

/* Masks and values corresponding to MSIM ddisk documentation. */

/** Mask used together with status register to initiate disk read operation. */
#define DISK_STATUS_READ_MASK		0x0000001

/** Mask used together with status register to deassert disk DMA interrupts. */
#define DISK_STATUS_DMA_INT_MASK	0x0000004

/** Mask used together with status register to determine last disk operation status. */
#define DISK_STATUS_ERROR_MASK		0x0000008

/** Offset in disk register used to set manipulating disk sector number. */
#define DISK_SECTOR_REG_OFFSET 		4

/** Offset in disk register representing disk status register. */
#define DISK_STATUS_REG_OFFSET 		8

/** Offset in disk register used to get disk device size. */
#define DISK_SIZE_REG_OFFSET 		12

/** Size of disk sector in bytes according to MSIM documentation. */
#define DISK_SECTOR_SIZE 			512


/* Externals are commented with implementation. */
extern void init_disk(void);
extern void disk_handle(void);
extern int disk_read_sector(int sector, char *buffer);


/** Set disk buffer address.
 *
 * Sets address of buffer where will be data loaded.
 *
 * @param buffer_addr Buffer virtual address.
 *
 */
static inline void set_disk_buffer_address(void *buffer_addr)
{
	void *phys_buffer_addr;

	if (VA_ADDR_IS_IN_DIRRECTLY_MAPPED_SEG(buffer_addr)) {
		phys_buffer_addr = VA_DIRECT_TO_PHYSICAL(buffer_addr);
	} else {
		phys_buffer_addr = addr_space_translate(addr_space_get_current_owned(), buffer_addr);
	}

	(*(volatile uintptr_t *)DISK_DEVICE_ADDRESS) = (uintptr_t)phys_buffer_addr;
}

/** Set disk sector number.
 *
 * Sets sector index with which will be disk manipulating during
 * next operation.
 *
 * @param sector_number Number of sector.
 *
 */
static inline void set_disk_sector_number(int sector_number)
{
	(*(volatile uintptr_t *)(DISK_DEVICE_ADDRESS+DISK_SECTOR_REG_OFFSET)) = sector_number;
}

/** Initiation of disk read operation.
 *
 * Sets a flag which enables disk DMA interrupts.
 *
 */
static inline void initiate_disk_read(void)
{
	(*(volatile uintptr_t *)(DISK_DEVICE_ADDRESS+DISK_STATUS_REG_OFFSET)) |= DISK_STATUS_READ_MASK;
}

/** Disk DMA deassertion.
 *
 * Sets a flag which deasserts DMA interrupts.
 *
 */
static inline void deassert_dma(void)
{
	(*(volatile uintptr_t *)(DISK_DEVICE_ADDRESS+DISK_STATUS_REG_OFFSET)) |= DISK_STATUS_DMA_INT_MASK;
}

/** Determine status of last disk operation.
 *
 * @return True if last operation failed, otherwise false.
 *
 */
static inline bool disk_operation_error(void)
{
	return (*(volatile uintptr_t *)(DISK_DEVICE_ADDRESS+DISK_STATUS_REG_OFFSET)) & DISK_STATUS_ERROR_MASK;
}

/** Get disk size in bytes.
 *
 * @return Capacity of disk device in bytes.
 *
 */
static inline int disk_size(void)
{
	return *(volatile uintptr_t *)(DISK_DEVICE_ADDRESS + DISK_SIZE_REG_OFFSET);
}

/** Get number of available disk sectors.
 *
 * @return Number of available disk sectors.
 *
 */
static inline int disk_sectors(void)
{
	return disk_size() / DISK_SECTOR_SIZE;
}

#endif /* DISK_H_ */

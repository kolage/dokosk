/**
 * @file kernel/adt/list.h
 *
 * Simple doubly linked list declarations.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef LIST_H_
#define LIST_H_

#include <include/c.h>


/* Forward declaration. */
struct item;

/** A doubly linked list.
 *
 */
typedef struct list {
	/** The first item on the list or ::NULL if empty. */
	struct item *head;
	
	/** The last item on the list or ::NULL if empty. */
	struct item *tail;
} list_t;

/** An item of a doubly linked list.
 *
 */
typedef struct item {
	/** The list that we currently belong to. */
	struct list *list;
	
	/** The next item on the list or ::NULL if first. */
	struct item *prev;
	
	/** The previous item on the list or ::NULL if last. */
	struct item *next;
	
	/** Data stored in the list item. */
	void *data;
} item_t;

/** A list iterator.
 *
 */
typedef struct list_it {
	/** Current item of the iterator. */
	struct item *current;
} list_it_t;


/* Externals are commented with implementation. */
extern void item_init(item_t *item, void *data);
extern void list_init(list_t *list);
extern void list_append(list_t *list, item_t *item);
extern void list_prepend(list_t *list, item_t *item);
extern void list_insert_before(list_t *list, item_t *old_item, item_t *new_item_before);
extern void list_remove(list_t *list, item_t *item);
extern item_t *list_rotate(list_t *list);
extern size_t list_count(list_t *list);
extern bool list_empty(list_t *list);
extern bool list_contains(list_t *list, item_t* item);

extern void list_it_init(list_it_t *it, list_t *list);
extern bool list_it_has_current(list_it_t *it);
extern void list_it_next(list_it_t *it);
extern void *list_it_data(list_it_t *it);

#endif /* LIST_H_ */

/**
 * @file kernel/adt/tree.h
 *
 * AVL tree declarations.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#ifndef TREE_H_
#define TREE_H_

#include <include/c.h>


/** Size of the tree stack. */
#define TREE_STACK_SIZE 32


/** Tree node.
 *
 * You can embed this structure to other structures to
 * allow keeping them in a tree.
 */
typedef struct tree_node {
	/** Item key.
	 *
	 * Set the value manually before inserting the
	 * node into a tree. Will be unique in the tree.
	 */
	uint32_t key;

	/** Left subtree. Can be ::NULL. */
	struct tree_node *left;

	/** Right subtree. Can be ::NULL. */
	struct tree_node *right;

	/** Height of the item and subtrees in the tree. */
	int height;
} tree_node_t;

/** Tree structure.
 *
 * All data are actually stored in the nodes, so it is
 * sufficient to keep just the root node.
 *
 */
typedef struct {
	/** Root node of the tree. Can be ::NULL. */
	tree_node_t *root;
} tree_t;

/** Tree stack used to store a path in a tree. */
typedef struct tree {
	/** Array of pointers to variables containing pointers to nodes */
	tree_node_t **stack[TREE_STACK_SIZE];
	/** Pointer to the end of the used segment in the array. */
	tree_node_t ***stack_top;
} tree_stack_t;

/** Iterator traversing all items in a tree. */
typedef struct{
	/** Stack containing the path to the current item. */
	tree_stack_t stack;
} tree_it_t;


/* Externals are commented with implementation. */
extern void tree_init(tree_t *tree);
extern tree_node_t *tree_find(tree_t *tree, uint32_t key);
extern tree_node_t *tree_remove(tree_t *tree, uint32_t key);
extern bool tree_add(tree_t *tree, tree_node_t *item);

extern void tree_it_init(tree_it_t *it, tree_t *tree);
extern bool tree_it_has_current(tree_it_t *it);
extern void tree_it_next(tree_it_t *it);
extern uint32_t tree_it_key(tree_it_t *it);

#endif /* TREE_H_ */

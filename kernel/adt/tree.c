/**
 * @file kernel/adt/tree.c
 *
 * AVL tree implementation.
 *
 * Trees consist of nodes connected by pointers.
 * The pointers are unidirectional and always go from the parent to child nodes.
 * When traversing the tree, the path can be recorded on a stack which can be used
 * to backtrack or balance the tree along the path. The stack contains pointers
 * to variables that contain pointers to the actual nodes, which means that
 * they can be used to replace the node pointer in its parent. This is mainly used
 * when balancing the tree.
 *
 * None of the functions does allocates or deallocates memory.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/math.h>
#include <adt/tree.h>
#include <drivers/printer.h>


/** Check if the stack is empty.
 *
 * @param  tree_stack Stack to check. Must be a valid stack.
 *
 * @return ::true if the stack contains items, ::false if empty.
 *
 */
static inline bool tree_stack_empty(tree_stack_t *tree_stack)
{
	return tree_stack->stack_top == tree_stack->stack;
}

/** Initialize stack structure.
 *
 * @param tree_stack Stack to initialize. Must not be ::NULL.
 *
 */
static inline void tree_stack_init(tree_stack_t *tree_stack)
{
	tree_stack->stack_top = tree_stack->stack;
}

/** Return the item on the top of a stack.
 *
 * @param  tree_stack Stack to check. Must be a valid stack and must
 *                    not be empty.
 *
 * @return The top item of the stack.
 *
 */
static inline tree_node_t **tree_stack_top(tree_stack_t *tree_stack)
{
	return *(tree_stack->stack_top - 1);
}

/** Return a pointer the item on the top of a stack.
 *
 * @param  tree_stack Stack to check. Must be a valid stack and must
 *                    not be empty.
 *
 * @return Pointer to the top item of the stack.
 *
 */
static inline tree_node_t ***tree_stack_top_ptr(tree_stack_t *tree_stack)
{
	return tree_stack->stack_top - 1;
}

/** Push an item to the stack.
 *
 * @param tree_stack Stack to modify. Must be a valid stack.
 * @param node       Node to push to the stack. Becomes the top item.
 *
 */
static inline void tree_stack_push(tree_stack_t *tree_stack, tree_node_t **node)
{
	*tree_stack->stack_top++ = node;
}

/** Pops an item from the stack.
 *
 * @param tree_stack Stack to check. Must be a valid stack and must
 *                   not be empty.
 *
 */
static inline void tree_stack_pop(tree_stack_t *tree_stack)
{
	tree_stack->stack_top--;
}

/** Initialize tree structure.
 *
 * @param tree Tree structure to initialize. Must not be ::NULL.
 *             Must be pointer to an uninitialized tree strucutre.
 *
 */
void tree_init(tree_t *tree)
{
	tree->root = NULL;
}

/** Find a node in the tree according to the specified key.
 *
 * @param  tree The tree to search. Must be a valid tree.
 * @param  key  The key to search for.
 *
 * @return Node from the three with the specified key, or ::NULL if such
 * 	       node does not exist.
 *
 */
tree_node_t *tree_find(tree_t *tree, uint32_t key)
{
	/* Start searching at the root. */
	tree_node_t *node = tree->root;

	while (node != NULL) {
		/* Select the subtree by comparing the keys. */
		if (key < node->key) {
			node = node->left;
		} else if (node->key < key) {
			node = node->right;
		} else {
			/* Found the right key. */
			return node;
		}
	}

	/* When we got to the bottom, the tree does not contain the key. */
	return NULL;
}

/** Get the height of a tree node.
 *
 * @param  node Tree node. Can be ::NULL.
 *
 * @return The height stored in the node or 0 if @p node is ::NULL.
 */
static int tree_h(tree_node_t *node)
{
	return node == NULL ? 0 : node->height;
}

/** Update the height of the node.
 *
 * The heights of the children must be already updated.
 *
 * @param node Tree node. Must be a valid node.
 *
 */
static void tree_update_h(tree_node_t *node)
{
	node->height = max(tree_h(node->left), tree_h(node->right)) + 1;
}

/** Balance the tree along the path stored on
 * the stack.
 *
 * @param tree_stack Stack that contains the path that should be balanced.
 *                   Must be a valid stack.
 *
 */
static void tree_balance(tree_stack_t *tree_stack)
{
	/* Go along the path to the root of the tree. */
	while (!tree_stack_empty(tree_stack)) {
		/* Pointer to the field pointing to the top node. */
		tree_node_t **top_ref = tree_stack_top(tree_stack);

		/* The top node. */
		tree_node_t *top = *top_ref;

		/* The top item on the stack can be null when removing nodes. */
		if (top != NULL) {
			/* Children of the top node. */
			tree_node_t *left = top->left;
			tree_node_t *right = top->right;

			/* Heights of the children. */
			int left_h = tree_h(left);
			int right_h = tree_h(right);

			if (left_h > right_h + 1) {
				/* The left subtree is too high. */
				if (tree_h(left->right) > tree_h(left->left)) {
					/* Need double rotation. */
					tree_node_t *new_top = left->right;
					left->right = new_top->left;
					top->left = new_top->right;
					new_top->left = left;
					new_top->right = top;

					/* Update heights. */
					tree_update_h(left);
					tree_update_h(top);
					tree_update_h(new_top);

					/* Update parent of the rotated node. */
					*top_ref = new_top;
				} else {
					/* Single rotation. */
					top->left = left->right;
					left->right = top;

					/* Update heights. The order is important. */
					tree_update_h(top);
					tree_update_h(left);

					/* Update parent of the rotated node. */
					*top_ref = left;
				}
			} else if(right_h > left_h + 1) {
				/* The left subtree is too high. */
				if (tree_h(right->left) > tree_h(right->right)) {
					/* Need double rotation. */
					tree_node_t* new_top = right->left;
					right->left = new_top->right;
					top->right = new_top->left;
					new_top->right = right;
					new_top->left = top;

					/* Update heights. */
					tree_update_h(right);
					tree_update_h(top);
					tree_update_h(new_top);

					/* Update parent of the rotated node. */
					*top_ref = new_top;
				} else {
					/* Single rotation */
					top->right = right->left;
					right->left = top;

					/* Update heights. The order is important. */
					tree_update_h(top);
					tree_update_h(right);

					/* Update parent of the rotated node. */
					*top_ref = right;
				}
			} else {
				/* No need to rotate. */
				tree_update_h(top);
			}
		}

		/* Move one node towards the root. */
		tree_stack_pop(tree_stack);
	}
}

/** Add a new node to the tree.
 *
 * The node must contain an initialized key. If a node with the same
 * key is found in the tree, the new node is not added.
 *
 * @param  tree     The tree to add to. Must be a valid tree.
 * @param  new_node The node to add. Must be a valid tree node.
 *
 * @return ::true if the item was added, ::false if a duplicate key was found.
 *
 */
bool tree_add(tree_t *tree, tree_node_t *new_node)
{
	tree_stack_t tree_stack;

	/* Start at the root of the tree and
	   push pointers to the traversed fields to the stack. */
	tree_stack_init(&tree_stack);
	tree_stack_push(&tree_stack, &tree->root);

	/* Go down the tree. */
	while (*tree_stack_top(&tree_stack) != NULL) {
		tree_node_t *top = *tree_stack_top(&tree_stack);

		/* Go to a subtree according to the key. */
		if (new_node->key < top->key) {
			tree_stack_push(&tree_stack, &top->left);
		} else if (top->key < new_node->key) {
			tree_stack_push(&tree_stack, &top->right);
		} else {
			/* Found the same key (duplicate). */
			return false;
		}
	}

	/* Insert the new node as a child of the last visited node. */
	*tree_stack_top(&tree_stack) = new_node;

	/* Initialize the children of the new node. */
	new_node->left = NULL;
	new_node->right = NULL;
	tree_balance(&tree_stack);

	return true;
}

/** Remove an item with the specified key from the tree.
 * If such an item does not exist in the tree, nothing is changed.
 *
 * @param  tree The tree to modify. Must be a valid tree.
 * @param  key  The key of the item to remove.
 *
 * @return The node removed from the tree or ::NULL if the key
 *         is not found in the tree.
 *
 */
tree_node_t *tree_remove(tree_t *tree, uint32_t key)
{
	tree_stack_t tree_stack;

	/* Start at the root. */
	tree_stack_init(&tree_stack);
	tree_stack_push(&tree_stack, &tree->root);

	/* Go down the tree. */
	while (*tree_stack_top(&tree_stack) != NULL) {
		tree_node_t *top = *tree_stack_top(&tree_stack);

		/* Go to a subtree according to the key. */
		if (key < top->key) {
			tree_stack_push(&tree_stack, &top->left);
		} else if(top->key < key) {
			tree_stack_push(&tree_stack, &top->right);
		} else {
			/* Found the key. */
			tree_node_t *removed = top;

			/* The node is removed directly only if it has no right subtree.
			   If it has right subtree, it is first swapped with its successor. */
			if (removed->right != NULL) {
				/* Save pointer to the parent and go to the right child. */
				tree_node_t **swap_parent = tree_stack_top(&tree_stack);
				tree_stack_push(&tree_stack, &removed->right);
				tree_node_t ***swap_ptr = tree_stack_top_ptr(&tree_stack);

				/* The right subtree is not NULL, so find the
				 leftmost node in it. */
				while ((*tree_stack_top(&tree_stack))->left != NULL) {
					tree_stack_push(&tree_stack, &(*tree_stack_top(&tree_stack))->left);
				}

				/* Found node whose left pointer is NULL, swap it. */
				tree_node_t *swapped = *tree_stack_top(&tree_stack);

				/* Update the parent of the swapped item. */
				*tree_stack_top(&tree_stack) = swapped->right;
				*swap_parent = swapped;

				/* Also change the pointer on the stack. */
				*swap_ptr = &swapped->right;
				swapped->left = removed->left;
				swapped->right = removed->right;
			}
			else {
				/* Remove the node directly. */
				*tree_stack_top(&tree_stack) = removed->left;
			}

			/* In every case, balance the tree and return the removed node. */
			tree_balance(&tree_stack);
			return removed;
		}
	}

	/* The item is not in the tree. */
	return NULL;
}

/** Go down in a tree recording the path to a stack.
 *
 * Goes to the left as long as the current node has a left child.
 *
 * @param tree_stack Stack containing the path. Must not be empty.
 *
 */
static void tree_it_stack_down(tree_stack_t *tree_stack)
{
	/* Start at the current node. */
	tree_node_t *current = *tree_stack_top(tree_stack);

	/* Repeat while there is a left child. */
	while (current->left != NULL) {
		/* Go to the left child. */
		tree_stack_push(tree_stack, &current->left);
		current = current->left;
	}
}

/** Go up in a tree shortening the path in a stack.
 *
 * Goes to the parent as long as the stack is not empty and the current
 * node is a right child.
 *
 * @param tree_stack Stack containing the path. Must not be empty.
 *
 */
static void tree_it_stack_up(tree_stack_t *tree_stack)
{
	/* Start at the current node */
	tree_node_t **top_ref = tree_stack_top(tree_stack);

	tree_stack_pop(tree_stack);

	/* Repeat while the stack is not empty. */
	while (!tree_stack_empty(tree_stack)) {
		tree_node_t **next_ref = tree_stack_top(tree_stack);

		/* Repeat while the node is not a left child. */
		if (top_ref == &((*next_ref)->left)) {
			return;
		}

		/* Go to the parent. */
		top_ref = next_ref;
		tree_stack_pop(tree_stack);
	}
}

/** Initialize an iterator which traverses the specified tree.
 *
 * @param it   Iterator structure to be initialized. Must not be ::NULL.
 * @param tree Tree which will be traversed by @p it. Must be a valid tree.
 *
 */
void tree_it_init(tree_it_t *it, tree_t *tree)
{
	/* Initialize emtpy stack. */
	tree_stack_init(&it->stack);

	/* Traverse the tree only if it has some nodes. */
	if (tree->root != NULL) {
		tree_stack_push(&it->stack, &tree->root);

		/* Find the first item. */
		tree_it_stack_down(&it->stack);
	}
}

/** Check if the iterator has a current node.
 *
 * @param  it Iterator to be checked. Must be a valid iterator.
 *
 * @return ::true if the iterator has a current node, ::false if
 *         the iterator has iterated over all nodes in the tree.
 */
bool tree_it_has_current(tree_it_t *it)
{
	return !tree_stack_empty(&it->stack);
}

/** Advance the iterator to the next node in the tree.
 *
 * @param it Iterator to be advanced. Must be a valid iterator and
 *           must have a current node.
 *
 */
void tree_it_next(tree_it_t *it)
{
	/* The operation depends on the current node. */
	tree_node_t *top = *tree_stack_top(&it->stack);

	if (top->right != NULL) {
		/* There is right subtree, so find the leftmost node in it. */
		tree_stack_push(&it->stack, &top->right);
		tree_it_stack_down(&it->stack);
	} else {
		/* No right subtree, the next node is
		   the first ancestor where this node is in left subtree. */
		tree_it_stack_up(&it->stack);
	}
}

/** Get the key of the current node of an iterator.
 *
 * @param  it Iterator to be checked. Must be a valid iterator and
 *            must have a current node.
 *
 * @return Key of the current node of @p it.
 *
 */
uint32_t tree_it_key(tree_it_t *it)
{
	/* The current node is on the top of the stack. */
	return (*tree_stack_top(&it->stack))->key;
}

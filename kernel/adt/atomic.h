/**
 * @file kernel/adt/atomic.h
 *
 * Atomic data types.
 *
 * Basic primitives for atomic variables. The implementation here uses the
 * LL (load linked) and SC (store conditional) instruction pair to achieve
 * atomicity of certain operations.
 *
 * The atomic variable data type and the associated interface for
 * manipulating it has been largely inspired by the Linux kernel.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef ATOMIC_H_
#define ATOMIC_H_

#include <include/shared.h>
#include <include/c.h>


/** Atomic variable.
 *
 * The atomic data type is opaque to the user to prevent
 * access by other than atomic operations. It is also
 * small enough to be passed by value.
 *
 */
typedef struct {
	volatile native_t value;
} atomic_t;


/** Macros for more elegent declaration of global atomic variables. */
#define ATOMIC_EXTERN(name)          extern atomic_t name
#define ATOMIC_DECLARE(name, value)  atomic_t name = { value }


/** Get the value of an atomic variable.
 *
 * @param var The variable to read.
 *
 */
static inline native_t atomic_get(const atomic_t *var)
{
	/* Reading an integer is atomic on MIPS. */
	return var->value;
}

/** Set the value of an atomic variable.
 *
 * @param var The variable to write.
 * @param val The value to write.
 *
 */
static inline void atomic_set(atomic_t *var, const native_t val)
{
	/* Writing an integer is atomic on MIPS. */
	var->value = val;
}

/** Add to the value of an atomic variable.
 *
 * @param var The variable to add to.
 * @param num The value to add.
 *
 * @return The new value of the variable.
 *
 */
static inline native_t atomic_add(atomic_t *var, const native_t num)
{
	/* This is an optimistic algorithm that keeps trying
	   to add until the LL and SC pair of instructions
	   succeeds, which means there was no other
	   access to the same variable in the
	   meantime. */
	native_t temp, result;
	
	asm volatile (
		".set push\n"
		".set noreorder\n"
		
		"1: ll %[temp], %[value]\n"
		"   addu %[result], %[temp], %[num]\n"
		"   sc %[result], %[value]\n"
		"   beqz %[result], 1b\n"
		
		"   addu %[result], %[temp], %[num]\n"
		"   sync\n"
		
		".set pop\n"
		: [temp] "=&r" (temp),
		  [result] "=&r" (result),
		  [value] "+m" ((var)->value)
		: [num] "Ir" (num)
		: "memory"
	);
	
	return result;
}

/** Increment the value of an atomic variable.
 *
 * @param var The variable to be incremented.
 *
 * @return The new value of the variable.
 *
 */
static inline native_t atomic_increment(atomic_t *var)
{
	return atomic_add(var, 1);
}

/** Subtract from the value of an atomic variable.
 *
 * @param var The variable to subtract from.
 * @param num The value to subtract.
 *
 * @return The new value of the variable.
 *
 */
static inline native_t atomic_sub(atomic_t *var, const native_t num)
{
	/* This is an optimistic algorithm that keeps trying
	   to subtract until the LL and SC pair of instructions
	   succeeds, which means there was no other access to the
	   same variable in the meantime. */
	native_t temp, result;

	asm volatile (
		".set push\n"
		".set noreorder\n"
		
		"1: ll %[temp], %[value]\n"
		"   subu %[result], %[temp], %[num]\n"
		"   sc %[result], %[value]\n"
		"   beqz %[result], 1b\n"
		
		"   subu %[result], %[temp], %[num]\n"
		"   sync\n"
		
		".set pop\n"
		: [temp] "=&r" (temp),
		  [result] "=&r" (result),
		  [value] "+m" ((var)->value)
		: [num] "Ir" (num)
		: "memory"
	);

	return result;
}

/** Decrement the value of an atomic variable.
 *
 * @param var The variable to be decremented.
 *
 * @return The new value of the variable.
 *
 */
static inline native_t atomic_decrement(atomic_t *var)
{
	return atomic_sub(var, 1);
}

#endif /* ATOMIC_H_ */

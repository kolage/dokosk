/**
 * Test template #1
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static const char *desc =
    "Test for simple debugging #1\n\n"
    "Test is used when developing to debug some functionality.\n\n";

#include <api.h>

void run_test(void)
{
	puts(desc);

	/* Test code goes here... */

	puts("Test passed...\n");
}

/**
 * VMA test #2
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static const char *desc =
    "VMA test #2\n\n"
    "Tests the vma_resize function.\n\n";

#include <api.h>

/* We need assert to work. */
#undef NDEBUG

void run_test(void)
{
	puts(desc);

	const vm_flags_t flags[] = { VF_AT_KSEG0, VF_AT_KUSEG, VF_AT_KSSEG, VF_AT_KSEG3 };
	const size_t flags_count = 4;

	vm_address_space_t *current_addr_space = addr_space_get_current_owned();
	size_t total_areas = 0;
	void *from1 = NULL;
	void *from2 = NULL;
	void *from3 = NULL;

	/* Run the test with different flags. */
	for (uint32_t i = 0; i < flags_count; i++) {
		printk("Running cycle %u\n", (i + 1));
		vm_flags_t space_flag = flags[i];

		/* Map some areas. */
		printk("Map some areas:\n");
		assert(vma_map(&from1, 4 * FRAME_SIZE, space_flag | VF_VA_AUTO) == EOK);
		assert(vma_map(&from3, 4 * FRAME_SIZE, space_flag | VF_VA_AUTO) == EOK);
		printk("    First: at %p\n", from1);
		printk("    Second: at %p\n", from3);

		/* Try to resize non-existing area. */
		printk("Try to resize non-existing area: ");
		assert(vma_resize(from1 + FRAME_SIZE, 8 * FRAME_SIZE) == EINVAL);
		printk("EINVAL\n");

		/* Try to resize the first area - should not work, because there is an area behind. */
		printk("Try to resize the first area: ");
		assert(vma_resize(from1, 8 * FRAME_SIZE) == EINVAL);
		printk("EINVAL\n");

		/* Try to resize the second area with invalid size. */
		printk("Try to resize the second area with invalid size: ");
		assert(vma_resize(from3, 2 * FRAME_SIZE - 1024) == EINVAL);
		printk("EINVAL\n");

		/* Try to resize the second area with size 0. */
		printk("Try to resize the second area with size 0: ");
		assert(vma_resize(from3, 0) == EINVAL);
		printk("EINVAL\n");

		/* Try to shrink the first area to the half size. */
		printk("Try to shrink the first area to the half size: ");
		assert(vma_resize(from1, 2 * FRAME_SIZE) == EOK);
		printk("EOK\n");

		/* Now there should be no translation for the pointer behind the second area end. */
		if (space_flag != VF_AT_KSEG0) {
			printk("Try to translate invalid pointer: ");
			assert(addr_space_translate(current_addr_space, from1 + 2 * FRAME_SIZE) == NULL);
			printk("OK\n");
		}

		/* Try to allocate new area between those two allocated areas. */
		printk("Try to allocate new area between those two allocated areas: ");
		from2 = from1 + 2 * FRAME_SIZE;
		assert(vma_map(&from2, 2 * FRAME_SIZE, VF_VA_USER) == EOK);
		printk("EOK; allocated at %p\n", from2);

		/* Now the translation should work, because we have allocated new area. */
		printk("Try to translate valid pointer: ");
		assert(addr_space_translate(current_addr_space, from1 + 2 * FRAME_SIZE) != NULL);
		printk("OK\n");

		/* Resizing of first area should not work either. */
		printk("Resizing of first area should not work either: ");
		assert(vma_resize(from1, 3 * FRAME_SIZE) == EINVAL);
		printk("EINVAL\n");

		/* Stretching the third area should work. */
		printk("Stretching the third area should work: ");
		assert(vma_resize(from3, 8 * FRAME_SIZE) == EOK);
		printk("EOK\n");

		puts("\n");
		total_areas += 3;
	}

	/* We have allocated 9 areas in total. */
	assert(current_addr_space->area_count == total_areas);
	addr_space_print_current();
	puts("Test passed...\n");
}

/***
 * Timer test #1
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Timer test without threads #1\n";

#include <api.h>
#include "../include/defs.h"

/*
 * Magic value passed to the timer handler.
 */
#define TIMER_MAGIC  ((void *)0xdeadbeef)

static void timer_proc1(struct timer *timer, void *data)
{
	assert(data == TIMER_MAGIC);
	putc('1');
	timer_start(timer);
}

static void timer_proc2(struct timer *timer, void *data)
{
	assert(data == TIMER_MAGIC);
	putc('2');
	timer_start(timer);
}

static void timer_proc3(struct timer *timer, void *data)
{
	putc('3');
	timer_destroy((timer_t*)data);
}

void run_test(void)
{
	struct timer tmr1, tmr2, tmr3;
	
	puts(desc);
	
	timer_init(&tmr1, 200000, timer_proc1, TIMER_MAGIC);
	timer_init(&tmr2, 100000, timer_proc2, TIMER_MAGIC);

	/*
	 * Test the first timer.
	 */
	printk("Testing timer(%p). Press any key:\n", &tmr1);
	timer_start(&tmr1);
	getc();
	timer_destroy(&tmr1);
	printk("\n");
	
	printk("Testing timer two times faster(%p). Press any key:\n", &tmr2);
	timer_start(&tmr2);
	getc();
	timer_destroy(&tmr2);
	printk("\n");

	printk("Testing both timers. Press any key:\n");
	timer_start(&tmr1);
	timer_start(&tmr2);
	getc();
	timer_destroy(&tmr1);
	timer_destroy(&tmr2);
	printk("\n");

	printk("Testing destroying timers. Press any 2 keys:\n");
	timer_start(&tmr1);
	timer_start(&tmr2);

	/* Kill the first timer after elapsed once. */
	timer_init(&tmr3, 250000, timer_proc3, &tmr1);
	timer_start(&tmr3);
	getc();
	timer_destroy(&tmr3);

	/* Kill the second timer immediately after pressing the key. */
	timer_init(&tmr3, 0, timer_proc3, &tmr2);
	timer_start(&tmr3);
	getc();
	timer_destroy(&tmr3);
	timer_destroy(&tmr1);
	timer_destroy(&tmr2);
	printk("\n");

	/* Print the result. */
	puts("Test passed...\n");
}

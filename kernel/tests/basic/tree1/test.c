/**
 * AVL tree test #1.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <api.h>
#include <adt/tree.h>
#include <include/math.h>
#include "../include/defs.h"
#include "../include/tst_rand.h"

tree_t tree;

struct tree_check {
	uint32_t low;
	uint32_t high;
};
static tree_node_t nodes[1000];
static bool added[1000];
static bool iterated[1000];


static void tree_check_node(tree_t *tree, tree_node_t *node, struct tree_check *check){

	int lheight = 0, rheight = 0;

	check->low = node->key;
	check->high = node->key;

	if (node->left != NULL) {
		struct tree_check lcheck;
		tree_check_node(tree, node->left, &lcheck);
		assert(lcheck.high < node->key);
		lheight = node->left->height;
		check->low = lcheck.low;
	}

	if (node->right != NULL) {
		struct tree_check rcheck;
		tree_check_node(tree, node->right, &rcheck);
		assert(rcheck.low > node->key);
		rheight = node->right->height;
		check->high = rcheck.high;
	}

	assert(lheight <= rheight + 1);
	assert(rheight <= lheight + 1);

	if (node->height != max(lheight, rheight) + 1) {
		panic("Inconsistent heights L: %d R: %d T: %d\n", lheight, rheight, node->height);
	}
}

static void tree_check(tree_t *tree)
{
	struct tree_check check;

	if (tree->root != NULL) {
		tree_check_node(tree, tree->root, &check);
		assert(check.low <= check.high);
	}

	for (int i = 0; i < 1000; ++i) {
		iterated[i] = 0;
	}

	tree_it_t iterator;
	for (tree_it_init(&iterator, tree); tree_it_has_current(&iterator); tree_it_next(&iterator)) {
		uint32_t key = tree_it_key(&iterator);
		assert(key<1000);
		iterated[key]++;
	}

	for (int i = 0; i < 1000; ++i) {
		assert(iterated[i] == added[i]);
	}
}

static void tree_test_queue(void)
{
	tree_init(&tree);

	for (int i = 0; i < 1000; ++i) {
		printk("A%d", i);
		nodes[i].key = i;
		tree_add(&tree, &nodes[i]);
		added[i] = true;
		tree_check(&tree);
	}

	for (int i = 0; i < 1000; ++i) {
		printk("R%d", i);
		assert(tree_remove(&tree, i) == &nodes[i]);
		added[i] = false;
		tree_check(&tree);
	}
}

static void tree_test_stack(void)
{
	tree_init(&tree);

	for (int i = 0; i < 1000; ++i) {
		printk("A%d", i);
		nodes[i].key = i;
		tree_add(&tree, &nodes[i]);
		added[i] = true;
		tree_check(&tree);
	}

	for (int i = 999; i >= 0; --i) {
		printk("R%d", i);
		assert(tree_remove(&tree, i) == &nodes[i]);
		added[i] = false;
		tree_check(&tree);
	}
}

static void tree_test_random(void)
{
	tree_init(&tree);

	for(int i = 0; i < 1000; ++i) {
		added[i] = false;
		nodes[i].key = i;
	}

	for (int i = 0; i < 5000; ++i) {
		bool add = tst_rand() % 2;
		int item = tst_rand() % 1000;

		if (add) {
			printk("A%d", item);
			bool r = tree_add(&tree, &nodes[item]);
			assert(r == !added[item]);
			added[item] = true;
			tree_check(&tree);
		} else {
			printk("R%d", item);
			tree_node_t* r = tree_remove(&tree, item);

			if (added[item]) {
				assert(r == &nodes[item]);
			} else {
				assert(r == NULL);
			}

			added[item] = false;
			tree_check(&tree);
		}
	}
}

void run_test(void)
{
	puts("Adding and removing in the same order.\n");
	tree_test_queue();

	puts("Adding and removing in the opposite order.\n");
	tree_test_stack();

	puts("Randomly adding and removing.\n");
	tree_test_random();

	puts("\nTest passed...\n");
}

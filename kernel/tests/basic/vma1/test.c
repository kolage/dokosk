/**
 * VMA test #1
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static const char *desc =
    "VMA test #1\n\n"
    "Tests the basic internal functionality of virtual address mapping.\n\n";

#include <api.h>

/* We need assert to work. */
#undef NDEBUG

void run_test(void)
{
	puts(desc);

	vm_address_space_t *current_addr_space = addr_space_get_current_owned();
	void *from = NULL;

	/* Try to map more than is possible. */
	for (uint32_t i = 0; i < MAX_AREAS; i++) {
		assert(vma_map(&from, FRAME_SIZE, VF_AT_KSEG0 | VF_VA_AUTO) == EOK);
	}

	assert(vma_map(&from, FRAME_SIZE, VF_AT_KSEG0 | VF_VA_AUTO) == ENOMEM);
	assert(current_addr_space->area_count == MAX_AREAS);

	/* Print the actual address space. */
	addr_space_print_current();

	for (uint32_t i = 0; i < MAX_AREAS; i++) {
		assert(vma_unmap(current_addr_space->areas[0].vma_start) == EOK);
	}

	assert(current_addr_space->area_count == 0);

	/* Try to map at the end of user space. */
	from = (void *)0xfffff000;
	assert(vma_map(&from, FRAME_SIZE, VF_VA_USER) == EOK);

	/* Try if we can not allocate the area at KSEG0 and KSEG1 when they
	   are mapped to the same physical addresses. */
	from = (void *)0x80000000;
	assert(vma_map(&from, 2 * FRAME_SIZE, VF_VA_USER) == EOK);

	from += 0x20000000 + FRAME_SIZE;
	assert(vma_map(&from, FRAME_SIZE, VF_VA_USER) == EINVAL);

	/* Try to allocate some areas in the user space and print the address space. */
	for (uint32_t i = 0; i < 5; i++) {
		assert(vma_map(&from, 4 * FRAME_SIZE, VF_AT_KUSEG | VF_VA_AUTO) == EOK);
	}

	assert(current_addr_space->area_count == 7);
	addr_space_print_current();

	/* Checks whether the areas are sorted according to the virtual address. */
	void *prev_addr = (void *)0x00000000;

	for (uint32_t i = 0; i < current_addr_space->area_count; i++) {
		vm_area_t *area = &current_addr_space->areas[i];
		assert(prev_addr <= area->vma_start);
		prev_addr = area->vma_start;
	}

	puts("Test passed...\n");
}

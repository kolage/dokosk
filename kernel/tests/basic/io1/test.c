/**
 * IO test #1
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static const char *desc =
    "IO test #1\n\n"
    "Tests the basic functionality of IO in concurent environment.\n\n";

#include <api.h>

/* We need assert to work. */
#undef NDEBUG

#define PRINT_CYCLES		100
#define READ_CYCLES			3
#define BUFFER_LENGTH		50

static void * thread_proc_print(void *data)
{
	unative_t thread_id = thread_get_current_impl()->thread_id;

	for (int i = 0; i < PRINT_CYCLES; i++) {
		printk("Thread %u, cycle %i - writing something quite long where the interrupt might happen...\n", thread_id, i);
		thread_yield();
	}

	printk("Thread %u: exiting\n", thread_id);

	return NULL;
} /* thread_proc_print */

static void * thread_proc_keyboard(void *data)
{
	unative_t thread_id = thread_get_current_impl()->thread_id;

	for (int i = 0; i < READ_CYCLES; i++) {
		printk("Thread %u, cycle %i - reading something: \n", thread_id, i);

		char str [BUFFER_LENGTH];
		ssize_t length = gets(str, BUFFER_LENGTH);
		printk("Thread %u read %i characters: %s", thread_id, length, str);
	}

	printk("Thread %u: exiting\n", thread_id);

	return NULL;
} /* thread_proc_keyboard */

void run_test(void)
{
	puts (desc);

	/* Test the output. */
	printk("Char: A == %c\n", 'A');
	printk("String: Test == %s\n", "Test");
	printk("Percents: %i %%\n", 50);
	printk("Pointer: 0xabcdef01 == %p\n", 0xabcdef01);
	printk("Hexadecimal 124 == 124: %x\n", 124);
	printk("Hexadecimal -124 (0xffffff84): %x\n", -124);
	printk("Zero: %i\n", 0);
	printk("Minint: %i\n", 0x80000000);
	printk("Maxint: %i\n", 0x7fffffff);
	printk("Unsigned maxint: %u\n", 0xffffffff);
	printk("Unsigned maxint as int (-1): %i\n", 0xffffffff);
	
	/* Test the output synchronization - the output should not be mixed up. */
	printk("\nTest the output synchronization:\n");

	thread_t thread_a;
	thread_t thread_b;

	assert(thread_create(&thread_a, thread_proc_print, THREAD_NO_DATA, TF_NEW_VMM) == EOK);
	assert(thread_create(&thread_b, thread_proc_print, THREAD_NO_DATA, TF_NEW_VMM) == EOK);

	thread_join(thread_a);
	thread_join(thread_b);

	/* Test the input synchronization. */
	assert(thread_create(&thread_a, thread_proc_keyboard, THREAD_NO_DATA, TF_NEW_VMM) == EOK);
	assert(thread_create(&thread_b, thread_proc_keyboard, THREAD_NO_DATA, TF_NEW_VMM) == EOK);

	thread_join(thread_a);
	thread_join(thread_b);

	puts ("Test passed...\n");
}

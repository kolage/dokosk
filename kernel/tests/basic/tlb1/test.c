/**
 * TLB test #1.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <api.h>
#include "../include/defs.h"

void run_test(void)
{
	volatile int* ptr = (volatile int*)(0x10000000);
	*ptr = 0;

	puts("Test failed...\n");
}

/**
 * Disk test #1.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <api.h>

#define OFFSET 512
#define COUNT 600

void run_test(void)
{
	printk("Disk test - START\n");

	char read_buffer[DISK_SECTOR_SIZE];
	printk("Disk size %d\n",disk_size());
	printk("%d sectors on disk.\n", disk_sectors());

	thread_sleep(1);

	for (int i = 0; i < disk_sectors(); i++) {
		if (disk_read_sector(i, &read_buffer[0]) == EOK) {
			printk("Successfully read sector %d\n", i);

			for(int j = 0; j < DISK_SECTOR_SIZE; j++) {
				printk("%d : %d\n", j, read_buffer[j]);
			}
		} else {
			printk("Error reading sector %d\n", i);
		}
	}

	printk("Test passed...\n");
}

#ifndef DEFS_H_
#define DEFS_H_


/** Size of an array
 *
 * @param array Array to determine the size of.
 *
 * @return Size of @p array in array elements.
 *
 */
#define sizeof_array(array) \
	(sizeof (array) / sizeof ((array) [0]))


#endif /* DEFS_H_ */

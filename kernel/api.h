/**
 * @file kernel/api.h
 *
 * API for kernel tests.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef API_H_
#define API_H_

#include <include/shared.h>
#include <include/c.h>
#include <drivers/keyboard.h>
#include <drivers/printer.h>
#include <drivers/disk.h>
#include <mm/falloc.h>
#include <mm/malloc.h>
#include <mm/vma.h>
#include <mm/thrcopy.h>
#include <adt/atomic.h>
#include <sync/mutex.h>
#include <sync/cond.h>
#include <include/codes.h>
#include <sched/timer.h>


/* Run the kernel test */
extern void run_test(void);

#endif /* API_H_ */

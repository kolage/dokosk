/**
 * @file kernel/include/math.h
 *
 * File with implementation of simple basic mathematical functions
 * like ::min, ::max, ::abs etc.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef MATH_H_
#define MATH_H_


/** Computes the minimum of given two numbers.
 *
 * @param first  First number.
 * @param second Second number.
 *
 */
static inline int32_t min(const int32_t first, const int32_t second)
{
	return first < second ? first : second;
}

/** Computes the maximum of given two numbers.
 *
 * @param first  First number.
 * @param second Second number.
 *
 */
static inline int32_t max(const int32_t first, const int32_t second)
{
	return first > second ? first : second;
}

/** Computes the absolute value of the given @p number.
 *
 * @param number Number for which the absolute value is computed.
 *
 */
static inline uint32_t abs(const int32_t number)
{
	return number > 0 ? number : -number;
}

/** Round a number to a power of two.
 *
 * Get smallest power of two that is greater or equal to given number.
 *
 * Fetched from stackoverflow.com.
 *
 * @param x Number to be rounded up.
 *
 */
static inline int pow2_roundup(int x)
{
    if (x < 0) {
        return 0;
    }

    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;

    return x + 1;
}

/** Binary logarithm of a number that is a power of 2.
 *
 * @param  x Input number.
 *
 * @return Binary logarithm of @p x.
 *
 */
static inline int log2_of_pow2(int x)
{
	int temp = 1;
	int i = 0;

	while (x > temp) {
		temp = temp << 1;
		i++;
	}

	return i;
}

/** Divide a 64-bit quotient by a 32-bit divisor.
 *
 * @param  quotient 64-bit unsigned quotient.
 * @param  divisor 32-bit unsigned divisor.
 *
 * @return 64-bit unsigned dividend.
 */
static inline uint64_t divide64_32(uint64_t quotient, uint32_t divisor)
{
	/* Find the number of bits in the divisor. */
	int divisor_bits = 0;

	while ((1llu << divisor_bits) <= divisor) {
		++divisor_bits;
	}

	/* Divide the quotient by subtracting shifted divisor. */
	uint64_t dividend = 0;

	for (int shift = 64 - divisor_bits; shift >= 0; --shift) {
		uint64_t shifted_divisor = (uint64_t)divisor << shift;

		if (quotient >= shifted_divisor) {
			quotient -= shifted_divisor;
			dividend |= 1llu << shift;
		}
	}

	return dividend;
}

#endif /* MATH_H_ */

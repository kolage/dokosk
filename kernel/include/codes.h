/**
 * @file kernel/include/codes.h
 *
 * File with definition of return codes.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef CODES_H_
#define CODES_H_

/** @name Error codes
 *  @{ */
#define EOK 		 	 0  /**< "Pseudo" error code - indicates success. */
#define ENOMEM 			-1  /**< Not enough memory for an operation. */
#define EINVAL      	-2	/**< Some of the arguments is invalid. */
#define EKILLED     	-3	/**< Thread that was the subject of waiting was cancelled. */
#define ETIMEDOUT   	-4	/**< The timeout for an operation has exceeded. */
#define EWOULDBLOCK 	-5	/**< An operation would block the calling thread. */
/** @} */


/* Externals are commented with implementation. */
extern char *code_name(int result);

#endif /* CODES_H_ */

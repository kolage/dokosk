/**
 * @file kernel/include/codes.c
 *
 * File with function for translation of return code to the human
 * readable form.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <include/codes.h>


/** Translate the passed @p code to the human readable form for
 *  easier debugging.
 *
 * @param  code Code number.
 *
 * @return Name of the passed return code.
 *
 */
char *code_name(int code)
{
	switch (code) {
		case EOK:
			return "EOK";

		case ENOMEM:
			return "ENOMEM";

		case EINVAL:
			return "EINVAL";

		case EKILLED:
			return "EKILLED";

		case ETIMEDOUT:
			return "ETIMEDOUT";

		case EWOULDBLOCK:
			return "EWOULDBLOCK";

		default:
			return "UNKNOWN";
	}
}

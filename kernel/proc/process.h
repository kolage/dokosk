/**
 * @file kernel/proc/process.h
 *
 * Declarations of @ref process_management_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <proc/thread.h>
#include <adt/tree.h>


/** Magic used to identify valid thread structure. */
#define PROCESS_MAGIC_VALUE  0xDABC4002

/** Virtual address on which is every process loaded. It is four pages after the
    virtual memory start, so the NULL pointer memory is not mapped. This is an
    address from user loader script. */
#define PROCESS_VIRT_ADDR					(void *)0x00004000

/* Physical address of the initial process binary image. This is hardwired from msim.conf. */
#define INIT_PROCESS_PHYS_ADDR				(void *)0x10001000

/** Size of the initial process binary image. */
#define INIT_PROCESS_SIZE					0x00010000

/** Number of resource types of every process. */
#define PROCESS_RESOURCES_TYPES_COUNT		3

/* Masks used for thread attributes. */
#define PROCESS_KILLED_MASK					0x00000100
#define PROCESS_ZOMBIE_MASK					0x00000200
#define PROCESS_IS_SOMEONE_WAITING_MASK		0x00000400


/** Types of process sources. */
typedef enum {
	RESOURCE_TYPE_MUTEX,
	RESOURCE_TYPE_CVAR,
	RESOURCE_TYPE_THREAD,
	RESOURCE_TYPE_PROCESS		/* Processes must be the last one, because we treat with them specially. */
} resource_type_t;

struct mutex;
struct cond;
struct thread_impl;

/** Structure of the process implementation. */
typedef struct process_impl {
	/** A magic value to detect valid process structure. */
	uint32_t magic;

	/** Start address of the process heap. */
	void *heap_start;

	/** Actual number of process running threads. */
	atomic_t running_threads_count;

	/** Actual number of process running threads. */
	atomic_t registered_threads_count;

	/** Mutex related to this process. */
	struct mutex mtx;

	/** Condition variable used for indicating process finish. */
	struct cond cvar;

	/** Resources associated with the process. */
	tree_t resources[PROCESS_RESOURCES_TYPES_COUNT];

	/** Resource tree mutexes. */
	struct mutex resource_mutexes[PROCESS_RESOURCES_TYPES_COUNT];

	/** Process is registered as resource. */
	tree_node_t resource_item;

	/** Process attributes */
	unative_t attrs;

	/** Process id for debugging. */
	unative_t pid;

	/** Process can be an item in the zombie list. */
	item_t zombie_item;
} process_impl_t;

/** Structure of the process. */
typedef struct process {
	process_impl_t *impl;
} process_t;


/* Several macros to determine process state. */
#define PROCESS_IS_KILLED(proc_impl) (proc_impl->attrs & PROCESS_KILLED_MASK)
#define PROCESS_IS_SOMEONE_WAITING(proc_impl) (proc_impl->attrs & PROCESS_IS_SOMEONE_WAITING_MASK)
#define PROCESS_IS_ZOMBIE(proc_impl) (proc_impl->attrs & PROCESS_ZOMBIE_MASK)


/** Check valid process structure.
 *
 * @param process Process implementation structure.
 *
 * @return True if process has valid structure, otherwise false.
 *
 */
static inline bool process_impl_is_valid(process_impl_t *process)
{
	if (process == NULL
			|| process->pid == 0
			|| process->magic != PROCESS_MAGIC_VALUE
			|| PROCESS_IS_ZOMBIE(process)) {
		return false;
	} else {
		return true;
	}
}


/* Externals are commented with implementation. */
extern void process_lock_resources_of(process_impl_t *proc, resource_type_t resource_type);
extern void process_lock_resources(resource_type_t resource_type);
extern void process_unlock_resources_of(process_impl_t *proc, resource_type_t resource_type);
extern void process_unlock_resources(resource_type_t resource_type);

extern void process_register_resource_to(process_impl_t *proc, void *resource_object,
		tree_node_t *resource_item, resource_type_t resource_type);
extern void process_register_resource(void *resource_object, tree_node_t *resource_item,
		resource_type_t resource_type);
extern void *process_get_resource(unative_t handle, resource_type_t resource_type);
extern struct mutex *process_get_mutex(unative_t handle);
extern struct cond *process_get_cvar(unative_t handle);
extern struct thread_impl *process_get_thread(unative_t handle);
extern struct process_impl *process_get(unative_t handle);
extern void process_decrease_running_threads(process_impl_t *proc);
extern void *process_unregister_resource_from(process_impl_t *proc, unative_t handle, resource_type_t resource_type);
extern void *process_unregister_resource(unative_t handle, resource_type_t resource_type);
extern struct mutex *process_unregister_mutex(unative_t handle);
extern struct cond *process_unregister_cvar(unative_t handle);
extern struct thread_impl *process_unregister_thread(unative_t handle);
extern struct process_impl *process_unregister(unative_t handle);

extern void init_processes(void);
extern void wait_for_all_processes(void);
extern int process_create(process_t *process_ptr, const void *img, const size_t size);
extern process_impl_t *process_self_impl(void);
extern process_t process_self(void);
extern int process_join(process_t proc);
extern int process_join_timeout(process_t proc, const unsigned int usec);
extern int kill(process_t proc);
extern void process_dealloc(process_impl_t *process);

#endif	/* PROCESS_H_ */

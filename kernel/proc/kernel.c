/**
 * @file kernel/proc/kernel.c
 *
 * Implementation of kernel threads and kernel specific functions.
 *
 * This file is related to the @ref kernel_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page kernel_feature Kernel lifetime
 *
 * This page describes what are the main tasks of kernel after booting up.
 *
 * Kernel can run in several different modes. When a KERNEL_TEST is
 * specified, no user process is created and kernel just calls the
 * ::run_test function.
 *
 * In the opposite case there is created an initial process, which is
 * loaded from the pre-specified physical address ::INIT_PROCESS_PHYS_ADDR.
 * At first a new area of virtual memory is mapped to this physical
 * address and then standard ::process_create function is called.
 * The area has to be mapped, because ::process_create function uses
 * mechanism for copying data between separate address spaces, concretely
 * it uses ::copy_to_thread function. Size of the initial process
 * is limited by the size of the area it mapped to. This is specified by
 * ::INIT_PROCESS_SIZE constant.
 *
 * After starting the initial process, kernel just waits until there
 * is no running process, then the MSIM is halted - ::wait_for_all_processes
 * function implements this feature.  When created processes
 * are completely independent, so process B created by process A can
 * rung even after process A has finished.
 *
 * The main task of kernel is to deallocate resources allocated by
 * running (or killed) processes. For that reason there is a system work
 * thread which functionality is implemented in ::kernel_work_thread
 * function. This thread test all the zombie lists whether they contain
 * some mutex, condition variable, thread or process, that can be
 * deallocated and if so, it deallocates it and removes it from the
 * zombie list. When the lists are empty, this thread just yields, so
 * it does not consume processor time unnecessarily.
 *
 * There is also one system idle thread that does nothing useful.
 * This thread is there just as assurance that there are always some
 * threads that can be scheduled. This thread is normally ignored
 * in scheduler, when there are some other schedulable threads.
 *
 * See also page about @userpage{process_feature, initial process in user space}.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <drivers/keyboard.h>
#include <drivers/printer.h>
#include <mm/malloc.h>
#include <proc/thread.h>
#include <proc/process.h>
#include <proc/kernel.h>
#include <sync/mutex.h>
#include <sync/cond.h>


#if !defined(DEBUG_KERNEL) || defined(__DOXYGEN__)
/** Debug the kernel?
 *
 * By default debugging prints for kernel are disabled.
 * You can define ::DEBUG_KERNEL to true to enable kernel debugging prints.
 * To do this, add KERNEL to KERNEL_DEBUG when building the OS:
 * <pre>make KERNEL_DEBUG="KERNEL"</pre>
 */
#define DEBUG_KERNEL false
#endif

#if DEBUG_KERNEL
#define kernel_debug_print(ARGS...) printk(ARGS);
#else
#define kernel_debug_print(ARGS...)
#endif


/** List with destroyed mutexes waiting to be deallocated */
static list_t zombie_mutexes;

/** List with destroyed condition variables waiting to be deallocated */
static list_t zombie_cvars;

/** List with finished or killed threads waiting to be deallocated */
static list_t zombie_threads;

/** List with finished or killed processes waiting to be deallocated */
static list_t zombie_processes;


/** Check whether the kernel is initialized.
 *
 * It is when the both system idle thread and the main kernel thread are
 * created, so some thread is actually running in the system.
 *
 * @return Is the kernel initialized?
 *
 */
bool kernel_is_initialized(void)
{
	return thread_get_current_impl() != NULL;
}

/** Put the mutex to the list with mutexes which should be deallocated
 *  by the system background thread.
 *
 * @param mutex Mutex which should be deallocated.
 *
 */
void kernel_put_mutex_to_zombies(mutex_t *mutex)
{
	ipl_t state = query_and_disable_interrupts();

	if (!list_contains(&zombie_mutexes, &mutex->zombie_item)) {
		list_append(&zombie_mutexes, &mutex->zombie_item);
	}

	conditionally_enable_interrupts(state);
}

/** Put the condition variable to the list with condition variables which
 *  should be deallocated by the system background thread.
 *
 * @param cvar Condition variable which should be deallocated.
 *
 */
void kernel_put_cond_to_zombies(cond_t *cvar)
{
	ipl_t state = query_and_disable_interrupts();

	if (!list_contains(&zombie_cvars, &cvar->zombie_item)) {
		list_append(&zombie_cvars, &cvar->zombie_item);
	}

	conditionally_enable_interrupts(state);
}

/** Put the thread to the list with threads which should be deallocated
 *  by the system background thread.
 *
 * @param thread Thread which should be deallocated.
 *
 */
void kernel_put_thread_to_zombies(thread_impl_t *thread)
{
	ipl_t state = query_and_disable_interrupts();

	if (!THREAD_IS_ZOMBIE(thread)) {
		thread->attrs |= THREAD_ZOMBIE_MASK;

		/* Now we now that now one should use the thread mutex and condition
		   variable any more, so we can destroy them. */
		mutex_destroy(&thread->mtx);
		cond_destroy(&thread->cvar);

		if (!list_contains(&zombie_threads, &thread->zombie_item)) {
			list_append(&zombie_threads, &thread->zombie_item);
		}
	}

	conditionally_enable_interrupts(state);
}

/** Put the process to the list with processes which should be deallocated
 *  by the system background thread.
 *
 * @param process Process which should be deallocated.
 *
 */
void kernel_put_process_to_zombies(process_impl_t *process)
{
	ipl_t state = query_and_disable_interrupts();

	if (!list_contains(&zombie_processes, &process->zombie_item)) {
		process->attrs |= PROCESS_ZOMBIE_MASK;
		list_append(&zombie_processes, &process->zombie_item);
	}

	conditionally_enable_interrupts(state);
}

/** Function executed in system idle thread.
 *
 * This thread is created as the first one (it has id 1) and it is
 * not planned when there are some other schedulable threads. Its
 * only purpose is to ensure that there is always at least one thread
 * that can be scheduled.
 *
 * @param  data Thread data. This parameter is unused.
 *
 * @return This function never returns, it is in infinite loop.
 *
 */
static void *kernel_idle_thread(void *data)
{
	while (true) {
		thread_yield();
	}

	return NULL;
}

/** Function executed in system working thread.
 *
 * This thread deallocates all the mutexes, condition variables, threads
 * and process that are in zombie state.
 *
 * @param  data Thread data. This parameter is unused.
 *
 * @return This function never returns, it is in infinite loop.
 *
 */
static void *kernel_work_thread(void *data)
{
	while (true) {
		/* Deallocate one of the zombie mutexes. */
		if (!list_empty(&zombie_mutexes)) {
			item_t *list_item = zombie_mutexes.head;
			mutex_t *mtx = (mutex_t *)list_item->data;

			/* Only mutexes that are not used in some syscall can be deallocated. */
			if (atomic_get(&mtx->syscall_use_count) == 0 || !process_impl_is_valid(mtx->process)) {
				kernel_debug_print("-- KERNEL: Deallocating mutex %u.\n", mtx->mutex_id);

				/* There might remain some blocked threads on the mutex, but only when the
				   process is already zombie or deallocated, so it does not matter if we just
				   reinitialize the waiting threads list and forget about them. */
				list_init(&mtx->waiting_threads);
				mutex_destroy(mtx);
				free(mtx);

				ipl_t state = query_and_disable_interrupts();
				list_remove(&zombie_mutexes, list_item);
				conditionally_enable_interrupts(state);
			} else {
				kernel_debug_print("-- KERNEL: Mutex %u can not be deallocated.\n", mtx->mutex_id);
				ipl_t state = query_and_disable_interrupts();
				list_rotate(&zombie_mutexes);
				conditionally_enable_interrupts(state);
			}
		}

		/* Deallocate one of the zombie condition variables. */
		if (!list_empty(&zombie_cvars)) {
			item_t *list_item = zombie_cvars.head;
			cond_t *cvar = (cond_t *)list_item->data;

			/* Only condition variables that are not used in some syscall can be deallocated. */
			if (atomic_get(&cvar->syscall_use_count) == 0 || PROCESS_IS_ZOMBIE(cvar->process)) {
				kernel_debug_print("-- KERNEL: Deallocating condition variable %u.\n", cvar->cvar_id);

				/* There might remain some waiting threads on the condition variable, but only when the
				   process is already zombie or deallocated, so it does not matter if we just
				   reinitialize the waiting threads list and forget about them. */
				list_init(&cvar->waiting_threads);
				cond_destroy(cvar);
				free(cvar);

				ipl_t state = query_and_disable_interrupts();
				list_remove(&zombie_cvars, list_item);
				conditionally_enable_interrupts(state);
			} else {
				kernel_debug_print("-- KERNEL: Condition variable %u can not be deallocated.\n", cvar->cvar_id);
				ipl_t state = query_and_disable_interrupts();
				list_rotate(&zombie_cvars);
				conditionally_enable_interrupts(state);
			}
		}

		/* Deallocate one of the zombie threads. */
		if (!list_empty(&zombie_threads)) {
			item_t *list_item = zombie_threads.head;
			thread_impl_t *thread = (thread_impl_t *)list_item->data;
			assert(THREAD_IS_ZOMBIE(thread));

			/* Only uninitialized threads can be deallocated. */
			if (THREAD_IS_UNINITIALIZED(thread)) {
				kernel_debug_print("-- KERNEL: Deallocating thread %u.\n", thread->thread_id);

				/* Unregister the thread from its process resource tree. */
				process_unregister_resource_from(thread->process, (unative_t)thread, RESOURCE_TYPE_THREAD);
				thread_dealloc(thread);

				ipl_t state = query_and_disable_interrupts();
				list_remove(&zombie_threads, list_item);
				conditionally_enable_interrupts(state);
			} else {
				kernel_debug_print("-- KERNEL: Thread %u can not be deallocated.\n", thread->thread_id);

				/* If we found thread that is not uninitialized, we rotate the list,
				   so we can continue in threads deallocation. */
				ipl_t state = query_and_disable_interrupts();
				list_rotate(&zombie_threads);
				conditionally_enable_interrupts(state);
			}
		}

		/* Deallocate one of the zombie processes. */
		if (!list_empty(&zombie_processes)) {
			item_t *list_item = zombie_processes.head;
			process_impl_t *process = (process_impl_t *)list_item->data;
			assert(PROCESS_IS_ZOMBIE(process));

			/* Only processes that have no registered threads (all of them have
			   been deallocated) can be deallocated. */
			if (atomic_get(&process->registered_threads_count) == 0) {
				kernel_debug_print("-- KERNEL: Deallocating process %u.\n", process->pid);
				process_dealloc(process);

				ipl_t state = query_and_disable_interrupts();
				list_remove(&zombie_processes, list_item);
				conditionally_enable_interrupts(state);
			} else {
				kernel_debug_print("-- KERNEL: Process %u can not be deallocated.\n", process->pid);

				/* If we found process that can not be deallocated, we rotate the list,
				   so we can continue in deallocation next time. */
				ipl_t state = query_and_disable_interrupts();
				list_rotate(&zombie_processes);
				conditionally_enable_interrupts(state);
			}
		}

		/* Yield so we do not get too much processor time when lists are empty. */
		thread_yield();
	}

	return NULL;
}

/** Create kernel system threads.
 *
 */
void init_kernel(void)
{
	/* Create the system threads. */
	thread_t thread;
	list_init(&zombie_threads);
	list_init(&zombie_mutexes);
	list_init(&zombie_cvars);
	list_init(&zombie_processes);

	if (thread_create(&thread, kernel_idle_thread, THREAD_NO_DATA, THREAD_STD_FLAGS | TF_NEW_VMM) != EOK) {
		panic("Could not create the system idle thread.");
	}

	if (thread_create(&thread, kernel_work_thread, THREAD_NO_DATA, THREAD_STD_FLAGS) != EOK) {
		panic("Could not create the system work thread.");
	}
}

/** Run the initial user process and wait until all the processes
 *  finishes to run.
 *
 */
void kernel_run_user_processes(void)
{
	process_t process;

	/* Create first user process which is mapped in the memory. */
	puts("Starting user main process...\n");

	/* Size of the initial process is limited. */
	size_t img_size = INIT_PROCESS_SIZE;
	img_size = ALIGN_UP(img_size, PAGE_SIZE);
	void *img_ptr = PTR_ADDR_IN_KSEG0(INIT_PROCESS_PHYS_ADDR);

	/* Map the process binary image to the virtual memory. */
	if (vma_map(&img_ptr, img_size, VF_AT_KSEG0 | VF_VA_USER | VF_VA_NOFRAME) != EOK) {
		panic("Could not create the mapping for the user binary image!");
	}

	/* Create the initial process. */
	if (process_create(&process, img_ptr, img_size) != EOK) {
		panic("Could not create the user main process!");
	}

	/* Wait for all the processes to finish. */
	wait_for_all_processes();
	printk_nb("\nAll processes have finished...\n");
}

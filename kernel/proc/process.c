/**
 * @file kernel/proc/process.c
 *
 * Implementation of @ref process_management_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page process_management_feature Process management
 *
 * This page describes how user processes are implemented in kernel.
 *
 * The key features of process management are ensuring that processes run
 * in different address spaces (it means that all of the process threads
 * share the same address space), keeping track about allocated resources
 * and deallocation of those resources when possible.
 *
 * Resources allocated while a process is running are stored in the AVL
 * tree ::tree_t structure. There is one tree for each type of resources
 * per process (resources are mutexes, condition variables and threads).
 * There is also one global AVL tree that keeps track about running processes.
 * Each of those trees has its own mutex, so the tree is not corrupted
 * while some thread is using it. Process is implemented in ::process_impl_t
 * structure.
 *
 * There are several functions for working with process resources like
 * ::process_register_resource_to, ::process_get_resource,
 * ::process_unregister_resource_from and their variants. Locking and unlocking
 * of resource trees can be managed by calling one of ::process_lock_resources_of
 * and ::process_unlock_resources_of functions.
 *
 * When new process is created a new address space is allocated and the binary
 * image of the process is loaded to that address space. This image is
 * loaded four pages after the start of the virtual address space. We wanted
 * to leave first pages of each address space unmapped, so ::NULL dereferences
 * cause the TLB refill exception and they can be more easily detected.
 *
 * Also a first thread of the process is created and started and process
 * initializes itself in user space. Area for heap is placed in virtual memory
 * just after the area with process binary image.
 *
 * Each created thread shares the same address space with thread that has
 * created it. It also allocates a new area for its stack. The free space
 * for this area is searched from the end of the address space which is
 * caused by ::VF_VA_FROM_END flag, so the process heap has a lot of free space
 * where it can grow up.
 *
 * There are two ways how a process can be terminated: it is when the process
 * detects that it has no running threads or when it is killed. Each process
 * has an atomic counter process::running_threads_count which indicates how many
 * running threads it has. When this counter reaches 0, the process calls
 * ::kill function on itself - it commits suicide.
 *
 * We must be very careful when killing the process - we want to avoid deallocation
 * of some resource which might be used in some thread running in the process.
 * Also the kernel must be in a consistent state when after the process is killed.
 *
 * We have solved it this way: the first first done in ::kill function is
 * signaling to the thread waiting for a join with the process and process
 * is atomically marked as killed, so it can not get killed more than once.
 * The second thing to be done is to stop all the remaining threads of the process.
 * Therefore the resource tree with the process threads is locked and repeatedly
 * traversed. We search there for threads that are not in critical section in
 * some syscall, because if we would kill a thread in critical section, the kernel
 * might get corrupted (for ex. mutex for kernel heap might remain locked etc.).
 *
 * Those threads are stopped and added to the list with zombie threads where the
 * system work thread will deallocate them. This resource tree is traversed until
 * all of threads it contains are stopped and added to the zombie list.
 *
 * After that we know that no thread of the process is running anymore (there is only
 * one exception from that - when the process is committing suicide, the thread
 * that called ::kill function is still running). So other resource trees of the
 * process are traversed and those resources (mutexes and condition variables)
 * are deallocated immediately. After that process adds itself to zombie list
 * (it will get deallocated by system work thread) and if it is committing suicide,
 * current thread is also unscheduled and added to the zombie list.
 *
 * This mechanism provides consistency of kernel data structures and it also
 * guarantees that all of the process resources get deallocated in the finite time
 * after the process has finished.
 *
 * See also page about @userpage{process_feature, initial process in user space}.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <proc/thread.h>
#include <proc/process.h>
#include <proc/kernel.h>
#include <drivers/printer.h>
#include <drivers/keyboard.h>
#include <sched/sched.h>
#include <mm/malloc.h>
#include <mm/vma.h>
#include <mm/thrcopy.h>
#include <sync/cond.h>
#include <sync/mutex.h>
#include <adt/tree.h>


/** The id for the process that will be created next. */
ATOMIC_DECLARE(newly_created_process_id, 0);

/** Tree of the created processes and its mutex. */
static tree_t process_handle_tree;
static mutex_t process_tree_mutex;

/** Condition variable and mutex for signaling when some of the processes has finished. */
static mutex_t process_mutex;
static cond_t process_cvar;


/** Initialize process management.
 *
 */
void init_processes(void)
{
	/* Initialize tree with handles of processes. */
	tree_init(&process_handle_tree);
	mutex_init_recursive(&process_tree_mutex);

	/* Initialize synchronization primitives for signaling. */
	mutex_init(&process_mutex);
	cond_init(&process_cvar);
}

/** Passively wait until all of the processes finish.
 *
 */
void wait_for_all_processes(void)
{
	mutex_lock(&process_mutex);

	/* Wait until the tree with processes is empty. */
	while (process_handle_tree.root != NULL) {
		cond_wait(&process_cvar, &process_mutex);
	}

	mutex_unlock(&process_mutex);
}

/** Initialize the structure with sources associated with the process.
 *
 * @param proc Process to get initialized.
 *
 */
static void process_init_resources(process_impl_t *proc)
{
	for (int i = 0; i < PROCESS_RESOURCES_TYPES_COUNT; i++) {
		tree_init(&proc->resources[i]);
		mutex_init_recursive(&proc->resource_mutexes[i]);
	}
}

/** Get the tree associated with the resources of specified type.
 *
 * @param  proc          Process of which we want to get the resource tree.
 * @param  resource_type Type of the registered resource.
 *
 * @return Tree with resources of specified type.
 *
 */
static tree_t *process_get_resource_tree(process_impl_t *proc, resource_type_t resource_type)
{
	/* We treat with processes specially - they are registered globally. */
	if (resource_type == RESOURCE_TYPE_PROCESS) {
		return &process_handle_tree;
	} else {
		return &proc->resources[resource_type];
	}
}

/** Get the mutex associated with the resource tree of specified type.
 *
 * @param  proc          Process of which we want to get the resource tree mutex.
 * @param  resource_type Type of the registered resource.
 *
 * @return Mutex of the resource tree.
 *
 */
static mutex_t *process_get_resource_tree_mutex(process_impl_t *proc, resource_type_t resource_type)
{
	/* We treat with processes specially - they are registered globally. */
	if (resource_type == RESOURCE_TYPE_PROCESS) {
		return &process_tree_mutex;
	} else {
		return &proc->resource_mutexes[resource_type];
	}
}

/** Lock the resource tree of passed @p proc.
 *
 * @param proc          Process of which we want to lock the resource tree.
 * @param resource_type Type of the tree to get locked.
 *
 */
void process_lock_resources_of(process_impl_t *proc, resource_type_t resource_type)
{
	mutex_t *mutex = process_get_resource_tree_mutex(proc, resource_type);
	mutex_lock(mutex);
}

/** Lock the resource tree of currently running process.
 *
 * @param resource_type Type of the tree to get locked.
 *
 */
void process_lock_resources(resource_type_t resource_type)
{
	process_lock_resources_of(process_self_impl(), resource_type);
}

/** Unlock the resource tree of passed @p proc.
 *
 * @param proc          Process of which we want to unlock the resource tree.
 * @param resource_type Type of the tree to get unlocked.
 *
 */
void process_unlock_resources_of(process_impl_t *proc, resource_type_t resource_type)
{
	mutex_t *mutex = process_get_resource_tree_mutex(proc, resource_type);
	mutex_unlock(mutex);
}

/** Unlock the resource tree of currently running process.
 *
 * @param resource_type Type of the tree to get unlocked.
 *
 */
void process_unlock_resources(resource_type_t resource_type)
{
	process_unlock_resources_of(process_self_impl(), resource_type);
}

/** Register new resource of passed @p proc.
 *
 * @param proc            Process to which we want to register the resource.
 * @param resource_object An resource which should be registered.
 * @param resource_item   Tree node that represents the resource in the process resources tree.
 * @param resource_type   Type of the registered resource.
 *
 */
void process_register_resource_to(process_impl_t *proc, void *resource_object, tree_node_t *resource_item, resource_type_t resource_type)
{
	if (proc == NULL && resource_type != RESOURCE_TYPE_PROCESS) {
		return;
	}

	tree_t *resource_tree = process_get_resource_tree(proc, resource_type);
	resource_item->key = (unative_t)resource_object;

	/* Add the resource to the list of resources. */
	process_lock_resources_of(proc, resource_type);
	tree_add(resource_tree, resource_item);
	process_unlock_resources_of(proc, resource_type);

	if (resource_type == RESOURCE_TYPE_THREAD) {
		atomic_increment(&proc->running_threads_count);
		atomic_increment(&proc->registered_threads_count);
	}
}

/** Register new resource of currently running process.
 *
 * @param resource_object An resource which should be registered.
 * @param resource_item   Tree node that represents the resource in the
 *                        process resources tree.
 * @param resource_type   Type of the registered resource.
 *
 */
void process_register_resource(void *resource_object, tree_node_t *resource_item, resource_type_t resource_type)
{
	process_register_resource_to(process_self_impl(), resource_object, resource_item, resource_type);
}

/** Get the resource registered in the currently running process resource
 *  tree under the passed @p handle.

 * @param  handle        An identifier of the resource in the process
 *                       resources tree.
 * @param  resource_type Type of the searched resource.
 *
 * @return Pointer to the registered resource or ::NULL when no resource is
 *         registered under the passed @p handle.
 *
 */
void *process_get_resource(unative_t handle, resource_type_t resource_type)
{
	process_impl_t *proc = process_self_impl();
	tree_t *resource_tree = process_get_resource_tree(proc, resource_type);

	process_lock_resources_of(proc, resource_type);
	tree_node_t *node = tree_find(resource_tree, handle);
	process_unlock_resources_of(proc, resource_type);

	if (node != NULL) {
		return (void *)node->key;
	} else {
		return NULL;
	}
}

/** Get the mutex registered in the currently running process resource
 *  tree under the passed @p handle.
 *
 * @param  handle An identifier of the mutex in the process resources tree.
 *
 * @return Pointer to the registered mutex or ::NULL when no mutex is
 *         registered under the passed @p handle.
 *
 */
mutex_t *process_get_mutex(unative_t handle)
{
	return (mutex_t *)process_get_resource(handle, RESOURCE_TYPE_MUTEX);
}

/** Get the condition variable registered in the currently running process
 *  resource tree under the passed @p handle.
 *
 * @param  handle An identifier of the condition variable in the process
 *                resources tree.
 *
 * @return Pointer to the registered condition variable or ::NULL when no
 *         condition variable is registered under the passed @p handle.
 *
 */
cond_t *process_get_cvar(unative_t handle)
{
	return (cond_t *)process_get_resource(handle, RESOURCE_TYPE_CVAR);
}

/** Get the thread registered in the currently running process resource
 *  tree under the passed @p handle.
 *
 * @param  handle An identifier of the thread in the process resources tree.
 *
 * @return Pointer to the registered thread or ::NULL when no thread is
 *         registered under the passed @p handle.
 *
 */
struct thread_impl *process_get_thread(unative_t handle)
{
	return (thread_impl_t *)process_get_resource(handle, RESOURCE_TYPE_THREAD);
}

/** Get the process registered under the passed @p handle.
 *
 * @param  handle An identifier of the process in the global process
 *                resource tree.
 *
 * @return Pointer to the registered process or ::NULL when no process is
 *         registered under the passed @p handle.
 *
 */
struct process_impl *process_get(unative_t handle)
{
	return (process_impl_t *)process_get_resource(handle, RESOURCE_TYPE_PROCESS);
}

/** Decrease the number of running threads of the passe @p proc.
 *
 * @param proc Affected process.
 *
 */
void process_decrease_running_threads(process_impl_t *proc)
{
	if (proc == NULL) {
		return;
	}

	assert(process_impl_is_valid(proc));
	ipl_t state = query_and_disable_interrupts();
	assert(atomic_get(&proc->running_threads_count) > 0);
	atomic_decrement(&proc->running_threads_count);

	/* Has the process finished? */
	if (atomic_get(&proc->running_threads_count) == 0) {
		process_t process;
		process.impl = proc;
		kill(process);
	}

	conditionally_enable_interrupts(state);
}

/** Unregister the resource with the passed @p handle from the resource tree
 *  of passed @p proc.
 *
 * @param proc          Process which we want to unregister the resource from.
 * @param handle        An identifier of the resource in the process
 *                      resource tree.
 * @param resource_type Type of the searched resource.
 *
 */
void *process_unregister_resource_from(process_impl_t *proc, unative_t handle, resource_type_t resource_type)
{
	if (proc == NULL && resource_type != RESOURCE_TYPE_PROCESS) {
		return NULL;
	}

	tree_t *resource_tree = process_get_resource_tree(proc, resource_type);

	/* Remove the resource from the list of resources. */
	process_lock_resources_of(proc, resource_type);
	tree_node_t *node = tree_remove(resource_tree, handle);
	process_unlock_resources_of(proc, resource_type);

	if (node != NULL) {
		if (resource_type == RESOURCE_TYPE_THREAD) {
			assert(atomic_get(&proc->registered_threads_count) > 0);
			atomic_decrement(&proc->registered_threads_count);
		}

		return (void *)node->key;
	} else {
		return NULL;
	}
}

/** Unregister the resource with the passed @p handle from the resource tree
 *  of currently running process.
 *
 * @param handle        An identifier of the resource in the process
 *                      resource tree.
 * @param resource_type Type of the searched resource.
 *
 */
void *process_unregister_resource(unative_t handle, resource_type_t resource_type)
{
	return process_unregister_resource_from(process_self_impl(), handle, resource_type);
}

/** Unregister the mutex with the passed @p handle from the resource tree
 *  of currently running process.
 *
 * @param handle An identifier of the mutex in the process resource tree.
 *
 */
mutex_t *process_unregister_mutex(unative_t handle)
{
	return (mutex_t *)process_unregister_resource(handle, RESOURCE_TYPE_MUTEX);
}

/** Unregisters the condition variable with the passed @p handle from the
 *  resource tree of currently running process.
 *
 * @param handle An identifier of the condition variable in the process
 *               resource tree.
 *
 */
cond_t *process_unregister_cvar(unative_t handle)
{
	return (cond_t *)process_unregister_resource(handle, RESOURCE_TYPE_CVAR);
}

/** Unregisters the thread with the passed @p handle from the resource tree
 *  of currently running process.
 *
 * @param handle An identifier of the thread in the process resource tree.
 *
 */
struct thread_impl *process_unregister_thread(unative_t handle)
{
	return (thread_impl_t *)process_unregister_resource(handle, RESOURCE_TYPE_THREAD);
}

/** Unregisters the process with the passed @p handle from the global
 *  resource tree with registered processes.
 *
 * @param handle An identifier of the process in the resource tree.
 *
 */
struct process_impl *process_unregister(unative_t handle)
{
	return (process_impl_t *)process_unregister_resource(handle, RESOURCE_TYPE_PROCESS);
}

/** Create new user process with a new address space.
 *
 * The binary image of the process is loaded from the address space of
 * currently running thread from address @p img.
 *
 * @param  process_ptr Identifier of process to get filled.
 * @param  img         Address of process binary image in the current
 *                     address space.
 * @param  size        Size of the process binary image.
 *
 * @return Returns ::EOK when the process is successfully created, other
 *         return codes when the process could not be created.
 *
 */
int process_create(process_t *process_ptr, const void *img, const size_t size)
{
	process_ptr->impl = malloc(sizeof(process_impl_t));

	if (process_ptr->impl == NULL) {
		return ENOMEM;
	}

	mutex_init(&process_ptr->impl->mtx);
	cond_init(&process_ptr->impl->cvar);
	item_init(&process_ptr->impl->zombie_item, process_ptr->impl);

	process_init_resources(process_ptr->impl);

	/* Note: running_threads_count item is increased when
	   process_register_resource with RESOURCE_TYPE_THREAD is called. */
	atomic_set(&process_ptr->impl->running_threads_count, 0);
	atomic_set(&process_ptr->impl->registered_threads_count, 0);

	/* Create the process first thread. */
	thread_t main_thread;
	void *process_virt_addr = PROCESS_VIRT_ADDR;
	int result = thread_create_user(
		&main_thread, process_virt_addr, THREAD_NO_DATA, THREAD_NO_DATA,
		process_ptr->impl, TF_NEW_VMM
	);

	if (result != EOK) {
		free(process_ptr->impl);
		return result;
	}

	/* Map the process binary to the thread virtual address space. */
	result = vma_map_in_addr_space(
		main_thread.impl->owned_addr_space, &process_virt_addr,
		ALIGN_UP(size, PAGE_SIZE), VF_VA_USER | VF_AT_KUSEG
	);

	if (result != EOK) {
		thread_dealloc(main_thread.impl);
		free(process_ptr->impl);
		return result;
	}

	/* Copy the process binary to its virtual memory. */
	copy_to_thread(main_thread, process_virt_addr, img, size);

	process_ptr->impl->magic = PROCESS_MAGIC_VALUE;
	process_ptr->impl->heap_start = NULL;
	process_ptr->impl->resource_item.key = (unative_t)process_ptr->impl;
	process_ptr->impl->attrs = 0;
	main_thread.impl->process = process_ptr->impl;

	/* Set the process id and atomically increase it. */
	process_ptr->impl->pid = atomic_increment(&newly_created_process_id);

	/* Register process as the resource - this is an atomic operation (protected by mutex). */
	process_register_resource(
		process_ptr->impl, &process_ptr->impl->resource_item, RESOURCE_TYPE_PROCESS
	);
	process_register_resource_to(
		process_ptr->impl, main_thread.impl, &main_thread.impl->resource_item, RESOURCE_TYPE_THREAD
	);

	/* Run the user main thread. */
	runnable(main_thread.impl);

	return EOK;
}

/** Get a currently running process.
 *
 * When the kernel thread calls this function, ::NULL value is returned.
 *
 * @return Currently running process.
 *
 */
process_impl_t *process_self_impl(void)
{
	if (kernel_is_initialized()) {
		return thread_get_current_impl()->process;
	} else {
		return NULL;
	}
}

/** Get an identifier of currently running process.
 *
 * When the kernel thread calls this function, ::NULL value is returned
 * as process impl.
 *
 * @return Currently running process.
 *
 */
process_t process_self(void)
{
	process_t process;
	process.impl = process_self_impl();

	return process;
}

/** Block the calling thread until the passed process finishes.
 *
 * Returns ::EINVAL when someone else is waiting for the process, or when
 * thread is calling the function for its own process.
 *
 * @param  proc    Process to wait for.
 * @param  usec    Number of microseconds to wait maximally.
 * @param  timeout Use the _timeout variant of the function?
 *
 * @return ::EINVAL when the process can not be joined, ::ETIMEDOUT when
 *         timeout elapsed, ::EOK otherwise.
 *
 */
static int process_join_proc(process_t proc, const unsigned int usec, bool timeout)
{
	ipl_t state = query_and_disable_interrupts();
	process_impl_t *proc_impl = proc.impl;

	if (!process_impl_is_valid(proc_impl)
			|| PROCESS_IS_SOMEONE_WAITING(proc_impl)
			|| PROCESS_IS_KILLED(proc_impl)
			|| proc_impl == process_self_impl()) {
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Mark down that we are waiting for the process. */
	proc_impl->attrs |= PROCESS_IS_SOMEONE_WAITING_MASK;

	/* Now we know that thread has not finished yet and it is not killed,
	   so we will wait on condition variable until the thread finishes. */
	int ret_value = EOK;
	mutex_t *proc_mutex = &proc_impl->mtx;
	cond_t *proc_cvar = &proc_impl->cvar;

	mutex_lock(proc_mutex);

	/* Handle both process_join versions. Process can not get deallocated
	   while we are waiting on condition variable, because when the process
	   finishes it signals and we are planned with high priority. Therefore
	   we successfully lock the mutex in cond_wait and leave this function
	   with no error (kill function yields at least once and thread in
	   process_join is planned before system work thread which deallocates
	   processes). */
	if (timeout) {
		ret_value = cond_wait_timeout(proc_cvar, proc_mutex, usec);
	} else {
		cond_wait(proc_cvar, proc_mutex);
	}

	mutex_unlock(proc_mutex);

	/* Mark down that we are not waiting anymore. */
	proc_impl->attrs &= ~PROCESS_IS_SOMEONE_WAITING_MASK;
	conditionally_enable_interrupts(state);

	return ret_value;
}

/** Block the calling thread until the passed process finishes.
 *
 * Returns ::EINVAL when someone else is waiting for the process, or when
 * thread is calling the function for its own process.
 *
 * @param  proc Process to wait for.
 *
 * @return ::EINVAL when the process can not be joined, otherwise ::EOK.
 *
 */
int process_join(process_t proc)
{
	return process_join_proc(proc, 0, false);
}

/** Block the calling thread until the passed process finishes or
 *  the timeout elapses.
 *
 * Returns ::EINVAL when someone else is waiting for the process, or when
 * thread is calling the function for its own process.
 *
 * This function waits maximally @p usec microseconds.
 *
 * When the waiting expired this function returns ::ETIMEDOUT.
 *
 * @param  proc Process to wait for.
 * @param  usec Number of microseconds to wait maximally.
 *
 * @return ::EINVAL when the process can not be joined, ::ETIMEDOUT when
 *         timeout elapsed, ::EOK otherwise.
 *
 */
int process_join_timeout(process_t proc, const unsigned int usec)
{
	return process_join_proc(proc, usec, true);
}

/** Kill the passed running process.
 *
 * Thread which is waiting for the process to finish is waken up.
 *
 * @param  proc Process to get killed.
 *
 * @return ::EINVAL when the passed @p proc is invalid or when it is
 *         aleready killed, ::EOK otherwise.
 *
 */
int kill(process_t proc)
{
	ipl_t state = query_and_disable_interrupts();
	process_impl_t *proc_impl = proc.impl;

	/* Is the process valid? */
	if (!process_impl_is_valid(proc_impl) || PROCESS_IS_KILLED(proc_impl)) {
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Set killed state. */
	proc_impl->attrs |= PROCESS_KILLED_MASK;

	/* Notify waiting thread that it does not have to wait anymore. */
	if (PROCESS_IS_SOMEONE_WAITING(proc_impl)) {
		/* Signal to the waiting thread. */
		mutex_lock(&proc_impl->mtx);
		cond_signal(&proc_impl->cvar);
		mutex_unlock(&proc_impl->mtx);
	}

	conditionally_enable_interrupts(state);

	/* Unregister the process if it has not been unregistered yet. */
	process_unregister((unative_t)proc_impl);

	/* Kill all the process remaining threads (some of them might be already killed). */
	tree_t *thread_tree = process_get_resource_tree(proc_impl, RESOURCE_TYPE_THREAD);
	bool some_thread_is_in_critical_section;

	do {
		tree_it_t it;
		list_it_t lit;
		list_t uninitialized_threads;
		some_thread_is_in_critical_section = false;

		list_init(&uninitialized_threads);
		process_lock_resources_of(proc_impl, RESOURCE_TYPE_THREAD);

		/* Loop through all the registered threads and try to uninitialize (stop) them. */
		for (tree_it_init(&it, thread_tree); tree_it_has_current(&it); tree_it_next(&it)) {
			thread_impl_t *thread = (thread_impl_t *)tree_it_key(&it);

			/* We do not want to stop ourselves when the process is committing suicide. */
			if (thread != thread_get_current_impl()) {
				ipl_t state = query_and_disable_interrupts();

				/* When the thread is in some critical section, we just mark it
				   as it should be killed. Thread then commits suicide when it
				   leaves the critical section or we will uninitialize it in
				   some next iteration. */
				if (THREAD_IS_IN_CRITICAL_SYSCALL_SECTION(thread)) {
					thread->attrs |= THREAD_TO_BE_KILLED_MASK;
					some_thread_is_in_critical_section = true;
				} else {
					/* We empty both lists of thread synchronization primitives,
					   so we do not get panic when there are still some threads
					   in those lists (we can just forget about them, because
					   those are only threads from killed process). */
					list_init(&thread->mtx.waiting_threads);
					list_init(&thread->cvar.waiting_threads);

					/* Release the mutex for std input (only if thread is holding it). */
					keyboard_release_mutex(thread);

					/* Uninitialize the thread and mark it as zombie. */
					thread_uninitialize(thread);
					kernel_put_thread_to_zombies(thread);

					/* Now the thread is not scheduled, so we can use its item property. */
					item_init(&thread->item, thread);
					list_append(&uninitialized_threads, &thread->item);
				}

				conditionally_enable_interrupts(state);
			}
		}

		/* Remove all the successfully uninitialized threads from the resource
		   tree, so we do not traverse the whole tree in the next iteration. */
		for (list_it_init(&lit, &uninitialized_threads); list_it_has_current(&lit); list_it_next(&lit)) {
			process_unregister_thread((unative_t)list_it_data(&lit));
		}

		process_unlock_resources_of(proc_impl, RESOURCE_TYPE_THREAD);

		/* When there are some threads in critical section in a syscall,
		   we give them a chance to leave the syscall. */
		if (some_thread_is_in_critical_section) {
			thread_yield();
		}
	} while (some_thread_is_in_critical_section);

	/* Uninitialize other process resources. We do not have to lock the
	   resource trees any more, because no thread from the process is
	   running any more. */
	tree_t *mtuex_tree = process_get_resource_tree(proc_impl, RESOURCE_TYPE_MUTEX);
	tree_t *cond_tree = process_get_resource_tree(proc_impl, RESOURCE_TYPE_CVAR);
	tree_it_t it;

	for (tree_it_init(&it, mtuex_tree); tree_it_has_current(&it); tree_it_next(&it)) {
		mutex_t *mtx = (mutex_t *)tree_it_key(&it);
		list_init(&mtx->waiting_threads);		/* We just want to be sure that there will be no panic
												   (potentially blocked threads are stopped anyway so
												   we can clean the list). */
		mutex_destroy(mtx);
		free(mtx);
	}

	for (tree_it_init(&it, cond_tree); tree_it_has_current(&it); tree_it_next(&it)) {
		cond_t *cvar = (cond_t *)tree_it_key(&it);
		list_init(&cvar->waiting_threads);		/* We just want to be sure that there will be no panic
												   (potentially blocked threads are stopped anyway so
												   we can clean the list). */
		cond_destroy(cvar);
		free(cvar);
	}

	/* Signal that the process has finished and if it is the last process, MSIM halts. */
	mutex_lock(&process_mutex);
	cond_signal(&process_cvar);
	mutex_unlock(&process_mutex);

	/* Put the process to the zombie list where it will get deallocated. */
	kernel_put_process_to_zombies(proc_impl);

	/* Do we commit suicide? */
	if (proc_impl == process_self_impl()) {
		state = query_and_disable_interrupts();
		thread_uninitialize(thread_get_current_impl());
		kernel_put_thread_to_zombies(thread_get_current_impl());
		conditionally_enable_interrupts(state);

		thread_suspend();
		panic("Process has returned to the kill function!");
	}

	return EOK;
}

/** Deallocate the previously allocated process and associated structures.
 *
 * @param process Process to be deallocated.
 *
 */
void process_dealloc(process_impl_t *process)
{
	mutex_destroy(&process->mtx);
	cond_destroy(&process->cvar);

	for (int i = 0; i < PROCESS_RESOURCES_TYPES_COUNT; i++) {
		mutex_destroy(&process->resource_mutexes[i]);
	}

	/* Unset the process magic, so we can easily recognize some design errors. */
	process->pid = 0;
	process->magic = 0;
	free(process);
}

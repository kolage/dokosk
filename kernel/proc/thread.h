/**
 * @file kernel/proc/thread.h
 *
 * Declarations of @ref thread_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <include/shared.h>
#include <include/c.h>
#include <adt/list.h>
#include <sync/mutex.h>
#include <sync/cond.h>
#include <mm/vma.h>
#include <proc/process.h>
#include <sched/timer.h>


/** Magic used to identify valid thread structure. */
#define THREAD_MAGIC_VALUE  0xBABE0001

/* Masks used for thread states. */
#define THREAD_DETACHED_MASK			0x00000100
#define THREAD_KILLED_MASK				0x00000200
#define THREAD_FINISHED_MASK			0x00000400
#define THREAD_UNINITIALIZED_MASK		0x00000800
#define THREAD_ZOMBIE_MASK				0x00001000
#define THREAD_SYSCALL_MASK				0x00002000
#define THREAD_IS_SOMEONE_WAITING_MASK	0x00004000
#define THREAD_TO_BE_KILLED_MASK		0x00008000

/** Thread flags - create new virtual memory space for the newly
    created thread (otherwise it will share the VM space with
    the thread that called thread_create function. */
#define TF_NEW_VMM 		1

/** Flag that indicates that we want to create an user thread. */
#define TF_USER_THREAD  2

/** Thread stack size.
 *
 * The size of the thread stack. This should be set liberally,
 * since stack overflow typically has obscure syndroms and
 * is notoriously difficult to debug.
 *
 */
#define THREAD_STACK_SIZE  PAGE_SIZE


/** Possible synchronization statuses. */
typedef enum {
	THREAD_SYNC_STATUS_KEEP_WAITING,
	THREAD_SYNC_STATUS_LOCKED,
	THREAD_SYNC_STATUS_SIGNALED,
	THREAD_SYNC_STATUS_TIMED_OUT
} thread_sync_status_t;

/** Thread flags structure.
 *
 * The structure represents various kinds of thread flags used
 * to manage thread running function behavior.
 *
 */
typedef unative_t thread_flags_t;

/** Thread control structure.
 *
 * The structure represents a kernel thread. Since the thread context
 * is kept on the thread stack, the only thing that the structure
 * contains is a reference to the stack. The structure can also
 * be an item on a list.
 *
 */
typedef struct thread_impl {
	/** A magic value to detect valid thread structure. */
	uint32_t magic;

	/** A thread can be an item on a scheduler list. */
	item_t item;

	/** A thread can be an item on thread zombie list. */
	item_t zombie_item;

	/** A thread can be an item on a synchronization list for mutex. */
	item_t mutex_sync_item;

	/** A thread can be an item on a synchronization list for cvar. */
	item_t cvar_sync_item;

	/** Keep waiting for the mutex to lock? We can wait at most at one mutex at
	    the time, so one variable is enough. */
	thread_sync_status_t mutex_sync_status;

	/** Keep waiting for the cvar? We can wait at most at one cvar at
	    the time, so one variable is enough. */
	thread_sync_status_t cvar_sync_status;

	/** Thread attributes. */
	unative_t attrs;

	/** Mutex related to this thread. */
	struct mutex mtx;

	/** Condition variable used for indicating thread finish. */
	struct cond cvar;

	/** Thread timer for waking up the thread. */
	struct timer tmr;

	/** Virtual memory table owned by the thread.
	    This field never changes during the lifetime of the thread. */
	struct vm_address_space *owned_addr_space;

	/** Virtual memory table used to map pages to frames.
	    This field can change, but can be accessed only by this thread. */
	struct vm_address_space *used_addr_space;

	/** Thread unique id at the moment. */
	unative_t thread_id;

	/** Stack block. */
	void *stack_block;

	/** Points to the saved context when the thread is not running. */
	context_t *saved_context;

	/** User stack for user threads or NULL for kernel threads. */
	void *user_stack;

	/** Process to which the thread is associated. Kernel threads has this field set to NULL. */
	struct process_impl *process;

	/** Thread can be registered as resource. */
	tree_node_t resource_item;

	/** Return value of finished thread worker function. */
	void *retval;
} thread_impl_t;

/** Thread user interface.
 *
 */
typedef struct thread {
	thread_impl_t *impl;
} thread_t;


/* Several macros to determine thread state. */
#define THREAD_IS_DETACHED(thr_impl) (thr_impl->attrs & THREAD_DETACHED_MASK)
#define THREAD_IS_CANCELLED(thr_impl) (is_user_thread(thr_impl) && (thr_impl->attrs & THREAD_KILLED_MASK))
#define THREAD_IS_KILLED(thr_impl) (thr_impl->attrs & THREAD_KILLED_MASK)
#define THREAD_HAS_FINISHED(thr_impl) (thr_impl->attrs & THREAD_FINISHED_MASK)
#define THREAD_IS_SOMEONE_WAITING(thr_impl) (thr_impl->attrs & THREAD_IS_SOMEONE_WAITING_MASK)
#define THREAD_IS_UNINITIALIZED(thr_impl) (thr_impl->attrs & THREAD_UNINITIALIZED_MASK)
#define THREAD_IS_ZOMBIE(thr_impl) (thr_impl->attrs & THREAD_ZOMBIE_MASK)
#define THREAD_TO_BE_KILLED(thr_impl) (thr_impl->attrs & THREAD_TO_BE_KILLED_MASK)

/* Threads that are in some critical section in syscall can not be killed until it leaves this section.
   Killed threads can not enter critical section any more (this an assurance to keep the kernel consistent).
   When thread is leaving the critical section it may commit suicide - it is when it was killed while it
   was in the critical section. */
#define THREAD_ENTER_CRITICAL_SYSCALL_SECTION()					\
	ipl_t _state = query_and_disable_interrupts(); 				\
	thread_impl_t *_cur_thread = thread_get_current_impl();		\
	while (THREAD_IS_KILLED(_cur_thread)) {						\
		thread_yield();											\
	}															\
	_cur_thread->attrs |= THREAD_SYSCALL_MASK; 					\
	conditionally_enable_interrupts(_state);
#define THREAD_IS_IN_CRITICAL_SYSCALL_SECTION(thr_impl)	(thr_impl->attrs & THREAD_SYSCALL_MASK)
#define THREAD_LEAVE_CRITICAL_SYSCALL_SECTION()					\
	_state = query_and_disable_interrupts(); 					\
	_cur_thread->attrs &= ~THREAD_SYSCALL_MASK;					\
	if (THREAD_TO_BE_KILLED(_cur_thread)) {						\
		thread_commit_suicide();								\
	}															\
	conditionally_enable_interrupts(_state);

/** Standard thread flags. */
#define THREAD_STD_FLAGS 	0

/** Synonym for no data passed to the thread function. */
#define THREAD_NO_DATA 		NULL


/** Check valid thread structure.
 *
 * @param thread Thread implementation structure.
 *
 * @return True if thread has valid structure, otherwise false.
 *
 */
static inline bool thread_impl_is_valid(thread_impl_t *thread)
{
	if (thread == NULL
			|| thread->thread_id == 0
			|| thread->magic != THREAD_MAGIC_VALUE
			|| THREAD_IS_ZOMBIE(thread)) {
		return false;
	} else {
		return true;
	}
}

/** Check valid thread structure.
 *
 * @param thread Thread structure.
 *
 * @return True if thread has valid structure, otherwise false.
 *
 */
static inline bool thread_is_valid(thread_t thread)
{
	return thread_impl_is_valid(thread.impl);
}


/* Externals are commented with implementation. */
extern void init_threads(void);
extern unative_t thread_get_next_id(void);
extern char *thread_tr_sync_status(thread_sync_status_t sync_status);
extern void thread_uninitialize(thread_impl_t *thread);
extern void thread_dealloc(thread_impl_t *thread);
extern void thread_exit(void *thread_retval);
extern void thread_run(void *(*start)(void *), void *data);
extern int thread_create(thread_t *thread_ptr, void *(*thread_start)(void *), void *data, const thread_flags_t flags);
extern int thread_create_user(thread_t *thread_ptr, void *(*start_wrapper)(void *,void *), void *thread_start,
		void *data, struct process_impl *process, const thread_flags_t flags);
extern thread_t thread_get_current(void);
extern thread_impl_t *thread_get_current_impl(void);
extern unative_t thread_get_current_id(void);

extern bool is_kernel_thread(thread_impl_t *thread);
extern bool is_user_thread(thread_impl_t *thread);

extern int thread_join(thread_t thread);
extern int thread_join_retval(thread_t thread, void **thread_retval);
extern int thread_join_timeout(thread_t thread, const unsigned int usec);
extern int thread_join_timeout_retval(thread_t thread, void **thread_retval, const unsigned int usec);
extern int thread_detach(thread_t thread);
extern void thread_sleep(const unsigned int sec);
extern void thread_usleep(const unsigned int usec);
extern void thread_yield(void);
extern void thread_suspend(void);
extern int thread_wakeup(thread_t thread);
extern int thread_wakeup_priority(thread_impl_t *thread, int priority);
extern void thread_commit_suicide(void);
extern int thread_kill(thread_t thread);

/** Switch processor thread context. */
extern void switch_to_thread(thread_impl_t *thread);
extern void switch_cpu_context(context_t **stack_top_old, context_t **stack_top_new);

extern void *thread_current_kernel_stack;

#endif /* THREAD_H_ */

/**
 * @file kernel/proc/kernel.h
 *
 * Declarations of kernel threads and kernel specific functions.
 *
 * This file is related to the @ref kernel_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include <include/shared.h>
#include <include/c.h>
#include <sync/mutex.h>
#include <sync/cond.h>
#include <proc/thread.h>
#include <proc/process.h>


/* Externals are commented with implementation. */
extern bool kernel_is_initialized(void);
extern void kernel_put_mutex_to_zombies(mutex_t *mutex);
extern void kernel_put_cond_to_zombies(cond_t *cvar);
extern void kernel_put_thread_to_zombies(thread_impl_t *thread);
extern void kernel_put_process_to_zombies(process_impl_t *process);

extern void init_kernel(void);
extern void kernel_run_user_processes(void);

#endif /* KERNEL_H_ */

/**
 * @file kernel/proc/thread.c
 *
 * Implementation of @ref thread_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page thread_feature Threads
 *
 * One of the crucial parts of operating system is kernel thread management.
 *
 * After structures for thread management are initialized, the main system
 * thread is created. Its task is to run user processes or launch tests if
 * they are compiled in.
 *
 * New thread can be created and scheduled by calling ::thread_create function.
 * The ::thread_create_user function is used for creating thread in user
 * space. Structure for flags could be used to determine whether new virtual
 * memory space should be created for the thread by using ::TF_NEW_VMM flag.
 *
 * We have decided to use separated stacks for kernel and user threads, so each
 * type of thread is managing only its own stack and they are not dependent
 * on each other.
 *
 * Main function that has to be running in thread is encapsulated by wrapper
 * function to easily handle function return value and deallocating thread
 * structures. Thread status remain available to other threads until the
 * thread is completely removed from system.
 * 
 * When thread has finished its run, its function return value is saved and
 * then the synchronization is handled. If thread was detached during its
 * runtime, it is deallocated immediately, otherwise it is checked, if
 * someone is waiting for this thread in ::thread_join function and if it is
 * so, the waiting thread is signaled by condition variable.  After that
 * the thread deallocation is handled in ::thread_join function by waiting 
 * thread.
 *
 * Because ::thread_join has to return ::EKILLED when it is trying to join
 * with a killed or finished thread, the thread can not be deallocated
 * immediately (added to the ::zombie_threads list). We have decided that threads are
 * added to the ::zombie_threads list only in ::thread_join and ::thread_join_timeout
 * functions (the only exception from that mechanism are detached threads).
 * That means that when no ::thread_join function is called to the thread,
 * it is not deallocated until its whole process finishes.
 *
 * Typical functions for managing thread life cycle operations such as
 * ::thread_suspend, ::thread_sleep, ::thread_kill etc. are naturally available.
 *
 */

#include <proc/thread.h>
#include <mm/malloc.h>
#include <mm/asid.h>
#include <drivers/printer.h>
#include <drivers/keyboard.h>
#include <proc/kernel.h>
#include <include/codes.h>
#include <sched/sched.h>


/** The id for the thread that will be created next. */
ATOMIC_DECLARE(newly_created_thread_id, 0);

/** Currently running thread. */
static thread_impl_t *currently_running_thread;

/** Kernel stack of the currently running thread.
    Used only in transition from user to kernel mode.
    This variable is used from head.S. */
void *thread_current_kernel_stack;


/** Get the id assigned to the last created thread.
 *
 * @return Id assigned to the last created thread.
 *
 */
unative_t thread_get_next_id(void)
{
	return atomic_get(&newly_created_thread_id);
}

/** Translate the synchronization status type from its constant representation
 *  to the human readable form.
 *
 * @param  sync_status Synchronization status type.
 *
 * @return Human readable translation of the passed synchronization status type.
 *
 */
char *thread_tr_sync_status(thread_sync_status_t sync_status)
{
	switch (sync_status) {
		case THREAD_SYNC_STATUS_KEEP_WAITING:
			return "KEEP_WAITING";
			break;

		case THREAD_SYNC_STATUS_LOCKED:
			return "LOCKED";
			break;

		case THREAD_SYNC_STATUS_SIGNALED:
			return "SIGNALED";
			break;

		case THREAD_SYNC_STATUS_TIMED_OUT:
			return "TIMED_OUT";
			break;
	}

	return "UNKNOWN";
}

/** Uninitialize the passed thread from all the relevant data structures.
 *
 * @param thread Thread to be uninitialized.
 *
 */
void thread_uninitialize(thread_impl_t *thread)
{
	ipl_t state = query_and_disable_interrupts();

	if (!THREAD_IS_UNINITIALIZED(thread)) {
		thread->attrs |= THREAD_UNINITIALIZED_MASK;
		timer_destroy(&thread->tmr);

		if (thread->item.list != NULL) {
			list_remove(thread->item.list, &thread->item);
		}

		if (thread->mutex_sync_item.list != NULL) {
			list_remove(thread->mutex_sync_item.list, &thread->mutex_sync_item);
		}

		if (thread->cvar_sync_item.list != NULL) {
			list_remove(thread->cvar_sync_item.list, &thread->cvar_sync_item);
		}

		disabled(thread);
	}

	conditionally_enable_interrupts(state);
}

/** Deallocate the previously allocated thread and associated structures.
 *
 * @param thread Thread to be deallocated.
 *
 */
void thread_dealloc(thread_impl_t *thread)
{
	if (thread->owned_addr_space != NULL) {
		if (thread->user_stack != NULL && is_user_thread(thread)) {
			vma_unmap_in_addr_space(thread->owned_addr_space, thread->user_stack);
		}

		if (thread->stack_block != NULL) {
			/* The thread always has kernel stack. */
			free(thread->stack_block);
		}

		addr_space_release(thread->owned_addr_space);
	}

	/* Unset the thread magic, so we can easily recognize some design errors. */
	thread->thread_id = 0;
	thread->magic = 0;

	free(thread);
}

/** Terminate thread execution.
 *
 * @param thread_retval Return value of thread worker function.
 *
 */
void thread_exit(void *thread_retval)
{
	/* We do not need to enable interrupts again in this thread. */
	disable_interrupts();

	thread_impl_t *current_thread = thread_get_current_impl();
	current_thread->attrs |= THREAD_FINISHED_MASK;
	process_decrease_running_threads(current_thread->process);

	/* Save return value of thread worker function. */
	current_thread->retval = thread_retval;

	/* Detached threads are deallocated immediately. */
	if (THREAD_IS_DETACHED(current_thread)) {
		kernel_put_thread_to_zombies(current_thread);
	} else if (THREAD_IS_SOMEONE_WAITING(current_thread)) {
		/* Get the thread mutex and condition variable for signaling that
		   thread has just finished. */
		mutex_t *thr_mutex = &current_thread->mtx;
		cond_t *thr_cvar = &current_thread->cvar;

		/* Waiting thread will add this thread to the zombie list. */
		mutex_lock(thr_mutex);
		cond_signal(thr_cvar);
		mutex_unlock(thr_mutex);
	}

	thread_uninitialize(current_thread);

	/* Yield the thread forever. */
	thread_yield();

	/* We should never return to this thread. */
	panic("Thread returned to the thread_exit function!");
}

/** Wrapper function to be called in thread.
 *
 * We use this wrapper function as the start function in threads to determine
 * when the function we originally wanted to start will end.
 *
 * @param start The function we want to start in thread.
 * @param data  Arguments of our function.
 *
 */
void thread_run(void *(*start)(void*), void *data)
{
	void *ret_value = start(data);
	thread_exit(ret_value);
}

/** Initialize threads management.
 *
 * Set the currently running thread to be ::NULL.
 *
 */
void init_threads(void)
{
	/* Initialize the state of current thread. */
	currently_running_thread = NULL;
}

/** Check whether the passed thread is a kernel thread.
 *
 * @param  thread Thread to be examined.
 *
 * @return ::true if the passed thread is a kernel thread, otherwise ::false.
 *
 */
inline bool is_kernel_thread(thread_impl_t *thread)
{
	return thread->process == NULL;
}

/** Check whether the passed thread is a user thread.
 *
 * @param  thread Thread to be examined.
 *
 * @return ::true if the passed thread is a user thread, otherwise ::false.
 *
 */
inline bool is_user_thread(thread_impl_t *thread)
{
	return !is_kernel_thread(thread);
}

/** Set the address space of source thread as the address space of the
 *  destination thread.
 *
 * @param thrdest Destination thread.
 * @param thrsrc  Source thread.
 *
 */
static void thread_addr_space_share(thread_impl_t *thrdest, thread_impl_t *thrsrc)
{
	assert(thrdest != NULL);
	assert(thrsrc != NULL);
	assert(atomic_get(&thrsrc->owned_addr_space->reference_count) > 0);

	atomic_increment(&thrsrc->owned_addr_space->reference_count);
	thrdest->owned_addr_space = thrsrc->owned_addr_space;
}

/** Handler function used for ::thread_usleep timer.
 *
 * @param timer Elapsed timer.
 * @param data  Data passed with the timer.
 *
 */
static void thread_timer_usleep_proc(struct timer *timer, void *data)
{
	thread_impl_t *thread = (thread_impl_t *)data;
	thread_wakeup_priority(thread, 0);
	timer_destroy(timer);
}

/** Initialize a new thread.
 *
 * The function initializes a thread. The initialization entails preparing
 * the thread context and allocating the thread stack. The thread is not
 * put on the run queue though.
 *
 * @param thread        The thread structure to initialize.
 * @param start_wrapper Pointer to the wrapper function for the thread
 *                      start function.
 * @param start         The start function of the thread.
 * @param data          Arguments to be passed to start function.
 * @param flags         Value used to manage thread function behavior.
 *
 */
static void thread_init(thread_impl_t *thread, void *start_wrapper, void *start, void *data, const thread_flags_t flags)
{
	/* Initialize basic thread features. */
	thread->magic = THREAD_MAGIC_VALUE;
	thread->mutex_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
	thread->cvar_sync_status = THREAD_SYNC_STATUS_KEEP_WAITING;
	thread->resource_item.key = (unative_t)thread;
	thread->attrs = 0;
	thread->retval = NULL;

	/* Initialize thread mutex and cvar. */
	mutex_init(&thread->mtx);
	cond_init(&thread->cvar);

	/* Initialize the list membership structure. */
	item_init(&thread->item, thread);
	item_init(&thread->zombie_item, thread);
	item_init(&thread->mutex_sync_item, thread);
	item_init(&thread->cvar_sync_item, thread);

	/* Initialize thread timer - it must be initialized, because it
	   is destroyed when the thread is destroyed. */
	timer_init(&thread->tmr, 0, thread_timer_usleep_proc, NULL);

	/* Stack grows downwards inside the allocated block. Its top
	   contains the thread context structure, which is restored
	   on context switch. Of the entire structure, only the
	   starting address of the thread and some vital
	   processor registers are set. */
	context_t *context = (context_t *)(thread->stack_block +
	    THREAD_STACK_SIZE - sizeof(context_t) - ABI_STACK_FRAME);

	thread->saved_context = context;

	/* We use a wrapper function thread_run as function that will run
	   in this thread */
	context->ra = (unative_t)start_wrapper;

	/* First argument of thread_run is function we want to start */
	context->a0 = (unative_t)start;

	/* Second argument of thread_run are arguments of our function */
	context->a1 = (unative_t)data;

	context->status = CP0_STATUS_IE_MASK | CP0_STATUS_IM_MASK;

	if (is_user_thread(thread)) {
		/* Set the user mode: the processor is in user mode, when
		   KSU = 10, EXL = 0, and ERL = 0 (page 109 of the manual). */
		context->status &= ~CP0_STATUS_KSU_MASK & ~CP0_STATUS_EXL_MASK & ~CP0_STATUS_ERL_MASK;
		context->status |= (CP0_KSU_USER_MODE << CP0_STATUS_KSU_SHIFT);

		/* Global pointer should be in the user space. */
		context->gp = 0;

		/* Set the user stack. The user stack is not in sp but t0 as described
		   in switch_cpu_context. This causes the rare case when the stack
		   pointer will be restored to a completely different location than
		   where the context strucutre is. */
		context->t0 = (unative_t)(thread->user_stack + THREAD_STACK_SIZE - ABI_STACK_FRAME);
	} else {
		context->status |= CP0_STATUS_CU0_MASK;
		context->gp = ADDR_IN_KSEG0(0);

		/* Set the kernel stack. Restoring this stack pointer has the same effect
		   as popping the context structure from the stack. */
		context->t0 = (unative_t)(thread->stack_block + THREAD_STACK_SIZE - ABI_STACK_FRAME);
	}

	/* Set the thread id and atomically increase it. */
	thread->thread_id = atomic_increment(&newly_created_thread_id);

	/* Control the overflow - id 1 is reserved for special kernel idle thread which is
	   not usually scheduled. */
	if (thread->thread_id == 0) {
		atomic_increment(&newly_created_thread_id);
		thread->thread_id = atomic_increment(&newly_created_thread_id);
	}
}

/** Worker procedure for creating new thread.
 *
 * The function creates new attached thread.
 *
 * When the ::TF_NEW_VMM flag is set, then a new virtual memory space
 * is created for the thread. Otherwise the created thread will share
 * the VM space with the currently running thread.
 *
 * ::TF_USER_THREAD flag is used when creating user thread.
 *
 * @param  thread_ptr    Newly created thread structure.
 * @param  start_wrapper Pointer to the wrapper function for the thread
 *                       start function.
 * @param  thread_start  Function to be started in newly created thread.
 * @param  data          Data related to thread_start function.
 * @param  process       Process to which the thread is attached, ::NULL
 *                       for kernel threads.
 * @param  flags         Value used to manage thread function behavior.
 *
 * @return ::ENOMEM when there was not enough memory to allocate thread
 *         structure, otherwise ::EOK.
 *
 */
static int thread_create_proc(thread_t *thread_ptr, void *start_wrapper, void *thread_start,
		void *data, process_impl_t *process, const thread_flags_t flags)
{
	thread_impl_t *thread = (thread_impl_t *)malloc(sizeof(thread_impl_t));

	/* If there was not enough memory for creating thread, return error code. */
	if (thread == NULL) {
		return ENOMEM;
	}

	/* Basic thread initialization. */
	thread->owned_addr_space = NULL;
	thread->used_addr_space = NULL;
	thread->stack_block = NULL;
	thread->user_stack = NULL;
	thread->process = process;

	/* Set the thread virtual memory space. */
	if (flags & TF_NEW_VMM) {
		/* Create a new thread virtual memory space. */
		thread->owned_addr_space = (vm_address_space_t *)malloc(sizeof(vm_address_space_t));

		/* If there was not enough memory for creating thread virtual memory
		   space, return error code. */
		if (thread->owned_addr_space == NULL) {
			/* Free the previously allocated structures. */
			thread_dealloc(thread);
			return ENOMEM;
		}

		/* Initialize the thread address space. */
		if (addr_space_init(thread->owned_addr_space) != EOK) {
			/* Free the previously allocated structures. */
			thread_dealloc(thread);
			return ENOMEM;
		}
	} else {
		/* Threads share the same address space. */
		if (kernel_is_initialized()) {
			thread_addr_space_share(thread, thread_get_current_impl());
		} else {
			/* When kernel is not initialized, we share the address space with
			   the first planned thread, that is the thread with id 1. */
			thread_addr_space_share(thread, scheduler_get_planned_thread());
		}
	}

	/* Create the thread user stack. */
	if (flags & TF_USER_THREAD) {
		/* Map one area for the user thread stack. We map the user thread stack
		   from the end of KUSEG segment, so there is a lot of space for the
		   user space heap. Note: Number of total areas is limited, so the
		   total number of allocated stacks at the time. */
		int result = vma_map_in_addr_space(
				thread->owned_addr_space,
				&thread->user_stack,
				THREAD_STACK_SIZE, VF_AT_KUSEG | VF_VA_AUTO | VF_VA_FROM_END
		);

		if (result != EOK) {
			/* Free the previously allocated structures. */
			thread_dealloc(thread);
			return ENOMEM;
		}
	}

	/* The used address space is equal to the owned address space most of the time. */
	thread->used_addr_space = thread->owned_addr_space;

	/* Allocate the kernel stack. */
	thread->stack_block = malloc(THREAD_STACK_SIZE);

	/* If there was not enough memory for creating a thread stack, return error code. */
	if (thread->stack_block == NULL) {
		/* Free the previously allocated structures. */
		thread_dealloc(thread);
		return ENOMEM;
	}

	/* Initialize the thread. */
	thread_ptr->impl = thread;
	thread_init(thread_ptr->impl, start_wrapper, thread_start, data, flags);

	/* Do not run user threads immediately, the wrapper function will do so. */
	if (is_kernel_thread(thread)) {
		runnable(thread);
	}

	return EOK;
}

/** Create a new thread.
 *
 * The function creates new attached thread.
 *
 * When the ::TF_NEW_VMM flag is set, then a new virtual memory space
 * is created for the thread. Otherwise the created thread will share
 * the VM space with the currently running thread.
 *
 * @param thread_ptr   Newly created thread structure.
 * @param thread_start Function to be started in newly created thread.
 * @param data         Data related to thread_start function.
 * @param flags        Value used to manage thread function behavior.
 *
 * @return ::ENOMEM when there was not enough memory to allocate thread
 *         structure, otherwise ::EOK.
 *
 */
int thread_create(thread_t *thread_ptr, void *(*thread_start)(void *), void *data, const thread_flags_t flags)
{
	return thread_create_proc(thread_ptr, &thread_run, thread_start, data, NULL, flags);
}

/** Create a new user thread.
 *
 * The function creates new attached thread.
 *
 * The stack is allocated in the user space of the memory. Also the
 * thread is not run immediately, the caller has to call the runnable
 * function and run the thread. Those user threads will run in the
 * user mode.
 *
 * When the ::TF_NEW_VMM flag is set, then a new virtual memory space
 * is created for the thread. Otherwise the created thread will share
 * the VM space with the currently running thread.
 *
 * @param  thread_ptr    Newly created thread structure.
 * @param  start_wrapper Pointer to the wrapper function for the thread
 *                       start function.
 * @param  thread_start  Function to be started in newly created thread.
 * @param  data          Data related to thread_start function.
 * @param  process       Process that the thread will be attached to.
 * @param  flags         Value used to manage thread function behavior.
 *
 * @return ::ENOMEM when there was not enough memory to allocate thread
 *         structure, otherwise ::EOK.
 *
 */
int thread_create_user(thread_t *thread_ptr, void *(*start_wrapper)(void *, void *),
		void *thread_start, void *data, process_impl_t *process, const thread_flags_t flags)
{
	return thread_create_proc(thread_ptr, start_wrapper, thread_start, data, process, flags | TF_USER_THREAD);
}

/** Start executing given thread.
 *
 * The function saves the context of the current thread and restores
 * the context of the thread given as argument.
 *
 * @param thread Thread to start executing.
 *
 */
void switch_to_thread(thread_impl_t *thread)
{
	assert(!THREAD_IS_UNINITIALIZED(thread));

	/* We disable interrupts so that the pointer to the current thread is
	   always in sync with the current thread. */
	ipl_t state = query_and_disable_interrupts();

	thread_impl_t *old_thread;
	old_thread = currently_running_thread;
	currently_running_thread = thread;

	/* Update the ASID. No virtual memory accesses can be performed before
	   or during switch_cpu_context. */
	asid_current_map_changed();

	/* Update the variable used to change stack when going from user to kernel mode. */
	thread_current_kernel_stack = thread->stack_block + THREAD_STACK_SIZE - ABI_STACK_FRAME;

	/* One special case to consider here is when switching context
	   for the very first time. At that time, we are running without
	   a thread structure and with a temporary stack, and therefore
	   we do not care about the context that gets saved. */
	if (old_thread == NULL) {
		context_t *dummy_saved_context;
		switch_cpu_context(&dummy_saved_context, &thread->saved_context);
	} else {
		switch_cpu_context(&old_thread->saved_context, &thread->saved_context);
	}

	/* This is subtle. The switch_cpu_context function will execute
	   another thread, which might switch to yet another thread, and so on,
	   eventually switching back to this thread. At that time, it will look
	   like we have just returned here from the switch_cpu_contextfunction. */
	conditionally_enable_interrupts(state);
}

/** Get currently running thread.
 *
 * @return Currently running thread.
 *
 */
thread_t thread_get_current(void)
{
	thread_t thread;
	thread.impl = thread_get_current_impl();

	return thread;
}

/** Get pointer to currently running thread.
 *
 * @return Currently running thread.
 *
 */
thread_impl_t *thread_get_current_impl(void)
{
	return currently_running_thread;
}

/** Get the id of currently running thread.
 *
 * @return Id of currently running thread.
 *
 */
unative_t thread_get_current_id(void)
{
	if (currently_running_thread != NULL) {
		return currently_running_thread->thread_id;
	} else {
		return 0;
	}
}

/** Function for writeback thread return value.
 *
 * @param thread Thread whose return value we want to store.
 * @param thread_retval Place where should be return value stored.
 *
 */
static inline void thread_set_retval(thread_impl_t *thread, void **thread_retval)
{
	if (thread_retval != NULL) {
		*thread_retval = thread->retval;
	}
}

/** Auxiliary function used to manage shared code of thread_join* functions.
 *
 * @param  thread        Thread to be waited for.
 * @param  usec          Time in microseconds representing the maximum time
 *                       to be waited for the thread.
 * @param  timeout       True to join with timeout, otherwise false.
 * @param  thread_retval Place to writeback return value.
 *
 * @return Error code (see thread_join* functions).
 *
 */
static int thread_join_proc(thread_impl_t *thread, const unsigned int usec, bool timeout, void **thread_retval)
{
	ipl_t state = query_and_disable_interrupts();

	if (!thread_impl_is_valid(thread)
			|| THREAD_IS_DETACHED(thread)
			|| THREAD_IS_SOMEONE_WAITING(thread)
			|| thread == thread_get_current_impl()) {
		conditionally_enable_interrupts(state);
	   	return EINVAL;
	}

	if (THREAD_IS_CANCELLED(thread)) {
		/* Cancelled thread in user space = killed thread in kernel, so we
		   have to add the thread to the zombie list for deallocation when
		   it has been cancelled. */
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	if (THREAD_IS_KILLED(thread)) {
		/* When the thread is killed and it is not detached, then it is not
		   deallocated yet, so we put it to the zombie list. */
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EKILLED;
	}

	if (THREAD_HAS_FINISHED(thread)) {
		/* Joining to the finished thread is simple. We just have to arrange
		   that thread will be deallocated. */
		thread_set_retval(thread, thread_retval);
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EOK;
	}

	/* Mark down that we are waiting for the thread. */
	thread->attrs |= THREAD_IS_SOMEONE_WAITING_MASK;

	/* Now we know that thread has not finished yet and it is not killed,
	   so we will wait on condition variable until the thread finishes. */
	int ret_value = EOK;
	mutex_t *thr_mutex = &thread->mtx;
	cond_t *thr_cvar = &thread->cvar;

	mutex_lock(thr_mutex);

	/* Handle both thread_join versions. */
	if (timeout) {
		ret_value = cond_wait_timeout(thr_cvar, thr_mutex, usec);
	} else {
		cond_wait(thr_cvar, thr_mutex);
	}

	mutex_unlock(thr_mutex);

	/* Check again if thread has been cancelled during waiting. */
	if (THREAD_IS_CANCELLED(thread)) {
		/* Return value must be OK, not ETIMEDOUT, because the thread
		   has been cancelled and therefore it has signaled. */
		assert(ret_value == EOK);
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Check again if thread has been killed during waiting. */
	if (THREAD_IS_KILLED(thread)) {
		/* Return value must be OK, not ETIMEDOUT, because the thread
		  has been killed and therefore it has signaled. */
		assert(ret_value == EOK);
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EKILLED;
	}

	/* Check again if thread has finished during waiting. */
	if (THREAD_HAS_FINISHED(thread)) {
		/* Return value must be OK, not ETIMEDOUT, because the thread
		   has been finished and therefore it has signaled. */
		assert(ret_value == EOK);
		thread_set_retval(thread, thread_retval);
		kernel_put_thread_to_zombies(thread);
		conditionally_enable_interrupts(state);
		return EOK;
	}

	/* Mark down that we are not waiting anymore. */
	thread->attrs &= ~THREAD_IS_SOMEONE_WAITING_MASK;
	conditionally_enable_interrupts(state);

	return ret_value;
}

/** Wait until thread run ends.
 *
 * The function waits till the given thread ends and then initiates memory
 * release containing kernel handling structure and stack.
 *
 * @param  thread Thread to be waited for.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread
 * 		was detached using thread_detach function, if someone else is
 * 		already waiting for this thread or if thread is calling function
 * 		to itself. It returns ::EKILLED if thread was killed by ::thread_kill
 * 		function. If no error is present, it returns ::EOK.
 *
 */
int thread_join(thread_t thread)
{
	return thread_join_proc(thread.impl, 0, false, NULL);
}

/** Wait until thread run ends.
 *
 * The function waits till the given thread ends and then initiates memory
 * release containing kernel handling structure and stack.
 *
 * @param  thread        Thread to be waited for.
 * @param  thread_retval Place to writeback return value.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread
 * 		was detached using ::thread_detach function, if someone else is
 * 		already waiting for this thread or if thread is calling function to itself.
 * 		It returns ::EKILLED if thread was killed by ::thread_kill function.
 * 		If no error is present, it returns ::EOK.
 *
 */
int thread_join_retval(thread_t thread, void **thread_retval)
{
	return thread_join_proc(thread.impl, 0, false, thread_retval);
}

/** Wait until thread run ends with maximum timeout.
 *
 * The function waits till the given thread ends, but no more than time given
 * by @p usec. Then initiates memory release containing kernel handling
 * structure and stack.
 *
 * @param  thread Thread to be waited for.
 * @param  usec   Time in microseconds representing the maximum time
 *                to be waited for the thread.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread was
 * 		detached using ::thread_detach function, if someone else is
 * 		already waiting for this thread or if thread is calling function to itself.
 * 		It returns ::EKILLED if thread was killed by ::thread_kill function
 * 		and ::ETIMEDOUT if thread is not terminated before @p usec time. If no
 * 		error is present, it returns ::EOK.
 *
 */
int thread_join_timeout(thread_t thread, const unsigned int usec)
{
	return thread_join_proc(thread.impl, usec, true, NULL);
}

/** Wait until thread run ends with upper bound
 *
 * The function waits till the given thread ends, but no more than time
 * given by @p usec. Then initiates memory release containing kernel
 * handling structure and stack.
 *
 * @param  thread        Thread to be waited for.
 * @param  thread_retval Place to writeback return value.
 * @param  usec          Time in microseconds representing the maximum
 *                       time to be waited for the thread.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread was
 * 		detached using ::thread_detach function, if someone else is already
 * 		waiting for this thread or if thread is calling function to itself.
 * 		It returns ::EKILLED if thread was killed by thread_kill function
 * 		and ::ETIMEDOUT if thread is not terminated before @p usec time. If
 * 		no error is present, it returns ::EOK.
 *
 */
int thread_join_timeout_retval(thread_t thread, void **thread_retval, const unsigned int usec)
{
	return thread_join_proc(thread.impl, usec, true, thread_retval);
}

/** Detach given thread.
 *
 * The function detaches the given thread.
 *
 * @param  thread Thread to be detached.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread is
 * 		already detached, if thread is already not running and waiting for join,
 * 		if someone is already waiting for thread in thread_join or if the thread
 * 		is killed, otherwise ::EOK.
 *
 */
int thread_detach(thread_t thread)
{
	ipl_t state = query_and_disable_interrupts();
	thread_impl_t *thread_impl = thread.impl;

	if (!thread_impl_is_valid(thread_impl)
			|| THREAD_IS_DETACHED(thread_impl)
			|| THREAD_IS_SOMEONE_WAITING(thread_impl)
			|| THREAD_IS_KILLED(thread_impl)
			|| THREAD_HAS_FINISHED(thread_impl)) {
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Set detached state */
	thread_impl->attrs |= THREAD_DETACHED_MASK;
	conditionally_enable_interrupts(state);

	return EOK;
}

/** Block currently running thread for given @p sec.
 *
 * @param sec Number of seconds to block thread.
 *
 */
void thread_sleep(const unsigned int sec)
{
	const unsigned int usec = 1000000;
	unsigned int i;

	for (i = 0; i < sec; i++) {
		thread_usleep(usec);
	}
}

/** Block currently running thread for given @p usec.
 *
 * @param usec Number of microseconds to block thread.
 *
 */
void thread_usleep(const unsigned int usec)
{
	thread_impl_t *current_thread = thread_get_current_impl();
	ipl_t state = query_and_disable_interrupts();

	timer_init(&current_thread->tmr, usec, thread_timer_usleep_proc, current_thread);

	/* Suspend thread. */
	disabled(current_thread);

	/* Wakeup thread after usec time using timer_usleep_proc as
	   handler function. */
	timer_start(&current_thread->tmr);
	conditionally_enable_interrupts(state);

	/* Reschedule to next thread */
	thread_yield();
}

/** Pause currently running thread.
 *
 * The function pauses currently running thread and allow other threads
 * to execute. It is paused until it is scheduled again.
 *
 */
void thread_yield(void)
{
	schedule();
}

/** Suspend currently running thread.
 *
 * The function suspends currently running thread until some other thread
 * unblocks it using ::thread_wakeup function.
 *
 */
void thread_suspend(void)
{
	thread_impl_t *current_thread = thread_get_current_impl();
	disabled(current_thread);
	thread_yield();
}

/** Wake up given thread.
 *
 * The function wakes up (unblocks) given thread. The calling thread
 * continues execution and it isn't rescheduled during this call.
 *
 * @param  thread Thread to be woken up.
 *
 * @return It returns ::EINVAL if thread identification is invalid, otherwise ::EOK.
 *
 */
int thread_wakeup(thread_t thread)
{
	return thread_wakeup_priority(thread.impl, 0);
}

/** Wake up given thread with a priority. Waking up
 *  the threads is an atomic operation.
 *
 * The function wakes up (unblocks) given thread. The calling thread
 * continues execution and it isn't rescheduled during this call.
 *
 * @param  thread   Thread to be woken up.
 * @param  priority Thread priority value.
 *
 * @return It returns ::EINVAL if thread identification is invalid, otherwise ::EOK.
 *
 */
int thread_wakeup_priority(thread_impl_t *thread, int priority)
{
	ipl_t state = query_and_disable_interrupts();

	/* Is thread valid? We can not wake up invalid or killed threads. */
	if (!thread_impl_is_valid(thread) || THREAD_IS_KILLED(thread) || THREAD_IS_UNINITIALIZED(thread)) {
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Add the thread to the schedulable threads with a priority. */
	if (priority > 0) {
		runnable_high_priority(thread);
	} else {
		runnable(thread);
	}

	conditionally_enable_interrupts(state);
	return EOK;
}

/** Commit the suicide of currently running thread.
 *
 */
void thread_commit_suicide(void)
{
	thread_kill(thread_get_current());
	thread_yield();
	panic("Thread returned after committing suicide!");
}

/** Kill given thread.
 *
 * The function kills given thread. Thread that is waiting for termination
 * of thread being killed is unblocked.
 *
 * @param  thread Thread to be killed.
 *
 * @return	It returns ::EINVAL if thread identification is invalid, if thread has
 * 		been already killed or if thread has already finished, otherwise ::EOK.
 *
 */
int thread_kill(thread_t thread)
{
	ipl_t state = query_and_disable_interrupts();
	thread_impl_t *thread_impl = thread.impl;

	/* Is the thread valid? */
	if (!thread_impl_is_valid(thread_impl) || THREAD_IS_KILLED(thread_impl) || THREAD_HAS_FINISHED(thread_impl)) {
		conditionally_enable_interrupts(state);
		return EINVAL;
	}

	/* Set killed state. */
	thread_impl->attrs |= THREAD_KILLED_MASK;
	process_decrease_running_threads(thread_impl->process);

	/* Release the mutex for std input (only if thread is holding it). */
	keyboard_release_mutex(thread_impl);

	/* No one can wait for detached threads, so we just put it to the zombie list. */
	if (THREAD_IS_DETACHED(thread_impl)) {
		kernel_put_thread_to_zombies(thread_impl);
	} else {
		/* Notify waiting thread that it does not have to wait anymore. */
		if (THREAD_IS_SOMEONE_WAITING(thread_impl)) {
			/* Signal to the waiting thread. */
			mutex_lock(&thread_impl->mtx);
			cond_signal(&thread_impl->cvar);
			mutex_unlock(&thread_impl->mtx);
		} /* Note: When no thread is waiting for join, then we do not
             deallocate the thread, but when someone attempts to join killed
             thread, then the thread is deallocated (added to the zombie list). */
	}

	/* Uninitialize the thread, so it is not scheduled again. */
	thread_uninitialize(thread_impl);
	conditionally_enable_interrupts(state);

	return EOK;
}

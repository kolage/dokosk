/**
 * @file kernel/sched/timer.c
 *
 * Implementation of @ref timer_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 */
/**
 * @page timer_feature Timers
 *
 * This page describes the low level timers.
 *
 * The timers provide a way to call a specified callback asynchronously
 * after the specified time. They can be used to implement waits
 * with timeouts in the kernel.
 * The timer callbacks are called with disabled interrupts and
 * are not suited to do time consuming operations.
 *
 * Milliseconds specified by callers are directly converted to ticks
 * according to the tick frequency (::TICK_FREQUENCY) which is a compile-time
 * constant.
 *
 * DTIME device is not used in any way, as it does not have any
 * reliable properties (can be not monotonic, jump forward and backward any time, ...).
 *
 * All times are 64-bit and relative. There is no absolute time counter.
 * The timers depend on the tick counter and periodic calling
 * of the ::timer_update function, which updates the timers according
 * to the difference of the CP0 Count register (which is 32-bit).
 * Pending timers are stored in a sorted list ::pending_timers.
 * The timers store the relative time to the previous timer. The first timer
 * stores the time relative to the last update. This is
 * never negative and stays at 0 even after the exact time elapses.
 *
 * There is no interface provided to the user by syscalls that would
 * expose the timers.
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <include/codes.h>
#include <include/math.h>
#include <adt/list.h>
#include <sched/timer.h>
#include <drivers/printer.h>


/** Frequency of counter ticks.
 *
 * This is used to convert microseconds to ticks.
 *
 */
#define TICK_FREQUENCY 16000000

/** Million microseconds per second. */
#define USEC_PER_SEC 1000000

/** Magic value used for debugging. */
#define TIMER_MAGIC 0x55AAFF00

/** Check integrity of the magic value. */
#define CHECK_TIMER_INTEGRITY(tmr) \
	assert(tmr != NULL); 	\
	assert(tmr->magic == TIMER_MAGIC);

/** Whether optimized 64-bit division can be used. */
#define TIMER_DIVIDE_OPTIMIZATION ((TICK_FREQUENCY) % (USEC_PER_SEC) == 0)


/** List of pending timers.
 *
 * The list is ordered by time to elapse and secondary ordered by
 * order of starting.
 *
 */
static list_t pending_timers;

/** Tick count of last timers update. */
static uint32_t reference_tick_count;


/** Update reference tick count and tick counters in pending timers.
 *
 * This function must be called with disabled interrupts.
 *
 */
static void timer_update(void)
{
	/* Read tick count now. This is the new reference count for following operations. */
	uint32_t new_reference_tick_count = read_cp0_count();

	/* Count ticks after last update. We assume the tick count advance is less than 2^32. */
	uint32_t tick_count_advance = new_reference_tick_count - reference_tick_count;

	/* Iterate through the list of pending timers and update counts. */
	list_it_t iterator;
	
	for (list_it_init(&iterator, &pending_timers); list_it_has_current(&iterator); list_it_next(&iterator)) {
		timer_t* timer = list_it_data(&iterator);
		
		if (timer->tick_difference >= (uint64_t)tick_count_advance) {
			timer->tick_difference -= (uint64_t)tick_count_advance;
			
			/* No need to iterate through further items. */
			break;
		} else {
			timer->tick_difference = 0;
			tick_count_advance-=(uint32_t) timer->tick_difference;
		}
	}
	
	/* Now the timer differences are consistent with the new reference count. */
	reference_tick_count = new_reference_tick_count;
}

/** Initialize timer framework.
 *
 * This must be called before any of ::timer_init, ::timer_start,
 * ::timer_destroy, ::timer_pending, ::pending_timers_count and
 * ::timer_run_elapsed is called.
 *
 */
void init_timers(void)
{
	list_init(&pending_timers);
}

/** Initialize a timer structure.
 *
 * This must be called io timer_t before it is passed to any other timer
 * function. The function can be called either with interrupts enabled
 * or disabled.
 *
 * @param tmr     Pointer to timer structure to initialize. The content
 *                can be undefined.
 * @param usec    Number of milliseconds to elapse between ::timer_start is
 *                called and the handler is executed.
 * @param handler Pointer to handler function. This function is called when
 *                the timer elapses.
 * @param data    Parameter to be passed to the handler.
 *
 */
int timer_init(timer_t *tmr, const unsigned int usec, timer_handler_t handler, void *data)
{
	if (handler == NULL) {
		return EINVAL;
	}

	tmr->magic = TIMER_MAGIC;

	item_init(&tmr->item, tmr);

	tmr->usec = usec;
	tmr->handler = handler;
	tmr->data = data;

	return EOK;
}

/** Start the timer.
 *
 * Compute ticks to elapse and add the timer to pending list.
 * If the timer is pending, it is cancelled first.
 * If the timer has timeout set to zero, it is executed immediately
 * (without disabling interrupts).
 *
 * @param tmr Pointer to the timer structure. Must be a valid timer
 *            initialized by ::timer_init.
 *
 */
void timer_start(timer_t *tmr)
{
	CHECK_TIMER_INTEGRITY(tmr);
	timer_destroy(tmr);

	/* Run the timer immediately when its timeout is set to 0. */
	if (tmr->usec == 0) {
		tmr->handler(tmr, tmr->data);
		return;
	}

	ipl_t int_state = query_and_disable_interrupts();

	/* Update timer counts so that the new timer is started relatively to this point in time. */
	timer_update();

	/* Compute the time in ticks. */
#if TIMER_DIVIDE_OPTIMIZATION
	/* Simple optimization for whole megahertzes. */
	uint64_t ticks = (uint64_t)tmr->usec * (uint64_t)(TICK_FREQUENCY / USEC_PER_SEC);
#else
	uint64_t ticks = divide64_32((uint64_t)tmr->usec * (uint64_t)TICK_FREQUENCY, USEC_PER_SEC);
#endif
	/* Insert the timer at the appropriate position.
	   The new timer should be inserted before the first timer that has a higher count. */
	list_it_t iterator;
	bool inserted = false;

	for (list_it_init(&iterator, &pending_timers); list_it_has_current(&iterator); list_it_next(&iterator)) {
		timer_t* timer = list_it_data(&iterator);

		if (timer->tick_difference <= ticks) {
			ticks-=timer->tick_difference;
		} else {
			timer->tick_difference -= ticks;
			tmr->tick_difference = ticks;
			list_insert_before(&pending_timers, &timer->item, &tmr->item);
			inserted = true;
			break;
		}
	}

	/* If there is no timer after this, append it to the list. */
	if (!inserted) {
		tmr->tick_difference = ticks;
		list_append(&pending_timers, &tmr->item);
	}

	conditionally_enable_interrupts(int_state);
}

/** Destroy the timer.
 *
 * If the timer is pending, it is removed from pending list.
 * Otherwise, do nothing.
 *
 * @param tmr Timer to destroy. Must be a valid timer initialized
 *            by ::timer_init.
 *
 */
void timer_destroy(timer_t *tmr)
{
	CHECK_TIMER_INTEGRITY(tmr);

	ipl_t int_state = query_and_disable_interrupts();

	/* If the timer is pending, remove it, otherwise do nothing. */
	if (list_contains(&pending_timers, &tmr->item)) {
		list_remove(&pending_timers, &tmr->item);
	}

	conditionally_enable_interrupts(int_state);
}
/** Check whether the timer is pending.
 *
 * Note that if you call this with enabled interrupts, the return value
 * can differ from the real situation.
 *
 * @param  tmr Timer to check. Must be a valid timer initialized
 *             by ::timer_init.
 *
 * @return 0 if not pending, nonzero if pending.
 *
 */
int timer_pending(timer_t *tmr)
{
	CHECK_TIMER_INTEGRITY(tmr);

	ipl_t int_state = query_and_disable_interrupts();
	int result = list_contains(&pending_timers, &tmr->item);
	conditionally_enable_interrupts(int_state);

	return result;
}

/** Get the number of pending timers.
 *
 * Note that if you call this with enabled interrupts,
 * the real timer count can differ after returning from this function.
 *
 * @return Number of pending timers.
 *
 */
size_t pending_timers_count(void)
{
	ipl_t int_state = query_and_disable_interrupts();
	size_t result = list_count(&pending_timers);
	conditionally_enable_interrupts(int_state);

	return result;
}

/** Run elapsed timers as long as there are some.
 *
 * Update timers and if any of them elapsed, run it.
 * After running the timers, check again.
 *
 */
void timer_run_elapsed(void)
{
	ipl_t int_state = query_and_disable_interrupts();

	while (true) {
		/* Update timers to current time. */
		timer_update();

		/* The timer can install other timers, so we should not iterate but
		   check every time. */
		list_it_t iterator;
		list_it_init(&iterator, &pending_timers);

		if (!list_it_has_current(&iterator)) {
			break;
		}

		/* Retrieve the first timer. */
		timer_t* timer = list_it_data(&iterator);

		if (timer->tick_difference == 0) {
			/* Remove the timer from the list. */
			list_remove(&pending_timers, &timer->item);

			/* Execute the timer handler. */
			timer->handler(timer, timer->data);
		} else{
			/* The first timer has not elapsed yet. */
			break;
		}
	}

	conditionally_enable_interrupts(int_state);
}

/**
 * @file kernel/sched/sched.h
 *
 * Declarations of round robin kernel thread scheduler.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef SCHED_H_
#define SCHED_H_

#include <adt/list.h>
#include <proc/thread.h>


/* Externals are commented with implementation. */
extern void init_scheduler(void);
extern thread_impl_t *scheduler_get_planned_thread(void);
extern thread_impl_t *scheduler_get_from_queue(list_t *waiting_queue);
extern void runnable(thread_impl_t *thread);
extern void runnable_high_priority(thread_impl_t *thread);
extern void disabled(thread_impl_t *thread);
extern void schedule(void);

#endif /* SCHED_H_ */

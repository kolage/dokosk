/**
 * @file kernel/sched/timer.h
 *
 * Declarations for @ref timer_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef SCHED_TIMER_H_
#define SCHED_TIMER_H_

#include <include/c.h>
#include <adt/list.h>


struct timer;
typedef void (*timer_handler_t)(struct timer *, void *);

typedef struct timer{
	/** Magic value for debugging purposes. */
	uint32_t magic;

	/** A timer can be an item on a list (of pending timers). */
	item_t item;

	/** Tick difference from the previous pending timer or from the last update time. */
	uint64_t tick_difference;

	/** Number of microseconds (specified in ::timer_init). */
	unsigned int usec;

	/** Elapsed handler (specified in ::timer_init). */
	timer_handler_t handler;

	/** Handler argument (specified in ::timer_init). */
	void *data;

} timer_t;


/* Externals are commented with implementation. */
extern void init_timers(void);
extern void timer_run_elapsed(void);

extern int timer_init(struct timer *tmr, const unsigned int usec, timer_handler_t handler, void *data);
extern void timer_start(struct timer *tmr);
extern void timer_destroy(struct timer *tmr);
extern int timer_pending(struct timer *tmr);
extern size_t pending_timers_count(void);

#endif /* SCHED_TIMER_H_ */

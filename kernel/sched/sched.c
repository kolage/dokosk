/**
 * @file kernel/sched/sched.c
 *
 * Implementation of round-robin kernel thread scheduler.
 *
 * Threads can be included in scheduling by calling ::runnable function.
 * This function appends the thread to the end of ::runnable_list.
 * Threads can be also planned with high priority by calling
 * ::runnable_high_priority function which prepends the thread to the
 * start of ::runnable_list.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <adt/list.h>
#include <proc/thread.h>
#include <drivers/printer.h>
#include <drivers/timer.h>
#include <sched/sched.h>
#include <sched/timer.h>


/** Minimum number of ticks a thread is allowed to run. */
#define THREAD_QUANTUM_MIN  1024

/** Maximum number of ticks a thread is allowed to run. */
#define THREAD_QUANTUM_MAX  4096

/** Actual number of ticks is randomized. */
#define THREAD_QUANTUM (THREAD_QUANTUM_MIN + random(&seed) % (THREAD_QUANTUM_MAX - THREAD_QUANTUM_MIN))


/** List of schedulable threads. */
static list_t runnable_list;

/** Random seed for the scheduler. */
static unsigned int seed = 0;


/** Initialize the scheduler.
 *
 * Initializes the scheduler structures and configures the scheduler interrupt.
 *
 */
void init_scheduler(void)
{
	/* Initialize the list of schedulable threads. */
	list_init(&runnable_list);
}

/** Get the thread that is planned to run.
 *
 * @return Thread that is planned to run.
 *
 */
thread_impl_t *scheduler_get_planned_thread(void)
{
	assert(!list_empty(&runnable_list));
	return (thread_impl_t *)runnable_list.head->data;
}

/** Select some thread from the passed queue and return it.
 *
 * This function always selects the first thread. This function
 * should be called with some mutex or disabled interrupts
 * when the queue is a shared structure.
 *
 * @param  waiting_queue Queue of waiting threads.
 *
 * @return Selected thread or ::NULL when the queue is empty.
 *
 */
thread_impl_t *scheduler_get_from_queue(list_t *waiting_queue)
{
	if (!list_empty(waiting_queue)) {
		return (thread_impl_t *)waiting_queue->head->data;
	} else {
		return NULL;
	}
}

/** Include thread in scheduling.
 *
 * The thread is appended to the list of schedulable threads.
 *
 * @param thread Thread to include in scheduling.
 *
 */
void runnable(thread_impl_t *thread)
{
	/* Disable interrupts while accessing shared structures. */
	ipl_t state = query_and_disable_interrupts();
	assert(thread != NULL);
	assert(thread->thread_id > 0);
	assert(!THREAD_IS_KILLED(thread));
	assert(!THREAD_IS_UNINITIALIZED(thread));

	/* Check if thread is not already on list. */
	if (!list_contains(&runnable_list, &thread->item)) {
		list_append(&runnable_list, &thread->item);
	}

	conditionally_enable_interrupts(state);
}

/** Include thread in scheduling with high priority.
 *
 * The thread is prepended to the list of schedulable threads.
 *
 * @param thread Thread to include in scheduling.
 *
 */
void runnable_high_priority(thread_impl_t *thread)
{
	/* Disable interrupts while accessing shared structures. */
	ipl_t state = query_and_disable_interrupts();

	/* Check if thread is not already on list. */
	if (!list_contains(&runnable_list, &thread->item)) {
		list_prepend(&runnable_list, &thread->item);
	}

	conditionally_enable_interrupts(state);
}

/** Exclude thread from scheduling.
 *
 * The thread is removed from the list of schedulable threads.
 *
 * @param thread Thread to include in scheduling.
 *
 */
void disabled(thread_impl_t *thread)
{
	/* Disable interrupts while accessing shared structures. */
	ipl_t state = query_and_disable_interrupts();

	/* Check if thread is on list. */
	if (list_contains(&runnable_list, &thread->item)) {
		list_remove(&runnable_list, &thread->item);
	}

	conditionally_enable_interrupts(state);
}

/** Schedule the next thread to run.
 *
 * The function is called from an interrupt handler.
 *
 */
void schedule(void)
{
	/* Disable interrupts while accessing shared structures. */
	ipl_t state = query_and_disable_interrupts();

	/* Run elapsed timers. */
	timer_run_elapsed();

	/* Just take the first thread on the list of schedulable threads. */
	if (list_empty(&runnable_list)) {
		/* This should never happen, because we always use one system thread
		   (with thread_impl_t::thread_id = 1). */
		panic("There are no threads to schedule!\n");
	} else {
		thread_impl_t *next_thread = (thread_impl_t *)list_rotate(&runnable_list)->data;

		/* A little performance optimization - we do not need to schedule
		   idle thread, when there are some more threads to schedule. */
		if (next_thread->thread_id == 1) {
			next_thread = (thread_impl_t *)list_rotate(&runnable_list)->data;
		}

		/* Prepare the next scheduler interrupt. */
		timer_setup(THREAD_QUANTUM);
		switch_to_thread(next_thread);
	}

	conditionally_enable_interrupts(state);
}

/**
 * @file kernel/exc/exc.c
 *
 * Implementation of exception handling.
 *
 * Most of the exceptions mean killing of the thread (when the exception
 * occur in the user space thread) or ::panic (when the exception
 * occur in the kernel thread).
 *
 * Interrupts are handled separately in ::interrupt function.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <exc/int.h>
#include <syscall/syscall.h>
#include <drivers/printer.h>
#include <exc/exc.h>
#include <mm/tlb.h>


/** Kill the current thread or cause kernel panic.
 *
 * When called from user thread it kills the current thread. Otherwise
 * it causes kernel ::panic.
 *
 * @param registers The registers at the time of the exception.
 * @param msg       Message to be printed when halting the MSIM.
 *
 */
void panic_or_kill(context_t *registers, char *msg, ...)
{
	if (is_user_thread(thread_get_current_impl())) {
		printk("Killing the thread %i\n", thread_get_current_id());
		va_list argptr;
		va_start(argptr, msg);
		printk_list(msg, argptr);
		putc('\n');

		/* Error occurred in the user thread -> commit suicide. */
		thread_commit_suicide();
	} else {
		va_list argptr;
		va_start(argptr, msg);

		/* Error occurred in the kernel thread -> panic. */
		panic_list(msg, argptr);
	}
}

/** Handle an exception.
 *
 * The function is called from the assembler exception handler.
 * Interrupts are disabled and the various System Control Coprocessor
 * registers identify what exception is being handled.
 *
 * @param registers The registers at the time of the exception.
 *
 */
void wrapped_general(context_t *registers)
{
	int cause = CP0_CAUSE_EXCCODE(registers->cause);
	
	/* The handling of the exception depends on its cause. */
	switch (cause) {
		case CP0_CAUSE_EXCCODE_INT:
			/* Interrupt Exception:
			   Interrupts are handled by a separate procedure. */
			interrupt(registers->cause);
			break;

		case CP0_CAUSE_EXCCODE_SYS:
			/* System Call Exception:
			   System calls are handled by a separate procedure. */
			syscall(registers);
			break;

		case CP0_CAUSE_EXCCODE_ADEL:
			/* Address Error Exceptions:
			   Code accessed an invalid address. */
			panic_or_kill(registers, "Address Error Load Exception.");
			break;

		case CP0_CAUSE_EXCCODE_ADES:
			/* Address Error Exceptions:
			   Code accessed an invalid address. */
			panic_or_kill(registers, "Address Error Store Exception.");
			break;

		case CP0_CAUSE_EXCCODE_IBE:
		case CP0_CAUSE_EXCCODE_DBE:
			/* Bus Error Exceptions:
			   Code accessed an invalid physical address. */
			panic_or_kill(registers, "Bus Error exception.");
			break;

		case CP0_CAUSE_EXCCODE_BP:
			/* The Breakpoint Exception is triggered when the BREAK instruction
			   is executed. The BREAK instruction can be inserted in code
			   to implement location breakpoints. When the exception is in
			   the branch delay slot, then the panic is caused in the kernel
			   mode and thread kill is caused in the user mode. */
			if (CP0_CAUSE_BD(registers->cause)) {
				panic_or_kill(registers, "Break exception.");
			} else {
				/* To resume execution, the EPC register must be altered so
				   that the BREAK instruction does not re-execute; this is
				   accomplished by adding a value of 4 to the EPC register
				   (EPC register + 4) before returning. */

				/* We have to write to the registers structure, because the
				   registers values are restored from that structure. */
				registers->epc += 4;
			}
			break;

		case CP0_CAUSE_EXCCODE_WATCH:
			/* We ignore the reference to WatchHi/WatchLo address
			   (WATCH) exception. */
			break;

		case CP0_CAUSE_EXCCODE_RI:
			/* Caused when the instruction op code is unknown- */
			panic_or_kill(registers, "Reserved Instruction exception.");
			break;

		case CP0_CAUSE_EXCCODE_CPU:
			/* Triggered when accessing coprocessor which is not accessible. */
			panic_or_kill(registers, "Coprocessor Unusable exception.");
			break;

		case CP0_CAUSE_EXCCODE_TR:
			/* The Trap Exception is triggered when the condition of the
			   TRAP instruction is met. The TRAP instruction can be
			   used in code to implement range checking.

			   Exception is not used here. */
			panic_or_kill(registers, "Trap exception.");
			break;

		case CP0_CAUSE_EXCCODE_OV:
			/* The Overflow Exception is triggered when an arithmetic
			   overflow occurs in signed arithmetic instructions. */
			panic_or_kill(registers, "Overflow exception.");
			break;

		case CP0_CAUSE_EXCCODE_TLBL:
		case CP0_CAUSE_EXCCODE_TLBS:
			/* TLB Invalid and TLB Modified exceptions are triggered
			   when the entry in the TLB does not have valid bit set
			   for reads or modified bit for writes.*/

			/* Delegate the exception to TLB handlers. */
			tlb_invalid_access(registers);
			break;

		default:
			/* Some other exceptions can occur but mostly they just
			   mean that something is terribly wrong and the kernel
			   halts. */
			panic("Unhandled exception %i.", cause);
	}
}

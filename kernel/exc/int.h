/**
 * @file kernel/exc/int.h
 *
 * Declarations of interrupt handling.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef INT_H_
#define INT_H_


/* Externals are commented with implementation. */
extern void interrupt(int cause);

#endif /* INT_H_ */


/**
 * @file kernel/exc/int.c
 *
 * Implementation of interrupt handling.
 *
 * Interrupts are handled by the ::interrupt function which is called from
 * exception handler implemented in ::wrapped_general function.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#include <include/shared.h>
#include <include/c.h>
#include <adt/list.h>
#include <proc/thread.h>
#include <sched/sched.h>
#include <exc/int.h>
#include <drivers/keyboard.h>
#include <drivers/printer.h>
#include <drivers/disk.h>


/** Handle an interrupt.
 *
 * The function is called from the exception handler when the exception
 * is identified as being caused by an interrupt request. Interrupts
 * are disabled and the IP bits in the Cause register identify which
 * interrupt requests are active. More than one bit can be set.
 *
 * The handler has to do whatever is necessary to satisfy the interrupt
 * request, otherwise the exception will be generated immediately after
 * interrupts are enabled.
 *
 * @param cause The cause of the interrupt (content of the CP0 cause
 *              register at the time of the interrupt).
 *
 */
void interrupt(int cause)
{
	if (cause & CP0_CAUSE_IP3_MASK) {
		/* IP3 is a keyboard interrupt. */
		keyboard_handle();
	}

	if (cause & CP0_CAUSE_IP5_MASK) {
		/* IP5 is a disk device interrupt. */
		disk_handle();
	}

	if (cause & CP0_CAUSE_IP7_MASK) {
		/* IP7 is a timer interrupt.
		   The scheduler handlers timer interrupts until a timer
		   framework is implemented. Since the scheduler switches
		   context, it is a good idea to handle the timer interrupt
		   as the last one, when all other interrupts are satisfied. */
		schedule();
	}

	/* All interrupt requests should be handled by now. */
}

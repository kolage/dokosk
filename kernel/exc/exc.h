/**
 * @file kernel/exc/exc.h
 *
 * Declarations of exception handling.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#ifndef EXC_H_
#define EXC_H_


/* Externals are commented with implementation. */
extern void panic_or_kill(context_t *registers, char *msg, ...);
extern void wrapped_general(context_t *registers);

#endif /* EXC_H_ */

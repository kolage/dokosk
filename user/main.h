/**
 * @file user/main.h
 *
 * Declarations of main user process.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef USER_MAIN_H_
#define USER_MAIN_H_


/* Externals are commented with implementation. */
extern int main(void);

#endif /* USER_MAIN_H_ */

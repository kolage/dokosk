/**
 * @file user/sync/cond.h
 *
 * Declarations of condition variable synchronization primitive
 * in the user space.
 *
 * This file is related to @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef COND_H_
#define COND_H_

#include <include/c.h>
#include <sync/mutex.h>


/** Structure defining the condition variables in the user space. */
typedef  struct cond {
	unative_t handle;
} cond_t;


/* Externals are commented with implementation. */
extern int cond_init(struct cond *cvar);
extern int cond_destroy(struct cond *cvar);
extern int cond_signal(struct cond *cvar);
extern int cond_broadcast(struct cond *cvar);
extern int cond_wait(struct cond *cvar, struct mutex *mtx);
extern int cond_wait_timeout(struct cond *cvar, struct mutex *mtx, const unsigned int usec);

#endif /* COND_H_ */

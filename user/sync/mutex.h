/**
 * @file user/sync/mutex.h
 *
 * Declarations of mutexes in the user space.
 *
 * This file is related to @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <include/c.h>


/** Structure defining the mutex in the user space. */
typedef struct mutex {
	unative_t handle;
} mutex_t;


/* Externals are commented with implementation. */
extern int mutex_init(struct mutex *mtx);
extern int mutex_init_recursive(struct mutex *mtx);
extern int mutex_destroy(struct mutex *mtx);
extern int mutex_lock(struct mutex *mtx);
extern int mutex_lock_timeout(struct mutex *mtx, const unsigned int usec);
extern int mutex_unlock_checked(struct mutex *mtx);
extern int mutex_unlock_unchecked(struct mutex *mtx);


/* Trick with the DEBUG_MUTEX macro - checked/unchecked version
   of mutex_unlock function. */
#if DEBUG_MUTEX >= 1
#define mutex_unlock(mtx)	mutex_unlock_checked(mtx)
#else
#define mutex_unlock(mtx)	mutex_unlock_unchecked(mtx)
#endif

#endif /* MUTEX_H_ */

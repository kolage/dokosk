/**
 * @file user/sync/mutex.c
 *
 * Implementation of mutexes in the user space.
 *
 * This file is related to @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** @page synchronization_feature Synchronization primitives
 *
 * This page describes synchronization primitives implemented in user space.
 *
 * There are two basic synchronization primitives implemented in user space:
 * mutexes (implemented in ::mutex structure) and condition variables
 * (implemented in ::cond structure). Condition variables are designed to be
 * used for all the synchronization that include waiting for some event.
 *
 * Mutex can be initialized as a simple one by ::mutex_init function
 * or as a recursive one by ::mutex_init_recursive function.
 * Recursive mutexes are realized by atomic counter of locks.
 *
 */

#include <syscall/syscall.h>
#include <sync/mutex.h>


/** Initialize the mutex to the unlocked state.
 *
 * This function returns ::EOK when  there was no problem with initialization
 * and ::ENOMEM when there is no memory for creating the mutex.
 *
 * @param  mtx Mutex to be initialized.
 *
 * @return ::EOK when there was no problem with initialization,
 *         ::ENOMEM when there is no memory for creating the mutex.
 *
 */
int mutex_init(struct mutex *mtx)
{
	return syscall_mutex_init(&mtx->handle, false);
}

/** Initialize the recursive mutex to the unlocked state.
 *
 * This function returns ::EOK when there was no problem with initialization
 * and ::ENOMEM when there is no memory for creating the mutex.
 *
 * @param  mtx Mutex to be initialized.
 *
 * @return ::EOK when there was no problem with initialization,
 *         ::ENOMEM when there is no memory for creating the mutex.
 *
 */
int mutex_init_recursive(struct mutex *mtx)
{
	return syscall_mutex_init(&mtx->handle, true);
}

/** Destroy the passed mutex and free its memory.
 *
 * If there are any blocked threads on the mutex, then the current thread
 * is killed. This function returns ::EOK when there was no problem with
 * mutex destruction and ::EINVAL when the mutex handle is invalid.
 *
 * @param  mtx Mutex to be destroyed.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int mutex_destroy(struct mutex *mtx)
{
	return syscall_mutex_destroy(mtx->handle);
}

/** Try to lock the passed mutex.
 *
 * When it does not succeed, it blocks actually running thread and puts it
 * into the queue of threads waiting for the mutex. ::mutex_unlock then
 * wakes up all those threads and the first to get planned then captures the
 * mutex, rest is waiting again etc.
 *
 * This function returns ::EOK when there was no problem with mutex locking
 * and ::EINVAL when the mutex handle is invalid.
 *
 * @param  mtx Mutex to be locked.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int mutex_lock(struct mutex *mtx)
{
	return syscall_mutex_lock(mtx->handle);
}

/** Try to lock the passed mutex.
 *
 * When it does not succeed, it blocks actually running thread and puts it
 * into the queue of threads waiting for the mutex. ::mutex_unlock then
 * wakes up all those threads and the first to get planned then captures
 * the mutex, rest is waiting again etc.
 *
 * This function waits at most @p usec microseconds to the lock. When the
 * timeout is exceeded, the waiting process is interrupted and the mutex
 * is not locked. When @p usec = 0 this method does not block the calling
 * thread at all.This function returns ::EOK when there was no problem with
 * mutex locking, ::EINVAL when the mutex handle is invalid and ::ETIMEDOUT
 * when the timeout has expired.
 *
 * @param  mtx Mutex to be locked.
 * @param  usec Timeout for the locking.
 *
 * @return ::EOK when the mutex was locked by the running thread, ::ETIMEDOUT
 *         when it timed out and ::EINVAL when passed pointer is invalid.
 *
 */
int mutex_lock_timeout(struct mutex *mtx, const unsigned int usec)
{
	return syscall_mutex_lock_timeout(mtx->handle, usec);
}

/** Unlock the mutex and wakes up one of the threads that are actually
 *  blocked on the mutex.
 *
 * The first thread planned to run than locks the mutex and the rest
 * keeps waiting in the ::mutex_lock waiting loop. This function returns
 * ::EOK when there was no problem with mutex unlocking and ::EINVAL when
 * the mutex handle address is invalid.
 *
 * This function also checks whether the mutex is unlocked from the
 * same thread as it was locked by.
 *
 * @param  mtx Mutex to be unlocked.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int mutex_unlock_checked(struct mutex *mtx)
{
	return syscall_mutex_unlock(mtx->handle, true);
}

/** Unlock the mutex and wakes up one of the threads that are actually
 *  blocked on the mutex.
 *
 * The first thread planned to run than locks the mutex and the rest keeps
 * waiting in the ::mutex_lock waiting loop. This function returns ::EOK when
 * there was no problem with mutex unlocking and ::EINVAL when the mutex
 * handle address is invalid.
 *
 * @param  mtx Mutex to be unlocked.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int mutex_unlock_unchecked(struct mutex *mtx)
{
	return syscall_mutex_unlock(mtx->handle, false);
}

/**
 * @file user/sync/cond.c
 *
 * Implementation of condition variable synchronization primitive
 * in the user space.
 *
 * This file is related to @ref synchronization_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <syscall/syscall.h>
#include <sync/mutex.h>
#include <sync/cond.h>


/** Initialize the condition variable.
 *
 * @param cvar Condition variable.
 *
 * @return ::EOK when everything is all right, ::ENOMEM when there is no
 *         memory for creating the condition variable.
 *
 */
int cond_init(struct cond *cvar)
{
	return syscall_cond_init(&cvar->handle);
}

/** Destroy the passed condition variable.
 *
 * @param cvar Condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int cond_destroy(struct cond *cvar)
{
	return syscall_cond_destroy(cvar->handle);
}

/** Wake up one thread waiting for the condition variable.
 *
 * @param  cvar Condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int cond_signal(struct cond *cvar)
{
	return syscall_cond_signal(cvar->handle);
}

/** Wake up all the threads waiting for the condition variable.
 *
 * @param  cvar Condition variable.
 *
 * @return ::EINVAL when the passed handle is invalid, ::EOK otherwise.
 *
 */
int cond_broadcast(struct cond *cvar)
{
	return syscall_cond_broadcast(cvar->handle);
}

/** Atomically unlock the passed @p mtx and block the calling thread.
 *
 * After unblocking the mutex is locked again.
 *
 * @param  cvar Condition variable.
 * @param  mtx  Mutex to get unlocked.
 *
 * @return ::EINVAL when the passed handle for @p cvar or @p mtx is invalid,
 *         ::EOK otherwise.
 *
 */
int cond_wait (struct cond *cvar, struct mutex *mtx)
{
	return syscall_cond_wait(cvar->handle, mtx->handle);
}

/** Atomically unlock the passed @p mtx and blocks the calling thread.
 *
 * After unblocking the mutex is locked again. Thread is blocked at most
 * for the @p usec microseconds. When the thread is unblocked during the
 * waiting process it returns ::EOK otherwise ::ETIMEDOUT. When the
 * @p usec = 0 the thread is not blocked at all, but before locking
 * again it yields once.
 *
 * @param  cvar Condition variable.
 * @param  mtx  Mutex to get unlocked.
 * @param  usec Timeout for the locking.
 *
 * @return ::EINVAL when the passed handle for @p cvar or @p mtx is invalid,
 *         ::ETIMEDOUT when the timeout expired and ::EOK otherwise.
 *
 */
int cond_wait_timeout(struct cond *cvar, struct mutex *mtx, const unsigned int usec)
{
	return syscall_cond_wait_timeout(cvar->handle, mtx->handle, usec);
}

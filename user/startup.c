/**
 * @file user/startup.c
 *
 * Entry point for user programs.
 *
 * The user process entry point should be ::_startup.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>
#include <main.h>

__attribute__((section(".startup")))
__attribute__((noreturn))
__attribute__((used))
void _startup(void);

/** Entry point for user programs.
 *
 * Initializes the user library and calls ::main. After ::main returns,
 * calls ::exit to exit the whole process.
 *
 * This function is located in a separate section so that it is
 * always located at the beginning of the user binary.
 *
 */
__attribute__((section(".startup")))
__attribute__((noreturn))
__attribute__((used))
void _startup(void)
{
	init_io();
	init_heap();
	init_disk();

	main();
	exit();
}

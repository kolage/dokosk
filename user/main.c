/**
 * @file user/main.c
 *
 * Implementation of main user process.
 *
 * This file is not compiled into the user library.
 *
 * This file is related to @ref process_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page process_feature Processes in user space
 *
 * This page describes what the initial user process does.
 *
 * When the process is started the wrapper function ::_startup is called which
 * initializes the process in user space. It has to initialize the input
 * and output operations, heap and disk management. Heap is initialized by
 * ::syscall_heap_init syscall.
 *
 * When USER_TEST is specified, this test is compiled with kernel
 * and user space source codes and ::main function in this test is run
 * by ::_startup function in user space. When there is no USER_TEST
 * specified, user space is compiled with default implementation of
 * ::main method defined in @ref user/main.c file.
 *
 * This default initial process provides a functionality for running
 * the processes loaded on the ddisk device. Currently running process
 * can be killed at any time by pressing ESC key on the keyboard.
 *
 */

#include <librt.h>
#include <main.h>


/** Currently running process. */
process_t process;


/** Killer thread.
 *
 * @param  data Data passed to the thread.
 *
 * @return Always returns ::NULL.
 *
 */
static void *killer_thread_proc(void *data)
{
	/* Wait until the user presses ESC. */
	while (checkc() != 27) {
		thread_yield();
	}

	/* Consume the ESC character and kill the running process. */
	getc();
	kill(process);

	return NULL;
}

/** Main user process function.
 *
 * @return Always returns 0.
 *
 */
int main(void)
{
	disk_programs_it_t it;
	int programs_count = disk_programs_count();
	int choice = 0;

	printf("Welcome, there are %u processes loaded at disk.\n", programs_count);

	do {
		printf("Select a program to run, write \"-1\" to quit.\n");
		printf("You can always kill running program by pressing ESC.\n");

		choice = 0;
		int i = 1;

		/* Write out all the available programs. */
		for (disk_programs_it_init(&it); disk_programs_it_has_current(&it); disk_programs_it_next(&it)) {
			printf("[%u] %s\n", i, disk_programs_it_name(&it));
			i++;
		}

		/* Read the program number. */
		printf("Enter the program number: ");
		char buffer[FILE_NAME_SIZE];
		gets(buffer, FILE_NAME_SIZE);
		puts(buffer);

		char *input = rtrim(buffer);

		/* Control the program number. */
		if (atoi(input, &choice) && choice >= 1 && choice <= programs_count) {
			printf("Running program...\n");

			/* Run the program. */
			run_program_by_index(&process, choice - 1);

			/* Create the killer thread. */
			thread_t killer_thread;
			thread_create(&killer_thread, killer_thread_proc, NULL);

			int ret = process_join(process);
			assert(ret == EOK);

			/* Cancel the killer thread. */
			thread_cancel(killer_thread);

			/* We have to reinitialize the I/O, because the lock of std input
			   might remain locked when we cancelled the thread. */
			init_io();
			printf("Program has finished...\n\n");
		} else if (choice == -1) {
			printf("Exiting...\n\n");
		} else {
			printf("\nError: Invalid program number!\n\n");
		}
	} while (choice != -1);

	return 0;
}

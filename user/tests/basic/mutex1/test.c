/***
 * Mutex test #1
 * Copied from the kernel test tests/as1/mutex4/test.c.
 *
 */

static char * desc =
    "Mutex test #1\n"
    "Creates a mutex and locks it in one thread and unlocks it\n"
    "in another. Since DEBUG_MUTEX is not set, no errors should "
    "be reported.\n\n";


#include <librt.h>


/*
 * The tested mutex.
 */
static struct mutex mtx;

/*
 * Global termination flag.
 */
static volatile int finish_flag;

static void *thread_proc (void * data)
{
	mutex_unlock (& mtx);
	finish_flag = 1;
	
	return NULL;
}

int main (void)
{
	thread_t thread;

	printf (desc);

	// Init.
	finish_flag = 0;

	mutex_init (& mtx);
	mutex_lock (& mtx);

	// Start the thread and wait until the mutex is unlocked.
	thread_create (&thread, thread_proc, NULL);

	while (!finish_flag) {
		thread_sleep (1);
	}

	void *ret;
	thread_join (thread, &ret);

	// Clean up.
	mutex_destroy (& mtx);

	printf ("Test passed...\n");

	return 0;
}

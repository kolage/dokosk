/***
 * Condition variable test #2
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Condition variable test #2\n"
    "Creates a condition variable and destroys it while some thread is "
    "waiting for it. This operation should result in killing the thread.\n\n";


#include <librt.h>

/*
 * The tested mutex.
 */
static struct cond cvar;
static struct mutex mtx;

static void *thread_proc_wait(void *data)
{
	printf("Thread B waiting on condition variable...\n");
	mutex_lock(&mtx);
	cond_wait(&cvar, &mtx);
	mutex_unlock(&mtx);
	printf("Thread B got signal...\n");

	return NULL;
}

static void *thread_proc_destroy(void *data)
{
	thread_sleep(1);

	printf("Thread C destroying condition variable...\n");
	cond_destroy(&cvar);
	printf("Thread C not killed - test failed...\n");
	exit();

	return NULL;
}

int main(void)
{
	printf(desc);

	cond_init(&cvar);
	mutex_init(&mtx);

	/* Start the threads.*/
	thread_t thread_wait;
	thread_t thread_destroy;

	thread_create(&thread_wait, thread_proc_wait, NULL);
	thread_create(&thread_destroy, thread_proc_destroy, NULL);

	thread_sleep(2);

	mutex_lock(&mtx);
	cond_signal(&cvar);
	mutex_unlock(&mtx);

	/* Try to join with threads. */
	if (thread_join(thread_wait, NULL) != EOK) {
		printf("Test failed - not EOK...\n");
		return 1;
	}

	if (thread_join(thread_destroy, NULL) != EINVAL) {
		printf("Test failed - not EINVAL...\n");
		return 1;
	}

	/* We should not reach this point. */
	printf("Test passed...\n");

	return 0;
}

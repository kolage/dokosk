/***
 * Process test #3
 *
 * Creates two threads in the process which block each other. Firstly kills one of
 * them with thread_cancel and secondly kills the whole process.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Process test #3\n"
    "Creates two threads in the process which block each other. Firstly kills one of "
	"them with thread_cancel and secondly kills the whole process.\n\n";

#include <librt.h>

#define PROCESS_START_ADDR		(void *)0x00004000		/* This is hardwired address from kernel. */
#define PROCESS_SIZE			0x00010000

static void *thread_proc(void *data)
{
	thread_t *thread = (thread_t *)data;
	printf("Thread %p started and waiting for thread %p\n", thread_self(), *thread);
	thread_usleep(100000);	/* Wait until the second thread is created. */

	int ret = thread_join(*thread, NULL);
	assert(ret == EINVAL);
	printf("Thread %p exiting...\n", thread_self());

	return NULL;
}

static void *thread_proc_kill(void *data)
{
	process_t *proc = (process_t *)data;
	printf("Killer thread started and waiting 1 second\n");

	thread_sleep(1);
	kill(*proc);

	printf("Killer thread exiting...\n", thread_self());

	return NULL;
}

int main(void)
{
	/* We reproduce the process, but we behave differently in each copy. */
	if (get_pid() == 1) {
		printf(desc);

		thread_t thread_one;
		thread_t thread_two;

		thread_create(&thread_one, thread_proc, &thread_two);
		thread_create(&thread_two, thread_proc, &thread_one);

		thread_usleep(100000);
		printf("Main thread killing thread %p...\n", thread_two);
		thread_cancel(thread_two);
		thread_usleep(100000);

		/* Do it again, but now we will kill the whole process. */
		printf("Main thread creating new process...\n", thread_two);

		process_t proc;
		int res = process_create(&proc, PROCESS_START_ADDR, PROCESS_SIZE);
		assert(res == EOK);

		/* Create thread that will kill the process after a while, we will wait for join with the process. */
		thread_create(&thread_one, thread_proc_kill, &proc);
		res = process_join(proc);
		thread_sleep(1);

		/* End of the work. */
		printf("Process A joined with process B with result with %u and exiting...\n", res);
		printf("Test passed...\n");
	/* Implementation of second process. */
	} else {
		printf("Process B started...\n");

		thread_t thread_one;
		thread_t main_thread = thread_self();
		thread_create(&thread_one, thread_proc, &main_thread);

		printf("Process B waiting for the second thread...\n");
		thread_join(thread_one, NULL);

		printf("Process B exiting...\n");
		printf("Test failed...\n");
	}

	return 0;
}

/***
 * Mutex test #3
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Mutex test #3\n"
    "Creates a mutex, destroys it and then tries to use it again.\n\n";

#include <librt.h>

/*
 * The tested mutex.
 */
static struct mutex	mtx;

int main(void)
{
	printf(desc);

	mutex_init(&mtx);
	mutex_lock(&mtx);

	mutex_destroy(&mtx);

	if (mutex_lock(&mtx) == EINVAL) {
		printf ("Test passed...\n");
	} else {
		printf ("Test failed...\n");
	}

	return 0;
}

/***
 * Mutex test #4
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Mutex test #4\n"
    "Creates a mutex, locks it, then tries to lock it in another thread "
	"and destroys the mutex -> thread should get killed, because second "
	"thread is still waiting for the mutex.\n\n";

#include <librt.h>

/*
 * The tested mutex.
 */
static struct mutex	mtx;

static void *thread_proc_wait(void *data)
{
	printf("Thread B waiting for mutex...\n");
	mutex_lock(&mtx);
	printf("Thread B got mutex...\n");

	return NULL;
}

static void *thread_proc_destroy(void *data)
{
	thread_sleep(1);

	printf("Thread C destroying mutex...\n");
	mutex_destroy(&mtx);
	printf("Thread C not killed - test failed...\n");
	exit();

	return NULL;
}

int main(void)
{
	printf(desc);

	mutex_init(& mtx);
	mutex_lock(& mtx);

	/* Start the threads.*/
	thread_t thread_wait;
	thread_t thread_destroy;

	thread_create(&thread_wait, thread_proc_wait, NULL);
	thread_create(&thread_destroy, thread_proc_destroy, NULL);

	/* Wait a while until the second thread start to wait for the mutex. */
	thread_sleep(2);
	mutex_unlock(&mtx);

	/* Try to join with threads. */
	if (thread_join(thread_wait, NULL) != EOK) {
		printf("Test failed - not EOK...\n");
		return 1;
	}

	if (thread_join(thread_destroy, NULL) != EINVAL) {
		printf("Test failed - not EINVAL...\n");
		return 1;
	}

	/* Destroying the mutex should be ok now. */
	mutex_destroy(&mtx);

	/* We should not reach this point. */
	printf("Test passed...\n");

	return 0;
}

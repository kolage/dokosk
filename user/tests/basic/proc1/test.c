/***
 * Process test #1
 *
 * Test basic functionality of processes - create, join and kill.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Process test #1\n"
    "Test basic functionality of processes.\n\n";

#include <librt.h>

#define COUNTER_ITERATIONS		2
#define PROCESS_START_ADDR		(void *)0x00004000		/* This is hardwired address from kernel. */
#define PROCESS_SIZE			0x00010000

int counter = 0;

int main(void)
{
	printf("Process started with pid %u\n", get_pid());

	/* We reproduce the process, but we behave differently in each copy. */
	if (get_pid() == 1) {
		printf(desc);
		printf("Creating new process...\n");

		process_t proc;
		process_create(&proc, PROCESS_START_ADDR, PROCESS_SIZE);
		process_join(proc);

		/* Couter should remain 0, because processes run in different address spaces. */
		assert(counter == 0);
		printf("Joined with process, counter: %u\n", counter);

		/* Create another process and test kill function. */
		printf("Creating another process...\n");
		process_create(&proc, PROCESS_START_ADDR, PROCESS_SIZE);
		printf("Created...\n");
		thread_sleep(1);
		printf("Killing the process...\n");
		kill(proc);

		printf("Test passed...\n");
	/* Implementation of second process. */
	} else {
		printf("-- New process started, counting to %u...\n", COUNTER_ITERATIONS);

		for (int i = 0; i < COUNTER_ITERATIONS; i++) {
			thread_sleep(1);
			counter++;
			printf("-- Counter: %u\n", counter);
		}

		printf("-- Process finished, counter: %u\n", counter);
	}

	return 0;
}

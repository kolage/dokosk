/**
 * Syscall test #1.
 *
 * Tests whether the thread gets killed when passing invalid pointer to the kernel.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>

int main(void)
{
	puts("Test passed...\n");

	char *ptr = (char *)(0x80000000);
	puts(ptr);
	puts("Test failed...\n");

	return 1;
}

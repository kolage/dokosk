/***
 * Process test #2
 *
 * Test the independence of processes - second process should run even
 * when the first one has finished.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

static char * desc =
    "Process test #2\n"
    "Test the independence of processes - second process should run even "
	"when the first one has finished.\n\n";

#include <librt.h>

#define PROCESS_START_ADDR		(void *)0x00004000		/* This is hardwired address from kernel. */
#define PROCESS_SIZE			0x00010000

int main(void)
{
	printf("Process started with pid %u\n", get_pid());

	/* We reproduce the process, but we behave differently in each copy. */
	if (get_pid() == 1) {
		printf(desc);
		printf("Process A started...\n");
		printf("Creating new process...\n");

		process_t proc;
		process_create(&proc, PROCESS_START_ADDR, PROCESS_SIZE);

		printf("Process A exiting...\n");
	/* Implementation of second process. */
	} else {
		printf("Process B started and sleeping for 1 second...\n");

		thread_sleep(1);

		printf("Process B exiting...\n");
		printf("Test passed...\n");
	}

	return 0;
}

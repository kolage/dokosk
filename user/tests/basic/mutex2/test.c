/***
 * Mutex test #2
 * Copied from the kernel test tests/as1/mutex5/test.c.
 *
 */

static char * desc =
    "Mutex test #2\n"
    "Creates a mutex and locks it in one thread and unlocks it\n"
    "in another. Since DEBUG_MUTEX is set, the operation should\n"
    "result in killing the thread.\n\n";


#define DEBUG_MUTEX	1

#include <librt.h>


/*
 * The tested mutex.
 */
static struct mutex	mtx;

/*
 * Global termination flag.
 */
static volatile int	finish_flag;


static void *thread_proc (void * data)
{
	mutex_unlock (& mtx);
	finish_flag = 1;
	
	return NULL;
}

int main (void)
{
	thread_t thread;
	
	printf (desc);
	
	// Init.
	finish_flag = 0;
	
	mutex_init (& mtx);
	mutex_lock (& mtx);
	
	/*
	 * Start the thread and wait until the mutex is unlocked.
	 */
	thread = thread_create (&thread, thread_proc, NULL);
	
	void *ret;
	thread_join (thread, &ret);

	/* Sleep so we could see that the thread is really killed. */
	thread_sleep (1);

	// Clean up.
	mutex_destroy (& mtx);

	// Print the result - finish flag should be false, since the thread should be killed.
	if (!finish_flag) {
		printf ("Test passed...\n");
	} else {
		printf ("Test failed...\n");
	}

	return 0;
}

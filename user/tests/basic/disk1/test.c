/***
 * Disk processes test #1
 *
 * Test basic functionality of running processes from disk.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>

static char * desc =
    "Disk processes test #1\n"
    "Loads and runs all sample processes from disk one after another.\n\n";

int main(void)
{
	printf(desc);

	int programs_count = disk_programs_count();
	printf("Programs count: %u\n", programs_count);

	for (int i = 0; i < programs_count; i++) {
		process_t process;

		printf("Starting program %u!\n\n", i);

		run_program_by_index(&process, i);
		process_join(process);

		printf("Program %u has finished!\n\n", i);
	}

	printf("Test passed...\n\n");

	return 0;
}

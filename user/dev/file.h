/**
 * @file user/dev/file.h
 *
 * Declarations of file operations.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef FILE_H_
#define FILE_H_

#include <include/c.h>
#include <include/list.h>


/** Maximum length of file name. */
#define FILE_NAME_SIZE 	100


/** File metadata. */
typedef struct file {
	/** The file can be an item on a file list. */
	item_t item;

	/** File name. */
	char name[FILE_NAME_SIZE];

	/** Size of the file. */
	int size;

	/** Disk offset in bytes where the file starts. */
	int start_offset;
} file_t;


/* Externals are commented with implementation. */
extern void file_load_file_list(list_t *file_list);

#endif /* FILE_H_ */

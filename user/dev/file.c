/**
 * @file user/dev/file.c
 *
 * Implementation of file operations.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page file_feature Files on the disk
 *
 * This page describes how files on the disk are organized.
 *
 * There should be one TAR file loaded on the disk, containing the files.
 * The TAR file should only contain simple files. The length of
 * the file name is restricted to max. 99 ASCII characters.
 * Most of the TAR header fields (mode, uid, git, mtime, ...)
 * are ignored. If an error is detected while parsing the TAR file,
 * a warning is printed and parsing the TAR file is aborted.
 * However, at least the already successfully parsed files are
 * returned.
 *
 * The function ::file_load_file_list loads metadata of the files into
 * a list. The caller can then inspect the metadata and use them
 * to load the content of the file by using disk functions (::disk_read_sector).
 *
 * Before this solution was implemented, we used to create program image
 * using shell script, but it was quite clumsy due to lot of aligning
 * operations. The TAR format was selected because it by design aligns 
 * the contents to 512 bytes which is the same as the disk sector size. 
 * Creating the file from the binaries of the programs is then a matter of 
 * a single command.
 *
 * The TAR file to be loaded on the disk is specified in msim.conf to
 * be user/prog/programs.tar. It is generated when building the user space
 * and contains the binaries of the sample programs from the
 * user/prog directory.
 *
 */

#include <dev/file.h>
#include <dev/disk.h>
#include <dev/io.h>
#include <mm/malloc.h>
#include <include/string.h>


/** TAR file header. */
typedef struct {
	/** File name. */
	char name[FILE_NAME_SIZE];

	/** Unused fields (mode, uid, gid). */
	char unused_1[24];

	/** File size in octal. */
	char size[12];

	/** Unused fields (mtime). */
	char unused_2[12];

	/** Header checksum in octal. */
	char checksum[8];

	/** File type. */
	char type;

	/** Unused fields and padding to 512 bytes. */
	char unused_4[355];
} tar_header_t;


/** Compute checksum of data in an array.
 *
 * @param  array Array with data.
 * @param  size  Size of buffer.
 *
 * @return Number representing data checksum.
 *
 */
static uint32_t file_array_checksum(const uint8_t *array, size_t size)
{
	uint32_t sum = 0;
	size_t i;

	for (i = 0; i < size; ++i) {
		sum += array[i];
	}

	return sum;
}

/** Compute TAR checksum.
 *
 * The TAR checksum is a sum of all bytes in the tar_header_t except for the
 * tar_header_t::checksum field which is replaced by spaces.
 *
 * @param  header TAR header structure.
 *
 * @return Number representing TAR checksum.
 *
 */
static uint32_t file_tar_checksum(const tar_header_t *header)
{
	return file_array_checksum((const uint8_t *)header, sizeof(tar_header_t))
			- file_array_checksum((const uint8_t *)header->checksum, sizeof(header->checksum))
			+ 32 * sizeof(header->checksum);
}

/** Decode integer from a field in TAR file header.
 *
 * @param  tar_int Integer in octal, can be padded with spaces. Can be
 *                 terminated by ::STR_END_CHAR.
 * @param  size    Size of the field of TAR header where the integer is stored.
 * @param  result  Pointer to the variable where the decoded integer
 *                 should be stored.
 *
 * @return ::true if the integer was decoded, ::false if the field contains invalid characters.
 */
static bool file_tar_int(const char *tar_int, size_t size, uint32_t *result)
{
	uint32_t value = 0;

	for (size_t i = 0; i < size; ++i) {
		if (tar_int[i] == STR_END_CHAR) {
			break;
		} else if (tar_int[i] >= '0' && tar_int[i] <= '7') {
			value = 8 * value + (tar_int[i] - '0');
		} else if (tar_int[i] != ' ') {
			*result = 0;
			return false;
		}
	}

	*result = value;
	return true;
}

/** Load metadata of files available on the disk store them to a list.
 *
 * @param file_list List where file metadata should be stored.
 *
 */
void file_load_file_list(list_t *file_list)
{
	tar_header_t tar_header;
	size_t current_sector = 0;

	/* Load file metadata. */
	for (;;) {
		disk_read_sector(current_sector, (char *)&tar_header);

		/* The end-of file marker is zero. Only the first byte triggers
		   the end-of file condition in this implementation. */
		if (tar_header.name[0] == 0) {
			break;
		}

		/* Check the checksum of the header. */
		uint32_t actual_sum = file_tar_checksum(&tar_header);
		uint32_t stored_sum;

		if (!file_tar_int(tar_header.checksum, sizeof(tar_header.checksum), &stored_sum)) {
			puts("WARNING: TAR - invalid checksum.\n");
			break;
		}

		if (actual_sum != stored_sum) {
			printf("WARNING: TAR - mismatching checksum %u %u.\n", actual_sum, stored_sum);
			break;
		}

		/* Check the type of the file. */
		if (tar_header.type != '0' && tar_header.type != 0) {
			puts("WARNING: TAR - unexpected file type.\n");
			break;
		}

		/* Get the size of the file. */
		uint32_t precise_size;
		if (!file_tar_int(tar_header.size, sizeof(tar_header.size), &precise_size)) {
			puts("WARNING: TAR - invalid size.\n");
			break;
		}

		uint32_t blocks = ALIGN_UP(precise_size, DISK_SECTOR_SIZE) / DISK_SECTOR_SIZE;

		/* Allocate an item for the file and append it to the provided list. */
		file_t *file = (file_t *) malloc(sizeof(file_t));
		if (file == NULL) {
			puts("WARNING: TAR - no memory.\n");
			break;
		}

		item_init(&file->item, file);
		list_append(file_list, &file->item);

		/* Copy file name. The last character is set to
		   STR_END_CHAR to avoid buffer overflow */
		tar_header.name[FILE_NAME_SIZE - 1] = STR_END_CHAR;
		strcpy(file->name, tar_header.name);

		file->size = precise_size;
		file->start_offset = (current_sector + 1) * DISK_SECTOR_SIZE;

		current_sector += 1 + blocks;
	}
}

/**
 * @file user/dev/disk.c
 *
 * Implementation of support for reading data from disk device.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <dev/disk.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <proc/program.h>


/** Disk size in bytes. */
static int size;


/** Initialize the disk and load programs metadata.
 *
 */
void init_disk(void)
{
	size = syscall_disk_size();
	init_programs();
}

/** Get the disk size in bytes.
 *
 * @return Disk size in bytes.
 *
 */
int disk_size(void)
{
	return size;
}

/** Read the specified data sector from disk.
 *
 * Read all bytes from specified data sector. Number of bytes are
 * specified by ::DISK_SECTOR_SIZE constant.
 *
 * @param  sector Number of sector to be read.
 * @param  buffer Allocated buffer where should data be stored.
 *
 * @return ::EINVAL when disk operation failed, otherwise ::EOK.
 *
 */
int disk_read_sector(int sector, char *buffer)
{
	return syscall_disk_read_sector(sector, buffer);
}

/**
 * @file user/dev/io.h
 *
 * Declarations of @ref io_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef IO_H_
#define IO_H_

#include <include/c.h>


/* Externals are commented with implementation. */
extern void init_io(void);
extern size_t putc(const char chr);
extern size_t puts(const char *str);
extern size_t printf(const char *format, ...);
extern char getc(void);
extern char checkc(void);
extern ssize_t gets(char *str, const size_t len);

#endif /* IO_H_ */

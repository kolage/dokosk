/**
 * @file user/dev/disk.h
 *
 * Declarations of support for reading data and running programs
 * from disk device.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef USER_DISK_H_
#define USER_DISK_H_

#include <proc/process.h>
#include <include/list.h>


/** Size of disk sector in bytes according to MSIM documentation. */
#define DISK_SECTOR_SIZE 		512


/* Externals are commented with implementation. */
extern void init_disk(void);
extern int disk_size(void);
extern int disk_read_sector(int sector, char *buffer);

#endif /* USER_DISK_H_ */

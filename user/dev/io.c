/**
 * @file user/dev/io.c
 *
 * Implementation of @ref io_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** @page io_feature Input and output
 *
 * This page describes how the standard input and output is managed in user space.
 *
 * In user space we use separate implementation of ::printf function. This
 * implementation uses a buffer of constant size to minimize the overal number
 * of syscalls. When the buffer is full or there are no more characters to be
 * printed out by ::printf function, then the whole buffer is sent to the kernel
 * by ::syscall_puts function. There is only one syscall ::syscall_puts for the
 * output which is also used by ::putc and ::puts functions.
 *
 * All of the user space output functions are protected by user space ::mutex,
 * so the output from concurrently printing threads is not mixed up. Because of
 * the use of buffer for the ::printf function, it is possible (for long strings)
 * that they might get mixed up with the output of some kernel thread (because
 * the output of long strings is realized in more syscalls and the kernel mutex
 * for printer is not hold between those syscalls).
 *
 * In user space the ::getc function is realized as simple syscall. On the other
 * hand the ::gets function is realized in user space, so there are as many
 * syscall as characters read from the std input. The advantage of that approach
 * is that we do not send a pointer to the string to the kernel, we work with it
 * only in user space and this model better separates the concerns between the kernel
 * and the user space.
 *
 * See also @kernelpage{io_feature, Kernel Input and Output}.
 *
 */

#include <include/c.h>
#include <include/codes.h>
#include <include/math.h>
#include <syscall/syscall.h>
#include <sync/mutex.h>
#include <dev/io.h>


/** Size of the output buffer. */
#define OUTPUT_BUFFER_SIZE	512


/* I/O mutexes. */
static mutex_t output_mtx;
static mutex_t input_mtx;

/* Output buffer definition - output buffer minimizes the total number
   of syscalls when printing out some string. */
static char output_buffer[OUTPUT_BUFFER_SIZE];
static size_t output_buffer_length = 0;


/** Initializes I/O.
 *
 */
void init_io(void)
{
	mutex_init_recursive(&output_mtx);
	mutex_init_recursive(&input_mtx);
}

/** Print a character.
 *
 * @param  chr The character to display.
 *
 * @return Number of printed characters.
 *
 */
size_t putc(const char chr)
{
	char buffer[2] = { chr, STR_END_CHAR };
	return puts(buffer);
}

/** Print a zero terminated string.
 *
 * @param  str The string to print.
 *
 * @return Number of printed characters.
 *
 */
size_t puts(const char *str)
{
	mutex_lock(&output_mtx);
	size_t total = syscall_puts(str);
	mutex_unlock(&output_mtx);

	return total;
}

/** Check whether the output buffer is empty.
 *
 * @return Is the output buffer empty?
 *
 */
static bool output_buffer_is_empty(void)
{
	return output_buffer_length == 0;
}

/** Check whether the output buffer is full.
 *
 * Output buffer is full when it contains one character less then
 * its capacity. This is because of the string ending \0 character.
 *
 * @return Is the output buffer full?
 *
 */
static bool output_buffer_is_full(void)
{
	return output_buffer_length == OUTPUT_BUFFER_SIZE - 1;
}

/** Flush the output buffer.
 *
 */
static void output_buffer_flush(void)
{
	/* We do want to do the syscall only when there are some
	   characters in the buffer. */
	if (!output_buffer_is_empty()) {
		puts(output_buffer);
		output_buffer_length = 0;
	}
}

/** Put one character to the output buffer.
 *
 * @param  chr Character to be added to the output buffer.
 *
 * @return Total count of characters added to the output buffer.
 *
 */
static size_t output_buffer_putc(const char chr)
{
	output_buffer[output_buffer_length] = chr;
	output_buffer[output_buffer_length + 1] = STR_END_CHAR;
	output_buffer_length++;

	if (output_buffer_is_full()) {
		output_buffer_flush();
	}

	return 1;
}

/** Put the string to the output buffer.
 *
 * @param  str String to be added to the output buffer.
 *
 * @return Total count of characters added to the output buffer.
 *
 */
static size_t output_buffer_puts(const char *str)
{
	size_t total = 0;

	while (*str) {
		total += output_buffer_putc(*str);
		str++;
	}

	return total;
}

/** Put the string representing a passed hexadecimal number to the
 *  output buffer.
 *
 * @param  num   Hexadecimal number to be converted to the string and
 *               added to the output buffer.
 * @param  align Align the number with the starting zeroes?
 *
 * @return Total count of characters added to the output buffer.
 *
 */
static size_t output_buffer_putx(const int32_t num, const bool align)
{
	const uint8_t bits_count = 32;
	const uint8_t bits_per_digit = 4;
	const uint32_t mask = 0xf0000000;
	size_t total = 0;
	uint32_t num_bits = num;
	bool send_output = align;

	/* Treat zero specifically. */
	if (!align && num == 0) {
		total += output_buffer_putc('0');
	} else {
		for (int i = 0; i < bits_count / bits_per_digit; i++) {
			/* Get the top 4 bits of the number and shift them to the lowest
			   4 bits of the number -> we get one digit. */
			uint32_t digit = (num_bits & mask) >> (bits_count - bits_per_digit);
			num_bits = num_bits << bits_per_digit;

			if (send_output == false && digit > 0) {
				send_output = true;
			}

			if (send_output) {
				/* Digits 0-9. */
				if (digit < 10) {
					total += output_buffer_putc(digit + '0');
				/* Digits a-f. */
				} else {
					total += output_buffer_putc((digit - 10) + 'a');
				}
			}
		}
	}

	return total;
}

/** Put the string representing a passed unsigned number to the output buffer.
 *
 * @param  num  Unsigned number to be converted to the string and added
 *              to the output buffer.
 * @param  base Base of the number.
 *
 * @return Total count of characters added to the output buffer.
 *
 */
static size_t output_buffer_putu(const uint32_t num, uint8_t base)
{
	base = max(base, 2);
	base = min(base, 16);
	size_t total = 0;

	/* Treat zero specifically. */
	if (num == 0) {
		total += output_buffer_putc('0');
	/* Non-zero value. */
	} else {
		/* Read the number into the buffer. The size of 11 should be
		   ok for the 32-bit numbers. */
		uint32_t num_dyn = num;
		int8_t buf_size = 0;
		char buf[11];

		while (num_dyn != 0) {
			uint32_t digit = num_dyn % base;

			/* Digits 0-9. */
			if (digit < 10) {
				buf[buf_size] = digit + '0';
			/* Digits a-f. */
			} else {
				buf[buf_size] = (digit - 10) + 'a';
			}

			num_dyn /= base;
			buf_size++;
		}

		/* Send the number to the output. */
		for (int8_t i = buf_size - 1; i >= 0; i--) {
			total += output_buffer_putc(buf[i]);
		}
	}

	return total;
}

/** Put the string representing a passed number to the output buffer.
 *
 * @param  num  Number to be converted to the string and added to
 *              the output buffer.
 * @param  base Base of the number.
 *
 * @return Total count of characters added to the output buffer.
 *
 */
static size_t output_buffer_puti(const int32_t num, uint8_t base)
{
	size_t total = 0;

	/* Handle negatives. */
	if (num < 0) {
		total += output_buffer_putc('-');
	}

	total += output_buffer_putu(abs(num), base);

	return total;
}

/** Print a formatted string.
 *
 * When the argument types do not correspond to the formating string,
 * the behavior is undefined. Double %% means a % character.
 *
 * Invalid formatting characters are ignored.
 *
 * @param  format The formatting string.
 *
 * @return Number of printed characters.
 *
 */
size_t printf(const char *format, ...)
{
	mutex_lock(&output_mtx);

	va_list argptr;
	va_start (argptr, format);

	size_t total = 0;
	bool formating = false;

	/* Go through the formatting string. */
	while (*format) {
		char actual = *format;

		/* No special formatting is needed. */
		if (formating == false) {
			if (actual == '%') {
				formating = true;
			} else {
				total += output_buffer_putc(actual);
			}
		/* There has been a formatting character. */
		} else {
			switch (actual) {
				case 'c':
					total += output_buffer_putc(va_arg(argptr, int));
					break;

				case 's':
					total += output_buffer_puts(va_arg(argptr, char*));
					break;

				case 'd':
				case 'i':
					total += output_buffer_puti(va_arg(argptr, int32_t), 10);
					break;

				case 'u':
					total += output_buffer_putu(va_arg(argptr, uint32_t), 10);
					break;

				case 'x':
					total += output_buffer_putx(va_arg(argptr, int32_t), false);
					break;

				case 'p':
					total += output_buffer_puts("0x");
					total += output_buffer_putx((int32_t)va_arg(argptr, void*), true);
					break;

				case '%':
					total += output_buffer_putc('%');
					break;

				default:
					/* We ignore an invalid formating sequence. */
					break;
			}

			formating = false;
		}

		format++;
	}

	va_end(argptr);
	output_buffer_flush();

	mutex_unlock(&output_mtx);

	return total;
}

/** Read the character from the standard input device.
 *
 * This is a blocking function which waits until the character is typed.
 *
 * @return Character read from the standard input device.
 *
 */
char getc(void)
{
	mutex_lock(&input_mtx);
	char c = syscall_getc();
	mutex_unlock(&input_mtx);

	return c;
}

/** Read the character from the standard input device and leave it in
 *  the std input buffer.
 *
 * This is a blocking function which waits until the character is typed.
 *
 * @return Character read from the standard input device.
 *
 */
char checkc(void)
{
	mutex_lock(&input_mtx);
	char c = syscall_checkc();
	mutex_unlock(&input_mtx);

	return c;
}

/** Get the string from the standard input device and save it to the
 *  parameter @p str.
 *
 * Strings are ended by the '\n' character which is also included in
 * the read string.
 *
 * @param  str Pointer to the memory where the read string should be saved.
 * @param  len Maximal length of the read string.
 *
 * @return Number of read characters without the ending '\0' character.
 *
 */
ssize_t gets(char *str, const size_t len)
{
	/* Length must be greater than 0. */
	if (len <= 0) {
		return EINVAL;
	}

	/* Read from the standard input. */
	int total = 0;
	int max_loops = len - 1;

	mutex_lock(&input_mtx);

	for (int i = 0; i < max_loops; i++) {
		int key = getc();
		*str = key;
		total++;
		str++;

		if (key == '\n') {
			break;
		}
	}

	mutex_unlock(&input_mtx);

	/* Add the ending zero character. */
	*str = '\0';

	return total;
}

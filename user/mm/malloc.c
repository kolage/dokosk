/**
 * @file user/mm/malloc.c
 *
 * Implementation of @ref malloc_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** @page malloc_feature User space memory allocator
 *
 * This page describes how the memory is managed on the user space heap.
 *
 * In user space we use simpler implementation of the heap. There are no
 * superblocks like on the @kernelpage{malloc_feature, kernel heap} - heap
 * consists just of simple blocks which have a header ::heap_block_head_t and
 * footer ::heap_block_foot_t. Those blocks are stored in the free lists also
 * ordered by the size of a block.
 *
 * When no suitable block is found when ::malloc is called, heap tries
 * to stretch itself - ::syscall_heap_resize syscall is used for
 * that purpose. When this syscall fails, we know that there is not enough
 * memory in the system and ::NULL pointer is returned to the caller.
 *
 * Heap can be also reduced - when there are enough free space at the end
 * of the heap, the same syscall is called and heap gets reduced. There is
 * minimum size of the heap specified in the ::HEAP_INIT_SIZE constant.
 *
 * Heap can be also printed out for debugging with ::heap_print function.
 *
 * If you want to see how the memory is managed on the kernel heap, see
 * the @kernelpage{malloc_feature, kernel heap allocator} description.
 *
 */

#include <include/c.h>
#include <include/codes.h>
#include <include/math.h>
#include <include/list.h>
#include <dev/io.h>
#include <proc/thread.h>
#include <syscall/syscall.h>
#include <mm/malloc.h>
#include <sync/mutex.h>


/** Magic used in heap headers. */
#define HEAP_BLOCK_HEAD_MAGIC   	0xBEEF0001

/** Magic used in heap footers. */
#define HEAP_BLOCK_FOOT_MAGIC   	0xBEEF0002

/* Initial heap size. */
#define HEAP_INIT_SIZE      		(16 * PAGE_SIZE)

/* Heap minimal size increment. */
#define HEAP_STRETCH_SIZE      		(8 * PAGE_SIZE)

/** Generic memory alignment. */
#define MEMORY_ALIGNMENT  			4

/** Size of the block metadata. */
#define HEAP_BLOCK_METADATA_SIZE	(sizeof(heap_block_head_t) + sizeof(heap_block_foot_t))

/** Size of the data of the heap free block. */
#define HEAP_FREE_BLOCK_DATA_SIZE	(sizeof(heap_free_block_info_t))

/** Total minimal size of the heap free block. */
#define HEAP_FREE_BLOCK_SIZE		(HEAP_BLOCK_METADATA_SIZE + HEAP_FREE_BLOCK_DATA_SIZE)

#if !defined(DEBUG_HEAP) || defined(__DOXYGEN__)
/** Debug the heap?
 *
 * By default debugging prints for heap are disabled.
 * You can define ::DEBUG_HEAP to true to enable heap debugging prints.
 * To do this, add HEAP to USER_DEBUG when building the OS:
 * <pre>make USER_DEBUG="HEAP"</pre>
 */
#define DEBUG_HEAP false
#endif

#if DEBUG_HEAP
#define heap_debug_print(ARGS...) printf(ARGS);
#else
#define heap_debug_print(ARGS...)
#endif


/** Header of a heap block.
 *
 */
typedef struct {
	/** Size of the block including header and footer. */
	size_t size;

	/** Indication of a free block. */
	bool free;

	/**
	 * A magic value to detect overwrite of heap header.
	 * The value is at the end of the header because
	 * that is where writing past block start will
	 * do damage.
	 */
	uint32_t magic;
} heap_block_head_t;

/** Footer of a heap block.
 *
 */
typedef struct {
	/**
	 * A magic value to detect overwrite of heap footer.
	 * The value is at the beginning of the footer
	 * because that is where writing past block
	 * end will do damage.
	 */
	uint32_t magic;

	/** Size of the block. */
	size_t size;
} heap_block_foot_t;

/** Simple structure for the free block data. Every free block can be an item
 *  in the list of free blocks.
 *
 */
typedef struct {
	item_t list_item;
} heap_free_block_info_t;


/** Address of heap start. */
static void *heap_start;

/** Address of heap end. */
static void *heap_end;


/** Lists with all the free blocks. In those lists we try to find the first
    free block when the malloc is running. There are separate lists for
    different block sizes for the speed up. */
static list_t free_blocks_list_less_than_64b;
static list_t free_blocks_list_64b_128b;
static list_t free_blocks_list_128b_512b;
static list_t free_blocks_list_512b_2k;
static list_t free_blocks_list_2k_8k;
static list_t free_blocks_list_8k_32k;
static list_t free_blocks_list_32k_128k;
static list_t free_blocks_list_more_than_128k;

/** Lock for the heap - when we allocate and deallocate memory. */
static mutex_t heap_mutex;


#ifdef NDEBUG

/** Dummy heap block check.
 *
 * Do not actually do any checking if we are building
 * a non-debug kernel.
 *
 */
#define heap_block_check(addr)

#else /* NDEBUG */

/** Check a heap block.
 *
 * Verifies that the structures related to a heap block still contain
 * the magic constants. This helps detect heap corruption early on.
 *
 * @param addr Address of the block.
 *
 */
static void heap_block_check(void *addr)
{
	/* Calculate the position of the header. */
	heap_block_head_t *head = (heap_block_head_t *) addr;

	/* Make sure the header is still intact. */
	assert(head->magic == HEAP_BLOCK_HEAD_MAGIC);

	/* Calculate the position of the footer. */
	heap_block_foot_t *foot = (heap_block_foot_t *)(addr + head->size - sizeof(heap_block_foot_t));

	/* Make sure the footer is still intact. */
	assert(foot->magic == HEAP_BLOCK_FOOT_MAGIC);

	/* And one extra check for the fun of it. */
	assert(head->size == foot->size);

	/* Check the block size - every block must be at least as big as the free block
	   (we need to store some metadata about free blocks in the data part of those blocks,
	   especially the pointer to the list of free blocks). */
	assert(head->size >= HEAP_FREE_BLOCK_SIZE);
}

#endif /* NDEBUG */

/** Count the real size of the block to be allocated.
 *
 * When we allocate the block, we have to count in the size of the
 * metadata about that block. Also we have to align the block size
 * to the 4 bytes, because we want to avoid the unaligned memory
 * access exception when accessing the block footer.
 *
 * @param  size Required block size.
 *
 * @return Real block size to be allocated.
 *
 */
static size_t heap_count_block_real_size(size_t size)
{
	return ALIGN_UP(size, MEMORY_ALIGNMENT) + HEAP_BLOCK_METADATA_SIZE;
}

/** Count the address of the data part of the heap block.
 *
 * Data part of the block is right after the block head.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the data part of the block.
 *
 */
static void *heap_get_block_data_addr(void *block_start_addr)
{
	return block_start_addr + sizeof(heap_block_head_t);
}

/** Get the free blocks list where the block of specified @p size should be.
 *
 * @param  size Size of the block.
 *
 * @return Free blocks list where the block of specified @p size should be.
 *
 */
static list_t *heap_get_free_blocks_list(size_t size)
{
	if (size < 64) {
		return &free_blocks_list_less_than_64b;
	} else if (size < 128) {
		return &free_blocks_list_64b_128b;
	} else if (size < 512) {
		return &free_blocks_list_128b_512b;
	} else if (size < PAGE_SIZE / 2) {
		return &free_blocks_list_512b_2k;
#ifdef USER_TEST
	/* Trick for the little speed up of malloc test. */
	} else if (size < heap_count_block_real_size(2500)) {
#else
	} else if (size < 2 * PAGE_SIZE) {
#endif
		return &free_blocks_list_2k_8k;
	} else if (size < 8 * PAGE_SIZE) {
		return &free_blocks_list_8k_32k;
	} else if (size < 32 * PAGE_SIZE) {
		return &free_blocks_list_32k_128k;
	} else {
		return &free_blocks_list_more_than_128k;
	}
}

/** Get the free blocks list which contains bigger blocks than passed
 *  @p blocks_list.
 *
 * @param  blocks_list Free blocks list.
 *
 * @return Free blocks list which contains bigger blocks than
 *         passed @p blocks_list.
 *
 */
static list_t *heap_get_next_free_blocks_list(list_t *blocks_list)
{
	if (blocks_list == &free_blocks_list_less_than_64b) {
		return &free_blocks_list_64b_128b;
	} else if (blocks_list == &free_blocks_list_64b_128b) {
		return &free_blocks_list_128b_512b;
	} else if (blocks_list == &free_blocks_list_128b_512b) {
		return &free_blocks_list_512b_2k;
	} else if (blocks_list == &free_blocks_list_512b_2k) {
		return &free_blocks_list_2k_8k;
	} else if (blocks_list == &free_blocks_list_2k_8k) {
		return &free_blocks_list_8k_32k;
	} else if (blocks_list == &free_blocks_list_8k_32k) {
		return &free_blocks_list_32k_128k;
	} else if (blocks_list == &free_blocks_list_32k_128k) {
		return &free_blocks_list_more_than_128k;
	} else {
		return NULL;
	}
}

/** Count all the free blocks in free blocks lists.
 *
 * @return Count of free blocks on heap.
 *
 */
static size_t heap_count_free_blocks_in_lists(void)
{
	return list_count(&free_blocks_list_less_than_64b)
			+ list_count(&free_blocks_list_64b_128b)
			+ list_count(&free_blocks_list_128b_512b)
			+ list_count(&free_blocks_list_512b_2k)
			+ list_count(&free_blocks_list_2k_8k)
			+ list_count(&free_blocks_list_8k_32k)
			+ list_count(&free_blocks_list_32k_128k)
			+ list_count(&free_blocks_list_more_than_128k);
}

/** Append the block to the list of free blocks.
 *
 * @param block_head Address of the heap block.
 *
 */
static void heap_append_block_to_free_list(heap_block_head_t *block_head)
{
	assert(block_head->free == true);
	heap_free_block_info_t *block_info = (heap_free_block_info_t *)heap_get_block_data_addr(block_head);
	item_init(&block_info->list_item, block_head);
	list_append(heap_get_free_blocks_list(block_head->size), &block_info->list_item);
}

/** Remove the block from the list of free blocks.
 *
 * @param block_head Address of the heap block.
 *
 */
static void heap_remove_block_from_free_list(heap_block_head_t *block_head)
{
	assert(block_head->free == true);
	heap_free_block_info_t *block_info = (heap_free_block_info_t *)heap_get_block_data_addr(block_head);
	list_remove(heap_get_free_blocks_list(block_head->size), &block_info->list_item);
}

/** Initialize a heap block
 *
 * Fills in the structures related to a heap block.
 *
 * @param addr Address of the block.
 * @param size Size of the block including the header and the footer.
 * @param free Indication of a free block.
 *
 */
static void heap_block_init(void *addr, size_t size, bool free)
{
	/* Calculate the position of the header and the footer. */
	heap_block_head_t *head = (heap_block_head_t *)(addr);
	heap_block_foot_t *foot = (heap_block_foot_t *)(addr + size - sizeof(heap_block_foot_t));

	/* Remove the block from the list of free blocks when it is an allocated block. */
	if (free == false) {
		heap_remove_block_from_free_list(head);
	}

	/* Fill the header. */
	head->size = size;
	head->free = free;
	head->magic = HEAP_BLOCK_HEAD_MAGIC;

	/* Fill the footer. */
	foot->magic = HEAP_BLOCK_FOOT_MAGIC;
	foot->size = size;

	/* Append the block to the list of free blocks when it is a free block. */
	if (free == true) {
		heap_append_block_to_free_list(head);
	}
}

/** Try to find the free block of suitable size for the block
 *  with passed size.
 *
 * When no block is found in the list with free blocks, then ::NULL
 * is returned.
 *
 * @param  size Required size of the free block.
 *
 * @return Address of the suitable free block or ::NULL when no suitable
 *         free block exists.
 *
 */
static heap_block_head_t *heap_find_free_block(const size_t size)
{
	list_t *free_blocks_list = heap_get_free_blocks_list(size);
	list_it_t it;

	do {
		/* Loop through the list of all free blocks. */
		for (list_it_init(&it, free_blocks_list); list_it_has_current(&it); list_it_next(&it)) {
			heap_block_head_t *block_head = (heap_block_head_t *)list_it_data(&it);
			heap_block_check(block_head);

			/* Just some basic control of the list. */
			assert(block_head->free == true);

			/* Have we found the suitable block? */
			if (block_head->size >= size) {
				return block_head;
			}
		}

		/* We have to loop through all the free blocks lists that contain bigger blocks. */
		free_blocks_list = heap_get_next_free_blocks_list(free_blocks_list);
	} while (free_blocks_list != NULL);

	return NULL;
}

/**
 * Allocate the memory in the passed block.
 *
 * @param  block_head Pointer to the block head.
 * @param  alloc_size Size of the memory to be allocated.
 *
 * @return Pointer to the block data section.
 *
 */
static void *heap_allocate_in_block(heap_block_head_t *block_head, size_t alloc_size)
{
	/* We have found a suitable block.
	   See if we should split it. */
	size_t split_limit = alloc_size + HEAP_FREE_BLOCK_SIZE;

	if (block_head->size > split_limit) {
		/* Block big enough -> split. */
		void *next = ((void *)block_head) + alloc_size;
		heap_block_init(next, block_head->size - alloc_size, true);
		heap_block_init(block_head, alloc_size, false);
	} else {
		/* Block too small -> use as is. */
		heap_block_init(block_head, block_head->size, false);
	}

	/* Either way we have our result. */
	return heap_get_block_data_addr(block_head);
}

/** Get the address of the block which is after the block with passed
 *  start address.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the next block.
 *
 */
static heap_block_head_t *heap_get_next_block_addr(void *block_start_addr)
{
	heap_block_head_t *block_head = (heap_block_head_t *)block_start_addr;

	return block_start_addr + block_head->size;
}

/** Get the address of the block which is before the block with passed
 *  start address.
 *
 * @param  block_start_addr Address of the block start.
 *
 * @return Address of the previous block.
 *
 */
static heap_block_head_t *heap_get_prev_block_addr(void *block_start_addr)
{
	heap_block_foot_t *prev_foot = (heap_block_foot_t *)(block_start_addr - sizeof(heap_block_foot_t));
	heap_block_head_t *prev_head = (heap_block_head_t *)(block_start_addr - prev_foot->size);

	return prev_head;
}

/** Try to reduce the heap.
 *
 * When there is an unused space at the end of the heap, then we can cut
 * that space off.
 *
 */
static void heap_try_reduce(void)
{
	/* There can never be two free blocks next to each other. So we check
	   whether the last block in the heap is free and if its is bigger
	   than the predefined limit and is so, we reduce the space for the heap */
	heap_block_head_t *last_block_head = heap_get_prev_block_addr(heap_end);

	/* Make sure the block is not corrupted. */
	heap_block_check(last_block_head);

	if (last_block_head->free) {
		/* We have to find how many pages can we cut from the block,
		   so we still have a space for its metadata and for the data
		   of the free block. */
		size_t cut_off_size = ALIGN_DOWN(last_block_head->size - HEAP_FREE_BLOCK_SIZE, PAGE_SIZE);

		/* Find out how much space would remain for the heap. */
		size_t new_heap_size = heap_end - heap_start - cut_off_size;

		/* If it is not enough, we recompute the cut of space. */
		if (new_heap_size < HEAP_INIT_SIZE) {
			cut_off_size -= (HEAP_INIT_SIZE - new_heap_size);
			new_heap_size = HEAP_INIT_SIZE;
		}

		/* Make sure we cut enough space. */
		if (cut_off_size >= HEAP_STRETCH_SIZE) {
			heap_debug_print(
				"-- HEAP REDUCE: Reducing the heap from %u to %u bytes; cut off: %u.\n",
				heap_end - heap_start, new_heap_size, cut_off_size
			);

			if (syscall_heap_resize(new_heap_size) == EOK) {
				/* We reduce the size of the last block. */
				heap_remove_block_from_free_list(last_block_head);
				heap_block_init(last_block_head, last_block_head->size - cut_off_size, true);

				/* We reduce the size of the whole heap. */
				heap_end = heap_end - cut_off_size;
				heap_debug_print(
					"-- HEAP REDUCE: Heap reduced; start: %p; end: %p; size: %u.\n",
					heap_start, heap_end, heap_end - heap_start
				);
			} else {
				heap_debug_print("-- HEAP REDUCE: Could not reduce the heap.\n");
			}
		}
	}
}

/** Initialize the heap allocator.
 *
 * Allocates the heap with default size of ::HEAP_INIT_SIZE and creates the
 * heap management structures that mark the whole physical memory as
 * a single free block.
 *
 */
void init_heap(void)
{
	list_init(&free_blocks_list_less_than_64b);
	list_init(&free_blocks_list_64b_128b);
	list_init(&free_blocks_list_128b_512b);
	list_init(&free_blocks_list_512b_2k);
	list_init(&free_blocks_list_2k_8k);
	list_init(&free_blocks_list_8k_32k);
	list_init(&free_blocks_list_32k_128k);
	list_init(&free_blocks_list_more_than_128k);
	mutex_init_recursive(&heap_mutex);

	/* We assume that our physical memory comes in a single
	   block that starts with the kernel image. We find
	   the end of the block by trial and error.

	   Memory regions should be 4 bytes aligned to avoid
	   unaligned memory access exceptions while accessing
	   the header and footer structures. */
	heap_start = (void *)syscall_heap_init(HEAP_INIT_SIZE);
	heap_end = heap_start + HEAP_INIT_SIZE;

	/* Make the entire area one large block. */
	heap_block_init(heap_start, heap_end - heap_start, true);
}

/**
 * Allocate memory.
 *
 * @param size Size of the returned memory.
 *
 * @return Pointer to the memory. Should be freed by ::free.
 *
 */
void *malloc(const size_t size)
{
	mutex_lock(&heap_mutex);
	heap_debug_print("-- MALLOC: Trying to allocate %u bytes.\n", size);

	/* We have to allocate a bit more to have room for
	   header and footer. The size of the memory block
	   also has to be 4 bytes aligned to avoid unaligned
	   memory access exception while accessing the
	   footer structure. */
	size_t real_size = heap_count_block_real_size(max(size, HEAP_FREE_BLOCK_DATA_SIZE));
	heap_block_head_t *block_info = heap_find_free_block(real_size);
	void *result = NULL;

	/* There is no free block large enough, we try to stretch the heap. */
	if (block_info == NULL) {
		size_t heap_size = heap_end - heap_start;
		size_t heap_increment = ALIGN_UP(max(real_size, HEAP_STRETCH_SIZE), PAGE_SIZE);
		heap_block_head_t *new_block_start = (heap_block_head_t *)heap_end;
		size_t new_block_size = heap_increment;

		heap_debug_print(
			"-- MALLOC: Increasing the heap from %u to %u bytes. %u %u %u\n",
			heap_size, heap_size + heap_increment, real_size, size, result
		);

		if (syscall_heap_resize(heap_size + heap_increment) == EOK) {
			heap_block_head_t *last_block_head = heap_get_prev_block_addr(heap_end);

			/* If the last heap block is free, we can merge it with the newly
			   allocated space and consequently increase the heap utilization. */
			if (last_block_head->free) {
				heap_remove_block_from_free_list(last_block_head);
				new_block_start = last_block_head;
				new_block_size += last_block_head->size;
			}

			/* Initialize the new block at the heap end. */
			heap_block_init(new_block_start, new_block_size, true);
			heap_block_check(new_block_start);
			result = heap_allocate_in_block(new_block_start, real_size);

			/* Stretch the heap. */
			heap_end = heap_end + heap_increment;
			heap_debug_print("-- MALLOC: Allocated new block at %p.\n", result);
		} else {
			heap_debug_print("-- MALLOC: Failed to stretch the heap.\n");
		}
	} else {
		result = heap_allocate_in_block(block_info, real_size);
		heap_debug_print("-- MALLOC: Allocated new block at %p.\n", result);
	}

	mutex_unlock(&heap_mutex);

	return result;
}

/** Allocate a memory block.
 *
 * Unlike standard ::malloc, this routine never returns ::NULL to
 * indicate an out of memory condition. When there is not enough memory
 * the calling thread is killed.
 *
 * @param size The size of the block to allocate.
 *
 * @return The address of the block.
 *
 */
void *safe_malloc(const size_t size)
{
	void *result = malloc(size);
	assert(result != NULL);
	return result;
}

/**
 * Deallocate memory previously allocated my ::malloc.
 *
 * @param ptr Pointer returned by call to ::malloc.
 *
 */
void free(const void *ptr)
{
	/* Calculate the position of the header. */
	heap_block_head_t *block_head = (heap_block_head_t *)(ptr - sizeof(heap_block_head_t));

	/* Make sure the block is not corrupted and not free. */
	heap_block_check(block_head);
	assert(!block_head->free);

	mutex_lock(&heap_mutex);
	heap_debug_print("-- FREE: Releasing the block at %p.\n", block_head);

	/* Mark down the position and size of newly created free block. */
	heap_block_head_t *new_block_addr = block_head;
	size_t new_block_size = block_head->size;

	/* Look at the previous block. If it is free, merge the two. */
	if ((void *)block_head > heap_start) {
		heap_block_head_t *prev_head = heap_get_prev_block_addr(block_head);
		heap_block_check(prev_head);

		if (prev_head->free) {
			heap_remove_block_from_free_list(prev_head);
			new_block_size += prev_head->size;
			new_block_addr = prev_head;
			heap_debug_print("-- FREE: Joining the block with previous block at %p.\n", prev_head);
		}
	}

	/* Look at the next block. If it is free, merge the two. */
	heap_block_head_t *next_head = heap_get_next_block_addr(block_head);

	if ((void *)next_head < heap_end) {
		heap_block_check(next_head);

		if (next_head->free) {
			heap_remove_block_from_free_list(next_head);
			new_block_size += next_head->size;
			heap_debug_print("-- FREE: Joining the block with next block at %p.\n", next_head);
		}
	}

	/* Init the newly counted free block. */
	heap_block_init(new_block_addr, new_block_size, true);
	heap_debug_print("-- FREE: Initializing new block at %p of size %u.\n", new_block_addr, new_block_size);

	/* Try to reduce the heap. When there is an unused space at its end -> reduce. */
	heap_try_reduce();

	heap_debug_print("-- FREE: Block %p released.\n", ptr);
	mutex_unlock(&heap_mutex);
}

/** Print the whole heap.
 *
 */
void heap_print(void)
{
	heap_block_head_t *pos = (heap_block_head_t *)heap_start;
	size_t total_size = 0;
	size_t free_blocks_count = 0;

	mutex_lock(&heap_mutex);

	while ((void *)pos < heap_end) {
		/* Make sure the heap is not corrupted. */
		heap_block_check(pos);
		total_size += pos->size;

		if (pos->free) {
			free_blocks_count++;
		}

		printf("Block: %p-%p, size: %u, free: %s\n", pos, (void *)pos + pos->size, pos->size, pos->free ? "yes" : "no");

		/* Advance to the next block. */
		pos = (heap_block_head_t *)(((void *)pos) + pos->size);
	}

	printf("Total size: %u; control sum: %u\n", total_size, heap_end - heap_start);
	printf("Free blocks count: %u; control sum: %u\n", free_blocks_count, heap_count_free_blocks_in_lists());
	assert(free_blocks_count == heap_count_free_blocks_in_lists());

	mutex_unlock(&heap_mutex);
}

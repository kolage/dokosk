/**
 * @file user/mm/malloc.h
 *
 * Declarations of @ref malloc_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef MALLOC_H_
#define MALLOC_H_

#include <include/c.h>


/* Externals are commented with implementation. */
extern void init_heap(void);
extern void *malloc(const size_t size);
extern void *safe_malloc(const size_t size);
extern void free(const void *ptr);
extern void heap_print(void);

#endif /* MALLOC_H_ */

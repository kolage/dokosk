/**
 * @file user/librt.h
 *
 * API for user processes.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/** @mainpage OS User Library Documentation
 *  @anchor user_main
 *
 * This documentation is a part of @maindoc.
 *
 * The user library contains functions to be used by user processes.
 * The functions wrap system calls and provide more functionality.
 * It also contains the entry point resposible for initialization
 * and calling the ::main function.
 *
 * The functions are compiled into librt.a library which should be linked
 * with the user program object files using the user.lds linker script.
 *
 * @section user_features Implemented features
 *
 * - @ref io_feature
 * - @ref process_feature
 * - @ref thread_feature
 * - @ref malloc_feature
 * - @ref synchronization_feature
 * - @ref file_feature
 * - @ref program_feature
 *
 */

#ifndef LIBRT_H_
#define LIBRT_H_

#include <include/c.h>
#include <include/codes.h>
#include <include/list.h>

#include <include/string.h>

#include <dev/io.h>
#include <dev/disk.h>
#include <dev/file.h>

#include <mm/malloc.h>

#include <proc/thread.h>
#include <proc/process.h>
#include <proc/program.h>

#include <sync/mutex.h>
#include <sync/cond.h>

#endif /* LIBRT_H_ */

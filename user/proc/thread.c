/**
 * @file user/proc/thread.c
 *
 * Implementation of thread management in user space.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page thread_feature User space threads
 * 
 * Thread management in user space is implemented quite straightforwardly 
 * by using system of syscalls.
 * 
 * As in the kernel space, the wrapper function is used for handling thread 
 * function return value and easily deallocating its structure.
 * 
 * When waiting for thread execution to complete, user can request for 
 * passing its function return value into defined place, by using
 * a special output parameter in ::thread_join.
 * 
 * User can use ::exit function to terminate all child threads of currently
 * running process. When there are no other processes scheduled, whole
 * system is terminated.
 * 
 * Typical functions implementing basic thread operations including thread 
 * synchronization such as ::thread_join, ::thread_suspend, ::thread_cancel 
 * etc. are available.
 * 
 */

#include <proc/thread.h>
#include <syscall/syscall.h>
#include <include/codes.h>
#include <include/c.h>
#include <dev/io.h>


/** Wrapper function to be called in thread.
 *
 * We use this wrapper function as the start function in threads to determine
 * when the function we originally wanted to start will end.
 *
 * We can not use kernel implementation due to different address space.
 *
 * @param start The function we want to start in thread.
 * @param data  Arguments of our function.
 *
 */
static void thread_run(void *(*start)(void *), void *data)
{
	void *ret_value = start(data);
	thread_exit(ret_value);
}

/** Create a new thread.
 *
 * The function creates new attached thread.
 *
 * @param thread_ptr   Newly created thread structure.
 * @param thread_start Function to be started in newly created thread.
 * @param data         Data related to thread_start function.
 *
 * @return ::ENOMEM when there was not enough memory to allocate thread
 *         structure, otherwise ::EOK.
 *
 */
int thread_create(thread_t *thread_ptr, void *(*thread_start)(void *), void *data)
{
	return syscall_thread_create(thread_ptr, &thread_run, thread_start, data);
}

/** Get currently running thread.
 *
 * @return Currently running thread.
 *
 */
thread_t thread_self(void)
{
	return syscall_thread_self();
}

/** Wait until thread run ends.
 *
 * The function waits till the given thread ends and then initiates memory
 * release containing kernel handling structure and stack.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread
 * was detached using ::thread_detach function, if someone else is
 * already waiting for this thread or if thread is calling function to itself.
 * It also returns ::EINVAL if thread was cancelled by ::thread_cancel function.
 * If no error is present, it returns ::EOK.
 *
 * @param  thread        Thread to be waited for.
 * @param  thread_retval Place to writeback return value.
 *
 * @return Error code.
 *
 */
int thread_join(thread_t thread, void **thread_retval)
{
	return syscall_thread_join(thread, thread_retval);
}

/** Wait until thread run ends with upper bound
 *
 * The function waits till the given thread ends, but no more than time
 * given by @p usec. Then initiates memory release containing kernel
 * handling structure and stack.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread was
 * detached using ::thread_detach function, if someone else is already
 * waiting for this thread or if thread is calling function to itself.
 * It also returns ::EINVAL if thread was cancelled by ::thread_cancel function
 * and ::ETIMEDOUT if thread is not terminated before @p usec time. If
 * no error is present, it returns ::EOK.
 *
 * @param  thread        Thread to be waited for.
 * @param  thread_retval Place to writeback return value.
 * @param  usec          Time in microseconds representing the maximum
 *                       time to be waited for the thread.
 *
 * @return Error code.
 *
 */
int thread_join_timeout(thread_t thread, void **thread_retval, const unsigned int usec)
{
	return syscall_thread_join_timeout(thread, thread_retval, usec);
}

/** Detach given thread.
 *
 * The function detaches the given thread.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread is
 * already detached, if thread is already not running and waiting for join,
 * if someone is already waiting for thread in thread_join or if the
 * thread is cancelled, otherwise ::EOK.
 *
 * @param  thread Thread to be detached.
 *
 * @return Error code.
 *
 */
int thread_detach(thread_t thread)
{
	return syscall_thread_detach(thread);
}

/** Cancel specified (running) thread.
 *
 * If thread is in a critical section, it is only marked to be cancelled
 * and on leaving CS it commits suicide. Otherwise function cancels given
 * thread, thread that is waiting for termination of thread being cancelled
 * is unblocked.
 *
 * It returns ::EINVAL if thread identification is invalid, if thread has
 * been already cancelled, if thread has been already marked to be cancelled or
 * if thread has already finished, otherwise ::EOK.
 *
 * @param  thread Thread to be cancelled.
 *
 * @return Error code.
 *
 */
int thread_cancel(thread_t thread)
{
	return syscall_thread_cancel(thread);
}

/** Block currently running thread for given @p sec.
 *
 * @param sec Number of seconds to block thread.
 *
 */
void thread_sleep(const unsigned int sec)
{
	syscall_thread_sleep(sec);
}

/** Block currently running thread for given @p usec.
 *
 * @param usec Number of microseconds to block thread.
 *
 */
void thread_usleep(const unsigned int usec)
{
	syscall_thread_usleep(usec);
}

/** Pause currently running thread.
 *
 * The function pauses currently running thread and allow other threads
 * to execute. It is paused until it is scheduled again.
 *
 */
void thread_yield(void)
{
	syscall_thread_yield();
}

/** Suspend currently running thread.
 *
 * The function suspends currently running thread until some other thread
 * unblocks it using ::thread_wakeup function.
 *
 */
void thread_suspend(void)
{
	syscall_thread_suspend();
}

/** Wake up given thread.
 *
 * The function wakes up (unblocks) given thread. The calling thread
 * continues execution and it isn't rescheduled during this call.
 *
 * It returns ::EINVAL if thread identification is invalid, otherwise ::EOK.
 *
 * @param  thread Thread to be woken up.
 *
 * @return Error code.
 *
 */
int thread_wakeup(thread_t thread)
{
	return syscall_thread_wakeup(thread);
}

/** Terminate thread execution.
 *
 * @param thread_retval Return value of thread worker function.
 *
 */
void thread_exit(void *thread_retval)
{
	syscall_thread_exit(thread_retval);
}

/** Terminate execution of all threads in current process.
 *
 * If there is no other running user process, whole system
 * is terminated.
 *
 */
__attribute__((noreturn))
void exit(void)
{
	syscall_exit();
	printf("Fatal error: thread has returned to the exit function!\n");

	/* Halt the MSIM when fatal error occurred. */
	asm volatile (
		".insn\n"
		".word 0x28\n"
	);
	while (true);
}

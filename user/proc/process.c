/**
 * @file user/proc/process.c
 *
 * Implementation of processes in the user space.
 *
 * There is a support for basic operations with processes like creating
 * of a new process with ::process_create function, joining to a running
 * process with ::process_join function or killing a running process with
 * ::kill function.
 *
 * This file is related to @ref process_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <syscall/syscall.h>
#include <proc/process.h>


/** Create new user process with a new address space.
 *
 * The binary image of the process is loaded from the address space of
 * currently running process from address @p img.
 *
 * @param  process_ptr Identifier of process to get filled.
 * @param  img         Address of process binary image in the current
 *                     address space.
 * @param  size        Size of the process binary image.
 *
 * @return Returns ::EOK when the process is successfully created,
 *         ::ENOMEM when there is not enough memory for creating the process
 *         and ::EINVAL when the process could not be creating because of
 *         other reason.
 *
 */
int process_create(process_t *process_ptr, const void *img, const size_t size)
{
	return syscall_process_create(process_ptr, img, size);
}

/** Get an identifier of currently running process.
 *
 * @return Currently running process.
 *
 */
process_t process_self(void)
{
	return syscall_process_self();
}

/** Block the calling thread until the passed process finishes.
 *
 * Returns ::EINVAL when someone else is waiting for the process, or when
 * thread is calling the function for its own process.
 *
 * @param  process Process to wait for.
 *
 * @return ::EINVAL when the process can not be joined, otherwise ::EOK.
 *
 */
int process_join(process_t process)
{
	return syscall_process_join(process);
}

/** Block the calling thread until the passed process finishes.
 *
 * Returns ::EINVAL when someone else is waiting for the process, or when
 * thread is calling the function for its own process. This function waits
 * maximally @p usec microseconds.
 *
 * When the waiting expired this function returns ::ETIMEDOUT.
 *
 * @param  process Process to wait for.
 * @param  usec    Number of microseconds to wait maximally.
 *
 * @return ::EINVAL when the process can not be joined, ::ETIMEDOUT when
 *         the timeout expired, ::EOK otherwise.
 *
 */
int process_join_timeout(process_t process, const unsigned int usec)
{
	return syscall_process_join_timeout(process, usec);
}

/** Kill the passed running process.
 *
 * Thread which is waiting for the process to finish is waken up.
 *
 * @param  process Process to get killed.
 *
 * @return ::EOK when the passed process identifier is valid and the process
 *         was successfully killed, ::EINVAL otherwise.
 *
 */
int kill(process_t process)
{
	return syscall_kill(process);
}

/** Get the id of currently running process.
 *
 * @return Id of currently running process.
 *
 */
unative_t get_pid(void)
{
	return syscall_get_pid();
}

/**
 * @file user/proc/program.c
 *
 * Implementation of support for running programs from disk device.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */
/**
 * @page program_feature Disk programs
 * 
 * There are several sample programs stored on disk device that can user switch between.
 * 
 * When starting main user process, programs metadata are loaded from disk into memory. 
 * User can use simple iterator interface to choose what program to run.
 * 
 * After user chooses a specific program by its name or its index (position) on drive, 
 * ::load_program function is called to load program binary from disk into buffer. Then 
 * the user process is created and the binary data are copied from buffer into newly 
 * created process virtual memory space.
 * 
 * User can implement his own programs simply by creating a new  directory inside prog/ 
 * folder and including our user space library @ref user/librt.h into his code. On system 
 * startup this program would be mapped on disk device, so it can be easily launched later.
 * 
 */

#include <proc/program.h>
#include <include/codes.h>
#include <dev/file.h>
#include <dev/disk.h>
#include <mm/malloc.h>
#include <include/string.h>


/** List of available disk programs. */
static list_t disk_programs;

/** Number of programs on disk. */
static int programs_count;


/** Initialize programs.
 *
 */
void init_programs(void)
{
	list_init(&disk_programs);
	file_load_file_list(&disk_programs);
	programs_count = list_count(&disk_programs);
}

/** Compute required buffer size to load program.
 *
 * @param  program Disk program structure.
 *
 * @return Buffer size.
 *
 */
static size_t get_required_buffer_size(file_t *program)
{
	size_t buffer_size = program->size;

	if (program->start_offset % DISK_SECTOR_SIZE != 0) {
		buffer_size += DISK_SECTOR_SIZE;
	}

	if ((program->start_offset + program->size) % DISK_SECTOR_SIZE != 0) {
		buffer_size += DISK_SECTOR_SIZE;
	}

	return buffer_size;
}

/** Get a program by its index on disk.
 *
 * @param  index Program index on disk.
 *
 * @return File with the program or ::NULL.
 *
 */
static file_t *get_program_by_index(size_t index)
{
	list_it_t it;
	list_it_init(&it, &disk_programs);
	size_t current_index = 0;

	while (list_it_has_current(&it)) {
		if (current_index == index) {
			return (file_t *)list_it_data(&it);
		}

		list_it_next(&it);
		current_index++;
	}

	return NULL;
}

/** Get a program by its name.
 *
 * @param  name Program name.
 *
 * @return File with the program or ::NULL.
 *
 */
static file_t *get_program_by_name(char *name)
{
	list_it_t it;
	list_it_init(&it, &disk_programs);

	while (list_it_has_current(&it)) {
		file_t *current_program = (file_t *)list_it_data(&it);

		if (strcmp(current_program->name, name) == 0) {
			return current_program;
		}

		list_it_next(&it);
	}

	return NULL;
}

/** Load specified program into buffer.
 *
 * @param  buffer        Allocated buffer where program binary will be stored.
 * @param  program       File with the program to be loaded.
 * @param  buffer_offset Output parameter used to determine start position
 *                       of program binary inside buffer.
 *
 * @return ::EOK if program was loaded successfully, otherwise ::EINVAL.
 *
 */
static int load_program(char *buffer, file_t *program, size_t *buffer_offset)
{
	if (buffer == NULL) {
		return EINVAL;
	}

	int ret_value = EINVAL;
	int start_sector = program->start_offset / DISK_SECTOR_SIZE;
	*buffer_offset = program->start_offset % DISK_SECTOR_SIZE;

	int end_sector = (program->start_offset + program->size) / DISK_SECTOR_SIZE;
	int count = 0;

	for (int i = start_sector; i <= end_sector; i++) {
		ret_value = disk_read_sector(i, &buffer[count * DISK_SECTOR_SIZE]);

		if (ret_value != EOK) {
			return ret_value;
		}

		count++;
	}

	return ret_value;

}

/** Worker procedure for running disk program.
 *
 * @param  process_ptr    Pointer to process with disk program.
 * @param  program_to_run File with the program to be run.
 *
 * @return ::EOK if program was launched successfully, otherwise ::EINVAL.
 *
 */
static int run_program_proc(process_t *process_ptr, file_t *program_to_run)
{
	if (program_to_run == NULL) {
		return EINVAL;
	}

	int ret_value;
	size_t buffer_size = get_required_buffer_size(program_to_run);
	char *buffer = (char *)malloc(buffer_size);
	size_t buffer_offset;

	if (load_program(buffer, program_to_run, &buffer_offset) == EOK) {
		ret_value = process_create(process_ptr, &buffer[buffer_offset], program_to_run->size);
	} else {
		ret_value = EINVAL;
	}

	free(buffer);

	return ret_value;
}

/** Run a program by its index on disk.
 *
 * @param  process_ptr Pointer to process with disk program.
 * @param  index       Index of program on disk.
 *
 * @return ::EOK if program was launched successfully, otherwise ::EINVAL.
 *
 */
int run_program_by_index(process_t *process_ptr, size_t index)
{
	file_t *program_to_run = get_program_by_index(index);

	return run_program_proc(process_ptr, program_to_run);
}

/** Run program by its name.
 *
 * @param  process_ptr Pointer to process with disk program.
 * @param  name        Name of program on disk.
 *
 * @return ::EOK if program was launched successfully, otherwise ::EINVAL.
 *
 */
int run_program_by_name(process_t *process_ptr, char *name)
{
	if (name == NULL) {
		return EINVAL;
	}

	file_t *program_to_run = get_program_by_name(name);

	return run_program_proc(process_ptr, program_to_run);
}

/** Get number of programs stored on disk.
 *
 * @return Number of programs stored on disk.
 *
 */
int disk_programs_count(void)
{
	return programs_count;
}

/** Initializes the iterator for programs loaded at disk.
 *
 * @param it Disk programs iterator.
 *
 */
void disk_programs_it_init(disk_programs_it_t *it)
{
	list_it_init(&it->list_it, &disk_programs);
}

/** Check whether the disk programs iterator points to a valid item of
 *  a programs list (has reached the end of the list).
 *
 * @param  it Disk programs iterator.
 *
 * @return ::true if the iterator has a current item, ::false if all
 *         items have been iterated over.
 *
 */
bool disk_programs_it_has_current(disk_programs_it_t *it)
{
	return list_it_has_current(&it->list_it);
}

/** Advance the iterator to the next item in the iterated disk programs.
 *
 * @param it Disk programs iterator. The iterator must have a valid
 *           item (check by ::disk_programs_it_has_current).
 *
 */
void disk_programs_it_next(disk_programs_it_t *it)
{
	list_it_next(&it->list_it);
}

/** Get the name of the current program in the iteration.
 *
 * @param  it Disk programs iterator. The iterator must have a valid
 *            item (check by ::disk_programs_it_has_current).
 *
 * @return Name of the current program in the iteration.
 *
 */
char *disk_programs_it_name(disk_programs_it_t *it)
{
	return ((file_t *)list_it_data(&it->list_it))->name;
}

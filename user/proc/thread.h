/**
 * @file user/proc/thread.h
 *
 * Declarations of thread management in user space.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <include/c.h>


/** Definition of type for thread handle in user space. */
typedef unative_t thread_t;

/* Externals are commented with implementation. */
extern int thread_create(thread_t *thread_ptr, void *(*thread_start)(void *), void *data);
extern thread_t thread_self(void);
extern int thread_join(thread_t thread, void **thread_retval);
extern int thread_join_timeout(thread_t thread, void **thread_retval, const unsigned int usec);
extern int thread_detach(thread_t thread);
extern int thread_cancel(thread_t thread);
extern void thread_sleep(const unsigned int sec);
extern void thread_usleep(const unsigned int usec);
extern void thread_yield(void);
extern void thread_suspend(void);
extern int thread_wakeup(thread_t thread);
extern void thread_exit(void *thread_retval);

__attribute__((noreturn))
extern void exit(void);

#endif /* THREAD_H_ */

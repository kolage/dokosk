/**
 * @file user/proc/process.h
 *
 * Declarations of processes in the user space.
 *
 * This file is related to @ref process_feature.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <include/c.h>


/** Definition of type for process handle in user space. */
typedef unative_t process_t;


/* Externals are commented with implementation. */
extern int process_create(process_t *process_ptr, const void *img, const size_t size);
extern process_t process_self(void);
extern int process_join(process_t process);
extern int process_join_timeout(process_t process, const unsigned int usec);
extern int kill(process_t process);
extern unative_t get_pid(void);

#endif /* PROCESS_H_ */

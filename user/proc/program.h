/**
 * @file user/proc/program.h
 *
 * Declarations of support for running programs from disk device.
 *
 * OS
 *
 * Copyright (c) 2014
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <include/c.h>
#include <include/list.h>
#include <proc/process.h>

/** A disk programs iterator.
 */
typedef struct disk_programs_it {
	/** A programs list iterator. */
	list_it_t list_it;
} disk_programs_it_t;

/* Externals are commented with implementation. */
extern void init_programs(void);
extern int run_program_by_index(process_t *process_ptr, size_t index);
extern int run_program_by_name(process_t *process_ptr, char *name);
extern int disk_programs_count(void);

extern void disk_programs_it_init(disk_programs_it_t *it);
extern bool disk_programs_it_has_current(disk_programs_it_t *it);
extern void disk_programs_it_next(disk_programs_it_t *it);
extern char *disk_programs_it_name(disk_programs_it_t *it);

#endif /* PROGRAM_H_ */

/**
 * @file user/prog/jasper/jasper.c
 *
 * Jasper is a sample application for ASCII drawing.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>

#define BUFFER_LENGTH 3

/** Draw diamond pattern on screen.
 *
 * @param rows Number of diamond rows.
 *
 */
static void diamond(int rows)
{
	int i, j, space = 1;
	space = rows - 1;

	for (i = 1; i <= rows; i++) {
		for (j = 1; j <= space; j++) {
			printf(" ");
		}

		space--;

		for (j = 1; j <= 2 * i - 1; j++) {
			printf("*");
		}

		printf("\n");
	}

	space = 1;

	for (i = 1; i <= rows - 1; i++) {
		for (j = 1; j <= space; j++) {
			printf(" ");
		}

		space++;

		for (j = 1 ; j <= 2 * (rows - i) - 1; j++) {
			printf("*");
		}

		printf("\n");
	}
}

int main()
{
	printf("Welcome to Jasper - a sample application for drawing diamond.\n\n");

	printf("Enter number of rows (1-9): ");
	char buffer[BUFFER_LENGTH];
	gets(buffer, BUFFER_LENGTH);
	puts(buffer);

	char *input = rtrim(buffer);
	int rows = 0;

	if (atoi(input, &rows) && rows >= 1 && rows <= 9) {
		diamond(rows);
	} else {
		printf("\nError: Invalid number of rows!\n");
	}

	return EOK;
}

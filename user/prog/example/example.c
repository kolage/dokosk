/**
 * @file user/prog/example/example.c
 *
 * Example user process which demonstrates multi-threading and synchronization.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>

thread_t thread1, thread2, thread3;

/** A trivial random number generator.
 *
 * Borrowed from Kalisto tests.
 *
 * @return A random unsigned long number.
 *
 */
inline static unsigned long my_rand(void)
{
	static unsigned long random_seed = 12345678;
	random_seed = (random_seed * 873511) % 22348977 + 7;

	return (random_seed >> 8);
}

static void *thread_activity_1(void *unused)
{
	puts("First counting thread started\n");

	int total = 10;

	for (int i = 0; i < total; i++) {
		printf("Counting ... %d/%d\n", i, total);
		thread_usleep(100000);
	}

	puts("First counting thread finished\n");

	return NULL;
}

static void *thread_activity_2(void *id)
{
	int thread_id = (int)id;
	char *thread_prefix;

	if (thread_id == 2) {
		thread_prefix = "--";
	} else {
		thread_prefix = "----";
	}

	printf("Counting thread %d started\n", thread_id);

	int total = 100;
	int suspend_us = 0;

	for (int i = 0; i < total; i++) {
		printf("%s Thread %d counting ... %d/%d\n", thread_prefix, thread_id, i + 1, total);
		suspend_us = my_rand() % 100000;
		thread_usleep(suspend_us);
	}

	printf("Counting thread %d finished\n", thread_id);

	return NULL;
}

static void *thread_activity_3(void *unused)
{
	puts("Thread 4 started\n\n");

	while (true) {
		putc('x');
		thread_usleep(10000);
	}

	return NULL;
}

int main(void)
{
	thread_t thread1, thread2, thread3, thread4;

	puts("\n *** User process example *** \n\n");
	puts("Thread 0: Waiting for thread 1 firstly ... \n\n");

	thread_usleep(500000);

	thread_create(&thread1, &thread_activity_1, NULL);
	thread_join(thread1, NULL);

	puts("\nThread 0: Thread 1 finished ... \n\n");
	puts("Thread 0: Now waiting for threads 2, 3 ... \n\n");

	thread_usleep(50000);

	thread_create(&thread2, &thread_activity_2, (void *)2);
	thread_create(&thread3, &thread_activity_2, (void *)3);
	thread_join(thread2, NULL);
	thread_join(thread3, NULL);

	puts("\nThread 0: Threads 2,3 finished ... \n\n");
	puts("\nThread 0: Gonna kill thread 4 after one second ... \n\n");

	thread_usleep(500000);

	thread_create(&thread4,&thread_activity_3,(void *) 4);
	thread_sleep(1);
	puts("\n\nThread 0: Cancelling thread 4 ... \n\n");

	int state = thread_cancel(thread4);
	printf("Thread 0: Thread 4 cancelled with status %d\n", state);
	puts("Thread 0: Trying to join thread 4 ...\n");
	state = thread_join(thread4, NULL);

	if (state != EOK) {
		printf("Thread 0: Joining thread 4 returned error code %d\n", state);
	}

	return 0;
}

/**
 * @file user/prog/adonis/adonis.c
 *
 * Adonis is a sample application for counting numbers.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <librt.h>

#define MAX 100

/** Sum of numbers 0 ... n
 *
 * @param  n Sum upper limit
 *
 * @return Sum of numbers 0 ... n
 *
 */
static int sum(int n)
{
	int sum = 0;

	for (int i = 0; i < n; i++) {
		sum += i;
		printf("Temporary sum is %d\n", sum);
	}

	return sum;
}

int main()
{
	printf("Welcome to Adonis - a sample application for counting numbers.\n\n");
	int total = sum(MAX);

	printf("Total sum is %d\n\n", total);
	printf("That's all from Adonis. Thanks for watching.\n\n");

	return EOK;
}

/**
 * @file user/syscall/syscall.h
 *
 * This file contains C declarations for syscalls.
 *
 * The definitions are in user/syscall/syscall.S.
 *
 * For the specifications of the syscalls, see @syscalltable table.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef SYSCALL_H_
#define SYSCALL_H_

#include <include/c.h>


/** Call @syscalltable 1. */
extern char syscall_getc();

/** Call @syscalltable 2. */
extern char syscall_checkc();

/** Call @syscalltable 3. */
extern size_t syscall_puts(const char *str);

/** Call @syscalltable 4. */
extern int syscall_disk_read_sector(int sector, char buffer[]);

/** Call @syscalltable 5. */
extern int syscall_disk_size(void);

/** Call @syscalltable 10. */
extern void *syscall_heap_init(size_t heap_size);

/** Call @syscalltable 11. */
extern int syscall_heap_resize(size_t heap_size);

/** Call @syscalltable 20. */
extern int syscall_mutex_init(unative_t *mutex_ptr, bool recursive);

/** Call @syscalltable 21. */
extern int syscall_mutex_destroy(unative_t mutex);

/** Call @syscalltable 22. */
extern int syscall_mutex_lock(unative_t mutex);

/** Call @syscalltable 23. */
extern int syscall_mutex_lock_timeout(unative_t mutex, unsigned int usec);

/** Call @syscalltable 24. */
extern int syscall_mutex_unlock(unative_t mutex, bool checked);

/** Call @syscalltable 30. */
extern int syscall_thread_create(unative_t *thread_ptr, void *start_wrapper, void *thread_start, void *data);

/** Call @syscalltable 31. */
extern unative_t syscall_thread_self(void);

/** Call @syscalltable 32. */
extern int syscall_thread_join(unative_t thread, void **thread_retval);

/** Call @syscalltable 33. */
extern int syscall_thread_join_timeout(unative_t thread, void **thread_retval, unsigned int usec);

/** Call @syscalltable 34. */
extern int syscall_thread_detach(unative_t thread);

/** Call @syscalltable 35. */
extern int syscall_thread_cancel(unative_t thread);

/** Call @syscalltable 36. */
extern void syscall_thread_sleep(unsigned int sec);

/** Call @syscalltable 37. */
extern void syscall_thread_usleep(unsigned int usec);

/** Call @syscalltable 38. */
extern void syscall_thread_yield(void);

/** Call @syscalltable 39. */
extern void syscall_thread_suspend(void);

/** Call @syscalltable 40. */
extern int syscall_thread_wakeup(unative_t thread);

/** Call @syscalltable 41. */
extern void syscall_thread_exit(void *thread_retval);

/** Call @syscalltable 42. */
extern void syscall_exit(void);

/** Call @syscalltable 50. */
extern int syscall_cond_init(unative_t *handle_ptr);

/** Call @syscalltable 51. */
extern int syscall_cond_destroy(unative_t handle);

/** Call @syscalltable 52. */
extern int syscall_cond_signal(unative_t handle);

/** Call @syscalltable 53. */
extern int syscall_cond_broadcast(unative_t handle);

/** Call @syscalltable 54. */
int syscall_cond_wait(unative_t handle, unative_t mtx_handle);

/** Call @syscalltable 55. */
extern int syscall_cond_wait_timeout(unative_t handle, unative_t mtx_handle, unsigned int usec);

/** Call @syscalltable 60. */
extern int syscall_process_create(unative_t *process_ptr, const void *img, size_t size);

/** Call @syscalltable 61. */
extern unative_t syscall_process_self(void);

/** Call @syscalltable 62. */
extern int syscall_process_join(unative_t process);

/** Call @syscalltable 63. */
extern int syscall_process_join_timeout(unative_t process, unsigned int usec);

/** Call @syscalltable 64. */
extern int syscall_kill(unative_t process);

/** Call @syscalltable 65. */
extern unative_t syscall_get_pid(void);

#endif /* SYSCALL_H_ */

/**
 * @file user/include/string.c
 *
 * Implementation of some basic string operations like converting string
 * to a number with ::atoi function, string comparison ::strcmp function,
 * copying of string with ::strcpy function etc.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#include <include/c.h>
#include <include/string.h>
#include <dev/io.h>


/** Convert string to an integer.
 *
 * @param  string Input string.
 * @param  number Output number.
 *
 * @return True if conversion was successful, otherwise false.
 *
 */
bool atoi(char *string, int *number)
{
	/* Handle negatives. */
	bool negative = false;
	int i = 0;

	if (string[0] == '-') {
		negative = true;
		i++;
	} else if (string[0] == '+') {
		i++;
	}

	/* Handle an invalid number (with zero length). */
	if (string[i] == STR_END_CHAR) {
		return false;
	} else {
		int res = 0;

		while (string[i] != STR_END_CHAR) {
			if (string[i] >= '0' && string[i] <= '9') {
				res = res * 10 + (string[i] - '0');
				i++;
			} else {
				/* Number is invalid. */
				return false;
			}
		}

		/* Passed string represented a number. */
		*number = negative ? -res : res;
		return true;
	}
}

/** Compare two strings.
 *
 * @param  first_string  First string.
 * @param  second_string Second string.
 *
 * @return 0 if strings are same, -1 if first string is lexicographically
 *         before second string, otherwise 1.
 *
 */
int strcmp(char *first_string, char *second_string)
{
	int i = 0;

	while (first_string[i] == second_string[i]) {
		if (first_string[i] == STR_END_CHAR) {
			return 0;
		}

		i++;
	}

	return (first_string[i] < second_string[i]) ? -1 : 1;
}

/** Get the length of passed @p string.
 *
 * @param  string Input string.
 *
 * @return String length.
 *
 */
int strlen(char *string)
{
	int i = 0;

	while (string[i] != STR_END_CHAR) {
		i++;
	}

	return i;
}

/** Trim trailing whitespace from the end of the passed @p string.
 *
 * @param  string Input string
 *
 * @return String with trimmed whitespace at its end.
 *
 */
char *rtrim(char *string)
{
	int i = strlen(string) - 1;

	while (i >= 0 && (string[i] == ' ' || string[i] == '\t' || string[i] == '\r' || string[i] == '\n')) {
		i--;
	}

	string[i + 1] = STR_END_CHAR;
	return string;
}

/** Copy passed @p string to a buffer.
 *
 * The caller must guarantee that the string fits into the provided buffer.
 *
 * @param dest Destination buffer.
 * @param src  String to be copied.
 *
 */
void strcpy(char *dest, const char *src)
{
	size_t i;

	for (i = 0; src[i] != STR_END_CHAR; i++) {
		dest[i] = src[i];
	}

	dest[i] = STR_END_CHAR;
}

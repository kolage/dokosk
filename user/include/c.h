/**
 * @file user/include/c.h
 *
 * Common C declarations.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef C_H_
#define C_H_


/** Process main method definition. */
extern int main(void);


/** Size of the one page. */
#define PAGE_SIZE 	4096

/** Align to the nearest lower address
 *
 * @param size  Address or size to be aligned.
 * @param align Size of alignment, must be power of 2.
 *
 */
#define ALIGN_DOWN(size, align)  	((size) & ~((align) - 1))

/** Align to the nearest higher address
 *
 * @param size  Address or size to be aligned.
 * @param align Size of alignment, must be power of 2.
 *
 */
#define ALIGN_UP(size, align)  		(((size) + ((align) - 1)) & ~((align) - 1))


/***************************************************************************\
| Basics                                                                    |
\***************************************************************************/

#define false  	0
#define true   	(!false)

/* An invalid pointer value. */
#define NULL  	0

/** Character for strings end. */
#define STR_END_CHAR		'\0'

/* Works only when NDEBUG is not defined.
   Make sure a condition holds at runtime.*/
#ifndef NDEBUG
#define assert(EXPR) \
	if (!(EXPR)) { \
		printf ("Assertion failed: " #EXPR "\nFunction: %s, file: %s, line: %i\n", __FUNCTION__, __FILE__, __LINE__); \
		exit(); \
	}
#else  /* NDEBUG is defined */
#define assert(EXPR)
#endif /* NDEBUG */

/* Works only when NDEBUG is not defined.
   Write the debug information: function name
   and the line number. It also writes
   a formated message. */
#ifndef NDEBUG
#define dprintf(ARGS...) \
{ \
	printf ("[DEBUG] Function: %s, line: %i; ", __FUNCTION__, __LINE__); \
	printf (ARGS); \
	puts ("\n"); \
}
#else  /* NDEBUG is defined */
#define dprintf(ARGS...)
#endif /* NDEBUG */


/** Basic platform types. */
typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef signed short int16_t;
typedef unsigned short uint16_t;

typedef signed long int32_t;
typedef unsigned long uint32_t;

typedef signed long long int64_t;
typedef unsigned long long uint64_t;

typedef int32_t native_t;
typedef uint32_t unative_t;
typedef uint32_t uintptr_t;
typedef uint32_t off_t;
typedef uint32_t size_t;
typedef int32_t ssize_t;
typedef uint8_t bool;

typedef uint32_t ipl_t;

/** Variadic arguments definitions. */
#define va_list __builtin_va_list
#define va_start __builtin_va_start
#define va_arg __builtin_va_arg
#define va_end __builtin_va_end

#endif /* C_H_ */

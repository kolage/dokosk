/**
 * @file user/include/math.h
 *
 * File with implementation of simple basic mathematical functions
 * ::min, ::max and ::abs.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef MATH_H_
#define MATH_H_


/** Computes the minimum of given two numbers.
 *
 * @param first  First number.
 * @param second Second number.
 *
 */
static inline int32_t min(const int32_t first, const int32_t second)
{
	return first < second ? first : second;
}

/** Computes the maximum of given two numbers.
 *
 * @param first  First number.
 * @param second Second number.
 *
 */
static inline int32_t max(const int32_t first, const int32_t second)
{
	return first > second ? first : second;
}

/** Computes the absolute value of the given @p number.
 *
 * @param number Number for which the absolute value is computed.
 *
 */
static inline uint32_t abs(const int32_t number)
{
	return number > 0 ? number : -number;
}

#endif /* MATH_H_ */

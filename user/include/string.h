/**
 * @file user/include/string.h
 *
 * Declarations of some basic string operations.
 *
 * OS
 *
 * Copyright (c) 2013
 *   Vlastimil Dort, Jan Kolarik, Jan Skvaril
 *
 */

#ifndef STRING_H_
#define STRING_H_


/* Externals are commented with implementation. */
extern bool atoi(char *string, int *number);
extern int strcmp(char *first_string, char *second_string);
extern int strlen(char *string);
extern char *rtrim(char *string);
extern void strcpy(char *dest, const char *src);

#endif /* STRING_H_ */

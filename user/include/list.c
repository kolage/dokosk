/**
 * @file user/include/list.c
 *
 * Implementation of simple doubly linked list implementation.
 *
 * Kalisto
 *
 * Copyright (c) 2001-2010
 *   Department of Distributed and Dependable Systems
 *   Faculty of Mathematics and Physics
 *   Charles University, Czech Republic
 *
 */

#include <include/c.h>
#include <include/list.h>
#include <dev/io.h>
#include <proc/thread.h>


/** Initialize an item.
 *
 * @param item The item to initialize.
 * @param data Data stored in the item.
 *
 */
void item_init(item_t *item, void *data)
{
	/* To catch list usage errors, items are initialized.
	   This makes it possible to detect double append
	   and double remove operations easily. */
	item->data = data;
	item->list = NULL;
}

/** Initialize a list.
 *
 * @param list The list to initialize.
 *
 */
void list_init(list_t *list)
{
	list->head = NULL;
	list->tail = NULL;
}

/** Append an item to a list.
 *
 * @param list The list to append to.
 * @param item The item to append.
 *
 */
void list_append(list_t *list, item_t *item)
{
	/* Make sure the item is not
	   a member of a list, then add it. */
	assert(item->list == NULL);
	item->list = list;

	/* In an empty list, attach us as head.
	   Otherwise, attach us to current tail. */
	if (list->tail == NULL) {
		list->head = item;
	} else {
		list->tail->next = item;
	}

	/* Our previous item is current tail.
	   We obviously have no next item. */
	item->prev = list->tail;
	item->next = NULL;

	/* We are the new tail. */
	list->tail = item;
}

/** Remove an item from a list.
 *
 * @param list The list to remove from.
 * @param item The item to remove.
 *
 */
void list_remove(list_t *list, item_t *item)
{
	/* Make sure the item is
	   a member of the list, then remove it. */
	assert(item->list == list);
	item->list = NULL;
	
	if (item->prev == NULL) {
		/* If we are list head, our next item is the new head.
		   This works even if we happen to be the tail too. */
		list->head = item->next;
	} else {
		/* Otherwise, just make our previous
		   item point to our next item. */
		item->prev->next = item->next;
	}

	/* The same for the other end of the list. */
	if (item->next == NULL) {
		list->tail = item->prev;
	} else {
		item->next->prev = item->prev;
	}
}

/** Count the items in the list.
 *
 * This method is not atomic in the sense that modifying the list while
 * counting the items is undefined behavior.
 *
 * @param list The list to count.
 *
 */
size_t list_count(list_t *list)
{
	assert(list != NULL);
	item_t *item = list->head;
	int count = 0;
	
	while (item != NULL) {
		item = item->next;
		count++;
	}
	
	return count;
}

/** Check whether the passed list is empty.
 *
 * @param list The list to count.
 *
 */
bool list_empty(list_t *list)
{
	assert(list != NULL);
	return list->tail == NULL;
}

/** Check whether the list contains the specified item.
 *
 * @param list The list to check for containing.
 * @param item The item to check for presence.
 *
 */
bool list_contains(list_t *list, item_t *item)
{
	assert(list != NULL);
	return item->list == list;
}

/** Initialize the list iterator.
 *
 * @param it   List iterator.
 * @param list The list to iterate over.
 *
 */
void list_it_init(list_it_t *it, list_t *list)
{
	it->current = list->head;
}

/** Check whether the list iterator points to a valid item of a list
 *  (has reached the end of the list).
 *
 * @param  it List iterator.
 *
 * @return ::true if the iterator has a current item, ::false if all
 *         items have been iterated over.
 *
 */
bool list_it_has_current(list_it_t *it)
{
	return it->current != NULL;
}

/** Advance the iterator to the next item in the iterated list.
 *
 * @param it List iterator. The iterator must have a valid item (check
 *           by ::list_it_has_current).
 *
 */
void list_it_next(list_it_t *it)
{
	it->current = it->current->next;
}

/** Get the data entry of the current item in the list iteration.
 *
 * @param  it List iterator. The iterator must have a valid item (check
 *            by ::list_it_has_current).
 *
 * @return Returns the data entry of the current item in the list iteration.
 *
 */
void *list_it_data(list_it_t *it)
{
	return it->current->data;
}

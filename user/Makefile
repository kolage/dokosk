#
# OS
#
# Copyright (c) 2001-2010
#   Department of Distributed and Dependable Systems
#   Faculty of Mathematics and Physics
#   Charles University, Czech Republic
#
# Makefile for future user space support
#

### Cross-compiler paths

ifndef CROSS_PREFIX
	CROSS_PREFIX = /usr/local/cross
endif

ARCH = mips32
TARGET = mipsel-linux-gnu

TOOLCHAIN_DIR = $(CROSS_PREFIX)/$(ARCH)

CC = $(TOOLCHAIN_DIR)/bin/$(TARGET)-gcc
LD = $(TOOLCHAIN_DIR)/bin/$(TARGET)-ld
OBJCOPY = $(TOOLCHAIN_DIR)/bin/$(TARGET)-objcopy
OBJDUMP = $(TOOLCHAIN_DIR)/bin/$(TARGET)-objdump
AR = $(TOOLCHAIN_DIR)/bin/$(TARGET)-ar

### Compiler, assembler, linker and archiver options

CCFLAGS = -O2 -march=r4000 -mabi=32 -mgp32 -msoft-float -mlong32 -G 0 -mno-abicalls -fno-pic -fno-builtin -ffreestanding -nostdlib -nostdinc -pipe -Wall -Wextra -Werror -Wno-unused-parameter -Wmissing-prototypes -g3 -std=gnu99 -I.
ASFLAGS = -march=r4000 -mabi=32 -mgp32 -msoft-float -mlong32 -G 0 -mno-abicalls -fno-pic -fno-builtin -ffreestanding -nostdlib -nostdinc -pipe -Wall -Wextra -Werror -Wno-unused-parameter -Wmissing-prototypes -g3 -std=gnu99 -I. -D__ASM__
LDFLAGS = -G 0 -static -g
ARFLAGS = 

### User linker script

LDSFILE = user.lds

### Library source files

LIBRT_SOURCES = \
	startup.c\
	syscall/syscall.S\
	proc/thread.c\
	proc/process.c\
	proc/program.c\
	dev/io.c\
	dev/disk.c\
	dev/file.c\
	include/list.c\
	include/string.c\
	mm/malloc.c\
	sync/cond.c\
	sync/mutex.c\

### User source files

ifneq ($(USER_TEST),)
	USER_SOURCES = $(USER_TEST)
else
	USER_SOURCES = main.c
endif

### Debugging options

ifneq ($(USER_DEBUG),)
	CCFLAGS += $(USER_DEBUG:%=-DDEBUG_%=1)
endif

### Object, output and temporary files

LIBRT_OBJECTS := $(addsuffix .o,$(basename $(LIBRT_SOURCES)))
LIBRT_A = librt.a

USER_OBJECTS := $(addsuffix .o,$(basename $(USER_SOURCES)))
USER_RAW = user.raw
USER_MAP = user.map
USER_BINARY = user.bin
USER_DISASM = user.disasm

DEPEND = Makefile.depend
DEPEND_PREV = $(DEPEND).prev

### User applications

USER_APPS = $(wildcard prog/*/Makefile)
USER_APPS_TAR = prog/programs.tar

### Phony targets

.PHONY: all clean distclean $(USER_APPS)

### Default target

all: $(LIBRT_A) $(USER_BINARY) $(USER_DISASM) $(USER_APPS_TAR)
	-[ -f $(DEPEND) ] && cp -a $(DEPEND) $(DEPEND_PREV)

### Maintenance

distclean: clean
	rm -f $(LIBRT_A) $(USER_BINARY)

clean:
	rm -f $(USER_DISASM) $(USER_MAP) $(USER_RAW) $(DEPEND) $(DEPEND_PREV)
	find . -name '*.o' -exec rm -f \{\} \;

### Dependencies

-include $(DEPEND)

### Disassembly dumps

$(USER_DISASM): $(USER_RAW)
	$(OBJDUMP) -d $< > $@

### Library

$(LIBRT_A): $(LIBRT_OBJECTS)
	ar rcs $(LIBRT_A) $(LIBRT_OBJECTS)

### Binary images

$(USER_BINARY): $(USER_RAW)
	$(OBJCOPY) -O binary $< $@

$(USER_RAW): $(LDSFILE) $(USER_OBJECTS) $(LIBRT_A)
	$(LD) $(LDFLAGS) -T $(LDSFILE) -Map $(USER_MAP) -o $@ $(USER_OBJECTS) $(LIBRT_A)

### User applications

$(USER_APPS):
	make -C $(dir $@)

$(USER_APPS_TAR): $(USER_APPS)
	tar -c -f $@ prog/*/*.bin

### Default patterns

%.o: %.c $(DEPEND)
	$(CC) $(CCFLAGS) -c -o $@ $<

%.o: %.S $(DEPEND)
	$(CC) $(ASFLAGS) -c -o $@ $<

### Source code dependencies

$(DEPEND):
	-makedepend -f - -- $(CCFLAGS) -- $(LIBRT_SOURCES) $(USER_SOURCES) > $@ 2> /dev/null
	-[ -f $(DEPEND_PREV) ] && diff -q $(DEPEND_PREV) $@ && mv -f $(DEPEND_PREV) $@

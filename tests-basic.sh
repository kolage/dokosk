#! /bin/sh

#
# Kalisto
#
# Copyright (c) 2001-2010
#   Department of Distributed and Dependable Systems
#   Faculty of Mathematics and Physics
#   Charles University, Czech Republic
#
# Compile and boot Kalisto with tests. The correct
# result of each test is signaled by
#
# Test passed...
#

fail() {
	rm -f test.log
	echo
	echo "Failure: $1"
	exit 1
}

SILENT_MAKE="--silent"

test() {
	emake distclean || fail "Cleanup before compilation"
	emake "KERNEL_TEST=$1" || fail "Compilation"
	msim | tee test.log || fail "Execution"
	grep '^Test passed\.\.\.$' test.log > /dev/null || fail "Test $1"
	rm -f test.log
	emake distclean || fail "Cleanup after compilation"
}

emake() {
	echo "Running make $SILENT_MAKE $@"
	make $SILENT_MAKE "$@"
}

# Kernel tests:
for TEST in \
	tests/basic/io1/test.c \
	tests/basic/timer1/test.c \
	tests/basic/tree1/test.c \
	tests/basic/atomic1/test.c \
	tests/basic/vma1/test.c \
	tests/basic/vma2/test.c \
	tests/basic/disk1/test.c \
	tests/basic/malloc1/test.c \
    ; do
	test "${TEST}"
done

# Kernel tests which do not print "Test passed..." string:
for TEST in \
    	tests/basic/tlb1/test.c \
    ; do
	emake distclean || fail "Cleanup before compilation"
	emake "KERNEL_TEST=$TEST" || fail "Compilation"
	msim
	emake distclean || fail "Cleanup after compilation"
done

# User space tests:
for TEST in \
    	tests/basic/mutex1/test.c \
    	tests/basic/mutex2/test.c \
    	tests/basic/mutex3/test.c \
    	tests/basic/mutex4/test.c \
    	tests/basic/condvar1/test.c \
    	tests/basic/condvar2/test.c \
    	tests/basic/syscall1/test.c \
    	tests/basic/proc1/test.c \
    	tests/basic/proc2/test.c \
    	tests/basic/proc3/test.c \
    	tests/basic/disk1/test.c \
    ; do
	emake distclean || fail "Cleanup before compilation"
	emake "USER_TEST=$TEST" || fail "Compilation"
	msim | tee test.log || fail "Execution"
	grep '^Test passed\.\.\.$' test.log > /dev/null || fail "Test $TEST"
	rm -f test.log
	emake distclean || fail "Cleanup after compilation"
done

echo
echo "All tests passed..."
